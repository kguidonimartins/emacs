## -*- mode: shell; -*-
## scratch-mode: shell-mode
## project:      .emacs.d.vanilla
## created-at:   20230206-111157

# set -euxo pipefail

cd ~/.emacs.d.vanilla/straight/repos/

if [[ -f needs-update ]]; then
        rm needs-update
fi

mgitstatus -f -e --throttle 1 >> needs-update

exit

while IFS= read -r line
do
       echo "Entrando $line"
       repo_dir=$(echo "$line" | awk '{print $1}' | sed 's#[./:]##g')
       cd $repo_dir
       git pull
       echo "Saindo $line"
       cd ..
done < ~/.emacs.d.vanilla/straight/repos/needs-update

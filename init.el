;;; init.el --- Main Emacs configuration file -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;; get user name

(setq-default user-full-name "Karlo Guidoni")
(setq-default user-mail-address "kguidonimartins@gmail.com")

;; Profile emacs startup

(add-hook 'emacs-startup-hook
          (lambda ()
            (message "*** Emacs loaded in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;;; Load custom layers

;;;; Load initial functions and paths

;; Load initial functions.

(load-file (expand-file-name "lisp/init-funk.el" user-emacs-directory))
(load-file (expand-file-name "lisp/init-contrib.el" user-emacs-directory))

;; Local layers lives on lisp folder.

(getel-files "lisp")

;;;; Load layers

;; CHECK: https://www.reddit.com/r/emacs/comments/gw0io1/organize_your_emacs_config_into_loadable_modules/
;; CHECK: https://github.com/SidharthArya/.emacs.d/blob/master/Vanilla/init.el
(defvar funk/layers '(;; keep the order!
                      init-system
                      init-backup
                      init-session
                      init-no-littering
                      init-projectile
                      init-ui
                      init-help
                      init-themes
                      init-fonts
                      init-scratch
                      init-keybinding
                      init-evil
                      init-keychord
                      init-programming
                      ;; init-restclient
                      init-indent
                      init-comment
                      init-folding
                      init-python
                      ;; init-clojure
                      init-jupyter
                      ;; init-repl
                      ;; init-company
                      init-corfu
                      ;; init-company-custom-backends
                      ;; init-company2
                      init-spell
                      init-linter
                      init-language-server
                      init-markdown
                      init-emacs-lisp
                      init-rstats
                      ;; init-julia
                      init-org
                      init-org-roam
                      init-mu4e
                      init-avy
                      init-ivy
                      init-www
                      init-hydra
                      init-rss
                      init-file-management
                      init-dotfiles
                      init-csv
                      ;; init-pdf
                      init-terminal
                      init-completion-et-al
                      init-ssh
                      ;; init-lua
                      ;; init-cloud
                      ;; init-docker
                      init-git
                      init-mode-line
                      ;; init-treemacs
                      ;; init-java
                      ;; init-try
                      ;; init-sql
                      ;; init-rust
                      ;; init-perspective
                      ;; init-note-taking
                      init-ai
                      init-custom-variables
                      init-buffer-window
                      init-snippet ; need to be the last one!
                      init-tree-sitter
                      )
  "Layers with packages/configurations. Do not change the order.")

(require-layers funk/layers)

;;; Remove unwanted buffers after startup

(add-hook 'after-change-major-mode-hook 'funk/remove-unwanted-buffers)

;;; Custom file

;; ;; NOTE: you can ignore custom-file as well.
;; (setq-default custom-file null-device)
(setq custom-file "/tmp/custom.el")

;;; Report load-time for each layer

(message "\nTime (in seconds) to load each layer.")
(pretty-print-hash report-table-time-to-load-layers)

;;; Force some packages after

(use-package shackle
  :config
  (setq shackle-lighter "")
  (setq shackle-select-reused-windows nil) ; default nil
  (setq shackle-default-alignment 'below) ; default below
  (setq shackle-rules
        ;;                          :regexp nil :select nil :inhibit-window-quit nil :size 0.00 :align nil :other nil :same|:popup
        '(("*undo-tree*"                                                             :size 0.25 :align right)
          ("*eshell*"                           :select t                                                  :other t)
          ("*Messages*"                         :select t   :align below                                   :other t)
          ("*Warnings*"                         :select nil :align below                                   :other t)
          ("*HTTP Response*"                    :select nil :align right                                   :other t)
          ("*Shell Command Output*"             :select nil)
          ("*Async Shell Command*"              :select nil :inhibit-window-quit nil :size 0.5  :align below)
          ("*PDF-Occur*"                        :select t   :inhibit-window-quit nil                       :other t)
          ;; ("*compilation*"                      :select nil :inhibit-window-quit nil :size 0.5  :align below)
          ("*interpretation*"                   :select nil :inhibit-window-quit nil :size 0.5  :align below)
          ("*helm-ag*"                          :select nil :inhibit-window-quit t                         :other t)
          ("*rg*"                               :select t   :inhibit-window-quit nil :size 0.5  :align left)
          (occur-mode                           :select t                                       :align t)
          ;; ("*Help*"                             :select t   :inhibit-window-quit t                         :other t)
          ("*Python Doc*"                       :select t)
          ("*Completions*"                                                           :size 0.3  :align t)
          ("*Occur*"                            :select t                            :size 0.5  :align left)
          ("*Register Preview*"                 :select t                            :size 0.5  :align below)
          ("*quarto-preview*"                   :select nil                          :size 0.5  :align below)
          ("*org-roam*"                         :select nil                          :size 0.3  :align left)
          ("^\\*vterm-proj*?"       :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^\\*vc-diff\\*"         :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^\\*VC-history\\*"      :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^magit-process:?"       :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^\\*Flymake diagnostics?" :regexp t :select t   :inhibit-window-quit nil :size 0.3  :align below :popup t)
          ("^\\*Fd\\*$"             :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^\\*vterm *?"           :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^\\*interpretation\\*$" :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^\\*compilation\\*$"    :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^\\*Compile-Log\\*$"    :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("\\*[Wo]*Man.*\\*"       :regexp t   :select t   :inhibit-window-quit t                         :other t)
          ;; ("\\*poporg.*\\*"         :regexp t   :select t                                                  :other t)
          ("\\`\\*helm.*?\\*\\'"    :regexp t                                        :size 0.3  :align t)
          ("*Calendar*"                         :select t                            :size 0.3  :align below)
          ("*Org Select*"                       :select t                            :size 0.3  :align below)
          ("*info*"                             :select t   :inhibit-window-quit t                                    :same t)
          (magit-status-mode                    :select t   :inhibit-window-quit t   :size 0.5  :align below          :same nil) ; use this for open magit below
          ;; (magit-status-mode                    :select t   :inhibit-window-quit t                                    :same t)
          (magit-log-mode                       :select t   :inhibit-window-quit t                                    :same t)
          ("\\`\\*R Data View:.*?\\*\\'" :regexp t   :select t :inhibit-window-quit nil :size 0.5 :align above)
          ))
  (shackle-mode +1)
  )

;; (setq-default mode-line-misc-info
;;               '((which-function-mode
;;                  (which-func-mode
;;                   ("" which-func-format " ")))
;;                 (eglot--managed-mode
;;                  (" [" eglot--mode-line-format "] "))
;;                 (global-mode-string
;;                  ("" global-mode-string))))

;; (defun funk/no-tab-bar-mode-after-desktop-read ()
;;   (tab-bar-mode -1))

;; (add-hook 'emacs-startup-hook (lambda () (tab-bar-mode -1)))

;; (tab-bar-mode -1)
;; (desktop-save-mode t)

;; (advice-add #'desktop-restore-frameset :around #'funk/no-tab-bar-mode-after-desktop-read)
;; (advice-add #'desktop-read :around #'funk/no-tab-bar-mode-after-desktop-read)
;; (global-tab-line-mode -1)
(tab-bar-mode -1)

;;; init.el ends here

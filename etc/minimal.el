;; run with `emacs -q -l /path/to/minimal.el

(defvar funk/frame-transparency '(99 . 99))
(set-frame-parameter (selected-frame) 'alpha funk/frame-transparency)
(add-to-list 'default-frame-alist `(alpha . ,funk/frame-transparency))

(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
;; activate use-package
(require 'use-package)


(use-package autothemer)

(autothemer-deftheme example-name "Autothemer example..."

                     ;; Specify the color classes used by the theme
                     ((((class color) (min-colors #xFFFFFF))
                       ((class color) (min-colors #xFF)))

                      ;; Specify the color palette for each of the classes above.
                      (example-red    "#781210" "#FF0000")
                      (example-green  "#22881F" "#00D700")
                      (example-blue   "#212288" "#0000FF")
                      (example-purple "#812FFF" "#Af00FF")
                      (example-yellow "#EFFE00" "#FFFF00")
                      (example-orange "#E06500" "#FF6600")
                      (example-cyan   "#22DDFF" "#00FFFF"))

                     ;; ;; specifications for Emacs faces.
                     ;; ((button (:underline t :weight 'bold :foreground example-yellow))
                     ;;  (error  (:foreground example-red)))

                     ;; Forms after the face specifications are evaluated.
                     ;; (palette vars can be used, read below for details.)
                     (custom-theme-set-variables 'example-name
                                                 `(ansi-color-names-vector [,example-red
                                                                            ,example-green
                                                                            ,example-blue
                                                                            ,example-purple
                                                                            ,example-yellow
                                                                            ,example-orange
                                                                            ,example-cyan])))

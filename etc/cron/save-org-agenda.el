;; -*- lexical-binding: t; -*-

;; NOTE 2021-10-27: see also ~/.local/bin/tools/cron/save-org-agenda
;; This file export the `org-agenda-view' for the current day as a
;; plain text file to be showed by the dunst.

(let* ((path "~/.emacs.d.vanilla/straight/build/")
       (local-pkgs (mapcar 'file-name-directory (directory-files-recursively path ".*\\.el"))))
  (if (file-accessible-directory-p path)
      (mapc (apply-partially 'add-to-list 'load-path) local-pkgs)
    (make-directory path :parents)))

(require 'org)
(require 'org-macs)
(require 'org-gcal)
(require 'org-habit)
(require 'projectile)
(require 'org-projectile)

(setq projectile-known-projects-file "/home/karlo/.local/share/shell-history/projectile-bookmarks.el")
(projectile-mode +1)

(add-to-list 'org-modules 'org-habit)
(setq org-habit-graph-column 60)

(defun funk-auth (key)
  (with-temp-buffer
    (insert-file-contents-literally "~/.config/api/emacs-keys.el")
    (alist-get key (read (current-buffer)))))

(setq org-agenda-start-with-log-mode t)
(setq org-log-done 'time)
(setq org-log-into-drawer t)
(setq org-agenda-include-diary t)
(setq org-agenda-include-deadlines t)
(setq org-agenda-include-inactive-timestamps nil)
(setq org-generic-id-locations-file "~/.emacs.d.vanilla/var/org/.org-generic-id-locations")
(setq org-deadline-warning-days 7)

(setq org-agenda-files
      '(
        "~/google-drive/kguidonimartins/org/archive.org"
        "~/google-drive/kguidonimartins/org/asana-karlo.org"
        "~/google-drive/kguidonimartins/org/asana-team.org"
        "~/google-drive/kguidonimartins/org/familia.org"
        "~/google-drive/kguidonimartins/data-broomer/CUSTOS.org"
        "~/google-drive/kguidonimartins/org/gcal.org"
        "~/google-drive/kguidonimartins/org/gcal-github-commits.org"
        "~/google-drive/kguidonimartins/org/bills.org"
        "~/google-drive/kguidonimartins/org/gcal.org_archive"
        "~/google-drive/kguidonimartins/org/inbox.org"
        "~/google-drive/kguidonimartins/org/meetings.org"
        "~/google-drive/kguidonimartins/org/projects.org"
        ;; "~/google-drive/kguidonimartins/org/todoist.org"
        ))

(message "Setting up org-projetile TODO files...")
(let ((inhibit-message t)
      (message-log-max nil))

  (org-projectile-per-project)

  (defun funk/org-projectile-return-alternative-todo (project-path)
    (interactive)
    (let* ((proj-root project-path)
           (todo-default "TODO.org")
           (todo-alt ".git/info/TODO.org")
           (todo-alt-proj (concat proj-root todo-alt))
           (todo-proj (concat proj-root todo-default)))
      (if (file-exists-p todo-alt-proj)
          (message todo-alt)
        (message todo-default))))

  (setq org-projectile-per-project-filepath 'funk/org-projectile-return-alternative-todo)

  (defun funk/org-projectile-todo-files ()
    (seq-filter #'file-exists-p (org-projectile-todo-files)))

  (defun funk/org-projectile-setup-project-todo ()
    (progn
      (message "Collecting TODO files in projects...")
      (let ((inhibit-message t)
            (message-log-max nil))
        (dolist (todo (funk/org-projectile-todo-files))
          ;; Only append if not exists in the `org-agenda-files'!
          (add-to-list 'org-agenda-files todo)))
      (message "Done!")))

  (funk/org-projectile-setup-project-todo))
(message "Done!")

(message "Backing up org-projectile TODO files")
(let ((inhibit-message t)
      (message-log-max nil))
  (dolist (todo (funk/org-projectile-todo-files))
    (let* ((todofile todo)
           (backupfile (string-replace "/" "!" (file-truename todofile)))
           (command (concat "cp " todofile " /home/karlo/google-drive/kguidonimartins/org/backup/" backupfile)))
      (shell-command command))))
(message "Done!")

(setq org-todo-keywords
      '((sequence "TODO(t!)" "NEXT(n!)" "REVIEW(r@/!)" "WAIT(w@/!)" "|" "DONE(d!)" "CANCELLED(c@/!)")))

(setq org-agenda-custom-commands
      '(("X" agenda ""
         (
          ;; (org-agenda-time-grid '((daily today require-timed)
          ;;                         (0600 0700 0800 0900 1000 1100
          ;;                               1200 1300 1400 1500 1600
          ;;                               1700 1800 1900 2000 2100)
          ;;                         " ....." "-----------------"))
          (org-agenda-time-grid nil)
          (org-agenda-remove-tags t)
          (org-agenda-span 1)
          (org-deadline-warning-days 7)
          (org-scheduled-past-days 7)
          (org-agenda-block-separator nil)
          ;; We don't need the `org-agenda-date-today'
          ;; highlight because that only has a practical
          ;; utility in multi-day views.
          (org-agenda-day-face-function (lambda (date) 'org-agenda-date))
          (org-agenda-format-date "%A %-e %B %Y")
          (org-agenda-entry-types '(:timestamp :scheduled*))
          (org-agenda-overriding-header "")
          )
         ("~/google-drive/kguidonimartins/org/index.txt"))))

(setq org-gcal-dir "~/.emacs.d.vanilla/var/org/gcal")
(setq org-gcal-token-file "~/.emacs.d.vanilla/var/org/gcal/.org-gcal-token")

(setq org-gcal-client-id (funk-auth 'gcalid)
      org-gcal-client-secret (funk-auth 'gcalkey)
      org-gcal-notify-p nil
      org-gcal-recurring-events-mode "nested"
      org-gcal-file-alist '(
                            ("kguidonimartins@gmail.com" . "~/google-drive/kguidonimartins/org/gcal.org")
                            ("f86sttov4l4o73j26uklbth268@group.calendar.google.com" . "~/google-drive/kguidonimartins/org/gcal-github-commits.org")
                            )
      )

(org-gcal-sync)

;; (message "%s" (funk/org-projectile-todo-files))

# Generate a new template containing all the faces

```elisp
(with-current-buffer (get-buffer-create "*Clean faces to copy*")
  (setq my-super-b-theme "funknight")
  (emacs-lisp-mode)
  (insert "(deftheme " my-super-b-theme ")\n\n")
  (insert "(let (
      (class '((class color) (min-colors 89)))
       (bg1 \"#ffffff\")
       (fg1 \"#000000\")
       )\n\n")
  (insert "(custom-theme-set-faces\n'" my-super-b-theme "\n\n")
  (insert ";; here begin the face list\n\n")
  (let ((faces (sort (face-list) #'string-lessp)))
    (mapcar (lambda (f)
              (insert
               (concat "`("
                       (symbol-name f)
                       " \n((,class (:background ,bg1 :foreground ,fg1))))\n\n"))) faces))
  (insert ")\n)\n\n")

  (insert
   ";;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))\n\n")

  (insert "(provide-theme '" my-super-b-theme ")\n\n")

  (insert ";; Local Variables:\n ;; no-byte-compile: t\n ;; End:\n")

  (save-excursion
    (indent-region (point-min) (point-max) nil))
  )
```

# Or just copy the template below

```elisp
(deftheme MY-AWESOME-THEME)

(let (
      (class
       '((class color) (min-colors 89))
       (bg1 "#ffffff")
       (fg1 "#000000")
       )
      )

  (custom-theme-set-faces
   'MY-AWESOME-THEME

   ;; here begin the face list

   `(Info-quoted
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(alert-high-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(alert-low-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(alert-moderate-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(alert-normal-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(alert-saved-fringe-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(alert-trivial-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(alert-urgent-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-blue
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-blue-alt
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-cyan
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-cyan-alt
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-dblue
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-dcyan
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-dgreen
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-dired-dir-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-dmaroon
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-dorange
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-dpink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-dpurple
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-dred
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-dsilver
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-dyellow
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-green
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ibuffer-dir-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ibuffer-file-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ibuffer-icon-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ibuffer-mode-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ibuffer-size-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-archive-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-bookmark-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-dir-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-doc-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-file-modes-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-file-name-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-file-owner-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-icon-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-indicator-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-install-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-major-mode-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-path-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-process-buffer-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-process-command-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-process-id-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-process-status-alt-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-process-status-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-process-thread-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-process-tty-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-project-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-size-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-time-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-ivy-rich-version-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-lblue
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-lcyan
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-lgreen
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-lmaroon
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-lorange
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-lpink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-lpurple
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-lred
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-lsilver
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-lyellow
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-maroon
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-orange
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-pink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-purple
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-purple-alt
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-red
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-red-alt
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-silver
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(all-the-icons-yellow
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-black
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-blue
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-bold
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-bright-black
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-bright-blue
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-bright-cyan
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-bright-green
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-bright-magenta
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-bright-red
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-bright-white
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-bright-yellow
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-cyan
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-faint
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-fast-blink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-green
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-inverse
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-italic
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-magenta
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-red
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-slow-blink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-underline
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-white
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ansi-color-yellow
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(avy-background-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(avy-goto-char-timer-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(avy-lead-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(avy-lead-face-0
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(avy-lead-face-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(avy-lead-face-2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(biblio-detail-header-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(biblio-highlight-extend-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(biblio-results-header-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(blamer-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(bold
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(bold-italic
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(bookmark-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(bookmark-menu-bookmark
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(bookmark-menu-heading
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(border
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(browse-url-button
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(buffer-menu-buffer
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(button
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(calc-nonselected-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(calc-selected-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(calendar-month-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(calendar-today
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(calendar-weekday-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(calendar-weekend-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(change-log-acknowledgment
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(change-log-conditionals
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(change-log-date
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(change-log-email
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(change-log-file
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(change-log-function
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(change-log-list
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(change-log-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(child-frame-border
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(citar
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(citar-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(citar-icon-dim
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(comint-highlight-input
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(comint-highlight-prompt
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-echo
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-echo-common
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-preview
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-preview-common
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-preview-search
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-tooltip
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-tooltip-annotation
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-tooltip-annotation-selection
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-tooltip-common
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-tooltip-common-selection
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-tooltip-deprecated
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-tooltip-mouse
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-tooltip-quick-access
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-tooltip-quick-access-selection
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-tooltip-scrollbar-thumb
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-tooltip-scrollbar-track
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-tooltip-search
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-tooltip-search-selection
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(company-tooltip-selection
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(compilation-column-number
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(compilation-error
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(compilation-info
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(compilation-line-number
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(compilation-mode-line-exit
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(compilation-mode-line-fail
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(compilation-mode-line-run
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(compilation-warning
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(completions-annotations
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(completions-common-part
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(completions-first-difference
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(completions-group-separator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(completions-group-title
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(confusingly-reordered
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-async-failed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-async-finished
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-async-running
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-async-split
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-bookmark
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-buffer
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-crm-selected
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-file
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-grep-context
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-help
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-key
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-line-number
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-line-number-prefix
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-line-number-wrapped
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-narrow-indicator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-preview-cursor
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-preview-error
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-preview-insertion
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-preview-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-preview-match
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(consult-separator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel--mark-ring-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel-active-mode
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel-application-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel-evil-register-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel-key-binding
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel-outline-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel-outline-2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel-outline-3
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel-outline-4
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel-outline-5
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel-outline-6
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel-outline-7
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel-outline-8
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel-outline-default
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(counsel-variable-documentation
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(cursor
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-button
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-button-mouse
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-button-pressed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-button-pressed-unraised
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-button-unraised
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-changed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-comment
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-comment-tag
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-documentation
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-face-tag
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-group-subtitle
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-group-tag
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-group-tag-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-invalid
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-link
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-modified
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-rogue
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-saved
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-set
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-state
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-themed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-variable-button
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-variable-obsolete
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-variable-tag
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(custom-visibility
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(default
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diary
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-added
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-changed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-changed-unspecified
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-context
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-error
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-file-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-function
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hl-change
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hl-delete
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hl-dired-change
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hl-dired-delete
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hl-dired-ignored
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hl-dired-insert
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hl-dired-unknown
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hl-insert
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hl-margin-change
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hl-margin-delete
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hl-margin-ignored
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hl-margin-insert
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hl-margin-unknown
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hl-reverted-hunk-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-hunk-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-index
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-indicator-added
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-indicator-changed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-indicator-removed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-nonexistent
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-refine-added
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-refine-changed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-refine-removed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diff-removed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-broken-symlink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-directory
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-filter-group-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-flagged
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-ignored
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-mark
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-marked
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-narrow-blink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-perm-write
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-set-id
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-special
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-subtree-depth-1-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-subtree-depth-2-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-subtree-depth-3-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-subtree-depth-4-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-subtree-depth-5-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-subtree-depth-6-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-symlink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(dired-warning
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-autofile-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-compressed-file-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-compressed-file-suffix
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-date-time
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-deletion
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-deletion-file-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-dir-heading
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-dir-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-dir-priv
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-exec-priv
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-executable-tag
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-file-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-file-suffix
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-flag-mark
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-flag-mark-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-ignored-file-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-link-priv
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-no-priv
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-number
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-other-priv
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-rare-priv
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-read-priv
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-symlink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-tagged-autofile-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(diredfl-write-priv
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(edebug-disabled-breakpoint
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(edebug-enabled-breakpoint
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eglot-highlight-symbol-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eglot-mode-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eieio-custom-slot-tag-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eldoc-highlight-function-argument
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(elisp-shorthand-font-lock-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(embark-collect-annotation
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(embark-collect-candidate
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(embark-collect-zebra-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(embark-keybinding
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(embark-keybinding-repeat
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(embark-keymap
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(embark-target
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(embark-verbose-indicator-documentation
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(embark-verbose-indicator-shadowed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(embark-verbose-indicator-title
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(epa-field-body
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(epa-field-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(epa-mark
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(epa-string
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(epa-validity-disabled
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(epa-validity-high
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(epa-validity-low
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(epa-validity-medium
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(error
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ert-test-result-expected
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ert-test-result-unexpected
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(escape-glyph
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eshell-prompt
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-%op%-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-assignment-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-bp-fringe-browser-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-bp-fringe-inactive-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-bp-fringe-logger-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-bp-fringe-recover-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-constant-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-debug-blink-ref-not-found-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-debug-blink-same-ref-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-debug-current-debug-line-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-function-call-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-keyword-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-matrix-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-modifiers-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-numbers-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-operator-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-paren-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-r-control-flow-keyword-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-tracebug-last-input-fringe-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ess-watch-current-block-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-ex-commands
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-ex-info
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-ex-lazy-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-ex-search
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-ex-substitute-matches
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-ex-substitute-replacement
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles--pulse-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-change-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-commentary-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-default-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-delete-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-fill-and-move-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-indent-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-join-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-nerd-commenter-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-paste-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-record-macro-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-replace-with-register-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-set-marker-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-shift-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-surround-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-goggles-yank-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(evil-visual-mark-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eww-form-checkbox
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eww-form-file
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eww-form-select
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eww-form-submit
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eww-form-text
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eww-form-textarea
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eww-invalid-certificate
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eww-valid-certificate
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eyebrowse-mode-line-active
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eyebrowse-mode-line-delimiters
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eyebrowse-mode-line-inactive
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(eyebrowse-mode-line-separator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ffap
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(file-name-shadow
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(fill-column-indicator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(fixed-pitch
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(fixed-pitch-serif
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-color-mode-line-error-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-color-mode-line-info-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-color-mode-line-running-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-color-mode-line-success-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-color-mode-line-warning-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-delimited-error
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-error
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-error-delimiter
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-error-list-checker-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-error-list-column-number
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-error-list-error
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-error-list-error-message
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-error-list-filename
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-error-list-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-error-list-id
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-error-list-id-with-explainer
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-error-list-info
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-error-list-line-number
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-error-list-warning
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-fringe-error
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-fringe-info
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-fringe-warning
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-info
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-inline-error
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-inline-info
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-inline-warning
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-verify-select-checker
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flycheck-warning
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flymake-error
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flymake-note
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flymake-warning
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flyspell-duplicate
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(flyspell-incorrect
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-builtin-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-comment-delimiter-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-comment-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-constant-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-doc-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-doc-markup-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-function-name-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-keyword-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-negation-char-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-preprocessor-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-regexp-grouping-backslash
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-regexp-grouping-construct
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-string-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-type-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-variable-name-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(font-lock-warning-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(forge-post-author
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(forge-post-date
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(forge-topic-closed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(forge-topic-label
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(forge-topic-merged
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(forge-topic-open
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(forge-topic-unmerged
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(forge-topic-unread
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(fringe
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(funk/modeline-buffer-file
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(funk/modeline-buffer-file-modified
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(funk/modeline-buffer-path
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(funk/modeline-project-dir
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(funk/modeline-project-parent-dir
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-commit-comment-action
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-commit-comment-branch-local
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-commit-comment-branch-remote
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-commit-comment-detached
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-commit-comment-file
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-commit-comment-heading
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-commit-keyword
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-commit-known-pseudo-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-commit-nonempty-second-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-commit-overlong-summary
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-commit-pseudo-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-commit-summary
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-gutter+-added
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-gutter+-commit-header-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-gutter+-deleted
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-gutter+-modified
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-gutter+-separator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-gutter+-unchanged
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-gutter:added
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-gutter:deleted
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-gutter:modified
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-gutter:separator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-gutter:unchanged
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-timemachine-commit
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-timemachine-minibuffer-author-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(git-timemachine-minibuffer-detail-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(glyphless-char
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-mail-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-mail-1-empty
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-mail-2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-mail-2-empty
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-mail-3
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-mail-3-empty
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-mail-low
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-mail-low-empty
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-news-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-news-1-empty
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-news-2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-news-2-empty
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-news-3
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-news-3-empty
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-news-4
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-news-4-empty
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-news-5
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-news-5-empty
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-news-6
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-news-6-empty
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-news-low
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-group-news-low-empty
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-splash
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-cancelled
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-high-ancient
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-high-read
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-high-ticked
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-high-undownloaded
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-high-unread
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-low-ancient
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-low-read
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-low-ticked
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-low-undownloaded
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-low-unread
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-normal-ancient
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-normal-read
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-normal-ticked
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-normal-undownloaded
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-normal-unread
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(gnus-summary-selected
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(header-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(header-line-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-action
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-buffer-archive
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-buffer-directory
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-buffer-file
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-buffer-modified
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-buffer-not-saved
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-buffer-process
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-buffer-saved-out
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-buffer-size
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-candidate-number
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-candidate-number-suspended
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-delete-async-message
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-eob-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-etags-file
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-backup-file
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-denied
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-directory
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-dirs
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-dotted-directory
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-dotted-symlink-directory
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-executable
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-file
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-file-extension
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-invalid-symlink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-pipe
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-prefix
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-rsync-progress
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-socket
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-suid
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-symlink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-ff-truename
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-grep-cmd-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-grep-file
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-grep-finish
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-grep-lineno
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-grep-match
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-header-line-left-margin
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-helper
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-history-deleted
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-history-remote
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-locate-finish
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-mark-prefix
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-match
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-match-item
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-minibuffer-prompt
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-moccur-buffer
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-no-file-buffer-modified
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-non-file-buffer
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-org-rifle-separator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-prefarg
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-resume-need-update
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-selection
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-selection-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-separator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-source-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helm-visible-mark
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(help-argument-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(help-for-help-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(help-key-binding
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(helpful-heading
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(highlight-parentheses-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(hl-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(hl-todo
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(holiday
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(homoglyph
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(hydra-face-amaranth
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(hydra-face-blue
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(hydra-face-pink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(hydra-face-red
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(hydra-face-teal
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ibuffer-locked-buffer
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ido-first-match
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ido-incomplete-regexp
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ido-indicator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ido-only-match
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ido-subdir
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ido-virtual
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(iedit-occurrence
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(iedit-read-only-occurrence
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(info-header-node
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(info-header-xref
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(info-index-match
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(info-menu-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(info-menu-star
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(info-node
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(info-title-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(info-title-2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(info-title-3
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(info-title-4
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(info-xref
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(info-xref-visited
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(internal-border
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(isearch
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(isearch-fail
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(isearch-group-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(isearch-group-2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(italic
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-action
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-completions-annotations
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-confirm-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-current-match
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-cursor
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-grep-info
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-grep-line-number
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-highlight-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-match-required-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-minibuffer-match-face-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-minibuffer-match-face-2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-minibuffer-match-face-3
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-minibuffer-match-face-4
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-minibuffer-match-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-modified-buffer
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-modified-outside-buffer
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-org
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-prompt-match
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-remote
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-separator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-subdir
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-virtual
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(ivy-yanked-word
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(jupyter-eval-overlay
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(jupyter-repl-input-prompt
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(jupyter-repl-output-prompt
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(jupyter-repl-traceback
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(langtool-correction-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(langtool-errline
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lazy-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(line-number
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(line-number-current-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(line-number-major-tick
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(line-number-minor-tick
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(link
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(link-visited
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(log-edit-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(log-edit-summary
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(log-edit-unknown-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(log-view-commit-body
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(log-view-file
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(log-view-message
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-details-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-face-highlight-read
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-face-highlight-textual
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-face-highlight-write
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-face-rename
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-installation-buffer-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-installation-finished-buffer-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-rename-placeholder-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-signature-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-signature-posframe
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-doc-background
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-doc-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-doc-highlight-hover
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-doc-url
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-peek-filename
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-peek-footer
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-peek-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-peek-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-peek-line-number
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-peek-list
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-peek-peek
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-peek-selection
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-sideline-code-action
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-sideline-current-symbol
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-sideline-global
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-sideline-symbol
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lsp-ui-sideline-symbol-info
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(lv-separator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-bisect-bad
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-bisect-good
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-bisect-skip
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-blame-date
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-blame-dimmed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-blame-hash
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-blame-heading
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-blame-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-blame-margin
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-blame-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-blame-summary
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-branch-current
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-branch-local
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-branch-remote
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-branch-remote-head
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-branch-upstream
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-branch-warning
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-cherry-equivalent
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-cherry-unmatched
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-added
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-added-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-base
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-base-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-conflict-heading
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-context
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-context-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-file-heading
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-file-heading-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-file-heading-selection
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-hunk-heading
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-hunk-heading-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-hunk-heading-selection
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-hunk-region
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-lines-boundary
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-lines-heading
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-our
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-our-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-removed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-removed-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-revision-summary
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-revision-summary-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-their
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-their-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diff-whitespace-warning
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diffstat-added
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-diffstat-removed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-dimmed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-filename
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-hash
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-head
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-header-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-header-line-key
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-header-line-log-select
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-keyword
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-keyword-squash
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-log-author
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-log-date
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-log-graph
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-mode-line-process
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-mode-line-process-error
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-process-ng
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-process-ok
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-reflog-amend
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-reflog-checkout
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-reflog-cherry-pick
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-reflog-commit
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-reflog-merge
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-reflog-other
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-reflog-rebase
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-reflog-remote
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-reflog-reset
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-refname
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-refname-pullreq
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-refname-stash
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-refname-wip
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-section-heading
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-section-heading-selection
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-section-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-section-secondary-heading
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-sequence-done
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-sequence-drop
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-sequence-exec
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-sequence-head
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-sequence-onto
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-sequence-part
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-sequence-pick
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-sequence-stop
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-signature-bad
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-signature-error
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-signature-expired
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-signature-expired-key
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-signature-good
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-signature-revoked
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-signature-untrusted
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(magit-tag
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-archive
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-char
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-date
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-documentation
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-file-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-file-owner
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-file-priv-dir
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-file-priv-exec
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-file-priv-link
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-file-priv-no
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-file-priv-other
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-file-priv-rare
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-file-priv-read
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-file-priv-write
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-function
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-installed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-key
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-lighter
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-list
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-mode
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-modified
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-null
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-number
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-off
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-on
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-size
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-string
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-symbol
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-true
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-type
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-value
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(marginalia-version
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-blockquote-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-bold-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-code-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-comment-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-footnote-marker-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-footnote-text-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-gfm-checkbox-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-header-delimiter-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-header-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-header-face-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-header-face-2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-header-face-3
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-header-face-4
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-header-face-5
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-header-face-6
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-header-rule-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-highlight-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-highlighting-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-hr-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-html-attr-name-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-html-attr-value-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-html-entity-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-html-tag-delimiter-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-html-tag-name-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-inline-code-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-italic-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-language-info-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-language-keyword-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-line-break-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-link-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-link-title-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-list-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-markup-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-math-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-metadata-key-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-metadata-value-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-missing-link-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-plain-url-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-pre-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-reference-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-strike-through-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-table-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(markdown-url-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(match
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(menu
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(message-cited-text-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(message-cited-text-2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(message-cited-text-3
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(message-cited-text-4
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(message-header-cc
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(message-header-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(message-header-newsgroups
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(message-header-other
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(message-header-subject
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(message-header-to
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(message-header-xheader
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(message-mml
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(message-separator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(message-signature-separator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(minibuffer-prompt
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(mm-command-output
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(mode-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(mode-line-active
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(mode-line-buffer-id
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(mode-line-emphasis
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(mode-line-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(mode-line-inactive
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(mouse
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(mouse-drag-and-drop-region
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(next-error
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(next-error-message
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(nobreak-hyphen
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(nobreak-space
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-calendar-event
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-calendar-sexp
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-clocking
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-column-dateline
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-current-time
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-date
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-date-today
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-date-weekend
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-date-weekend-today
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-diary
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-dimmed-todo-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-done
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-filter-category
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-filter-effort
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-filter-regexp
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-filter-tags
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-restriction-lock
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-structure
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-structure-filter
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-agenda-structure-secondary
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-archived
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-block
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-block-begin-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-block-end-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-checkbox
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-checkbox-statistics-done
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-checkbox-statistics-todo
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-cite
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-cite-key
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-clock-overlay
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-code
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-column
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-column-title
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-date
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-date-selected
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-default
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-dispatcher-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-document-info
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-document-info-keyword
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-document-title
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-done
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-drawer
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-ellipsis
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-footnote
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-formula
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-headline-done
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-headline-todo
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-hide
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-imminent-deadline
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-latex-and-related
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-level-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-level-2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-level-3
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-level-4
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-level-5
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-level-6
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-level-7
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-level-8
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-link
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-list-dt
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-macro
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-meta-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-mode-line-clock
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-mode-line-clock-overrun
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-priority
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-property-value
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-quote
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-scheduled
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-scheduled-previously
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-scheduled-today
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-sexp-date
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-special-keyword
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-table
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-table-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-tag
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-tag-group
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-target
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-time-grid
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-todo
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-upcoming-deadline
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-upcoming-distant-deadline
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-verbatim
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-verse
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(org-warning
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(outline-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(outline-2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(outline-3
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(outline-4
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(outline-5
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(outline-6
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(outline-7
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(outline-8
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(package-description
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(package-help-section-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(package-name
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(package-status-avail-obso
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(package-status-available
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(package-status-built-in
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(package-status-dependency
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(package-status-disabled
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(package-status-external
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(package-status-held
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(package-status-incompat
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(package-status-installed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(package-status-new
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(package-status-unsigned
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(popup-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(popup-isearch-match
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(popup-menu-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(popup-menu-mouse-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(popup-menu-selection-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(popup-menu-summary-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(popup-scroll-bar-background-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(popup-scroll-bar-foreground-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(popup-summary-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(popup-tip-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(pretty-hydra-toggle-off-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(pretty-hydra-toggle-on-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(pulse-highlight-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(pulse-highlight-start-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(query-replace
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rainbow-delimiters-base-error-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rainbow-delimiters-base-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rainbow-delimiters-depth-1-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rainbow-delimiters-depth-2-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rainbow-delimiters-depth-3-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rainbow-delimiters-depth-4-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rainbow-delimiters-depth-5-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rainbow-delimiters-depth-6-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rainbow-delimiters-depth-7-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rainbow-delimiters-depth-8-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rainbow-delimiters-depth-9-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rainbow-delimiters-mismatched-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rainbow-delimiters-unmatched-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(read-multiple-choice-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(reb-match-0
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(reb-match-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(reb-match-2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(reb-match-3
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(reb-regexp-grouping-backslash
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(reb-regexp-grouping-construct
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rectangle-preview
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(region
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rg-column-number-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rg-context-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rg-error-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rg-file-tag-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rg-filename-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rg-info-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rg-line-number-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rg-literal-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rg-match-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rg-match-position-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rg-regexp-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rg-toggle-off-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rg-toggle-on-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rg-warning-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(rxt-highlight-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(scroll-bar
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(secondary-selection
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(selectrum-completion-annotation
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(selectrum-completion-docsig
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(selectrum-current-candidate
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(selectrum-group-separator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(selectrum-group-title
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(selectrum-mouse-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(selectrum-prescient-primary-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(selectrum-prescient-secondary-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(selectrum-quick-keys-highlight
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(selectrum-quick-keys-match
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(separator-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(shadow
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(show-paren-match
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(show-paren-match-expression
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(show-paren-mismatch
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(shr-abbreviation
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(shr-h1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(shr-h2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(shr-h3
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(shr-h4
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(shr-h5
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(shr-h6
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(shr-link
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(shr-selected-link
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(shr-strike-through
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(shr-sup
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(shr-text
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(smerge-base
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(smerge-lower
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(smerge-markers
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(smerge-refined-added
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(smerge-refined-changed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(smerge-refined-removed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(smerge-upper
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(success
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(swiper-background-match-face-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(swiper-background-match-face-2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(swiper-background-match-face-3
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(swiper-background-match-face-4
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(swiper-line-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(swiper-match-face-1
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(swiper-match-face-2
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(swiper-match-face-3
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(swiper-match-face-4
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(tab-bar
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(tab-bar-tab
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(tab-bar-tab-group-current
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(tab-bar-tab-group-inactive
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(tab-bar-tab-inactive
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(tab-bar-tab-ungrouped
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(tab-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(tabulated-list-fake-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-bold
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-black
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-blue
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-bright-black
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-bright-blue
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-bright-cyan
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-bright-green
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-bright-magenta
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-bright-red
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-bright-white
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-bright-yellow
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-cyan
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-green
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-magenta
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-red
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-white
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-color-yellow
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-faint
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-fast-blink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-italic
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-slow-blink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(term-underline
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(tool-bar
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(tooltip
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(trailing-whitespace
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-active-infix
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-amaranth
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-argument
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-blue
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-disabled-suffix
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-enabled-suffix
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-heading
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-higher-level
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-inactive-argument
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-inactive-value
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-inapt-suffix
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-key
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-mismatched-key
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-nonstandard-key
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-pink
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-red
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-separator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-teal
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-unreachable
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-unreachable-key
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(transient-value
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(tty-menu-disabled-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(tty-menu-enabled-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(tty-menu-selected-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(tutorial-warning-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(underline
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(undo-tree-visualizer-active-branch-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(undo-tree-visualizer-current-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(undo-tree-visualizer-default-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(undo-tree-visualizer-register-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(undo-tree-visualizer-unmodified-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(variable-pitch
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(variable-pitch-text
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-conflict-state
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-dir-directory
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-dir-file
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-dir-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-dir-header-value
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-dir-mark-indicator
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-dir-status-edited
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-dir-status-ignored
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-dir-status-up-to-date
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-dir-status-warning
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-edited-state
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-locally-added-state
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-locked-state
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-missing-state
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-needs-update-state
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-removed-state
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-state-base
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vc-up-to-date-state
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vertical-border
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vterm-color-black
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vterm-color-blue
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vterm-color-cyan
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vterm-color-green
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vterm-color-inverse-video
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vterm-color-magenta
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vterm-color-red
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vterm-color-underline
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vterm-color-white
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(vterm-color-yellow
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(warning
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(wgrep-delete-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(wgrep-done-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(wgrep-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(wgrep-file-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(wgrep-reject-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(which-func
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(which-key-command-description-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(which-key-docstring-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(which-key-group-description-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(which-key-highlighted-command-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(which-key-key-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(which-key-local-map-description-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(which-key-note-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(which-key-separator-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(which-key-special-key-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(whitespace-big-indent
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(whitespace-empty
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(whitespace-hspace
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(whitespace-indentation
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(whitespace-line
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(whitespace-missing-newline-at-eof
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(whitespace-newline
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(whitespace-space
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(whitespace-space-after-tab
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(whitespace-space-before-tab
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(whitespace-tab
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(whitespace-trailing
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(widget-button
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(widget-button-pressed
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(widget-documentation
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(widget-field
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(widget-inactive
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(widget-single-line-field
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(window-divider
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(window-divider-first-pixel
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(window-divider-last-pixel
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(xref-file-header
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(xref-line-number
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(xref-match
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(yaml-tab-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(yas--field-debug-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   `(yas-field-highlight-face
     ((,class (:background ,bg1 :foreground ,fg1))))

   )
  )

;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'MY-AWESOME-THEME)

;; Local Variables:
;; no-byte-compile: t
;; End:
```

# list attributes

```elisp
(let ((faces (sort (face-list) #'string-lessp)))
    (mapcar (lambda (f)
              (cons f (face-all-attributes f))
              )
            faces))


(defun funk/get-face-attributes (face)
  (mapcar (lambda (pair)
            (let ((attr (car pair)))
              (cons attr (face-attribute face attr))))
          face-attribute-name-alist)
  )

(funk/get-face-attributes 'default)

(let ((faces (sort (face-list) #'string-lessp)))
  (mapcar (lambda (f)
            (cons f (list (list (funk/get-face-attributes f))))
            )
          faces))
```

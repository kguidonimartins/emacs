(deftheme funknight)

(custom-theme-set-faces
 'funknight

 '(default
   ((t
     (
      :width  normal
      :height  98
      :weight  regular
      :slant  normal
      :foreground  "#FFFFFF"
      :background  "#132738"
      )
     )))

 '(mode-line
   ((t
     (
      :foreground  "#FFFFFF"
      :background  "#2A265A"
      )
     )))

 '(Info-quoted
   ((t
     (
      :inherit  fixed-pitch-serif
      )
     )))

 '(alert-high-face
   ((t
     (
      :foreground  "#f1fa8c"
      :inherit  t
      )
     )))

 '(alert-low-face
   ((t
     (
      :foreground  "#565761"
      )
     )))

 '(alert-moderate-face
   ((t
     (
      :foreground  "#e2e2dc"
      :inherit  t
      )
     )))

 '(alert-trivial-face
   ((t
     (
      :foreground  "#8995ba"
      )
     )))

 '(alert-urgent-face
   ((t
     (
      :foreground  "#ff5555"
      :inherit  t
      )
     )))

 '(all-the-icons-blue
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(all-the-icons-blue-alt
   ((t
     (
      :foreground  "#0189cc"
      )
     )))

 '(all-the-icons-cyan
   ((t
     (
      :foreground  "#8be9fd"
      )
     )))

 '(all-the-icons-cyan-alt
   ((t
     (
      :foreground  "#8be9fd"
      )
     )))

 '(all-the-icons-dblue
   ((t
     (
      :foreground  "#0189cc"
      )
     )))

 '(all-the-icons-dcyan
   ((t
     (
      :foreground  "#8be9fd"
      )
     )))

 '(all-the-icons-dgreen
   ((t
     (
      :foreground  "#37ae56"
      )
     )))

 '(all-the-icons-dmaroon
   ((t
     (
      :foreground  "#b2548a"
      )
     )))

 '(all-the-icons-dorange
   ((t
     (
      :foreground  "#b2804b"
      )
     )))

 '(all-the-icons-dpink
   ((t
     (
      :foreground  "#ff6e6e"
      )
     )))

 '(all-the-icons-dpurple
   ((t
     (
      :foreground  "#8466ae"
      )
     )))

 '(all-the-icons-dred
   ((t
     (
      :foreground  "#b23b3b"
      )
     )))

 '(all-the-icons-dsilver
   ((t
     (
      :foreground  "#666770"
      )
     )))

 '(all-the-icons-dyellow
   ((t
     (
      :foreground  "#a8ae62"
      )
     )))

 '(all-the-icons-green
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(all-the-icons-ibuffer-dir-face
   ((t
     (
      :inherit  font-lock-doc-face
      )
     )))

 '(all-the-icons-ibuffer-file-face
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(all-the-icons-ibuffer-icon-face
   ((t
     (
      :inherit  default
      )
     )))

 '(all-the-icons-ibuffer-mode-face
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(all-the-icons-ibuffer-size-face
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(all-the-icons-ivy-rich-archive-face
   ((t
     (
      :inherit  font-lock-type-face
      )
     )))

 '(all-the-icons-ivy-rich-bookmark-face
   ((t
     (
      :inherit  all-the-icons-ivy-rich-doc-face
      )
     )))

 '(all-the-icons-ivy-rich-dir-face
   ((t
     (
      :inherit  font-lock-doc-face
      )
     )))

 '(all-the-icons-ivy-rich-doc-face
   ((t
     (
      :inherit  ivy-completions-annotations
      )
     )))

 '(all-the-icons-ivy-rich-file-modes-face
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(all-the-icons-ivy-rich-file-name-face
   ((t
     (
      :inherit  all-the-icons-ivy-rich-doc-face
      )
     )))

 '(all-the-icons-ivy-rich-file-owner-face
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(all-the-icons-ivy-rich-icon-face
   ((t
     (
      :inherit  default
      )
     )))

 '(all-the-icons-ivy-rich-indicator-face
   ((t
     (
      :inherit  error
      )
     )))

 '(all-the-icons-ivy-rich-install-face
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(all-the-icons-ivy-rich-major-mode-face
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(all-the-icons-ivy-rich-path-face
   ((t
     (
      :inherit  all-the-icons-ivy-rich-doc-face
      )
     )))

 '(all-the-icons-ivy-rich-process-buffer-face
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(all-the-icons-ivy-rich-process-command-face
   ((t
     (
      :inherit  all-the-icons-ivy-rich-doc-face
      )
     )))

 '(all-the-icons-ivy-rich-process-id-face
   ((t
     (
      :inherit  default
      )
     )))

 '(all-the-icons-ivy-rich-process-status-alt-face
   ((t
     (
      :inherit  error
      )
     )))

 '(all-the-icons-ivy-rich-process-status-face
   ((t
     (
      :inherit  success
      )
     )))

 '(all-the-icons-ivy-rich-process-thread-face
   ((t
     (
      :inherit  font-lock-doc-face
      )
     )))

 '(all-the-icons-ivy-rich-process-tty-face
   ((t
     (
      :inherit  font-lock-doc-face
      )
     )))

 '(all-the-icons-ivy-rich-project-face
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(all-the-icons-ivy-rich-size-face
   ((t
     (
      :inherit  shadow
      )
     )))

 '(all-the-icons-ivy-rich-time-face
   ((t
     (
      :inherit  shadow
      )
     )))

 '(all-the-icons-ivy-rich-version-face
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(all-the-icons-lblue
   ((t
     (
      :foreground  "#90d2ff"
      )
     )))

 '(all-the-icons-lcyan
   ((t
     (
      :foreground  "#adeffd"
      )
     )))

 '(all-the-icons-lgreen
   ((t
     (
      :foreground  "#84fba2"
      )
     )))

 '(all-the-icons-lmaroon
   ((t
     (
      :foreground  "#ffa1d7"
      )
     )))

 '(all-the-icons-lorange
   ((t
     (
      :foreground  "#ffcd98"
      )
     )))

 '(all-the-icons-lpink
   ((t
     (
      :foreground  "#ffb2b2"
      )
     )))

 '(all-the-icons-lpurple
   ((t
     (
      :foreground  "#d0b3fa"
      )
     )))

 '(all-the-icons-lred
   ((t
     (
      :foreground  "#ff8888"
      )
     )))

 '(all-the-icons-lsilver
   ((t
     (
      :foreground  "#cccccf"
      )
     )))

 '(all-the-icons-lyellow
   ((t
     (
      :foreground  "#f5fbae"
      )
     )))

 '(all-the-icons-maroon
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(all-the-icons-orange
   ((t
     (
      :foreground  "#ffb86c"
      )
     )))

 '(all-the-icons-pink
   ((t
     (
      :foreground  "#ff9090"
      )
     )))

 '(all-the-icons-purple
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(all-the-icons-purple-alt
   ((t
     (
      :foreground  "#656077"
      )
     )))

 '(all-the-icons-red
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(all-the-icons-red-alt
   ((t
     (
      :foreground  "#6f565f"
      )
     )))

 '(all-the-icons-silver
   ((t
     (
      :foreground  "#a2a2a8"
      )
     )))

 '(all-the-icons-yellow
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(all-the-icons-dired-dir-face
   ((t
     (
      :foreground  "#8995ba"
      )
     )))

 '(ansi-color-black
   ((t
     (
      :foreground  "black"
      :background  "black"
      )
     )))

 '(ansi-color-blue
   ((t
     (
      :foreground  "#FFFFFF"
      :background  "#FFFFFF"
      )
     )))

 '(ansi-color-bold
   ((t
     (
      :inherit  bold
      )
     )))

 '(ansi-color-bright-black
   ((t
     (
      :foreground  "gray30"
      :background  "gray30"
      )
     )))

 '(ansi-color-bright-blue
   ((t
     (
      :foreground  "blue1"
      :background  "blue1"
      )
     )))

 '(ansi-color-bright-cyan
   ((t
     (
      :foreground  "cyan2"
      :background  "cyan2"
      )
     )))

 '(ansi-color-bright-green
   ((t
     (
      :foreground  "green2"
      :background  "green2"
      )
     )))

 '(ansi-color-bright-magenta
   ((t
     (
      :foreground  "magenta2"
      :background  "magenta2"
      )
     )))

 '(ansi-color-bright-red
   ((t
     (
      :foreground  "red2"
      :background  "red2"
      )
     )))

 '(ansi-color-bright-white
   ((t
     (
      :foreground  "white"
      :background  "white"
      )
     )))

 '(ansi-color-bright-yellow
   ((t
     (
      :foreground  "yellow2"
      :background  "yellow2"
      )
     )))

 '(ansi-color-cyan
   ((t
     (
      :foreground  "cyan3"
      :background  "cyan3"
      )
     )))

 '(ansi-color-faint
   ((t
     (
      :weight  light
      )
     )))

 '(ansi-color-green
   ((t
     (
      :foreground  "green3"
      :background  "green3"
      )
     )))

 '(ansi-color-italic
   ((t
     (
      :inherit  italic
      )
     )))

 '(ansi-color-magenta
   ((t
     (
      :foreground  "magenta3"
      :background  "magenta3"
      )
     )))

 '(ansi-color-red
   ((t
     (
      :foreground  "red3"
      :background  "red3"
      )
     )))

 '(ansi-color-underline
   ((t
     (
      :inherit  underline
      )
     )))

 '(ansi-color-white
   ((t
     (
      :foreground  "grey90"
      :background  "gray90"
      )
     )))

 '(ansi-color-yellow
   ((t
     (
      :foreground  "yellow3"
      :background  "yellow3"
      )
     )))

 '(avy-background-face
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(avy-goto-char-timer-face
   ((t
     (
      :inherit  highlight
      )
     )))

 '(avy-lead-face
   ((t
     (
      :weight  bold
      :foreground  "#282a36"
      :background  "#bd93f9"
      )
     )))

 '(avy-lead-face-0
   ((t
     (
      :background  "#d0b3fa"
      :inherit  avy-lead-face
      )
     )))

 '(avy-lead-face-1
   ((t
     (
      :background  "#e4d3fc"
      :inherit  avy-lead-face
      )
     )))

 '(avy-lead-face-2
   ((t
     (
      :background  "#f8f4fe"
      :inherit  avy-lead-face
      )
     )))

 '(aw-background-face
   ((t
     (
      :foreground  "gray40"
      )
     )))

 '(aw-key-face
   ((t
     (
      :inherit  font-lock-builtin-face
      )
     )))

 '(aw-leading-char-face
   ((t
     (
      :height  10.0
      :inherit  ace-jump-face-foreground
      )
     )))

 '(aw-minibuffer-leading-char-face
   ((t
     (
      :inherit  aw-leading-char-face
      )
     )))

 '(aw-mode-line-face
   ((t
     (
      :inherit  mode-line-buffer-id
      )
     )))

 '(biblio-detail-header-face
   ((t
     (
      :slant  normal
      )
     )))

 '(biblio-highlight-extend-face
   ((t
     (
      :inherit  highlight
      )
     )))

 '(biblio-results-header-face
   ((t
     (
      :height  1.5
      :weight  bold
      :inherit  font-lock-preprocessor-face
      )
     )))

 '(blamer-face
   ((t
     (
      :height  90
      :foreground  "#7a88cf"
      :background  "#132738"
      )
     )))

 '(bold
   ((t
     (
      :weight  bold
      )
     )))

 '(bold-italic
   ((t
     (
      :inherit bold italic
      )
     )))

 '(bookmark-face
   ((t
     (
      :background  "#363449"
      )
     )))

 '(bookmark-menu-bookmark
   ((t
     (
      :weight  bold
      )
     )))

 '(bookmark-menu-heading
   ((t
     (
      :inherit  font-lock-type-face
      )
     )))

 '(browse-url-button
   ((t
     (
      :inherit  link
      )
     )))

 '(buffer-menu-buffer
   ((t
     (
      :weight  bold
      )
     )))

 '(button
   ((t
     (
      :inherit  link
      )
     )))

 '(calc-nonselected-face
   ((t
     (
      :slant  italic
      :inherit  shadow
      )
     )))

 '(calc-selected-face
   ((t
     (
      :weight  bold
      )
     )))

 '(calendar-month-header
   ((t
     (
      :inherit  font-lock-function-name-face
      )
     )))

 '(calendar-today
   ((t
     (
      :background "#FFFFFF"
      :foreground "#2A265A"
      :weight bold
      )
     )))

 '(calendar-weekday-header
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(calendar-weekend-header
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(change-log-acknowledgment
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(change-log-conditionals
   ((t
     (
      :inherit  font-lock-variable-name-face
      )
     )))

 '(change-log-date
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(change-log-email
   ((t
     (
      :inherit  font-lock-variable-name-face
      )
     )))

 '(change-log-file
   ((t
     (
      :inherit  font-lock-function-name-face
      )
     )))

 '(change-log-function
   ((t
     (
      :inherit  font-lock-variable-name-face
      )
     )))

 '(change-log-list
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(change-log-name
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(citar
   ((t
     (
      :inherit  font-lock-doc-face
      )
     )))

 '(citar-highlight
   ((t
     (
      :weight  bold
      )
     )))

 '(citar-icon-dim
   ((t
     (
      :foreground  "#282c34"
      )
     )))

 '(comint-highlight-input
   ((t
     (
      :weight  bold
      )
     )))

 '(comint-highlight-prompt
   ((t
     (
      :inherit  minibuffer-prompt
      )
     )))

 '(company-echo-common
   ((t
     (
      :foreground  "firebrick1"
      )
     )))

 '(company-preview
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(company-preview-common
   ((t
     (
      :foreground  "#bd93f9"
      :background  "#44475a"
      )
     )))

 '(company-preview-search
   ((t
     (
      :inherit  company-tooltip-search
      )
     )))

 '(company-tooltip
   ((t
     (
      :inherit  tooltip
      )
     )))

 '(company-tooltip-annotation
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(company-tooltip-annotation-selection
   ((t
     (
      :inherit  company-tooltip-annotation
      )
     )))

 '(company-tooltip-common
   ((t
     (
      :weight  bold
      :foreground  "#bd93f9"
      )
     )))

 '(company-tooltip-common-selection
   ((t
     (
      :inherit  company-tooltip-common
      )
     )))

 '(company-tooltip-mouse
   ((t
     (
      :foreground  "#282a36"
      :background  "#ff79c6"
      )
     )))

 '(company-tooltip-quick-access
   ((t
     (
      :inherit  company-tooltip-annotation
      )
     )))

 '(company-tooltip-quick-access-selection
   ((t
     (
      :inherit  company-tooltip-annotation-selection
      )
     )))

 '(company-tooltip-scrollbar-thumb
   ((t
     (
      :background  "#bd93f9"
      )
     )))

 '(company-tooltip-scrollbar-track
   ((t
     (
      :inherit  tooltip
      )
     )))

 '(company-tooltip-search
   ((t
     (
      :weight  bold
      :foreground  "#282a36"
      :background  "#bd93f9"
      )
     )))

 '(company-tooltip-search-selection
   ((t
     (
      :background  "#006699"
      )
     )))

 '(company-tooltip-selection
   ((t
     (
      :background  "#44475a"
      )
     )))

 '(compilation-column-number
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(compilation-error
   ((t
     (
      :weight  bold
      :inherit  error
      )
     )))

 '(compilation-info
   ((t
     (
      :inherit  success
      )
     )))

 '(compilation-line-number
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(compilation-mode-line-exit
   ((t
     (
      :inherit  compilation-info
      )
     )))

 '(compilation-mode-line-fail
   ((t
     (
      :inherit  compilation-error
      )
     )))

 '(compilation-mode-line-run
   ((t
     (
      :inherit  compilation-warning
      )
     )))

 '(compilation-warning
   ((t
     (
      :slant  italic
      :inherit  warning
      )
     )))

 '(completions-annotations
   ((t
     (
      :inherit italic shadow
      )
     )))

 '(completions-common-part
   ((t
     (
      :foreground  "lightblue"
      )
     )))

 '(completions-first-difference
   ((t
     (
      :inherit  bold
      )
     )))

 '(completions-group-separator
   ((t
     (
      :inherit  shadow
      )
     )))

 '(completions-group-title
   ((t
     (
      :slant  italic
      :inherit  shadow
      )
     )))

 '(confusingly-reordered
   ((t
     (
      :underline (:style wave :color "Red1")
      )
     )))

 '(consult-async-failed
   ((t
     (
      :inherit  error
      )
     )))

 '(consult-async-finished
   ((t
     (
      :inherit  success
      )
     )))

 '(consult-async-running
   ((t
     (
      :inherit  consult-narrow-indicator
      )
     )))

 '(consult-async-split
   ((t
     (
      :inherit  font-lock-negation-char-face
      )
     )))

 '(consult-bookmark
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(consult-crm-selected
   ((t
     (
      :inherit  secondary-selection
      )
     )))

 '(consult-file
   ((t
     (
      :inherit  font-lock-function-name-face
      )
     )))

 '(consult-grep-context
   ((t
     (
      :inherit  shadow
      )
     )))

 '(consult-help
   ((t
     (
      :inherit  shadow
      )
     )))

 '(consult-key
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(consult-line-number
   ((t
     (
      :inherit  consult-key
      )
     )))

 '(consult-line-number-prefix
   ((t
     (
      :inherit  line-number
      )
     )))

 '(consult-line-number-wrapped
   ((t
     (
      :inherit  font-lock-warning-face
      )
     )))

 '(consult-narrow-indicator
   ((t
     (
      :inherit  warning
      )
     )))

 '(consult-preview-cursor
   ((t
     (
      :inherit  consult-preview-match
      )
     )))

 '(consult-preview-error
   ((t
     (
      :inherit  isearch-fail
      )
     )))

 '(consult-preview-insertion
   ((t
     (
      :inherit  region
      )
     )))

 '(consult-preview-line
   ((t
     (
      :inherit  consult-preview-insertion
      )
     )))

 '(consult-preview-match
   ((t
     (
      :inherit  match
      )
     )))

 '(consult-separator
   ((t
     (
      :foreground  "#333"
      )
     )))

 '(counsel--mark-ring-highlight
   ((t
     (
      :inherit  highlight
      )
     )))

 '(counsel-active-mode
   ((t
     (
      :inherit  font-lock-builtin-face
      :weight bold
      )
     )))

 '(counsel-application-name
   ((t
     (
      :inherit  font-lock-builtin-face
      )
     )))

 '(counsel-evil-register-face
   ((t
     (
      :inherit  counsel-outline-1
      )
     )))

 '(counsel-key-binding
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(counsel-outline-1
   ((t
     (
      :inherit  org-level-1
      )
     )))

 '(counsel-outline-2
   ((t
     (
      :inherit  org-level-2
      )
     )))

 '(counsel-outline-3
   ((t
     (
      :inherit  org-level-3
      )
     )))

 '(counsel-outline-4
   ((t
     (
      :inherit  org-level-4
      )
     )))

 '(counsel-outline-5
   ((t
     (
      :inherit  org-level-5
      )
     )))

 '(counsel-outline-6
   ((t
     (
      :inherit  org-level-6
      )
     )))

 '(counsel-outline-7
   ((t
     (
      :inherit  org-level-7
      )
     )))

 '(counsel-outline-8
   ((t
     (
      :inherit  org-level-8
      )
     )))

 '(counsel-outline-default
   ((t
     (
      :inherit  minibuffer-prompt
      )
     )))

 '(counsel-variable-documentation
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(cursor
   ((t
     (
      :background  "#f0cc09"
      )
     )))

 '(custom-button
   ((t
     (
      :foreground  "#61bfff"
      :background  "#282a36"
      )
     )))

 '(custom-button-mouse
   ((t
     (
      :foreground  "#282a36"
      :background  "#61bfff"
      )
     )))

 '(custom-button-pressed
   ((t
     (
      :foreground  "#282a36"
      :background  "#61bfff"
      )
     )))

 '(custom-button-pressed-unraised
   ((t
     (
      :foreground  "#282a36"
      :background  "#bd93f9"
      )
     )))

 '(custom-button-unraised
   ((t
     (
      :foreground  "#bd93f9"
      :background  "#282a36"
      )
     )))

 '(custom-changed
   ((t
     (
      :foreground  "#61bfff"
      :background  "#282a36"
      )
     )))

 '(custom-comment
   ((t
     (
      :foreground  "#f8f8f2"
      :background  "#44475a"
      )
     )))

 '(custom-comment-tag
   ((t
     (
      :foreground  "#565761"
      )
     )))

 '(custom-documentation
   ((t
     (
      )
     )))

 '(custom-face-tag
   ((t
     (
      :inherit  custom-variable-tag
      )
     )))

 '(custom-group-subtitle
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(custom-group-tag
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(custom-group-tag-1
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(custom-invalid
   ((t
     (
      :foreground  "#ff5555"
      :background  "#53323c"
      )
     )))

 '(custom-link
   ((t
     (
      :inherit  link
      )
     )))

 '(custom-modified
   ((t
     (
      :foreground  "#61bfff"
      :background  "#33475e"
      )
     )))

 '(custom-rogue
   ((t
     (
      :foreground  "pink"
      :background  "black"
      )
     )))

 '(custom-saved
   ((t
     (
      :weight  bold
      :foreground  "#50fa7b"
      :background  "#305343"
      )
     )))

 '(custom-set
   ((t
     (
      :foreground  "#f1fa8c"
      :background  "#282a36"
      )
     )))

 '(custom-state
   ((t
     (
      :foreground  "#50fa7b"
      :background  "#305343"
      )
     )))

 '(custom-themed
   ((t
     (
      :foreground  "#f1fa8c"
      :background  "#282a36"
      )
     )))

 '(custom-variable-button
   ((t
     (
      :underline  t
      :foreground  "#50fa7b"
      )
     )))

 '(custom-variable-obsolete
   ((t
     (
      :foreground  "#565761"
      :background  "#282a36"
      )
     )))

 '(custom-variable-tag
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(custom-visibility
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(diary
   ((t
     (
      :foreground  "yellow1"
      )
     )))

 '(diff-added
   ((t
     (
      :foreground  "#50fa7b"
      :inherit  hl-line
      )
     )))

 '(diff-changed
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(diff-context
   ((t
     (
      :foreground  "#dadad4"
      )
     )))

 '(diff-error
   ((t
     (
      :weight  bold
      :foreground  "red"
      :background  "black"
      )
     )))

 '(diff-file-header
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(diff-function
   ((t
     (
      :inherit  diff-header
      )
     )))

 '(diff-header
   ((t
     (
      :foreground  "#8be9fd"
      )
     )))

 '(diff-hl-change
   ((t
     (
      :foreground  "#6666ff"
      :background  "#132738"
      )
     )))

 '(diff-hl-delete
   ((t
     (
      :foreground  "#FF6E67"
      :background  "#132738"
      )
     )))

 '(diff-hl-dired-change
   ((t
     (
      :inherit  diff-hl-change
      )
     )))

 '(diff-hl-dired-delete
   ((t
     (
      :inherit  diff-hl-delete
      )
     )))

 '(diff-hl-dired-ignored
   ((t
     (
      :inherit  dired-ignored
      )
     )))

 '(diff-hl-dired-insert
   ((t
     (
      :inherit  diff-hl-insert
      )
     )))

 '(diff-hl-dired-unknown
   ((t
     (
      :inherit  dired-ignored
      )
     )))

 '(diff-hl-insert
   ((t
     (
      :foreground  "#50fa7b"
      :background  "#50fa7b"
      )
     )))

 '(diff-hl-margin-change
   ((t
     (
      :inherit  diff-hl-change
      )
     )))

 '(diff-hl-margin-delete
   ((t
     (
      :inherit  diff-hl-delete
      )
     )))

 '(diff-hl-margin-ignored
   ((t
     (
      :inherit  dired-ignored
      )
     )))

 '(diff-hl-margin-insert
   ((t
     (
      :inherit  diff-hl-insert
      )
     )))

 '(diff-hl-margin-unknown
   ((t
     (
      :inherit  dired-ignored
      )
     )))

 '(diff-hunk-header
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(diff-index
   ((t
     (
      :inherit  diff-file-header
      )
     )))

 '(diff-indicator-added
   ((t
     (
      :foreground  "#22aa22"
      :inherit  diff-added
      )
     )))

 '(diff-indicator-changed
   ((t
     (
      :foreground  "#aaaa22"
      :inherit  diff-changed
      )
     )))

 '(diff-indicator-removed
   ((t
     (
      :foreground  "#aa2222"
      :inherit  diff-removed
      )
     )))

 '(diff-nonexistent
   ((t
     (
      :inherit  diff-file-header
      )
     )))

 '(diff-refine-added
   ((t
     (
      :inherit  diff-added
      )
     )))

 '(diff-refine-changed
   ((t
     (
      :inherit  diff-changed
      )
     )))

 '(diff-refine-removed
   ((t
     (
      :inherit  diff-removed
      )
     )))

 '(diff-removed
   ((t
     (
      :foreground  "#ff5555"
      :background  "#44475a"
      )
     )))

 '(dired-broken-symlink
   ((t
     (
      :weight  bold
      :foreground  "yellow1"
      :background  "red1"
      )
     )))

 '(dired-directory
   ((t
     (
      :foreground  "#F1FA8C"
      )
     )))

 '(dired-filter-group-header
   ((t
     (
      :inherit  header-line
      )
     )))

 '(dired-flagged
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(dired-header
   ((t
     (
      :weight  bold
      :foreground  "#61bfff"
      )
     )))

 '(dired-ignored
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(dired-mark
   ((t
     (
      :weight  bold
      :foreground  "#ffb86c"
      )
     )))

 '(dired-marked
   ((t
     (
      :weight  bold
      :foreground  "#ff79c6"
      )
     )))

 '(dired-narrow-blink
   ((t
     (
      :foreground  "black"
      :background  "#eadc62"
      )
     )))

 '(dired-perm-write
   ((t
     (
      :underline  t
      :foreground  "#f8f8f2"
      )
     )))

 '(dired-set-id
   ((t
     (
      :inherit  font-lock-warning-face
      )
     )))

 '(dired-special
   ((t
     (
      :inherit  font-lock-variable-name-face
      )
     )))

 '(dired-subtree-depth-1-face
   ((t
     (
      :background  "#132738"
      )
     )))

 '(dired-subtree-depth-2-face
   ((t
     (
      :background  "#132738"
      )
     )))

 '(dired-subtree-depth-3-face
   ((t
     (
      :background  "#132738"
      )
     )))

 '(dired-subtree-depth-4-face
   ((t
     (
      :background  "#132738"
      )
     )))

 '(dired-subtree-depth-5-face
   ((t
     (
      :background  "#132738"
      )
     )))

 '(dired-subtree-depth-6-face
   ((t
     (
      :background  "#132738"
      )
     )))

 '(dired-symlink
   ((t
     (
      :weight  bold
      :foreground  "#8be9fd"
      )
     )))

 '(dired-warning
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(diredfl-autofile-name
   ((t
     (
      :foreground  "#565761"
      )
     )))

 '(diredfl-compressed-file-name
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(diredfl-compressed-file-suffix
   ((t
     (
      :foreground  "#a97f56"
      )
     )))

 '(diredfl-date-time
   ((t
     (
      :weight  light
      :foreground  "#8be9fd"
      )
     )))

 '(diredfl-deletion
   ((t
     (
      :weight  bold
      :foreground  "#ff5555"
      :background  "#53323c"
      )
     )))

 '(diredfl-deletion-file-name
   ((t
     (
      :foreground  "#ff5555"
      :background  "#53323c"
      )
     )))

 '(diredfl-dir-heading
   ((t
     (
      :weight  bold
      :foreground  "#61bfff"
      )
     )))

 '(diredfl-dir-name
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(diredfl-dir-priv
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(diredfl-exec-priv
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(diredfl-executable-tag
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(diredfl-file-name
   ((t
     (
      :foreground  "#f8f8f2"
      )
     )))

 '(diredfl-file-suffix
   ((t
     (
      :foreground  "#a4a5a6"
      )
     )))

 '(diredfl-flag-mark
   ((t
     (
      :weight  bold
      :foreground  "#f1fa8c"
      :background  "#505347"
      )
     )))

 '(diredfl-flag-mark-line
   ((t
     (
      :background  "#3c3e3e"
      )
     )))

 '(diredfl-ignored-file-name
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(diredfl-link-priv
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(diredfl-no-priv
   ((t
     (
      :inherit  shadow
      )
     )))

 '(diredfl-number
   ((t
     (
      :foreground  "#ffb86c"
      )
     )))

 '(diredfl-other-priv
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(diredfl-rare-priv
   ((t
     (
      :foreground  "#f8f8f2"
      )
     )))

 '(diredfl-read-priv
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(diredfl-symlink
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(diredfl-tagged-autofile-name
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(diredfl-write-priv
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(edebug-disabled-breakpoint
   ((t
     (
      :background  "#335533"
      )
     )))

 '(edebug-enabled-breakpoint
   ((t
     (
      :inherit  highlight
      )
     )))

 '(eglot-highlight-symbol-face
   ((t
     (
      :inherit  bold
      )
     )))

 '(eglot-mode-line
   ((t
     (
      :weight  bold
      :inherit  font-lock-constant-face
      )
     )))

 '(eieio-custom-slot-tag-face
   ((t
     (
      :foreground  "light blue"
      )
     )))

 '(eldoc-highlight-function-argument
   ((t
     (
      :inherit  bold
      )
     )))

 '(elisp-shorthand-font-lock-face
   ((t
     (
      :foreground  "cyan"
      :inherit  font-lock-keyword-face
      )
     )))

 '(embark-collect-annotation
   ((t
     (
      :inherit  completions-annotations
      )
     )))

 '(embark-collect-candidate
   ((t
     (
      :inherit  default
      )
     )))

 '(embark-collect-zebra-highlight
   ((t
     (
      :background  "#242424"
      )
     )))

 '(embark-keybinding
   ((t
     (
      :inherit  success
      )
     )))

 '(embark-keybinding-repeat
   ((t
     (
      :inherit  font-lock-builtin-face
      )
     )))

 '(embark-keymap
   ((t
     (
      :slant  italic
      )
     )))

 '(embark-target
   ((t
     (
      :inherit  highlight
      )
     )))

 '(embark-verbose-indicator-documentation
   ((t
     (
      :inherit  completions-annotations
      )
     )))

 '(embark-verbose-indicator-shadowed
   ((t
     (
      :inherit  shadow
      )
     )))

 '(embark-verbose-indicator-title
   ((t
     (
      :height  1.1
      :weight  bold
      )
     )))

 '(epa-field-body
   ((t
     (
      :slant  italic
      :foreground  "turquoise"
      )
     )))

 '(epa-field-name
   ((t
     (
      :weight  bold
      :foreground  "PaleTurquoise"
      )
     )))

 '(epa-mark
   ((t
     (
      :weight  bold
      :foreground  "orange"
      )
     )))

 '(epa-string
   ((t
     (
      :foreground  "lightyellow"
      )
     )))

 '(epa-validity-disabled
   ((t
     (
      :slant  italic
      )
     )))

 '(epa-validity-high
   ((t
     (
      :weight  bold
      :foreground  "PaleTurquoise"
      )
     )))

 '(epa-validity-low
   ((t
     (
      :slant  italic
      )
     )))

 '(epa-validity-medium
   ((t
     (
      :slant  italic
      :foreground  "PaleTurquoise"
      )
     )))

 '(error
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(ert-test-result-expected
   ((t
     (
      :background  "green3"
      )
     )))

 '(ert-test-result-unexpected
   ((t
     (
      :background  "red3"
      )
     )))

 '(escape-glyph
   ((t
     (
      :foreground  "#8be9fd"
      )
     )))

 '(eshell-prompt
   ((t
     (
      :weight  bold
      :foreground  "#bd93f9"
      )
     )))

 '(ess-%op%-face
   ((t
     (
      :inherit  ess-operator-face
      )
     )))

 '(ess-assignment-face
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(ess-bp-fringe-browser-face
   ((t
     (
      :foreground  "deep sky blue"
      )
     )))

 '(ess-bp-fringe-inactive-face
   ((t
     (
      :foreground  "LightGray"
      )
     )))

 '(ess-bp-fringe-logger-face
   ((t
     (
      :foreground  "tomato1"
      )
     )))

 '(ess-bp-fringe-recover-face
   ((t
     (
      :foreground  "magenta"
      )
     )))

 '(ess-constant-face
   ((t
     (
      :inherit  font-lock-type-face
      )
     )))

 '(ess-debug-blink-ref-not-found-face
   ((t
     (
      :background  "dark red"
      )
     )))

 '(ess-debug-blink-same-ref-face
   ((t
     (
      :background  "midnight blue"
      )
     )))

 '(ess-debug-current-debug-line-face
   ((t
     (
      :inherit  highlight
      )
     )))

 '(ess-function-call-face
   ((t
     (
      :weight  bold
      :foreground  "#bc6ec5"
      )
     )))

 '(ess-keyword-face
   ((t
     (
      :inherit  ess-function-call-face
      )
     )))

 '(ess-matrix-face
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(ess-modifiers-face
   ((t
     (
      :inherit  ess-function-call-face
      )
     )))

 '(ess-numbers-face
   ((t
     (
      :slant  normal
      :inherit  font-lock-type-face
      )
     )))

 '(ess-operator-face
   ((t
     (
      :foreground  "#87cefa"
      )
     )))

 '(ess-paren-face
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(ess-r-control-flow-keyword-face
   ((t
     (
      :inherit  ess-keyword-face
      )
     )))

 '(ess-tracebug-last-input-fringe-face
   ((t
     (
      :foreground  "deep sky blue"
      )
     )))

 '(ess-watch-current-block-face
   ((t
     (
      :inherit  highlight
      )
     )))

 '(evil-ex-commands
   ((t
     (
      :slant  italic
      :underline  t
      )
     )))

 '(evil-ex-info
   ((t
     (
      :slant  italic
      :foreground  "#ff5555"
      )
     )))

 '(evil-ex-lazy-highlight
   ((t
     (
      :inherit  lazy-highlight
      )
     )))

 '(evil-ex-search
   ((t
     (
      :weight  bold
      :foreground  "#1E2029"
      :background  "#bd93f9"
      )
     )))

 '(evil-ex-substitute-matches
   ((t
     (
      :weight  bold
      :foreground  "#ff5555"
      :background  "#1E2029"
      )
     )))

 '(evil-ex-substitute-replacement
   ((t
     (
      :weight  bold
      :foreground  "#50fa7b"
      :background  "#1E2029"
      )
     )))

 '(evil-goggles--pulse-face
   ((t
     (
      :inherit evil-goggles-yank-face
      )
     )))

 '(evil-goggles-change-face
   ((t
     (
      :inherit  diff-removed
      )
     )))

 '(evil-goggles-commentary-face
   ((t
     (
      :inherit  evil-goggles-default-face
      )
     )))

 '(evil-goggles-default-face
   ((t
     (
      :background  "#363848"
      :inherit  region
      )
     )))

 '(evil-goggles-delete-face
   ((t
     (
      :inherit  diff-removed
      )
     )))

 '(evil-goggles-fill-and-move-face
   ((t
     (
      :inherit  evil-goggles-yank-face
      )
     )))

 '(evil-goggles-indent-face
   ((t
     (
      :inherit  evil-goggles-default-face
      )
     )))

 '(evil-goggles-join-face
   ((t
     (
      :inherit  evil-goggles-default-face
      )
     )))

 '(evil-goggles-nerd-commenter-face
   ((t
     (
      :inherit  evil-goggles-default-face
      )
     )))

 '(evil-goggles-paste-face
   ((t
     (
      :inherit  evil-goggles-yank-face
      )
     )))

 '(evil-goggles-record-macro-face
   ((t
     (
      :inherit  evil-goggles-default-face
      )
     )))

 '(evil-goggles-replace-with-register-face
   ((t
     (
      :inherit  evil-goggles-default-face
      )
     )))

 '(evil-goggles-set-marker-face
   ((t
     (
      :inherit  evil-goggles-default-face
      )
     )))

 '(evil-goggles-shift-face
   ((t
     (
      :inherit  evil-goggles-default-face
      )
     )))

 '(evil-goggles-surround-face
   ((t
     (
      :inherit  evil-goggles-default-face
      )
     )))

 '(evil-goggles-yank-face
   ((t
     (
      :foreground "yellow"
      :weight ultra-bold
      )
     )))

 '(evil-visual-mark-face
   ((t
     (
      :underline  t
      :foreground  "white"
      :background  "#8b008b"
      )
     )))

 '(eww-form-checkbox
   ((t
     (
      :inherit  eww-form-file
      )
     )))

 '(eww-form-file
   ((t
     (
      :background  "#1E2029"
      :inherit  eww-form-submit
      )
     )))

 '(eww-form-select
   ((t
     (
      :background  "#1E2029"
      :inherit  eww-form-submit
      )
     )))

 '(eww-form-submit
   ((t
     (
      :background  "#373844"
      :inherit  eww-form-text
      )
     )))

 '(eww-form-text
   ((t
     (
      :foreground  "#f8f8f2"
      :background  "#282a36"
      )
     )))

 '(eww-form-textarea
   ((t
     (
      :inherit  eww-form-text
      )
     )))

 '(eww-invalid-certificate
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(eww-valid-certificate
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(eyebrowse-mode-line-active
   ((t
     (
      :weight  ultra-bold
      :foreground  "#F1FA8C"
      )
     )))

 '(ffap
   ((t
     (
      :inherit  highlight
      )
     )))

 '(file-name-shadow
   ((t
     (
      :inherit  shadow
      )
     )))

 '(fill-column-indicator
   ((t
     (
      :weight  normal
      :slant  normal
      :inherit  shadow
      )
     )))

 '(fixed-pitch
   ((t
     (
      :width  normal
      :height  100
      :weight  regular
      :slant  normal
      )
     )))

 '(flycheck-color-mode-line-error-face
   ((t
     (
      :inherit  flycheck-fringe-error
      )
     )))

 '(flycheck-color-mode-line-info-face
   ((t
     (
      :inherit  flycheck-fringe-info
      )
     )))

 '(flycheck-color-mode-line-running-face
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(flycheck-color-mode-line-warning-face
   ((t
     (
      :inherit  flycheck-fringe-warning
      )
     )))

 '(flycheck-error
   ((t
     (
      :underline (:style wave :color "#ff5555")
      )
     )))

 '(flycheck-error-list-checker-name
   ((t
     (
      :inherit  font-lock-function-name-face
      )
     )))

 '(flycheck-error-list-error
   ((t
     (
      :inherit  error
      )
     )))

 '(flycheck-error-list-filename
   ((t
     (
      :weight  normal
      :inherit  mode-line-buffer-id
      )
     )))

 '(flycheck-error-list-highlight
   ((t
     (
      :weight  bold
      )
     )))

 '(flycheck-error-list-id
   ((t
     (
      :inherit  font-lock-type-face
      )
     )))

 '(flycheck-error-list-id-with-explainer
   ((t
     (
      :inherit  flycheck-error-list-id
      )
     )))

 '(flycheck-error-list-info
   ((t
     (
      :inherit  success
      )
     )))

 '(flycheck-error-list-warning
   ((t
     (
      :inherit  warning
      )
     )))

 '(flycheck-fringe-error
   ((t
     (
      :foreground  "#ff5555"
      :inherit  fringe
      )
     )))

 '(flycheck-fringe-info
   ((t
     (
      :foreground  "#50fa7b"
      :inherit  fringe
      )
     )))

 '(flycheck-fringe-warning
   ((t
     (
      :foreground  "#f1fa8c"
      :inherit  fringe
      )
     )))

 '(flycheck-info
   ((t
     (
      :underline (:style wave :color "#50fa7b")
      )
     )))

 '(flycheck-inline-error
   ((t
     (
      :inherit  compilation-error
      )
     )))

 '(flycheck-inline-info
   ((t
     (
      :inherit  compilation-info
      )
     )))

 '(flycheck-inline-warning
   ((t
     (
      :inherit  compilation-warning
      )
     )))

 '(flycheck-warning
   ((t
     (
      :underline (:style wave :color "#f1fa8c")
      )
     )))

 '(flymake-error
   ((t
     (
      :underline (:style wave :color "#ff5555")
      )
     )))

 '(flymake-note
   ((t
     (
      :underline (:style wave :color "#50fa7b")
      )
     )))

 '(flymake-warning
   ((t
     (
      :underline (:style wave :color "#ffb86c")
      )
     )))

 '(flyspell-duplicate
   ((t
     (
      :underline (:style wave :color "#f1fa8c")
      )
     )))

 '(flyspell-incorrect
   ((t
     (
      :underline (:style wave :color "#ff5555")
      )
     )))

 '(font-lock-builtin-face
   ((t
     (
      :foreground  "#F1FA8C"
      )
     )))

 '(font-lock-comment-delimiter-face
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(font-lock-comment-face
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(font-lock-constant-face
   ((t
     (
      :foreground  "#8be9fd"
      )
     )))

 '(font-lock-doc-face
   ((t
     (
      :foreground  "#bd93f9"
      :inherit  font-lock-comment-face
      )
     )))

 '(font-lock-doc-markup-face
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(font-lock-function-name-face
   ((t
     (
      :weight  ultra-bold
      :foreground  "#bc6ec5"
      )
     )))

 '(font-lock-keyword-face
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(font-lock-negation-char-face
   ((t
     (
      :foreground  "#bd93f9"
      :inherit  bold
      )
     )))

 '(font-lock-preprocessor-face
   ((t
     (
      :foreground  "#bd93f9"
      :inherit  bold
      )
     )))

 '(font-lock-regexp-grouping-backslash
   ((t
     (
      :foreground  "#bd93f9"
      :inherit  bold
      )
     )))

 '(font-lock-regexp-grouping-construct
   ((t
     (
      :foreground  "#bd93f9"
      :inherit  bold
      )
     )))

 '(font-lock-string-face
   ((t
     (
      :foreground  "#F1FA8C"
      )
     )))

 '(font-lock-type-face
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(font-lock-variable-name-face
   ((t
     (
      :foreground  "#ffc9e8"
      )
     )))

 '(font-lock-warning-face
   ((t
     (
      :inherit  warning
      )
     )))

 '(forge-post-author
   ((t
     (
      :inherit  bold
      )
     )))

 '(forge-post-date
   ((t
     (
      :inherit  italic
      )
     )))

 '(forge-topic-closed
   ((t
     (
      :inherit  magit-dimmed
      )
     )))

 '(forge-topic-label
   ((t
     (
      )
     )))

 '(forge-topic-merged
   ((t
     (
      :inherit  magit-dimmed
      )
     )))

 '(forge-topic-open
   ((t
     (
      :inherit  default
      )
     )))

 '(forge-topic-unmerged
   ((t
     (
      :slant  italic
      :inherit  magit-dimmed
      )
     )))

 '(forge-topic-unread
   ((t
     (
      :inherit  bold
      )
     )))

 '(fringe
   ((t
     (
      :foreground  "#565761"
      :inherit  default
      )
     )))

 '(funk/modeline-buffer-file
   ((t
     (
      :inherit (mode-line-buffer-id regular)
      )
     )))

 '(funk/modeline-buffer-file-modified
   ((t
     (
      :foreground  "#FF6E67"
      :inherit  funk/modeline-buffer-file
      )
     )))

 '(funk/modeline-buffer-path
   ((t
     (
      :inherit funk/modeline-project-dir
      )
     )))

 '(funk/modeline-project-dir
   ((t
     (
      :inherit (font-lock-string-face bold)
      )
     )))

 '(funk/modeline-project-parent-dir
   ((t
     (
      :inherit (font-lock-comment-face bold)
      )
     )))

 '(git-commit-comment-action
   ((t
     (
      :inherit  bold
      )
     )))

 '(git-commit-comment-branch-local
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(git-commit-comment-branch-remote
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(git-commit-comment-detached
   ((t
     (
      :foreground  "#ffb86c"
      )
     )))

 '(git-commit-comment-file
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(git-commit-comment-heading
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(git-commit-keyword
   ((t
     (
      :slant  italic
      :foreground  "#8be9fd"
      )
     )))

 '(git-commit-known-pseudo-header
   ((t
     (
      :weight  bold
      :slant  italic
      :foreground  "#8995ba"
      )
     )))

 '(git-commit-nonempty-second-line
   ((t
     (
      :inherit  git-commit-overlong-summary
      )
     )))

 '(git-commit-overlong-summary
   ((t
     (
      :weight  bold
      :slant  italic
      :background  "#1E2029"
      :inherit  error
      )
     )))

 '(git-commit-pseudo-header
   ((t
     (
      :slant  italic
      :foreground  "#8995ba"
      )
     )))

 '(git-commit-summary
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(git-gutter+-added
   ((t
     (
      :foreground  "#edc809"
      :background  "#132738"
      :inherit  fringe
      )
     )))

 '(git-gutter+-commit-header-face
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(git-gutter+-deleted
   ((t
     (
      :foreground  "#FF6E67"
      :background  "#132738"
      :inherit  fringe
      )
     )))

 '(git-gutter+-modified
   ((t
     (
      :foreground  "#6666ff"
      :background  "#132738"
      :inherit  fringe
      )
     )))

 '(git-gutter+-separator
   ((t
     (
      :weight  bold
      :foreground  "cyan"
      )
     )))

 '(git-gutter+-unchanged
   ((t
     (
      :background  "yellow"
      )
     )))

 '(git-gutter:added
   ((t
     (
      :foreground  "#50fa7b"
      :inherit  fringe
      )
     )))

 '(git-gutter:deleted
   ((t
     (
      :foreground  "#ff5555"
      :inherit  fringe
      )
     )))

 '(git-gutter:modified
   ((t
     (
      :foreground  "#ffb86c"
      :inherit  fringe
      )
     )))

 '(git-gutter:separator
   ((t
     (
      :weight  bold
      :foreground  "cyan"
      :inherit  default
      )
     )))

 '(git-gutter:unchanged
   ((t
     (
      :background  "yellow"
      :inherit  default
      )
     )))

 '(git-timemachine-commit
   ((t
     (
      :weight  bold
      )
     )))

 '(git-timemachine-minibuffer-author-face
   ((t
     (
      :foreground  "orange"
      )
     )))

 '(git-timemachine-minibuffer-detail-face
   ((t
     (
      :foreground  "yellow"
      )
     )))

 '(glyphless-char
   ((t
     (
      :height  0.6
      )
     )))

 '(gnus-group-mail-1
   ((t
     (
      :weight  bold
      :foreground  "#f8f8f2"
      )
     )))

 '(gnus-group-mail-1-empty
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(gnus-group-mail-2
   ((t
     (
      :inherit  gnus-group-mail-1
      )
     )))

 '(gnus-group-mail-2-empty
   ((t
     (
      :inherit  gnus-group-mail-1-empty
      )
     )))

 '(gnus-group-mail-3
   ((t
     (
      :inherit  gnus-group-mail-1
      )
     )))

 '(gnus-group-mail-3-empty
   ((t
     (
      :inherit  gnus-group-mail-1-empty
      )
     )))

 '(gnus-group-mail-low
   ((t
     (
      :weight  normal
      :inherit  gnus-group-mail-1
      )
     )))

 '(gnus-group-mail-low-empty
   ((t
     (
      :inherit  gnus-group-mail-1-empty
      )
     )))

 '(gnus-group-news-1
   ((t
     (
      :inherit  gnus-group-mail-1
      )
     )))

 '(gnus-group-news-1-empty
   ((t
     (
      :inherit  gnus-group-mail-1-empty
      )
     )))

 '(gnus-group-news-2
   ((t
     (
      :inherit  gnus-group-news-1
      )
     )))

 '(gnus-group-news-2-empty
   ((t
     (
      :inherit  gnus-group-news-1-empty
      )
     )))

 '(gnus-group-news-3
   ((t
     (
      :inherit  gnus-group-news-1
      )
     )))

 '(gnus-group-news-3-empty
   ((t
     (
      :inherit  gnus-group-news-1-empty
      )
     )))

 '(gnus-group-news-4
   ((t
     (
      :inherit  gnus-group-news-1
      )
     )))

 '(gnus-group-news-4-empty
   ((t
     (
      :inherit  gnus-group-news-1-empty
      )
     )))

 '(gnus-group-news-5
   ((t
     (
      :inherit  gnus-group-news-1
      )
     )))

 '(gnus-group-news-5-empty
   ((t
     (
      :inherit  gnus-group-news-1-empty
      )
     )))

 '(gnus-group-news-6
   ((t
     (
      :inherit  gnus-group-news-1
      )
     )))

 '(gnus-group-news-6-empty
   ((t
     (
      :inherit  gnus-group-news-1-empty
      )
     )))

 '(gnus-group-news-low
   ((t
     (
      :foreground  "#6272a4"
      :inherit  gnus-group-mail-1
      )
     )))

 '(gnus-group-news-low-empty
   ((t
     (
      :weight  normal
      :inherit  gnus-group-news-low
      )
     )))

 '(gnus-splash
   ((t
     (
      :foreground  "#cccccc"
      )
     )))

 '(gnus-summary-cancelled
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(gnus-summary-high-ancient
   ((t
     (
      :foreground  "#818eb6"
      :inherit  italic
      )
     )))

 '(gnus-summary-high-read
   ((t
     (
      :foreground  "#f9f9f4"
      )
     )))

 '(gnus-summary-high-ticked
   ((t
     (
      :foreground  "#ff93d1"
      )
     )))

 '(gnus-summary-high-undownloaded
   ((t
     (
      :weight  bold
      :inherit  gnus-summary-normal-undownloaded
      )
     )))

 '(gnus-summary-high-unread
   ((t
     (
      :foreground  "#72fb95"
      )
     )))

 '(gnus-summary-low-ancient
   ((t
     (
      :foreground  "#4e5b83"
      :inherit  italic
      )
     )))

 '(gnus-summary-low-read
   ((t
     (
      :foreground  "#c6c6c1"
      )
     )))

 '(gnus-summary-low-ticked
   ((t
     (
      :foreground  "#cc609e"
      )
     )))

 '(gnus-summary-low-undownloaded
   ((t
     (
      :slant  italic
      :inherit  gnus-summary-normal-undownloaded
      )
     )))

 '(gnus-summary-low-unread
   ((t
     (
      :foreground  "#40c862"
      )
     )))

 '(gnus-summary-normal-ancient
   ((t
     (
      :foreground  "#6272a4"
      :inherit  italic
      )
     )))

 '(gnus-summary-normal-read
   ((t
     (
      :foreground  "#f8f8f2"
      )
     )))

 '(gnus-summary-normal-ticked
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(gnus-summary-normal-undownloaded
   ((t
     (
      :weight  normal
      :foreground  "cyan4"
      )
     )))

 '(gnus-summary-normal-unread
   ((t
     (
      :foreground  "#50fa7b"
      :inherit  bold
      )
     )))

 '(gnus-summary-selected
   ((t
     (
      :weight  bold
      :foreground  "#61bfff"
      )
     )))

 '(header-line
   ((t
     (
      :background  "#2A265A"
      :inherit  mode-line
      )
     )))

 '(header-line-highlight
   ((t
     (
      :inherit  mode-line-highlight
      )
     )))

 '(helm-action
   ((t
     (
      :underline  t
      )
     )))

 '(helm-buffer-archive
   ((t
     (
      :foreground  "Gold"
      )
     )))

 '(helm-buffer-directory
   ((t
     (
      :foreground  "DarkRed"
      :background  "LightGray"
      )
     )))

 '(helm-buffer-file
   ((t
     (
      :inherit  font-lock-builtin-face
      )
     )))

 '(helm-buffer-modified
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(helm-buffer-not-saved
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(helm-buffer-process
   ((t
     (
      :foreground  "#ffb86c"
      )
     )))

 '(helm-buffer-saved-out
   ((t
     (
      :foreground  "#f8f8f2"
      )
     )))

 '(helm-buffer-size
   ((t
     (
      :foreground  "#f8f8f2"
      )
     )))

 '(helm-candidate-number
   ((t
     (
      :foreground  "#282a36"
      :background  "#f8f8f2"
      )
     )))

 '(helm-candidate-number-suspended
   ((t
     (
      :inherit  helm-candidate-number
      )
     )))

 '(helm-delete-async-message
   ((t
     (
      :foreground  "yellow"
      )
     )))

 '(helm-eob-line
   ((t
     (
      :inherit  default
      )
     )))

 '(helm-etags-file
   ((t
     (
      :underline  t
      :foreground  "Lightgoldenrod4"
      )
     )))

 '(helm-ff-backup-file
   ((t
     (
      :foreground  "DimGray"
      )
     )))

 '(helm-ff-denied
   ((t
     (
      :foreground  "red"
      :background  "black"
      )
     )))

 '(helm-ff-directory
   ((t
     (
      :weight  bold
      :foreground  "#50fa7b"
      )
     )))

 '(helm-ff-dirs
   ((t
     (
      :inherit  font-lock-function-name-face
      )
     )))

 '(helm-ff-dotted-directory
   ((t
     (
      :foreground  "#565761"
      )
     )))

 '(helm-ff-dotted-symlink-directory
   ((t
     (
      :foreground  "DarkOrange"
      :background  "DimGray"
      )
     )))

 '(helm-ff-executable
   ((t
     (
      :foreground  "#0189cc"
      :inherit  italic
      )
     )))

 '(helm-ff-file
   ((t
     (
      :foreground  "#f8f8f2"
      )
     )))

 '(helm-ff-file-extension
   ((t
     (
      :foreground  "magenta"
      )
     )))

 '(helm-ff-invalid-symlink
   ((t
     (
      :weight  bold
      :foreground  "#ff79c6"
      )
     )))

 '(helm-ff-pipe
   ((t
     (
      :foreground  "yellow"
      :background  "black"
      )
     )))

 '(helm-ff-prefix
   ((t
     (
      :foreground  "#282a36"
      :background  "#ff79c6"
      )
     )))

 '(helm-ff-rsync-progress
   ((t
     (
      :inherit  font-lock-warning-face
      )
     )))

 '(helm-ff-socket
   ((t
     (
      :foreground  "DeepPink"
      )
     )))

 '(helm-ff-suid
   ((t
     (
      :foreground  "white"
      :background  "red"
      )
     )))

 '(helm-ff-symlink
   ((t
     (
      :weight  bold
      :foreground  "#ff79c6"
      )
     )))

 '(helm-ff-truename
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(helm-grep-cmd-line
   ((t
     (
      :inherit  font-lock-type-face
      )
     )))

 '(helm-grep-file
   ((t
     (
      :foreground  "#0189cc"
      )
     )))

 '(helm-grep-finish
   ((t
     (
      :foreground  "#373844"
      )
     )))

 '(helm-grep-lineno
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(helm-grep-match
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(helm-header
   ((t
     (
      :foreground  "#373844"
      )
     )))

 '(helm-header-line-left-margin
   ((t
     (
      :foreground  "black"
      :background  "yellow"
      )
     )))

 '(helm-helper
   ((t
     (
      :inherit  helm-header
      )
     )))

 '(helm-history-deleted
   ((t
     (
      :inherit  helm-ff-invalid-symlink
      )
     )))

 '(helm-history-remote
   ((t
     (
      :foreground  "Indianred1"
      )
     )))

 '(helm-locate-finish
   ((t
     (
      :foreground  "Green"
      )
     )))

 '(helm-mark-prefix
   ((t
     (
      :inherit  default
      )
     )))

 '(helm-match
   ((t
     (
      :foreground  "#bd93f9"
      :inherit  bold
      )
     )))

 '(helm-match-item
   ((t
     (
      :inherit  isearch
      )
     )))

 '(helm-minibuffer-prompt
   ((t
     (
      :inherit  minibuffer-prompt
      )
     )))

 '(helm-moccur-buffer
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(helm-no-file-buffer-modified
   ((t
     (
      :foreground  "orange"
      :background  "black"
      )
     )))

 '(helm-non-file-buffer
   ((t
     (
      :inherit  italic
      )
     )))

 '(helm-org-rifle-separator
   ((t
     (
      :background  "black"
      )
     )))

 '(helm-prefarg
   ((t
     (
      :foreground  "green"
      )
     )))

 '(helm-resume-need-update
   ((t
     (
      :background  "red"
      )
     )))

 '(helm-selection
   ((t
     (
      :background  "#0189cc"
      :inherit  bold
      )
     )))

 '(helm-selection-line
   ((t
     (
      :inherit  highlight
      )
     )))

 '(helm-separator
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(helm-source-header
   ((t
     (
      :weight  bold
      :foreground  "#ff79c6"
      :background  "#373844"
      )
     )))

 '(helm-visible-mark
   ((t
     (
      :foreground  "#282a36"
      :background  "#44475a"
      )
     )))

 '(help-argument-name
   ((t
     (
      :inherit  italic
      )
     )))

 '(help-for-help-header
   ((t
     (
      :height  1.26
      )
     )))

 '(help-key-binding
   ((t
     (
      :foreground  "LightBlue"
      :background  "grey19"
      :inherit  fixed-pitch
      )
     )))

 '(helpful-heading
   ((t
     (
      :height  1.2
      :weight  bold
      )
     )))

 '(highlight
   ((t
     (
      :foreground  "#1E2029"
      :background  "#bd93f9"
      )
     )))

 '(highlight-parentheses-highlight
   ((t
     (
      )
     )))

 '(highlight-indent-guides-character-face
   ((t
     (
      :foreground  "#565761"
      )
     )))

 '(highlight-indent-guides-stack-character-face
   ((t
     (
      :foreground  "#565761"
      )
     )))

 '(highlight-indent-guides-top-character-face
   ((t
     (
      :foreground  "#565761"
      )
     )))

 '(hl-DONE-TODO
   ((t
     (
      :weight  ultra-bold
      :foreground  "orange"
      )
     )))

 '(hl-line
   ((t
     (
      :background  "#212026"
      )
     )))

 '(hl-note-TODO
   ((t
     (
      :foreground  "#FFFFFF"
      :background  "#6666ff"
      :inherit hl-todo
      )
     )))

 '(hl-todo
   ((t
     (
      :weight  bold
      :foreground  "#ff5555"
      )
     )))

 '(hl-todo-TODO
   ((t
     (
      :foreground  "#00ff00"
      :background  "#ff0000"
      :inherit hl-todo
      )
     )))

 '(holiday
   ((t
     (
      :background  "chocolate4"
      )
     )))

 '(homoglyph
   ((t
     (
      :foreground  "cyan"
      )
     )))

 '(hydra-face-amaranth
   ((t
     (
      :weight  bold
      :foreground  "#ff79c6"
      )
     )))

 '(hydra-face-blue
   ((t
     (
      :weight  bold
      :foreground  "#61bfff"
      )
     )))

 '(hydra-face-pink
   ((t
     (
      :weight  bold
      :foreground  "#bd93f9"
      )
     )))

 '(hydra-face-red
   ((t
     (
      :weight  bold
      :foreground  "#ff5555"
      )
     )))

 '(hydra-face-teal
   ((t
     (
      :weight  bold
      :foreground  "#0189cc"
      )
     )))

 '(ibuffer-locked-buffer
   ((t
     (
      :foreground  "RosyBrown"
      )
     )))

 '(ido-first-match
   ((t
     (
      :foreground  "#ffb86c"
      )
     )))

 '(ido-incomplete-regexp
   ((t
     (
      :inherit  font-lock-warning-face
      )
     )))

 '(ido-indicator
   ((t
     (
      :foreground  "#ff5555"
      :background  "#282a36"
      )
     )))

 '(ido-only-match
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(ido-subdir
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(ido-virtual
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(iedit-occurrence
   ((t
     (
      :weight  bold
      :foreground  "#ff79c6"
      :inverse-video t
      )
     )))

 '(iedit-read-only-occurrence
   ((t
     (
      :inherit  region
      )
     )))

 '(info-header-node
   ((t
     (
      :inherit  info-node
      )
     )))

 '(info-header-xref
   ((t
     (
      :inherit  info-xref
      )
     )))

 '(info-index-match
   ((t
     (
      :inherit  match
      )
     )))

 '(info-menu-header
   ((t
     (
      :weight  bold
      :inherit  variable-pitch
      )
     )))

 '(info-menu-star
   ((t
     (
      :foreground  "red1"
      )
     )))

 '(info-node
   ((t
     (
      :weight  bold
      :slant  italic
      :foreground  "white"
      )
     )))

 '(info-title-1
   ((t
     (
      :height  1.2
      :inherit  info-title-2
      )
     )))

 '(info-title-2
   ((t
     (
      :height  1.2
      :inherit  info-title-3
      )
     )))

 '(info-title-3
   ((t
     (
      :height  1.2
      :inherit  info-title-4
      )
     )))

 '(info-title-4
   ((t
     (
      :weight  bold
      :inherit  variable-pitch
      )
     )))

 '(info-xref
   ((t
     (
      :inherit  link
      )
     )))

 '(info-xref-visited
   ((t
     (
      :inherit link-visited info-xref
      )
     )))

 '(isearch
   ((t
     (
      :weight  bold
      :inherit  lazy-highlight
      )
     )))

 '(isearch-fail
   ((t
     (
      :weight  bold
      :foreground  "#1E2029"
      :background  "#ff5555"
      )
     )))

 '(isearch-group-1
   ((t
     (
      :foreground  "brown4"
      :background  "palevioletred1"
      )
     )))

 '(isearch-group-2
   ((t
     (
      :foreground  "brown4"
      :background  "palevioletred3"
      )
     )))

 '(italic
   ((t
     (
      :slant  italic
      )
     )))

 '(ivy-action
   ((t
     (
      :inherit  font-lock-builtin-face
      )
     )))

 '(ivy-completions-annotations
   ((t
     (
      :inherit  completions-annotations
      )
     )))

 '(ivy-confirm-face
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(ivy-current-match
   ((t
     (
      :background  "#44475a"
      )
     )))

 '(ivy-cursor
   ((t
     (
      :foreground  "black"
      :background  "white"
      )
     )))

 '(ivy-grep-info
   ((t
     (
      :inherit  compilation-info
      )
     )))

 '(ivy-grep-line-number
   ((t
     (
      :inherit  compilation-line-number
      )
     )))

 '(ivy-highlight-face
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(ivy-match-required-face
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(ivy-minibuffer-match-face-1
   ((t
     (
      :weight  light
      :foreground  "#6d6e77"
      :underline t
      )
     )))

 '(ivy-minibuffer-match-face-2
   ((t
     (
      :weight  semi-bold
      :foreground  "#ff79c6"
      :background  "#282a36"
      :inherit  ivy-minibuffer-match-face-1
      :underline t
      )
     )))

 '(ivy-minibuffer-match-face-3
   ((t
     (
      :weight  semi-bold
      :foreground  "#50fa7b"
      :inherit  ivy-minibuffer-match-face-2
      :underline t
      )
     )))

 '(ivy-minibuffer-match-face-4
   ((t
     (
      :weight  semi-bold
      :foreground  "#f1fa8c"
      :inherit  ivy-minibuffer-match-face-2
      :underline t
      )
     )))

 '(ivy-minibuffer-match-highlight
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(ivy-modified-buffer
   ((t
     (
      :foreground  "#f1fa8c"
      :inherit  bold
      )
     )))

 '(ivy-modified-outside-buffer
   ((t
     (
      :inherit  default
      )
     )))

 '(ivy-org
   ((t
     (
      :inherit  default
      )
     )))

 '(ivy-prompt-match
   ((t
     (
      :inherit  ivy-current-match
      )
     )))

 '(ivy-remote
   ((t
     (
      :foreground  "#7B6BFF"
      )
     )))

 '(ivy-separator
   ((t
     (
      :inherit  font-lock-doc-face
      )
     )))

 '(ivy-subdir
   ((t
     (
      :inherit  dired-directory
      )
     )))

 '(ivy-virtual
   ((t
     (
      :foreground  "#8995ba"
      :inherit  italic
      )
     )))

 '(ivy-yanked-word
   ((t
     (
      :inherit  highlight
      )
     )))

 '(jupyter-eval-overlay
   ((t
     (
      :weight  bold
      :foreground  "dodger blue"
      )
     )))

 '(jupyter-repl-input-prompt
   ((t
     (
      :foreground  "darkolivegreen"
      )
     )))

 '(jupyter-repl-output-prompt
   ((t
     (
      :foreground  "darkred"
      )
     )))

 '(jupyter-repl-traceback
   ((t
     (
      :background  "firebrick"
      )
     )))

 '(langtool-correction-face
   ((t
     (
      :weight  bold
      :foreground  "yellow"
      :background  "red1"
      )
     )))

 '(langtool-errline
   ((t
     (
      :background  "Firebrick4"
      )
     )))

 '(lazy-highlight
   ((t
     (
      :weight  bold
      :foreground  "#f8f8f2"
      :background  "#8466ae"
      )
     )))

 '(line-number
   ((t
     (
      :weight  normal
      :slant  normal
      :foreground  "#6272a4"
      :inherit  default
      )
     )))

 '(line-number-current-line
   ((t
     (
      :weight  normal
      :slant  normal
      :foreground  "#f8f8f2"
      :inherit hl-line default
      )
     )))

 '(line-number-major-tick
   ((t
     (
      :weight  bold
      :background  "grey75"
      )
     )))

 '(line-number-minor-tick
   ((t
     (
      :weight  bold
      :background  "grey55"
      )
     )))

 '(link
   ((t
     (
      :weight  bold
      :underline  t
      :foreground  "#bd93f9"
      )
     )))

 '(link-visited
   ((t
     (
      :foreground  "violet"
      :inherit  link
      )
     )))

 '(log-edit-header
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(log-edit-summary
   ((t
     (
      :inherit  font-lock-function-name-face
      )
     )))

 '(log-edit-unknown-header
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(log-view-commit-body
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(log-view-file
   ((t
     (
      :weight  bold
      )
     )))

 '(log-view-message
   ((t
     (
      :weight  bold
      )
     )))

 '(lsp-details-face
   ((t
     (
      :height  0.8
      :inherit  shadow
      )
     )))

 '(lsp-face-highlight-read
   ((t
     (
      :inherit  lsp-face-highlight-textual
      )
     )))

 '(lsp-face-highlight-textual
   ((t
     (
      :weight  bold
      :foreground  "#f8f8f2"
      :background  "#544970"
      )
     )))

 '(lsp-face-highlight-write
   ((t
     (
      :inherit  lsp-face-highlight-textual
      )
     )))

 '(lsp-face-rename
   ((t
     (
      :underline  t
      )
     )))

 '(lsp-installation-buffer-face
   ((t
     (
      :foreground  "green"
      )
     )))

 '(lsp-installation-finished-buffer-face
   ((t
     (
      :foreground  "orange"
      )
     )))

 '(lsp-rename-placeholder-face
   ((t
     (
      :inherit  font-lock-variable-name-face
      )
     )))

 '(lsp-signature-face
   ((t
     (
      :inherit  lsp-details-face
      )
     )))

 '(lsp-signature-posframe
   ((t
     (
      :inherit  tooltip
      )
     )))

 '(lsp-ui-doc-background
   ((t
     (
      :inherit  tooltip
      )
     )))

 '(lsp-ui-doc-header
   ((t
     (
      :foreground  "black"
      :background  "deep sky blue"
      )
     )))

 '(lsp-ui-doc-highlight-hover
   ((t
     (
      :inherit  region
      )
     )))

 '(lsp-ui-doc-url
   ((t
     (
      :inherit  link
      )
     )))

 '(lsp-ui-peek-filename
   ((t
     (
      :inherit  mode-line-buffer-id
      )
     )))

 '(lsp-ui-peek-footer
   ((t
     (
      :inherit  lsp-ui-peek-header
      )
     )))

 '(lsp-ui-peek-header
   ((t
     (
      :weight  bold
      :foreground  "#f8f8f2"
      :background  "#3d3f4a"
      )
     )))

 '(lsp-ui-peek-highlight
   ((t
     (
      :foreground  "#282a36"
      :background  "#44475a"
      :inherit  lsp-ui-peek-header
      )
     )))

 '(lsp-ui-peek-line-number
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(lsp-ui-peek-list
   ((t
     (
      :background  "#242530"
      )
     )))

 '(lsp-ui-peek-peek
   ((t
     (
      :background  "#242530"
      )
     )))

 '(lsp-ui-peek-selection
   ((t
     (
      :weight  bold
      :foreground  "#282a36"
      :background  "#61bfff"
      )
     )))

 '(lsp-ui-sideline-code-action
   ((t
     (
      :foreground  "#a683db"
      )
     )))

 '(lsp-ui-sideline-current-symbol
   ((t
     (
      :inherit  highlight
      )
     )))

 '(lsp-ui-sideline-global
   ((t
     (
      )
     )))

 '(lsp-ui-sideline-symbol
   ((t
     (
      :height  0.99
      :foreground  "grey"
      )
     )))

 '(lsp-ui-sideline-symbol-info
   ((t
     (
      :foreground  "#596793"
      :background  "#1E2029"
      )
     )))

 '(lv-separator
   ((t
     (
      :background  "grey30"
      )
     )))

 '(magit-bisect-bad
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(magit-bisect-good
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(magit-bisect-skip
   ((t
     (
      :foreground  "#ffb86c"
      )
     )))

 '(magit-blame-date
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(magit-blame-dimmed
   ((t
     (
      :weight  normal
      :slant  normal
      :inherit  magit-dimmed
      )
     )))

 '(magit-blame-hash
   ((t
     (
      :foreground  "#8be9fd"
      )
     )))

 '(magit-blame-heading
   ((t
     (
      :foreground  "#ffb86c"
      :background  "#44475a"
      )
     )))

 '(magit-blame-highlight
   ((t
     (
      :foreground  "white"
      :background  "grey25"
      )
     )))

 '(magit-blame-margin
   ((t
     (
      :weight  normal
      :slant  normal
      :inherit  magit-blame-highlight
      )
     )))

 '(magit-branch-current
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(magit-branch-local
   ((t
     (
      :foreground  "#8be9fd"
      )
     )))

 '(magit-branch-remote
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(magit-branch-remote-head
   ((t
     (
      :inherit  magit-branch-remote
      )
     )))

 '(magit-branch-upstream
   ((t
     (
      :slant  italic
      )
     )))

 '(magit-branch-warning
   ((t
     (
      :inherit  warning
      )
     )))

 '(magit-cherry-equivalent
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(magit-cherry-unmatched
   ((t
     (
      :foreground  "#8be9fd"
      )
     )))

 '(magit-diff-added
   ((t
     (
      :foreground  "#40c862"
      :background  "#2c3e3c"
      )
     )))

 '(magit-diff-added-highlight
   ((t
     (
      :weight  bold
      :foreground  "#50fa7b"
      :background  "#305343"
      )
     )))

 '(magit-diff-base
   ((t
     (
      :foreground  "#cc9356"
      :background  "#3d383b"
      )
     )))

 '(magit-diff-base-highlight
   ((t
     (
      :weight  bold
      :foreground  "#ffb86c"
      :background  "#534640"
      )
     )))

 '(magit-diff-conflict-heading
   ((t
     (
      :inherit  magit-diff-hunk-heading
      )
     )))

 '(magit-diff-context
   ((t
     (
      :foreground  "#949491"
      :background  "#282a36"
      )
     )))

 '(magit-diff-context-highlight
   ((t
     (
      :foreground  "#f8f8f2"
      :background  "#1E2029"
      )
     )))

 '(magit-diff-file-heading
   ((t
     (
      :weight  bold
      :foreground  "#f8f8f2"
      )
     )))

 '(magit-diff-file-heading-highlight
   ((t
     (
      :inherit  magit-section-highlight
      )
     )))

 '(magit-diff-file-heading-selection
   ((t
     (
      :weight  bold
      :foreground  "#ff79c6"
      :background  "#0189cc"
      )
     )))

 '(magit-diff-hunk-heading
   ((t
     (
      :foreground  "#282a36"
      :background  "#544970"
      )
     )))

 '(magit-diff-hunk-heading-highlight
   ((t
     (
      :weight  bold
      :foreground  "#282a36"
      :background  "#bd93f9"
      )
     )))

 '(magit-diff-hunk-heading-selection
   ((t
     (
      :foreground  "LightSalmon3"
      :inherit  magit-diff-hunk-heading-highlight
      )
     )))

 '(magit-diff-hunk-region
   ((t
     (
      :inherit  bold
      )
     )))

 '(magit-diff-lines-boundary
   ((t
     (
      :inherit  magit-diff-lines-heading
      )
     )))

 '(magit-diff-lines-heading
   ((t
     (
      :foreground  "#f1fa8c"
      :background  "#ff5555"
      )
     )))

 '(magit-diff-our
   ((t
     (
      :inherit  magit-diff-removed
      )
     )))

 '(magit-diff-our-highlight
   ((t
     (
      :inherit  magit-diff-removed-highlight
      )
     )))

 '(magit-diff-removed
   ((t
     (
      :foreground  "#cc4444"
      :background  "#564859"
      )
     )))

 '(magit-diff-removed-highlight
   ((t
     (
      :weight  bold
      :foreground  "#ff5555"
      :background  "#694959"
      )
     )))

 '(magit-diff-revision-summary
   ((t
     (
      :inherit  magit-diff-hunk-heading
      )
     )))

 '(magit-diff-revision-summary-highlight
   ((t
     (
      :inherit  magit-diff-hunk-heading-highlight
      )
     )))

 '(magit-diff-their
   ((t
     (
      :inherit  magit-diff-added
      )
     )))

 '(magit-diff-their-highlight
   ((t
     (
      :inherit  magit-diff-added-highlight
      )
     )))

 '(magit-diff-whitespace-warning
   ((t
     (
      :inherit  trailing-whitespace
      )
     )))

 '(magit-diffstat-added
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(magit-diffstat-removed
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(magit-dimmed
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(magit-filename
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(magit-hash
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(magit-head
   ((t
     (
      :inherit  magit-branch-local
      )
     )))

 '(magit-header-line
   ((t
     (
      :weight  bold
      :foreground  "#f8f8f2"
      :background  "#0189cc"
      )
     )))

 '(magit-header-line-key
   ((t
     (
      :inherit  font-lock-builtin-face
      )
     )))

 '(magit-header-line-log-select
   ((t
     (
      :inherit  bold
      )
     )))

 '(magit-keyword
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(magit-keyword-squash
   ((t
     (
      :inherit  font-lock-warning-face
      )
     )))

 '(magit-log-author
   ((t
     (
      :foreground  "#ffb86c"
      )
     )))

 '(magit-log-date
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(magit-log-graph
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(magit-mode-line-process
   ((t
     (
      :inherit  mode-line-emphasis
      )
     )))

 '(magit-mode-line-process-error
   ((t
     (
      :inherit  error
      )
     )))

 '(magit-process-ng
   ((t
     (
      :inherit  error
      )
     )))

 '(magit-process-ok
   ((t
     (
      :inherit  success
      )
     )))

 '(magit-reflog-amend
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(magit-reflog-checkout
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(magit-reflog-cherry-pick
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(magit-reflog-commit
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(magit-reflog-merge
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(magit-reflog-other
   ((t
     (
      :foreground  "#8be9fd"
      )
     )))

 '(magit-reflog-rebase
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(magit-reflog-remote
   ((t
     (
      :foreground  "#8be9fd"
      )
     )))

 '(magit-reflog-reset
   ((t
     (
      :inherit  error
      )
     )))

 '(magit-refname
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(magit-refname-pullreq
   ((t
     (
      :inherit  magit-refname
      )
     )))

 '(magit-refname-stash
   ((t
     (
      :inherit  magit-refname
      )
     )))

 '(magit-refname-wip
   ((t
     (
      :inherit  magit-refname
      )
     )))

 '(magit-section-heading
   ((t
     (
      :weight  bold
      :foreground  "#61bfff"
      )
     )))

 '(magit-section-heading-selection
   ((t
     (
      :weight  bold
      :foreground  "#ffb86c"
      )
     )))

 '(magit-section-highlight
   ((t
     (
      :inherit  hl-line
      )
     )))

 '(magit-section-secondary-heading
   ((t
     (
      :weight  bold
      :foreground  "#bd93f9"
      )
     )))

 '(magit-sequence-done
   ((t
     (
      :inherit  magit-hash
      )
     )))

 '(magit-sequence-drop
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(magit-sequence-exec
   ((t
     (
      :inherit  magit-hash
      )
     )))

 '(magit-sequence-head
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(magit-sequence-onto
   ((t
     (
      :inherit  magit-sequence-done
      )
     )))

 '(magit-sequence-part
   ((t
     (
      :foreground  "#ffb86c"
      )
     )))

 '(magit-sequence-pick
   ((t
     (
      :inherit  default
      )
     )))

 '(magit-sequence-stop
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(magit-signature-bad
   ((t
     (
      :inherit  error
      )
     )))

 '(magit-signature-error
   ((t
     (
      :inherit  error
      )
     )))

 '(magit-signature-expired
   ((t
     (
      :foreground  "#ffb86c"
      )
     )))

 '(magit-signature-expired-key
   ((t
     (
      :inherit  magit-signature-expired
      )
     )))

 '(magit-signature-good
   ((t
     (
      :inherit  success
      )
     )))

 '(magit-signature-revoked
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(magit-signature-untrusted
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(magit-tag
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(marginalia-archive
   ((t
     (
      :inherit  warning
      )
     )))

 '(marginalia-char
   ((t
     (
      :inherit  marginalia-key
      )
     )))

 '(marginalia-date
   ((t
     (
      :inherit  marginalia-key
      )
     )))

 '(marginalia-documentation
   ((t
     (
      :inherit  font-lock-doc-face
      )
     )))

 '(marginalia-file-name
   ((t
     (
      :inherit  marginalia-documentation
      )
     )))

 '(marginalia-file-owner
   ((t
     (
      :inherit  font-lock-preprocessor-face
      )
     )))

 '(marginalia-file-priv-dir
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(marginalia-file-priv-exec
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(marginalia-file-priv-link
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(marginalia-file-priv-no
   ((t
     (
      :inherit  shadow
      )
     )))

 '(marginalia-file-priv-other
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(marginalia-file-priv-rare
   ((t
     (
      :foreground  "#f8f8f2"
      )
     )))

 '(marginalia-file-priv-read
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(marginalia-file-priv-write
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(marginalia-function
   ((t
     (
      :inherit  font-lock-function-name-face
      )
     )))

 '(marginalia-installed
   ((t
     (
      :inherit  success
      )
     )))

 '(marginalia-key
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(marginalia-lighter
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(marginalia-list
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(marginalia-mode
   ((t
     (
      :inherit  marginalia-key
      )
     )))

 '(marginalia-modified
   ((t
     (
      :inherit  font-lock-negation-char-face
      )
     )))

 '(marginalia-null
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(marginalia-number
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(marginalia-off
   ((t
     (
      :inherit  error
      )
     )))

 '(marginalia-on
   ((t
     (
      :inherit  success
      )
     )))

 '(marginalia-size
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(marginalia-string
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(marginalia-symbol
   ((t
     (
      :inherit  font-lock-type-face
      )
     )))

 '(marginalia-true
   ((t
     (
      :inherit  font-lock-builtin-face
      )
     )))

 '(marginalia-type
   ((t
     (
      :inherit  marginalia-key
      )
     )))

 '(marginalia-value
   ((t
     (
      :inherit  marginalia-key
      )
     )))

 '(marginalia-version
   ((t
     (
      :inherit  marginalia-number
      )
     )))

 '(markdown-blockquote-face
   ((t
     (
      :foreground  "#8995ba"
      :inherit  italic
      )
     )))

 '(markdown-bold-face
   ((t
     (
      :foreground  "#ffb86c"
      :inherit  bold
      )
     )))

 '(markdown-code-face
   ((t
     (
      :background  "#252631"
      )
     )))

 '(markdown-comment-face
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(markdown-footnote-marker-face
   ((t
     (
      :inherit  markdown-markup-face
      )
     )))

 '(markdown-footnote-text-face
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(markdown-gfm-checkbox-face
   ((t
     (
      :inherit  font-lock-builtin-face
      )
     )))

 '(markdown-header-delimiter-face
   ((t
     (
      :weight  bold
      :foreground  "#F1FA8C"
      :inherit  markdown-header-face
      )
     )))

 '(markdown-header-face
   ((t
     (
      :weight  bold
      :foreground  "#bc6ec5"
      :inherit  bold
      )
     )))

 '(markdown-header-face-1
   ((t
     (
      :height  1.0
      :inherit  markdown-header-face
      )
     )))

 '(markdown-header-face-2
   ((t
     (
      :height  1.0
      :inherit  markdown-header-face
      )
     )))

 '(markdown-header-face-3
   ((t
     (
      :height  1.0
      :inherit  markdown-header-face
      )
     )))

 '(markdown-header-face-4
   ((t
     (
      :height  1.0
      :inherit  markdown-header-face
      )
     )))

 '(markdown-header-face-5
   ((t
     (
      :height  1.0
      :inherit  markdown-header-face
      )
     )))

 '(markdown-header-face-6
   ((t
     (
      :height  1.0
      :inherit  markdown-header-face
      )
     )))

 '(markdown-header-rule-face
   ((t
     (
      :inherit  markdown-markup-face
      )
     )))

 '(markdown-highlight-face
   ((t
     (
      :inherit  highlight
      )
     )))

 '(markdown-highlighting-face
   ((t
     (
      :foreground  "black"
      :background  "yellow"
      )
     )))

 '(markdown-hr-face
   ((t
     (
      :inherit  markdown-markup-face
      )
     )))

 '(markdown-html-attr-name-face
   ((t
     (
      :inherit  font-lock-variable-name-face
      )
     )))

 '(markdown-html-attr-value-face
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(markdown-html-entity-face
   ((t
     (
      :inherit  font-lock-variable-name-face
      )
     )))

 '(markdown-html-tag-delimiter-face
   ((t
     (
      :inherit  markdown-markup-face
      )
     )))

 '(markdown-html-tag-name-face
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(markdown-inline-code-face
   ((t
     (
      :inherit markdown-code-face markdown-pre-face
      )
     )))

 '(markdown-italic-face
   ((t
     (
      :foreground  "#c6c6c6"
      :inherit  italic
      )
     )))

 '(markdown-language-info-face
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(markdown-language-keyword-face
   ((t
     (
      :inherit  font-lock-type-face
      )
     )))

 '(markdown-line-break-face
   ((t
     (
      :underline  t
      :inherit  font-lock-constant-face
      )
     )))

 '(markdown-link-face
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(markdown-link-title-face
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(markdown-list-face
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(markdown-markup-face
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(markdown-math-face
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(markdown-metadata-key-face
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(markdown-metadata-value-face
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(markdown-missing-link-face
   ((t
     (
      :inherit  font-lock-warning-face
      )
     )))

 '(markdown-plain-url-face
   ((t
     (
      :inherit  markdown-link-face
      )
     )))

 '(markdown-pre-face
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(markdown-reference-face
   ((t
     (
      :foreground  "#8995ba"
      )
     )))

 '(markdown-table-face
   ((t
     (
      :inherit markdown-code-face
      )
     )))

 '(markdown-url-face
   ((t
     (
      :weight  normal
      :foreground  "#ff79c6"
      )
     )))

 '(match
   ((t
     (
      :weight  bold
      :foreground  "#50fa7b"
      :background  "#1E2029"
      )
     )))

 '(message-cited-text-1
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(message-cited-text-2
   ((t
     (
      :inherit  gnus-cite-2
      )
     )))

 '(message-cited-text-3
   ((t
     (
      :inherit  gnus-cite-3
      )
     )))

 '(message-cited-text-4
   ((t
     (
      :inherit  gnus-cite-4
      )
     )))

 '(message-header-cc
   ((t
     (
      :foreground  "#a07cd3"
      :inherit  message-header-to
      )
     )))

 '(message-header-name
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(message-header-newsgroups
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(message-header-other
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(message-header-subject
   ((t
     (
      :weight  bold
      :height  1.5
      :foreground  "#bd93f9"
      )
     )))

 '(message-header-to
   ((t
     (
      :weight  bold
      :foreground  "#bd93f9"
      )
     )))

 '(message-header-xheader
   ((t
     (
      :foreground  "#8995ba"
      )
     )))

 '(message-mml
   ((t
     (
      :slant  italic
      :foreground  "#6272a4"
      )
     )))

 '(message-separator
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(message-signature-separator
   ((t
     (
      :weight  bold
      )
     )))

 '(minibuffer-prompt
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(mm-command-output
   ((t
     (
      :foreground  "ForestGreen"
      )
     )))

 '(mode-line-active
   ((t
     (
      :inherit  mode-line
      )
     )))

 '(mode-line-buffer-id
   ((t
     (
      :weight  bold
      )
     )))

 '(mode-line-emphasis
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(mode-line-highlight
   ((t
     (
      :inherit  highlight
      )
     )))

 '(mode-line-inactive
   ((t
     (
      :foreground  "#bc6ec5"
      :background  "#2A265A"
      )
     )))

 '(mouse
   ((t
     (
      )
     )))

 '(mouse-drag-and-drop-region
   ((t
     (
      :inherit  region
      )
     )))

 '(mu4e-header-highlight-face
   ((t
     (
      :inherit  ivy-current-match
      )
     )))

 '(next-error
   ((t
     (
      :inherit  region
      )
     )))

 '(next-error-message
   ((t
     (
      :inherit  highlight
      )
     )))

 '(nobreak-hyphen
   ((t
     (
      :foreground  "cyan"
      )
     )))

 '(nobreak-space
   ((t
     (
      :inherit  default
      )
     )))

 '(org-agenda-calendar-event
   ((t
     (
      :inherit  default
      )
     )))

 '(org-agenda-calendar-sexp
   ((t
     (
      :inherit  default
      )
     )))

 '(org-agenda-clocking
   ((t
     (
      :background  "#33475e"
      )
     )))

 '(org-agenda-column-dateline
   ((t
     (
      :inherit  org-column
      )
     )))

 '(org-agenda-current-time
   ((t
     (
      :inherit  org-time-grid
      )
     )))

 '(org-agenda-date
   ((t
     (
      :weight ultra-bold
      :foreground  "#d7befb"
      )
     )))

 '(org-agenda-date-today
   ((t
     (
      :weight  ultra-bold
      :foreground  "#d7befb"
      :inherit ivy-current-match
      )
     )))

 '(org-agenda-date-weekend
   ((t
     (
      :weight  ultra-bold
      :foreground  "#715895"
      )
     )))

 '(org-agenda-date-weekend-today
   ((t
     (
      :inherit  org-agenda-date-today
      )
     )))

 '(org-agenda-diary
   ((t
     (
      :inherit  default
      )
     )))

 '(org-agenda-dimmed-todo-face
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(org-agenda-done
   ((t
     (
      :foreground  "#565761"
      )
     )))

 '(org-agenda-filter-category
   ((t
     (
      :inherit  mode-line
      )
     )))

 '(org-agenda-filter-effort
   ((t
     (
      :inherit  mode-line
      )
     )))

 '(org-agenda-filter-regexp
   ((t
     (
      :inherit  mode-line
      )
     )))

 '(org-agenda-filter-tags
   ((t
     (
      :inherit  mode-line
      )
     )))

 '(org-agenda-restriction-lock
   ((t
     (
      :background  "#1C1C1C"
      )
     )))

 '(org-agenda-structure
   ((t
     (
      :height 1.3
      :foreground  "#bd93f9"
      :weight bold
      )
     )))

 '(org-agenda-structure-filter
   ((t
     (
      :inherit org-warning org-agenda-structure
      )
     )))

 '(org-agenda-structure-secondary
   ((t
     (
      :inherit  org-agenda-structure
      )
     )))

 '(org-archived
   ((t
     (
      :foreground  "#8995ba"
      )
     )))

 '(org-block
   ((t
     (
      :foreground  "#bd93f9"
      :background  "#23242f"
      )
     )))

 '(org-block-begin-line
   ((t
     (
      :foreground  "#6272a4"
      :background  "#23242f"
      :inherit  org-block
      )
     )))

 '(org-block-end-line
   ((t
     (
      :inherit  org-block-begin-line
      )
     )))

 '(org-checkbox
   ((t
     (
      :inherit  org-todo
      )
     )))

 '(org-checkbox-statistics-done
   ((t
     (
      :inherit  org-done
      )
     )))

 '(org-checkbox-statistics-todo
   ((t
     (
      :inherit  org-todo
      )
     )))

 '(org-cite
   ((t
     (
      :foreground  "#1994cf"
      )
     )))

 '(org-cite-key
   ((t
     (
      :underline  t
      :foreground  "#63b5db"
      )
     )))

 '(org-clock-overlay
   ((t
     (
      :foreground  "white"
      :background  "SkyBlue4"
      )
     )))

 '(org-code
   ((t
     (
      :foreground  "#f1fa8c"
      :inherit  org-block
      )
     )))

 '(org-column
   ((t
     (
      :background  "#282a36"
      )
     )))

 '(org-column-title
   ((t
     (
      :weight  bold
      :underline  t
      :background  "#282a36"
      )
     )))

 '(org-date
   ((t
     (
      :foreground  "#8be9fd"
      )
     )))

 '(org-date-selected
   ((t
     (
      :inherit calendar-today
      )
     )))

 '(org-default
   ((t
     (
      :inherit  variable-pitch
      )
     )))

 '(org-dispatcher-highlight
   ((t
     (
      :weight  bold
      :foreground  "gold1"
      :background  "gray20"
      )
     )))

 '(org-document-info
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(org-document-info-keyword
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(org-document-title
   ((t
     (
      :weight  bold
      :foreground  "#ffb86c"
      :height  1.20
      )
     )))

 '(org-done
   ((t
     (
      :weight  bold
      :foreground  "#50fa7b"
      :background  "#373844"
      )
     )))

 '(org-drawer
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(org-ellipsis
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(org-footnote
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(org-formula
   ((t
     (
      :foreground  "#8be9fd"
      )
     )))

 '(org-headline-done
   ((t
     (
      :foreground  "#565761"
      )
     )))

 '(org-headline-todo
   ((t
     (
      :foreground  "Pink2"
      )
     )))

 '(org-hide
   ((t
     (
      :foreground  "#282a36"
      )
     )))

 '(org-imminent-deadline
   ((t
     (
      :inherit  org-warning
      )
     )))

 '(org-latex-and-related
   ((t
     (
      :weight  bold
      :foreground  "#f8f8f2"
      )
     )))

 '(org-level-1
   ((t
     (
      :width  normal
      :height  1.15
      :weight  regular
      :slant  normal
      :inherit  outline-1
      )
     )))

 '(org-level-2
   ((t
     (
      :width  normal
      :height  1.12
      :weight  regular
      :slant  normal
      :inherit  outline-2
      )
     )))

 '(org-level-3
   ((t
     (
      :width  normal
      :height  1.075
      :weight  regular
      :slant  normal
      :inherit  outline-3
      )
     )))

 '(org-level-4
   ((t
     (
      :width  normal
      :height  1.05
      :weight  regular
      :slant  normal
      :inherit  outline-4
      )
     )))

 '(org-level-5
   ((t
     (
      :width  normal
      :height  1.025
      :weight  regular
      :slant  normal
      :inherit  outline-5
      )
     )))

 '(org-level-6
   ((t
     (
      :width  normal
      :height  1.0
      :weight  regular
      :slant  normal
      :inherit  outline-6
      )
     )))

 '(org-level-7
   ((t
     (
      :width  normal
      :height  1.0
      :weight  regular
      :slant  normal
      :inherit  outline-7
      )
     )))

 '(org-level-8
   ((t
     (
      :width  normal
      :height  1.0
      :weight  regular
      :slant  normal
      :inherit  outline-8
      )
     )))

 '(org-link
   ((t
     (
      :foreground  "#ffb86c"
      :inherit  link
      )
     )))

 '(org-list-dt
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(org-macro
   ((t
     (
      :inherit  org-latex-and-related
      )
     )))

 '(org-meta-line
   ((t
     (
      :foreground  "#8995ba"
      )
     )))

 '(org-mode-line-clock
   ((t
     (
      :inherit  mode-line
      )
     )))

 '(org-mode-line-clock-overrun
   ((t
     (
      :background  "red"
      :inherit  mode-line
      )
     )))

 '(org-priority
   ((t
     (
      :foreground  "#8be9fd"
      )
     )))

 '(org-property-value
   ((t
     (
      :foreground  "#8995ba"
      )
     )))

 '(org-quote
   ((t
     (
      :slant  italic
      :background  "#23242f"
      :inherit  org-block
      )
     )))

 '(org-scheduled
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(org-scheduled-previously
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(org-scheduled-today
   ((t
     (
      :foreground  "#ffb86c"
      )
     )))

 '(org-sexp-date
   ((t
     (
      :foreground  "#565761"
      )
     )))

 '(org-special-keyword
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(org-table
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(org-table-header
   ((t
     (
      :foreground  "Black"
      :background  "LightGray"
      :inherit  org-table
      )
     )))

 '(org-tag
   ((t
     (
      :weight  normal
      :foreground  "#ffcd98"
      )
     )))

 '(org-tag-group
   ((t
     (
      :inherit  org-tag
      )
     )))

 '(org-target
   ((t
     (
      :underline  t
      )
     )))

 '(org-time-grid
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(org-todo
   ((t
     (
      :weight  bold
      :foreground  "#ffb86c"
      :background  "#272934"
      )
     )))

 '(org-upcoming-deadline
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(org-upcoming-distant-deadline
   ((t
     (
      :foreground  "#909193"
      )
     )))

 '(org-verbatim
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(org-verse
   ((t
     (
      :inherit  org-block
      )
     )))

 '(org-warning
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(outline-1
   ((t
     (
      :height 1.1
      :weight  bold
      :foreground  "#bd93f9"
      )
     )))

 '(outline-2
   ((t
     (
      :height 1.075
      :foreground  "#ff79c6"
      :weight  bold
      )
     )))

 '(outline-3
   ((t
     (
      :height 1.050
      :foreground  "#d4b8fb"
      :weight  bold
      )
     )))

 '(outline-4
   ((t
     (
      :height 1.025
      :foreground  "#ffa7d9"
      :weight  bold
      )
     )))

 '(outline-5
   ((t
     (
      :foreground  "#e4d3fc"
      :weight  bold
      )
     )))

 '(outline-6
   ((t
     (
      :foreground  "#ffc9e8"
      :weight  bold
      )
     )))

 '(outline-7
   ((t
     (
      :foreground  "#f5eefe"
      :weight  bold
      )
     )))

 '(outline-8
   ((t
     (
      :foreground  "#ffeaf6"
      :weight  bold
      )
     )))

 '(package-description
   ((t
     (
      :inherit  default
      )
     )))

 '(package-help-section-name
   ((t
     (
      :inherit bold font-lock-function-name-face
      )
     )))

 '(package-name
   ((t
     (
      :inherit  link
      )
     )))

 '(package-status-avail-obso
   ((t
     (
      :inherit  package-status-incompat
      )
     )))

 '(package-status-available
   ((t
     (
      :inherit  default
      )
     )))

 '(package-status-built-in
   ((t
     (
      :inherit  font-lock-builtin-face
      )
     )))

 '(package-status-dependency
   ((t
     (
      :inherit  package-status-installed
      )
     )))

 '(package-status-disabled
   ((t
     (
      :inherit  font-lock-warning-face
      )
     )))

 '(package-status-external
   ((t
     (
      :inherit  package-status-built-in
      )
     )))

 '(package-status-held
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(package-status-incompat
   ((t
     (
      :inherit  error
      )
     )))

 '(package-status-installed
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(package-status-new
   ((t
     (
      :inherit bold package-status-available
      )
     )))

 '(package-status-unsigned
   ((t
     (
      :inherit  font-lock-warning-face
      )
     )))

 '(popup-face
   ((t
     (
      :inherit  tooltip
      )
     )))

 '(popup-isearch-match
   ((t
     (
      :background  "sky blue"
      :inherit  default
      )
     )))

 '(popup-menu-face
   ((t
     (
      :inherit  popup-face
      )
     )))

 '(popup-menu-mouse-face
   ((t
     (
      :foreground  "white"
      :background  "blue"
      )
     )))

 '(popup-menu-selection-face
   ((t
     (
      :foreground  "white"
      :background  "steelblue"
      :inherit  default
      )
     )))

 '(popup-menu-summary-face
   ((t
     (
      :inherit  popup-summary-face
      )
     )))

 '(popup-scroll-bar-background-face
   ((t
     (
      :background  "gray"
      )
     )))

 '(popup-scroll-bar-foreground-face
   ((t
     (
      :background  "black"
      )
     )))

 '(popup-summary-face
   ((t
     (
      :foreground  "dimgray"
      :inherit  popup-face
      )
     )))

 '(popup-tip-face
   ((t
     (
      :foreground  "#bd93f9"
      :background  "#1E2029"
      :inherit  popup-face
      )
     )))

 '(pretty-hydra-toggle-off-face
   ((t
     (
      :inherit quote font-lock-comment-face
      )
     )))

 '(pretty-hydra-toggle-on-face
   ((t
     (
      :inherit quote font-lock-keyword-face
      )
     )))

 '(pulse-highlight-face
   ((t
     (
      :background  "#bd93f9"
      )
     )))

 '(pulse-highlight-start-face
   ((t
     (
      :background  "#bd93f9"
      )
     )))

 '(query-replace
   ((t
     (
      :inherit  isearch
      )
     )))

 '(rainbow-delimiters-base-error-face
   ((t
     (
      :foreground  "#ff5555"
      :inherit  rainbow-delimiters-base-face
      )
     )))

 '(rainbow-delimiters-base-face
   ((t
     (
      :inherit  default
      )
     )))

 '(rainbow-delimiters-depth-1-face
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(rainbow-delimiters-depth-2-face
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(rainbow-delimiters-depth-3-face
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(rainbow-delimiters-depth-4-face
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(rainbow-delimiters-depth-5-face
   ((t
     (
      :foreground  "#0189cc"
      )
     )))

 '(rainbow-delimiters-depth-6-face
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(rainbow-delimiters-depth-7-face
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(rainbow-delimiters-depth-8-face
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(rainbow-delimiters-depth-9-face
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(rainbow-delimiters-mismatched-face
   ((t
     (
      :inherit  rainbow-delimiters-unmatched-face
      )
     )))

 '(rainbow-delimiters-unmatched-face
   ((t
     (
      :weight  bold
      :foreground  "#ff5555"
      )
     )))

 '(read-multiple-choice-face
   ((t
     (
      :weight  bold
      :inherit  underline
      )
     )))

 '(reb-match-0
   ((t
     (
      :foreground  "#ffb86c"
      )
     )))

 '(reb-match-1
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(reb-match-2
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(reb-match-3
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(reb-regexp-grouping-backslash
   ((t
     (
      :weight  bold
      :underline  t
      :inherit  font-lock-keyword-face
      )
     )))

 '(reb-regexp-grouping-construct
   ((t
     (
      :weight  bold
      :underline  t
      :inherit  font-lock-keyword-face
      )
     )))

 '(rectangle-preview
   ((t
     (
      :inherit  region
      )
     )))

 '(region
   ((t
     (
      :background "DeepSkyBlue4"
      )
     )))

 '(rg-column-number-face
   ((t
     (
      :inherit  compilation-column-number
      )
     )))

 '(rg-context-face
   ((t
     (
      :inherit  shadow
      )
     )))

 '(rg-error-face
   ((t
     (
      :inherit  compilation-error
      )
     )))

 '(rg-file-tag-face
   ((t
     (
      :inherit  rg-info-face
      )
     )))

 '(rg-filename-face
   ((t
     (
      :inherit  rg-info-face
      )
     )))

 '(rg-info-face
   ((t
     (
      :inherit  compilation-info
      )
     )))

 '(rg-line-number-face
   ((t
     (
      :inherit  compilation-line-number
      )
     )))

 '(rg-literal-face
   ((t
     (
      :inherit  rg-filename-face
      )
     )))

 '(rg-match-face
   ((t
     (
      :inherit  match
      )
     )))

 '(rg-match-position-face
   ((t
     (
      :inherit  default
      )
     )))

 '(rg-regexp-face
   ((t
     (
      :inherit  compilation-line-number
      )
     )))

 '(rg-toggle-off-face
   ((t
     (
      :inherit  rg-error-face
      )
     )))

 '(rg-toggle-on-face
   ((t
     (
      :inherit  rg-file-tag-face
      )
     )))

 '(rg-warning-face
   ((t
     (
      :inherit  compilation-warning
      )
     )))

 '(rxt-highlight-face
   ((t
     (
      :background  "#222222"
      )
     )))

 '(scroll-bar
   ((t
     (
      :foreground  "white"
      )
     )))

 '(secondary-selection
   ((t
     (
      :background  "#565761"
      )
     )))

 '(selectrum-completion-annotation
   ((t
     (
      :inherit  completions-annotations
      )
     )))

 '(selectrum-completion-docsig
   ((t
     (
      :slant  italic
      :inherit  selectrum-completion-annotation
      )
     )))

 '(selectrum-current-candidate
   ((t
     (
      :background  "#44475a"
      )
     )))

 '(selectrum-group-separator
   ((t
     (
      :inherit  shadow
      )
     )))

 '(selectrum-group-title
   ((t
     (
      :slant  italic
      :inherit  shadow
      )
     )))

 '(selectrum-mouse-highlight
   ((t
     (
      :underline  t
      :inherit  selectrum-current-candidate
      )
     )))

 '(selectrum-prescient-primary-highlight
   ((t
     (
      :weight  bold
      )
     )))

 '(selectrum-prescient-secondary-highlight
   ((t
     (
      :underline  t
      :inherit  selectrum-prescient-primary-highlight
      )
     )))

 '(selectrum-quick-keys-highlight
   ((t
     (
      :inherit  lazy-highlight
      )
     )))

 '(selectrum-quick-keys-match
   ((t
     (
      :inherit  isearch
      )
     )))

 '(separator-line
   ((t
     (
      :height  0.1
      :background  "#505050"
      )
     )))

 '(shadow
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(show-paren-match
   ((t
     (
      :weight  ultra-bold
      :foreground  "#5317ac"
      :background  "#FFFFFF"
      )
     )))

 '(show-paren-match-expression
   ((t
     (
      :inherit  show-paren-match
      )
     )))

 '(show-paren-mismatch
   ((t
     (
      :weight  ultra-bold
      :foreground  "#1E2029"
      :background  "#ff5555"
      )
     )))

 '(shr-abbreviation
   ((t
     (
      :underline (:style wave)
      :inherit  underline
      )
     )))

 '(shr-h1
   ((t
     (
      :height  1.3
      :weight  bold
      )
     )))

 '(shr-h2
   ((t
     (
      :weight  bold
      )
     )))

 '(shr-h3
   ((t
     (
      :slant  italic
      )
     )))

 '(shr-link
   ((t
     (
      :inherit  link
      )
     )))

 '(shr-selected-link
   ((t
     (
      :background  "red"
      :inherit  shr-link
      )
     )))

 '(shr-sup
   ((t
     (
      :height  0.8
      )
     )))

 '(shr-text
   ((t
     (
      :inherit  variable-pitch-text
      )
     )))

 '(smerge-base
   ((t
     (
      :background  "#33475e"
      )
     )))

 '(smerge-lower
   ((t
     (
      :background  "#305343"
      )
     )))

 '(smerge-markers
   ((t
     (
      :weight  bold
      :foreground  "#282a36"
      :background  "#6272a4"
      )
     )))

 '(smerge-refined-added
   ((t
     (
      :inherit  diff-added
      )
     )))

 '(smerge-refined-removed
   ((t
     (
      :inherit  diff-removed
      )
     )))

 '(smerge-upper
   ((t
     (
      :background  "#694959"
      )
     )))

 '(success
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(swiper-background-match-face-1
   ((t
     (
      :inherit  swiper-match-face-1
      )
     )))

 '(swiper-background-match-face-2
   ((t
     (
      :inherit  swiper-match-face-2
      )
     )))

 '(swiper-background-match-face-3
   ((t
     (
      :inherit  swiper-match-face-3
      )
     )))

 '(swiper-background-match-face-4
   ((t
     (
      :inherit  swiper-match-face-4
      )
     )))

 '(swiper-line-face
   ((t
     (
      :foreground  "#1E2029"
      :background  "#61bfff"
      )
     )))

 '(swiper-match-face-1
   ((t
     (
      :foreground  "#6272a4"
      :background  "#1E2029"
      )
     )))

 '(swiper-match-face-2
   ((t
     (
      :weight  bold
      :foreground  "#1E2029"
      :background  "#ffb86c"
      )
     )))

 '(swiper-match-face-3
   ((t
     (
      :weight  bold
      :foreground  "#1E2029"
      :background  "#ff79c6"
      )
     )))

 '(swiper-match-face-4
   ((t
     (
      :weight  bold
      :foreground  "#1E2029"
      :background  "#50fa7b"
      )
     )))

 '(tab-bar
   ((t
     (
      :foreground  "#1E2029"
      :background  "#1E2029"
      )
     )))

 '(tab-bar-tab
   ((t
     (
      :foreground  "#f8f8f2"
      ;; :background  "#2A265A"
      :inherit ivy-current-match
      )
     )))

 '(tab-bar-tab-group-current
   ((t
     (
      :weight  bold
      :inherit  tab-bar-tab
      )
     )))

 '(tab-bar-tab-group-inactive
   ((t
     (
      :inherit shadow tab-bar-tab-inactive
      )
     )))

 '(tab-bar-tab-inactive
   ((t
     (
      :foreground  "#e2e2dc"
      :background  "#1E2029"
      :inherit  tab-line-tab
      )
     )))

 '(tab-bar-tab-ungrouped
   ((t
     (
      :inherit shadow tab-bar-tab-inactive
      )
     )))

 '(tab-line
   ((t
     (
      :foreground  "#1E2029"
      :background  "#1E2029"
      )
     )))

 '(tabulated-list-fake-header
   ((t
     (
      :weight  bold
      :underline  t
      )
     )))

 '(term
   ((t
     (
      :foreground  "#f8f8f2"
      )
     )))

 '(term-bold
   ((t
     (
      :weight  bold
      )
     )))

 '(term-color-black
   ((t
     (
      :foreground  "#1E2029"
      :background  "#1E2029"
      )
     )))

 '(term-color-blue
   ((t
     (
      :foreground  "#61bfff"
      :background  "#61bfff"
      )
     )))

 '(term-color-bright-black
   ((t
     (
      :inherit  ansi-color-bright-black
      )
     )))

 '(term-color-bright-blue
   ((t
     (
      :inherit  ansi-color-bright-blue
      )
     )))

 '(term-color-bright-cyan
   ((t
     (
      :inherit  ansi-color-bright-cyan
      )
     )))

 '(term-color-bright-green
   ((t
     (
      :inherit  ansi-color-bright-green
      )
     )))

 '(term-color-bright-magenta
   ((t
     (
      :inherit  ansi-color-bright-magenta
      )
     )))

 '(term-color-bright-red
   ((t
     (
      :inherit  ansi-color-bright-red
      )
     )))

 '(term-color-bright-white
   ((t
     (
      :inherit  ansi-color-bright-white
      )
     )))

 '(term-color-bright-yellow
   ((t
     (
      :inherit  ansi-color-bright-yellow
      )
     )))

 '(term-color-cyan
   ((t
     (
      :foreground  "#8be9fd"
      :background  "#8be9fd"
      )
     )))

 '(term-color-green
   ((t
     (
      :foreground  "#50fa7b"
      :background  "#50fa7b"
      )
     )))

 '(term-color-magenta
   ((t
     (
      :foreground  "#ff79c6"
      :background  "#ff79c6"
      )
     )))

 '(term-color-red
   ((t
     (
      :foreground  "#ff5555"
      :background  "#ff5555"
      )
     )))

 '(term-color-white
   ((t
     (
      :foreground  "#f8f8f2"
      :background  "#f8f8f2"
      )
     )))

 '(term-color-yellow
   ((t
     (
      :foreground  "#f1fa8c"
      :background  "#f1fa8c"
      )
     )))

 '(term-faint
   ((t
     (
      :inherit  ansi-color-faint
      )
     )))

 '(term-fast-blink
   ((t
     (
      :inherit  ansi-color-fast-blink
      )
     )))

 '(term-italic
   ((t
     (
      :inherit  ansi-color-italic
      )
     )))

 '(term-slow-blink
   ((t
     (
      :inherit  ansi-color-slow-blink
      )
     )))

 '(term-underline
   ((t
     (
      :inherit  ansi-color-underline
      )
     )))

 '(tool-bar
   ((t
     (
      :foreground  "black"
      :background  "grey75"
      )
     )))

 '(tooltip
   ((t
     (
      :foreground  "#f8f8f2"
      :background  "#1E2029"
      )
     )))

 '(trailing-whitespace
   ((t
     (
      :background  "#ff5555"
      )
     )))

 '(transient-active-infix
   ((t
     (
      :inherit  secondary-selection
      )
     )))

 '(transient-amaranth
   ((t
     (
      :foreground  "#E52B50"
      :inherit  transient-key
      )
     )))

 '(transient-argument
   ((t
     (
      :inherit  font-lock-warning-face
      )
     )))

 '(transient-blue
   ((t
     (
      :foreground  "blue"
      :inherit  transient-key
      )
     )))

 '(transient-disabled-suffix
   ((t
     (
      :weight  bold
      :foreground  "black"
      :background  "red"
      )
     )))

 '(transient-enabled-suffix
   ((t
     (
      :weight  bold
      :foreground  "black"
      :background  "green"
      )
     )))

 '(transient-heading
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(transient-higher-level
   ((t
     (
      :underline  t
      )
     )))

 '(transient-inactive-argument
   ((t
     (
      :inherit  shadow
      )
     )))

 '(transient-inactive-value
   ((t
     (
      :inherit  shadow
      )
     )))

 '(transient-inapt-suffix
   ((t
     (
      :slant  italic
      :inherit  shadow
      )
     )))

 '(transient-key
   ((t
     (
      :inherit  font-lock-builtin-face
      )
     )))

 '(transient-mismatched-key
   ((t
     (
      :underline  t
      )
     )))

 '(transient-nonstandard-key
   ((t
     (
      :underline  t
      )
     )))

 '(transient-pink
   ((t
     (
      :foreground  "#FF6EB4"
      :inherit  transient-key
      )
     )))

 '(transient-red
   ((t
     (
      :foreground  "red"
      :inherit  transient-key
      )
     )))

 '(transient-separator
   ((t
     (
      :background  "grey30"
      )
     )))

 '(transient-teal
   ((t
     (
      :foreground  "#367588"
      :inherit  transient-key
      )
     )))

 '(transient-unreachable
   ((t
     (
      :inherit  shadow
      )
     )))

 '(transient-unreachable-key
   ((t
     (
      :inherit  shadow
      )
     )))

 '(transient-value
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(tty-menu-disabled-face
   ((t
     (
      :foreground  "lightgray"
      :background  "blue"
      )
     )))

 '(tty-menu-enabled-face
   ((t
     (
      :weight  bold
      :foreground  "yellow"
      :background  "blue"
      )
     )))

 '(tty-menu-selected-face
   ((t
     (
      :background  "red"
      )
     )))

 '(underline
   ((t
     (
      :underline  t
      )
     )))

 '(undo-tree-visualizer-active-branch-face
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(undo-tree-visualizer-current-face
   ((t
     (
      :weight  bold
      :foreground  "#50fa7b"
      )
     )))

 '(undo-tree-visualizer-default-face
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(undo-tree-visualizer-register-face
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(undo-tree-visualizer-unmodified-face
   ((t
     (
      :foreground  "#6272a4"
      )
     )))

 '(variable-pitch
   ((t
     (
      :width  normal
      :height  100
      :weight  regular
      :slant  normal
      )
     )))

 '(variable-pitch-text
   ((t
     (
      :height  1.1
      :inherit  variable-pitch
      )
     )))

 '(vc-conflict-state
   ((t
     (
      :inherit  vc-state-base
      )
     )))

 '(vc-dir-directory
   ((t
     (
      :inherit  font-lock-comment-delimiter-face
      )
     )))

 '(vc-dir-file
   ((t
     (
      :inherit  font-lock-function-name-face
      )
     )))

 '(vc-dir-header
   ((t
     (
      :inherit  font-lock-type-face
      )
     )))

 '(vc-dir-header-value
   ((t
     (
      :inherit  font-lock-variable-name-face
      )
     )))

 '(vc-dir-mark-indicator
   ((t
     (
      :inherit  font-lock-type-face
      )
     )))

 '(vc-dir-status-edited
   ((t
     (
      :inherit  font-lock-variable-name-face
      )
     )))

 '(vc-dir-status-ignored
   ((t
     (
      :inherit  shadow
      )
     )))

 '(vc-dir-status-up-to-date
   ((t
     (
      :inherit  font-lock-builtin-face
      )
     )))

 '(vc-dir-status-warning
   ((t
     (
      :inherit  font-lock-warning-face
      )
     )))

 '(vc-edited-state
   ((t
     (
      :weight  bold
      :foreground  "#FF6E67"
      )
     )))

 '(vc-locally-added-state
   ((t
     (
      :inherit  vc-state-base
      )
     )))

 '(vc-locked-state
   ((t
     (
      :inherit  vc-state-base
      )
     )))

 '(vc-missing-state
   ((t
     (
      :inherit  vc-state-base
      )
     )))

 '(vc-needs-update-state
   ((t
     (
      :inherit  vc-state-base
      )
     )))

 '(vc-removed-state
   ((t
     (
      :inherit  vc-state-base
      )
     )))

 '(vc-up-to-date-state
   ((t
     (
      :weight  bold
      :foreground  "#edc809"
      )
     )))

 '(vertical-border
   ((t
     (
      :weight  normal
      :foreground  "#2A265A"
      :background  "#2A265A"
      )
     )))

 '(vterm-color-black
   ((t
     (
      :foreground  "#1E2029"
      :background  "#56575e"
      )
     )))

 '(vterm-color-blue
   ((t
     (
      :foreground  "#61bfff"
      :background  "#88cfff"
      )
     )))

 '(vterm-color-cyan
   ((t
     (
      :foreground  "#8be9fd"
      :background  "#a8eefd"
      )
     )))

 '(vterm-color-green
   ((t
     (
      :foreground  "#50fa7b"
      :background  "#7bfb9c"
      )
     )))

 '(vterm-color-magenta
   ((t
     (
      :foreground  "#ff79c6"
      :background  "#ff9ad4"
      )
     )))

 '(vterm-color-red
   ((t
     (
      :foreground  "#ff5555"
      :background  "#ff7f7f"
      )
     )))

 '(vterm-color-underline
   ((t
     (
      :inherit  default
      )
     )))

 '(vterm-color-white
   ((t
     (
      :foreground  "#f8f8f2"
      :background  "#f9f9f5"
      )
     )))

 '(vterm-color-yellow
   ((t
     (
      :foreground  "#f1fa8c"
      :background  "#f4fba8"
      )
     )))

 '(warning
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(wgrep-delete-face
   ((t
     (
      :foreground  "#44475a"
      :background  "#ff5555"
      )
     )))

 '(wgrep-done-face
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(wgrep-face
   ((t
     (
      :weight  bold
      :foreground  "#50fa7b"
      :background  "#6272a4"
      )
     )))

 '(wgrep-file-face
   ((t
     (
      :foreground  "#FF0000"
      :background  "#FFFF00"
      :weight ultra-bold
      :underline t
      )
     )))

 '(wgrep-reject-face
   ((t
     (
      :weight  bold
      :foreground  "#ff5555"
      )
     )))

 '(which-func
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(which-key-command-description-face
   ((t
     (
      :foreground  "#61bfff"
      )
     )))

 '(which-key-docstring-face
   ((t
     (
      :inherit  which-key-note-face
      )
     )))

 '(which-key-group-description-face
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(which-key-highlighted-command-face
   ((t
     (
      :underline  t
      :inherit  which-key-command-description-face
      )
     )))

 '(which-key-key-face
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(which-key-local-map-description-face
   ((t
     (
      :foreground  "#ff79c6"
      )
     )))

 '(which-key-note-face
   ((t
     (
      :inherit  which-key-separator-face
      )
     )))

 '(which-key-separator-face
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(which-key-special-key-face
   ((t
     (
      :weight  bold
      :inherit  which-key-key-face
      )
     )))

 '(whitespace-big-indent
   ((t
     (
      :foreground  "firebrick"
      :background  unspecified
      )
     )))

 '(whitespace-empty
   ((t
     (
      :background  unspecified
      )
     )))

 '(whitespace-hspace
   ((t
     (
      :foreground  "darkgray"
      :background  unspecified
      )
     )))

 '(whitespace-indentation
   ((t
     (
      :foreground  "#565761"
      :background  unspecified
      )
     )))

 '(whitespace-line
   ((t
     (
      :weight  bold
      :foreground  "#ff5555"
      :background  unspecified
      )
     )))

 '(whitespace-missing-newline-at-eof
   ((t
     (
      :foreground  "black"
      :background  unspecified
      )
     )))

 '(whitespace-newline
   ((t
     (
      :foreground  "#2f4f4f"
      :background  unspecified
      )
     )))

 '(whitespace-space
   ((t
     (
      :foreground  "#2f4f4f"
      :background  unspecified
      )
     )))

 '(whitespace-space-after-tab
   ((t
     (
      :foreground  "firebrick"
      :background  unspecified
      )
     )))

 '(whitespace-space-before-tab
   ((t
     (
      :foreground  "firebrick"
      :background  unspecified
      )
     )))

 '(whitespace-tab
   ((t
     (
      :foreground  "#2f4f4f"
      :background  unspecified
      )
     )))

 '(whitespace-trailing
   ((t
     (
      :inherit  trailing-whitespace
      :background  unspecified
      )
     )))

 '(widget-button
   ((t
     (
      :foreground  "#f1fa8c"
      )
     )))

 '(widget-button-pressed
   ((t
     (
      :foreground  "#ff5555"
      )
     )))

 '(widget-documentation
   ((t
     (
      :foreground  "#50fa7b"
      )
     )))

 '(widget-field
   ((t
     (
      :background  "#44475a"
      )
     )))

 '(widget-inactive
   ((t
     (
      :inherit  shadow
      )
     )))

 '(widget-single-line-field
   ((t
     (
      :background  "#44475a"
      )
     )))

 '(window-divider
   ((t
     (
      :weight  normal
      :foreground  "#2A265A"
      :background  "#2A265A"
      :inherit  vertical-border
      )
     )))

 '(window-divider-first-pixel
   ((t
     (
      :inherit  window-divider
      )
     )))

 '(window-divider-last-pixel
   ((t
     (
      :inherit  window-divider
      )
     )))

 '(xref-file-header
   ((t
     (
      :inherit  success
      )
     )))

 '(xref-line-number
   ((t
     (
      :foreground  "#bd93f9"
      )
     )))

 '(xref-match
   ((t
     (
      :weight  bold
      :foreground  "#50fa7b"
      :background  "#1E2029"
      )
     )))

 '(yaml-tab-face
   ((t
     (
      :weight  bold
      :foreground  "red"
      :background  "red"
      )
     )))

 '(yas-field-highlight-face
   ((t
     (
      :inherit  match
      )
     )))

 )

;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'funknight)

;; Local Variables:
;; no-byte-compile: t
;; End:

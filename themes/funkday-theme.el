(deftheme funkday)

(custom-theme-set-faces
 'funkday

 '(Info-quoted
   ((t
     (
      :foreground  "#61284f"
      :background  "#f0f0f0"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(alert-high-face
   ((t
     (
      :foreground  "#972500"
      :inherit  bold
      )
     )))

 '(alert-low-face
   ((t
     (
      :foreground  "#184034"
      )
     )))

 '(alert-moderate-face
   ((t
     (
      :foreground  "#813e00"
      :inherit  bold
      )
     )))

 '(alert-normal-face
   ((t
     (
      )
     )))

 '(alert-saved-fringe-face
   ((t
     (
      :foreground  "#000000"
      :background  "#ffffff"
      :inherit  default
      )
     )))

 '(alert-trivial-face
   ((t
     (
      :foreground  "#61284f"
      )
     )))

 '(alert-urgent-face
   ((t
     (
      :foreground  "#b60000"
      :inherit  bold
      )
     )))

 '(all-the-icons-blue
   ((t
     (
      :foreground  "#0031a9"
      )
     )))

 '(all-the-icons-blue-alt
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(all-the-icons-cyan
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(all-the-icons-cyan-alt
   ((t
     (
      :foreground  "#30517f"
      )
     )))

 '(all-the-icons-dblue
   ((t
     (
      :foreground  "#0000c0"
      )
     )))

 '(all-the-icons-dcyan
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(all-the-icons-dgreen
   ((t
     (
      :foreground  "#145c33"
      )
     )))

 '(all-the-icons-dmaroon
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(all-the-icons-dorange
   ((t
     (
      :foreground  "#a0132f"
      )
     )))

 '(all-the-icons-dpink
   ((t
     (
      :foreground  "#721045"
      )
     )))

 '(all-the-icons-dpurple
   ((t
     (
      :foreground  "#8f0075"
      )
     )))

 '(all-the-icons-dred
   ((t
     (
      :foreground  "#a60000"
      )
     )))

 '(all-the-icons-dsilver
   ((t
     (
      :foreground  "#093060"
      )
     )))

 '(all-the-icons-dyellow
   ((t
     (
      :foreground  "#813e00"
      )
     )))

 '(all-the-icons-green
   ((t
     (
      :foreground  "#005e00"
      )
     )))

 '(all-the-icons-dired-dir-face
   ((t
     (
      :foreground  "#0031a9"
      )
     )))

 '(all-the-icons-ibuffer-dir-face
   ((t
     (
      :inherit  font-lock-doc-face
      )
     )))

 '(all-the-icons-ibuffer-file-face
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(all-the-icons-ibuffer-icon-face
   ((t
     (
      :inherit  default
      )
     )))

 '(all-the-icons-ibuffer-mode-face
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(all-the-icons-ibuffer-size-face
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(all-the-icons-ivy-rich-archive-face
   ((t
     (
      :inherit  font-lock-type-face
      )
     )))

 '(all-the-icons-ivy-rich-bookmark-face
   ((t
     (
      :inherit  all-the-icons-ivy-rich-doc-face
      )
     )))

 '(all-the-icons-ivy-rich-dir-face
   ((t
     (
      :inherit  font-lock-doc-face
      )
     )))

 '(all-the-icons-ivy-rich-doc-face
   ((t
     (
      :inherit  ivy-completions-annotations
      )
     )))

 '(all-the-icons-ivy-rich-file-modes-face
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(all-the-icons-ivy-rich-file-name-face
   ((t
     (
      :inherit  all-the-icons-ivy-rich-doc-face
      )
     )))

 '(all-the-icons-ivy-rich-file-owner-face
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(all-the-icons-ivy-rich-icon-face
   ((t
     (
      :inherit  default
      )
     )))

 '(all-the-icons-ivy-rich-indicator-face
   ((t
     (
      :inherit  error
      )
     )))

 '(all-the-icons-ivy-rich-install-face
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(all-the-icons-ivy-rich-major-mode-face
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(all-the-icons-ivy-rich-path-face
   ((t
     (
      :inherit  all-the-icons-ivy-rich-doc-face
      )
     )))

 '(all-the-icons-ivy-rich-process-buffer-face
   ((t
     (
      :inherit  font-lock-keyword-face
      )
     )))

 '(all-the-icons-ivy-rich-process-command-face
   ((t
     (
      :inherit  all-the-icons-ivy-rich-doc-face
      )
     )))

 '(all-the-icons-ivy-rich-process-id-face
   ((t
     (
      :inherit  default
      )
     )))

 '(all-the-icons-ivy-rich-process-status-alt-face
   ((t
     (
      :inherit  error
      )
     )))

 '(all-the-icons-ivy-rich-process-status-face
   ((t
     (
      :inherit  success
      )
     )))

 '(all-the-icons-ivy-rich-process-thread-face
   ((t
     (
      :inherit  font-lock-doc-face
      )
     )))

 '(all-the-icons-ivy-rich-process-tty-face
   ((t
     (
      :inherit  font-lock-doc-face
      )
     )))

 '(all-the-icons-ivy-rich-project-face
   ((t
     (
      :inherit  font-lock-string-face
      )
     )))

 '(all-the-icons-ivy-rich-size-face
   ((t
     (
      :inherit  shadow
      )
     )))

 '(all-the-icons-ivy-rich-time-face
   ((t
     (
      :inherit  shadow
      )
     )))

 '(all-the-icons-ivy-rich-version-face
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(all-the-icons-lblue
   ((t
     (
      :foreground  "#002f88"
      )
     )))

 '(all-the-icons-lcyan
   ((t
     (
      :foreground  "#004850"
      )
     )))

 '(all-the-icons-lgreen
   ((t
     (
      :foreground  "#004c00"
      )
     )))

 '(all-the-icons-lmaroon
   ((t
     (
      :foreground  "#770077"
      )
     )))

 '(all-the-icons-lorange
   ((t
     (
      :foreground  "#780000"
      )
     )))

 '(all-the-icons-lpink
   ((t
     (
      :foreground  "#770077"
      )
     )))

 '(all-the-icons-lpurple
   ((t
     (
      :foreground  "#770077"
      )
     )))

 '(all-the-icons-lred
   ((t
     (
      :foreground  "#780000"
      )
     )))

 '(all-the-icons-lsilver
   ((t
     (
      :foreground  "#093060"
      )
     )))

 '(all-the-icons-lyellow
   ((t
     (
      :foreground  "#604000"
      )
     )))

 '(all-the-icons-maroon
   ((t
     (
      :foreground  "#721045"
      )
     )))

 '(all-the-icons-orange
   ((t
     (
      :foreground  "#972500"
      )
     )))

 '(all-the-icons-pink
   ((t
     (
      :foreground  "#721045"
      )
     )))

 '(all-the-icons-purple
   ((t
     (
      :foreground  "#8f0075"
      )
     )))

 '(all-the-icons-purple-alt
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(all-the-icons-red
   ((t
     (
      :foreground  "#a60000"
      )
     )))

 '(all-the-icons-red-alt
   ((t
     (
      :foreground  "#972500"
      )
     )))

 '(all-the-icons-silver
   ((t
     (
      :foreground  "#093060"
      )
     )))

 '(all-the-icons-yellow
   ((t
     (
      :foreground  "#813e00"
      )
     )))

 '(ansi-color-black
   ((t
     (
      :foreground  "black"
      :background  "black"
      )
     )))

 '(ansi-color-blue
   ((t
     (
      :foreground  "#0031a9"
      :background  "#0031a9"
      )
     )))

 '(ansi-color-bold
   ((t
     (
      :weight  bold
      :inherit default
      )
     )))

 '(ansi-color-bright-black
   ((t
     (
      :foreground  "gray35"
      :background  "gray35"
      )
     )))

 '(ansi-color-bright-blue
   ((t
     (
      :foreground  "#2544bb"
      :background  "#2544bb"
      )
     )))

 '(ansi-color-bright-cyan
   ((t
     (
      :foreground  "#005a5f"
      :background  "#005a5f"
      )
     )))

 '(ansi-color-bright-green
   ((t
     (
      :foreground  "#145c33"
      :background  "#145c33"
      )
     )))

 '(ansi-color-bright-magenta
   ((t
     (
      :foreground  "#5317ac"
      :background  "#5317ac"
      )
     )))

 '(ansi-color-bright-red
   ((t
     (
      :foreground  "#972500"
      :background  "#972500"
      )
     )))

 '(ansi-color-bright-white
   ((t
     (
      :foreground  "white"
      :background  "white"
      )
     )))

 '(ansi-color-bright-yellow
   ((t
     (
      :foreground  "#70480f"
      :background  "#70480f"
      )
     )))

 '(ansi-color-cyan
   ((t
     (
      :foreground  "#00538b"
      :background  "#00538b"
      )
     )))

 '(ansi-color-faint
   ((t
     (
      :weight  light
      )
     )))

 '(ansi-color-green
   ((t
     (
      :foreground  "#005e00"
      :background  "#005e00"
      )
     )))

 '(ansi-color-inverse
   ((t
     (
      :inverse-video  t
      )
     )))

 '(ansi-color-italic
   ((t
     (
      :inherit  italic
      )
     )))

 '(ansi-color-magenta
   ((t
     (
      :foreground  "#721045"
      :background  "#721045"
      )
     )))

 '(ansi-color-red
   ((t
     (
      :foreground  "#a60000"
      :background  "#a60000"
      )
     )))

 '(ansi-color-underline
   ((t
     (
      :inherit  underline
      )
     )))

 '(ansi-color-white
   ((t
     (
      :foreground  "gray65"
      :background  "gray65"
      )
     )))

 '(ansi-color-yellow
   ((t
     (
      :foreground  "#813e00"
      :background  "#813e00"
      )
     )))

 '(avy-background-face
   ((t
     (
      :extend  t
      :foreground  "#282828"
      :background  "#f8f8f8"
      )
     )))

 '(avy-goto-char-timer-face
   ((t
     (
      :background "red"
      :foreground "yellow"
      :weight bold
      :inherit (modus-themes-intense-yellow bold)
      )
     )))

 '(avy-lead-face
   ((t
     (
      :background "red"
      :foreground "yellow"
      :weight bold
      :inherit (modus-themes-intense-magenta bold modus-themes-reset-soft)
      )
     )))

 '(avy-lead-face-0
   ((t
     (
      :background "red"
      :foreground "yellow"
      :weight bold
      :inherit (modus-themes-refine-cyan bold modus-themes-reset-soft)
      )
     )))

 '(avy-lead-face-1
   ((t
     (
      :background "red"
      :foreground "yellow"
      :weight bold
      :inherit (modus-themes-intense-neutral bold modus-themes-reset-soft)
      )
     )))

 '(avy-lead-face-2
   ((t
     (
      :background "red"
      :foreground "yellow"
      :weight bold
      :inherit (modus-themes-refine-red bold modus-themes-reset-soft)
      )
     )))

 '(aw-background-face
   ((t
     (
      :foreground  "#56576d"
      )
     )))

 '(aw-key-face
   ((t
     (
      :inherit  modus-themes-key-binding
      )
     )))

 '(aw-leading-char-face
   ((t
     (
      :height  7.0
      :foreground  "#b60000"
      :inherit  ace-jump-face-foreground
      )
     )))

 '(aw-minibuffer-leading-char-face
   ((t
     (
      :inherit (modus-themes-intense-red bold)
      )
     )))

 '(aw-mode-line-face
   ((t
     (
      :inherit  bold
      )
     )))

 '(biblio-detail-header-face
   ((t
     (
      :slant  normal
      )
     )))

 '(biblio-highlight-extend-face
   ((t
     (
      :extend  t
      :inherit  highlight
      )
     )))

 '(biblio-results-header-face
   ((t
     (
      :height  1.5
      :weight  bold
      :inherit  font-lock-preprocessor-face
      )
     )))

 '(blamer-face
   ((t
     (
      :height  90
      :foreground  "#7a88cf"
      :background  "#FFFFFF"
      )
     )))

 '(bold
   ((t
     (
      :weight  bold
      )
     )))

 '(bold-italic
   ((t
     (
      :inherit bold italic
      )
     )))

 '(bookmark-face
   ((t
     (
      :inherit  modus-themes-fringe-cyan
      )
     )))

 '(bookmark-menu-bookmark
   ((t
     (
      :inherit  bold
      )
     )))

 '(bookmark-menu-heading
   ((t
     (
      :inherit  font-lock-type-face
      )
     )))

 '(border
   ((t
     (
      )
     )))

 '(browse-url-button
   ((t
     (
      :inherit  link
      )
     )))

 '(buffer-menu-buffer
   ((t
     (
      :inherit  bold
      )
     )))

 '(button
   ((t
     (
      :underline  t
      :foreground  "#0000c0"
      )
     )))

 '(calc-nonselected-face
   ((t
     (
      :slant  italic
      :inherit  shadow
      )
     )))

 '(calc-selected-face
   ((t
     (
      :weight  bold
      )
     )))

 '(calendar-month-header
   ((t
     (
      :inherit  modus-themes-pseudo-header
      )
     )))

 '(calendar-today
   ((t
     (
      :underline  t
      :inherit  bold
      )
     )))

 '(calendar-weekday-header
   ((t
     (
      :foreground  "#56576d"
      )
     )))

 '(calendar-weekend-header
   ((t
     (
      :foreground  "#7f1010"
      )
     )))

 '(change-log-acknowledgment
   ((t
     (
      :inherit  shadow
      )
     )))

 '(change-log-conditionals
   ((t
     (
      :foreground  "#813e00"
      )
     )))

 '(change-log-date
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(change-log-email
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(change-log-file
   ((t
     (
      :foreground  "#093060"
      :inherit  bold
      )
     )))

 '(change-log-function
   ((t
     (
      :foreground  "#145c33"
      )
     )))

 '(change-log-list
   ((t
     (
      :foreground  "#8f0075"
      )
     )))

 '(change-log-name
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(child-frame-border
   ((t
     (
      )
     )))

 '(citar
   ((t
     (
      :inherit  font-lock-doc-face
      )
     )))

 '(citar-highlight
   ((t
     (
      :weight  bold
      )
     )))

 '(citar-icon-dim
   ((t
     (
      :foreground  "#fafafa"
      )
     )))

 '(comint-highlight-input
   ((t
     (
      :inherit  bold
      )
     )))

 '(comint-highlight-prompt
   ((t
     (
      :inherit  modus-themes-prompt
      )
     )))

 '(company-echo
   ((t
     (
      )
     )))

 '(company-echo-common
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(company-preview
   ((t
     (
      :foreground  "#282828"
      :background  "#f8f8f8"
      )
     )))

 '(company-preview-common
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(company-preview-search
   ((t
     (
      :inherit  modus-themes-special-calm
      )
     )))

 '(company-tooltip
   ((t
     (
      :foreground  "#505050"
      :background  "#f0f0f0"
      )
     )))

 '(company-tooltip-annotation
   ((t
     (
      :foreground  "#093060"
      :inherit  modus-themes-slant
      )
     )))

 '(company-tooltip-annotation-selection
   ((t
     (
      :foreground  "#000000"
      :inherit  bold
      )
     )))

 '(company-tooltip-common
   ((t
     (
      :foreground  "#2544bb"
      :inherit  bold
      )
     )))

 '(company-tooltip-common-selection
   ((t
     (
      :foreground  "#000000"
      )
     )))

 '(company-tooltip-deprecated
   ((t
     (
      :strike-through  t
      :inherit  company-tooltip
      )
     )))

 '(company-tooltip-mouse
   ((t
     (
      :inherit  modus-themes-intense-blue
      )
     )))

 '(company-tooltip-quick-access
   ((t
     (
      :inherit  company-tooltip-annotation
      )
     )))

 '(company-tooltip-quick-access-selection
   ((t
     (
      :inherit  company-tooltip-annotation-selection
      )
     )))

 '(company-tooltip-scrollbar-thumb
   ((t
     (
      :background  "#0a0a0a"
      )
     )))

 '(company-tooltip-scrollbar-track
   ((t
     (
      :background  "#d7d7d7"
      )
     )))

 '(company-tooltip-search
   ((t
     (
      :inherit (modus-themes-search-success-lazy bold)
      )
     )))

 '(company-tooltip-search-selection
   ((t
     (
      :underline  t
      :inherit (modus-themes-search-success bold)
      )
     )))

 '(company-tooltip-selection
   ((t
     (
      :background "#c1c1ff"
      )
     )))

 '(compilation-column-number
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(compilation-error
   ((t
     (
      :foreground  "#a60000"
      :inherit  modus-themes-bold
      )
     )))

 '(compilation-info
   ((t
     (
      :foreground  "#093060"
      :inherit  modus-themes-bold
      )
     )))

 '(compilation-line-number
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(compilation-mode-line-exit
   ((t
     (
      :foreground  "#0030b4"
      :inherit  modus-themes-bold
      )
     )))

 '(compilation-mode-line-fail
   ((t
     (
      :foreground  "#8a0000"
      :inherit  modus-themes-bold
      )
     )))

 '(compilation-mode-line-run
   ((t
     (
      :foreground  "#5c2092"
      :inherit  modus-themes-bold
      )
     )))

 '(compilation-warning
   ((t
     (
      :foreground  "#813e00"
      :inherit  modus-themes-bold
      )
     )))

 '(completions-annotations
   ((t
     (
      :foreground  "#005077"
      :inherit  modus-themes-slant
      )
     )))

 '(completions-common-part
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(completions-first-difference
   ((t
     (
      :foreground  "#8f0075"
      :inherit  bold
      )
     )))

 '(completions-group-separator
   ((t
     (
      :strike-through  t
      :inherit  shadow
      )
     )))

 '(completions-group-title
   ((t
     (
      :slant  italic
      :inherit  shadow
      )
     )))

 '(confusingly-reordered
   ((t
     (
      :inherit  modus-themes-lang-error
      )
     )))

 '(consult-async-failed
   ((t
     (
      :inherit  error
      )
     )))

 '(consult-async-finished
   ((t
     (
      :inherit  success
      )
     )))

 '(consult-async-running
   ((t
     (
      :foreground  "#0031a9"
      :inherit  bold
      )
     )))

 '(consult-async-split
   ((t
     (
      :foreground  "#8f0075"
      )
     )))

 '(consult-bookmark
   ((t
     (
      :foreground  "#0031a9"
      )
     )))

 '(consult-buffer
   ((t
     (
      )
     )))

 '(consult-crm-selected
   ((t
     (
      :inherit  secondary-selection
      )
     )))

 '(consult-file
   ((t
     (
      :foreground  "#093060"
      )
     )))

 '(consult-grep-context
   ((t
     (
      :inherit  shadow
      )
     )))

 '(consult-help
   ((t
     (
      :inherit  shadow
      )
     )))

 '(consult-key
   ((t
     (
      :inherit  modus-themes-key-binding
      )
     )))

 '(consult-line-number
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(consult-line-number-prefix
   ((t
     (
      :foreground  "#56576d"
      )
     )))

 '(consult-line-number-wrapped
   ((t
     (
      :inherit  font-lock-warning-face
      )
     )))

 '(consult-narrow-indicator
   ((t
     (
      :foreground  "#8f0075"
      )
     )))

 '(consult-preview-cursor
   ((t
     (
      :inherit  modus-themes-intense-blue
      )
     )))

 '(consult-preview-error
   ((t
     (
      :inherit  modus-themes-intense-red
      )
     )))

 '(consult-preview-insertion
   ((t
     (
      :inherit  region
      )
     )))

 '(consult-preview-line
   ((t
     (
      :extend  t
      :background  "#e8dfd1"
      )
     )))

 '(consult-preview-match
   ((t
     (
      :inherit  match
      )
     )))

 '(consult-separator
   ((t
     (
      :foreground  "#ccc"
      )
     )))

 '(corfu-current
   ((t
     (
      :foreground "black"
      :background "#c1c1ff"
      )
     )))

 '(counsel--mark-ring-highlight
   ((t
     (
      :inherit  highlight
      )
     )))

 '(counsel-active-mode
   ((t
     (
      :foreground  "#5317ac"
      :weight bold
      )
     )))

 '(counsel-application-name
   ((t
     (
      :foreground  "#a0132f"
      )
     )))

 '(counsel-evil-register-face
   ((t
     (
      :inherit  counsel-outline-1
      )
     )))

 '(counsel-key-binding
   ((t
     (
      :inherit  modus-themes-key-binding
      )
     )))

 '(counsel-outline-1
   ((t
     (
      :inherit  org-level-1
      )
     )))

 '(counsel-outline-2
   ((t
     (
      :inherit  org-level-2
      )
     )))

 '(counsel-outline-3
   ((t
     (
      :inherit  org-level-3
      )
     )))

 '(counsel-outline-4
   ((t
     (
      :inherit  org-level-4
      )
     )))

 '(counsel-outline-5
   ((t
     (
      :inherit  org-level-5
      )
     )))

 '(counsel-outline-6
   ((t
     (
      :inherit  org-level-6
      )
     )))

 '(counsel-outline-7
   ((t
     (
      :inherit  org-level-7
      )
     )))

 '(counsel-outline-8
   ((t
     (
      :inherit  org-level-8
      )
     )))

 '(counsel-outline-default
   ((t
     (
      :foreground  "#000000"
      )
     )))

 '(counsel-variable-documentation
   ((t
     (
      :foreground  "#863927"
      :inherit  modus-themes-slant
      )
     )))

 '(cursor
   ((t
     (
      :background  "#f0cc09"
      )
     )))

 '(custom-button
   ((t
     (
      :box (:line-width 2 :color nil :style released-button)
      :foreground  "#000000"
      :background  "#d7d7d7"
      )
     )))

 '(custom-button-mouse
   ((t
     (
      :box (:line-width 2 :color nil :style released-button)
      :foreground  "#0a0a0a"
      :background  "#d7d7d7"
      )
     )))

 '(custom-button-pressed
   ((t
     (
      :box (:line-width 2 :color nil :style pressed-button)
      :foreground  "#000000"
      :background  "#d7d7d7"
      )
     )))

 '(custom-button-pressed-unraised
   ((t
     (
      :foreground  "magenta4"
      :inherit  custom-button-unraised
      )
     )))

 '(custom-button-unraised
   ((t
     (
      :inherit  underline
      )
     )))

 '(custom-changed
   ((t
     (
      :inherit  modus-themes-subtle-cyan
      )
     )))

 '(custom-comment
   ((t
     (
      :inherit  shadow
      )
     )))

 '(custom-comment-tag
   ((t
     (
      :foreground  "#863927"
      :background  "#f0f0f0"
      )
     )))

 '(custom-documentation
   ((t
     (
      )
     )))

 '(custom-face-tag
   ((t
     (
      :foreground  "#1f1fce"
      :inherit  bold
      )
     )))

 '(custom-group-subtitle
   ((t
     (
      :weight  bold
      )
     )))

 '(custom-group-tag
   ((t
     (
      :foreground  "#8f0075"
      :inherit  modus-themes-pseudo-header
      )
     )))

 '(custom-group-tag-1
   ((t
     (
      :inherit  modus-themes-special-warm
      )
     )))

 '(custom-invalid
   ((t
     (
      :inherit (modus-themes-intense-red bold)
      )
     )))

 '(custom-link
   ((t
     (
      :inherit  link
      )
     )))

 '(custom-modified
   ((t
     (
      :inherit  modus-themes-subtle-cyan
      )
     )))

 '(custom-rogue
   ((t
     (
      :inherit  modus-themes-refine-magenta
      )
     )))

 '(custom-saved
   ((t
     (
      :underline  t
      )
     )))

 '(custom-set
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(custom-state
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(custom-themed
   ((t
     (
      :inherit  modus-themes-subtle-blue
      )
     )))

 '(custom-variable-button
   ((t
     (
      :weight  bold
      :underline  t
      )
     )))

 '(custom-variable-obsolete
   ((t
     (
      :foreground  "blue1"
      )
     )))

 '(custom-variable-tag
   ((t
     (
      :foreground  "#00538b"
      :inherit  bold
      )
     )))

 '(custom-visibility
   ((t
     (
      :height  0.8
      :inherit  link
      )
     )))

 '(default
   ((t
     (
      :width  normal
      :height  98
      :weight  regular
      :slant  normal
      :foreground  "#000000"
      :background  "#FFFFFF"
      )
     )))

 '(diary
   ((t
     (
      :foreground  "#0000c0"
      :background  "#f3f3ff"
      )
     )))

 '(diff-added
   ((t
     (
      :extend  t
      :inherit  modus-themes-diff-added
      )
     )))

 '(diff-changed
   ((t
     (
      :extend  t
      :inherit  modus-themes-diff-changed
      )
     )))

 '(diff-context
   ((t
     (
      :extend  t
      :foreground  "#56576d"
      )
     )))

 '(diff-error
   ((t
     (
      :inherit  modus-themes-intense-red
      )
     )))

 '(diff-file-header
   ((t
     (
      :inherit bold diff-header
      )
     )))

 '(diff-function
   ((t
     (
      :inherit  modus-themes-diff-heading
      )
     )))

 '(diff-header
   ((t
     (
      :extend  t
      :foreground  "#000000"
      )
     )))

 '(diff-hl-change
   ((t
     (
      :inherit  modus-themes-fringe-yellow
      )
     )))

 '(diff-hl-delete
   ((t
     (
      :inherit  modus-themes-fringe-red
      )
     )))

 '(diff-hl-dired-change
   ((t
     (
      :inherit  diff-hl-change
      )
     )))

 '(diff-hl-dired-delete
   ((t
     (
      :inherit  diff-hl-delete
      )
     )))

 '(diff-hl-dired-ignored
   ((t
     (
      :inherit  dired-ignored
      )
     )))

 '(diff-hl-dired-insert
   ((t
     (
      :inherit  diff-hl-insert
      )
     )))

 '(diff-hl-dired-unknown
   ((t
     (
      :inherit  dired-ignored
      )
     )))

 '(diff-hl-insert
   ((t
     (
      :inherit  modus-themes-fringe-green
      )
     )))

 '(diff-hl-margin-change
   ((t
     (
      :foreground  "#6666ff"
      )
     )))

 '(diff-hl-margin-delete
   ((t
     (
      :foreground  "#FF6E67"
      )
     )))

 '(diff-hl-margin-ignored
   ((t
     (
      )
     )))

 '(diff-hl-margin-insert
   ((t
     (
      :foreground  "#edc809"
      )
     )))

 '(diff-hl-margin-unknown
   ((t
     (
      :foreground  "#FF6E67"
      )
     )))

 '(diff-hl-reverted-hunk-highlight
   ((t
     (
      :foreground  "#ffffff"
      :background  "#000000"
      )
     )))

 '(diff-hunk-header
   ((t
     (
      :inherit bold modus-themes-diff-heading
      )
     )))

 '(diff-index
   ((t
     (
      :foreground  "#2544bb"
      :inherit  bold
      )
     )))

 '(diff-indicator-added
   ((t
     (
      :foreground  "#005e00"
      :inherit diff-added bold
      )
     )))

 '(diff-indicator-changed
   ((t
     (
      :foreground  "#813e00"
      :inherit diff-changed bold
      )
     )))

 '(diff-indicator-removed
   ((t
     (
      :foreground  "#a60000"
      :inherit diff-removed bold
      )
     )))

 '(diff-nonexistent
   ((t
     (
      :inherit (modus-themes-neutral bold)
      )
     )))

 '(diff-refine-added
   ((t
     (
      :inherit  modus-themes-diff-refine-added
      )
     )))

 '(diff-refine-changed
   ((t
     (
      :inherit  modus-themes-diff-refine-changed
      )
     )))

 '(diff-refine-removed
   ((t
     (
      :inherit  modus-themes-diff-refine-removed
      )
     )))

 '(diff-removed
   ((t
     (
      :extend  t
      :inherit  modus-themes-diff-removed
      )
     )))

 '(dired-broken-symlink
   ((t
     (
      :foreground  "#a60000"
      :inherit  button
      )
     )))

 '(dired-directory
   ((t
     (
      :foreground  "#0031a9"
      )
     )))

 '(dired-filter-group-header
   ((t
     (
      :inherit  header-line
      )
     )))

 '(dired-flagged
   ((t
     (
      :inherit  modus-themes-mark-del
      )
     )))

 '(dired-header
   ((t
     (
      :inherit  modus-themes-pseudo-header
      )
     )))

 '(dired-ignored
   ((t
     (
      :inherit  shadow
      )
     )))

 '(dired-mark
   ((t
     (
      :inherit  modus-themes-mark-symbol
      )
     )))

 '(dired-marked
   ((t
     (
      :inherit  modus-themes-mark-sel
      )
     )))

 '(dired-narrow-blink
   ((t
     (
      :inherit (modus-themes-subtle-cyan bold)
      )
     )))

 '(dired-perm-write
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(dired-set-id
   ((t
     (
      :inherit  font-lock-warning-face
      )
     )))

 '(dired-special
   ((t
     (
      :inherit  font-lock-variable-name-face
      )
     )))

 '(dired-subtree-depth-1-face
   ((t
     (
      )
     )))

 '(dired-subtree-depth-2-face
   ((t
     (
      )
     )))

 '(dired-subtree-depth-3-face
   ((t
     (
      )
     )))

 '(dired-subtree-depth-4-face
   ((t
     (
      )
     )))

 '(dired-subtree-depth-5-face
   ((t
     (
      )
     )))

 '(dired-subtree-depth-6-face
   ((t
     (
      )
     )))

 '(dired-symlink
   ((t
     (
      :underline  t
      :foreground  "#30517f"
      :inherit  button
      )
     )))

 '(dired-warning
   ((t
     (
      :foreground  "#813e00"
      :inherit  bold
      )
     )))

 '(diredfl-autofile-name
   ((t
     (
      :inherit  modus-themes-special-cold
      )
     )))

 '(diredfl-compressed-file-name
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(diredfl-compressed-file-suffix
   ((t
     (
      :foreground  "#972500"
      )
     )))

 '(diredfl-date-time
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(diredfl-deletion
   ((t
     (
      :inherit  modus-themes-mark-del
      )
     )))

 '(diredfl-deletion-file-name
   ((t
     (
      :inherit  modus-themes-mark-del
      )
     )))

 '(diredfl-dir-heading
   ((t
     (
      :inherit  modus-themes-pseudo-header
      )
     )))

 '(diredfl-dir-name
   ((t
     (
      :inherit  dired-directory
      )
     )))

 '(diredfl-dir-priv
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(diredfl-exec-priv
   ((t
     (
      :foreground  "#8f0075"
      )
     )))

 '(diredfl-executable-tag
   ((t
     (
      :foreground  "#8f0075"
      )
     )))

 '(diredfl-file-name
   ((t
     (
      :foreground  "#000000"
      )
     )))

 '(diredfl-file-suffix
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(diredfl-flag-mark
   ((t
     (
      :inherit  modus-themes-mark-sel
      )
     )))

 '(diredfl-flag-mark-line
   ((t
     (
      :inherit  modus-themes-mark-sel
      )
     )))

 '(diredfl-ignored-file-name
   ((t
     (
      :inherit  shadow
      )
     )))

 '(diredfl-link-priv
   ((t
     (
      :foreground  "#0000c0"
      )
     )))

 '(diredfl-no-priv
   ((t
     (
      :foreground  "gray50"
      )
     )))

 '(diredfl-number
   ((t
     (
      :foreground  "#125458"
      )
     )))

 '(diredfl-other-priv
   ((t
     (
      :foreground  "#813e00"
      )
     )))

 '(diredfl-rare-priv
   ((t
     (
      :foreground  "#a60000"
      )
     )))

 '(diredfl-read-priv
   ((t
     (
      :foreground  "#000000"
      )
     )))

 '(diredfl-symlink
   ((t
     (
      :inherit  dired-symlink
      )
     )))

 '(diredfl-tagged-autofile-name
   ((t
     (
      :inherit  modus-themes-refine-magenta
      )
     )))

 '(diredfl-write-priv
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(edebug-disabled-breakpoint
   ((t
     (
      :extend  t
      :background  "#ddffdd"
      )
     )))

 '(edebug-enabled-breakpoint
   ((t
     (
      :inherit  highlight
      )
     )))

 '(eglot-highlight-symbol-face
   ((t
     (
      :inherit  bold
      )
     )))

 '(eglot-mode-line
   ((t
     (
      :foreground  "#5c2092"
      :inherit  modus-themes-bold
      )
     )))

 '(eieio-custom-slot-tag-face
   ((t
     (
      :foreground  "#972500"
      )
     )))

 '(eldoc-highlight-function-argument
   ((t
     (
      :foreground  "#0000c0"
      :inherit  bold
      )
     )))

 '(elisp-shorthand-font-lock-face
   ((t
     (
      :foreground  "cyan"
      :inherit  font-lock-keyword-face
      )
     )))

 '(embark-collect-annotation
   ((t
     (
      :inherit  completions-annotations
      )
     )))

 '(embark-collect-candidate
   ((t
     (
      :inherit  default
      )
     )))

 '(embark-collect-zebra-highlight
   ((t
     (
      :extend  t
      :background  "#efefef"
      )
     )))

 '(embark-keybinding
   ((t
     (
      :inherit  modus-themes-key-binding
      )
     )))

 '(embark-keybinding-repeat
   ((t
     (
      :inherit  font-lock-builtin-face
      )
     )))

 '(embark-keymap
   ((t
     (
      :slant  italic
      )
     )))

 '(embark-target
   ((t
     (
      :inherit  highlight
      )
     )))

 '(embark-verbose-indicator-documentation
   ((t
     (
      :inherit  completions-annotations
      )
     )))

 '(embark-verbose-indicator-shadowed
   ((t
     (
      :inherit  shadow
      )
     )))

 '(embark-verbose-indicator-title
   ((t
     (
      :height  1.1
      :weight  bold
      )
     )))

 '(epa-field-body
   ((t
     (
      :foreground  "#000000"
      )
     )))

 '(epa-field-name
   ((t
     (
      :foreground  "#282828"
      :inherit  bold
      )
     )))

 '(epa-mark
   ((t
     (
      :foreground  "#721045"
      :inherit  bold
      )
     )))

 '(epa-string
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(epa-validity-disabled
   ((t
     (
      :foreground  "#a60000"
      )
     )))

 '(epa-validity-high
   ((t
     (
      :foreground  "#00538b"
      :inherit  bold
      )
     )))

 '(epa-validity-low
   ((t
     (
      :inherit  shadow
      )
     )))

 '(epa-validity-medium
   ((t
     (
      :foreground  "#315b00"
      )
     )))

 '(error
   ((t
     (
      :foreground  "#a60000"
      :inherit  bold
      )
     )))

 '(ert-test-result-expected
   ((t
     (
      :inherit  modus-themes-intense-green
      )
     )))

 '(ert-test-result-unexpected
   ((t
     (
      :inherit  modus-themes-intense-red
      )
     )))

 '(escape-glyph
   ((t
     (
      :foreground  "#8b1030"
      )
     )))

 '(eshell-prompt
   ((t
     (
      :inherit  modus-themes-prompt
      )
     )))

 '(ess-%op%-face
   ((t
     (
      :inherit  ess-operator-face
      )
     )))

 '(ess-assignment-face
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(ess-bp-fringe-browser-face
   ((t
     (
      :foreground  "medium blue"
      )
     )))

 '(ess-bp-fringe-inactive-face
   ((t
     (
      :foreground  "DimGray"
      )
     )))

 '(ess-bp-fringe-logger-face
   ((t
     (
      :foreground  "dark red"
      )
     )))

 '(ess-bp-fringe-recover-face
   ((t
     (
      :foreground  "dark magenta"
      )
     )))

 '(ess-constant-face
   ((t
     (
      :inherit  font-lock-type-face
      )
     )))

 '(ess-debug-blink-ref-not-found-face
   ((t
     (
      :background  "IndianRed4"
      )
     )))

 '(ess-debug-blink-same-ref-face
   ((t
     (
      :background  "steel blue"
      )
     )))

 '(ess-debug-current-debug-line-face
   ((t
     (
      :inherit  highlight
      )
     )))

 '(ess-function-call-face
   ((t
     (
      :weight  bold
      :foreground  "#5317ac"
      )
     )))

 '(ess-keyword-face
   ((t
     (
      :inherit  ess-function-call-face
      )
     )))

 '(ess-matrix-face
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(ess-modifiers-face
   ((t
     (
      :inherit  ess-function-call-face
      )
     )))

 '(ess-numbers-face
   ((t
     (
      :slant  normal
      :inherit  font-lock-type-face
      )
     )))

 '(ess-operator-face
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(ess-paren-face
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(ess-r-control-flow-keyword-face
   ((t
     (
      :inherit  ess-keyword-face
      )
     )))

 '(ess-tracebug-last-input-fringe-face
   ((t
     (
      :overline  "medium blue"
      :foreground  "medium blue"
      )
     )))

 '(ess-watch-current-block-face
   ((t
     (
      :inherit  highlight
      )
     )))

 '(evil-ex-commands
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(evil-ex-info
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(evil-ex-lazy-highlight
   ((t
     (
      :inherit  modus-themes-search-success-lazy
      )
     )))

 '(evil-ex-search
   ((t
     (
      :inherit  modus-themes-search-success
      )
     )))

 '(evil-ex-substitute-matches
   ((t
     (
      :strike-through t
      :bold t
      :background  "#604000"
      :foreground  "#fff29a"
      )
     )))

 '(evil-ex-substitute-replacement
   ((t
     (
      :bold t
      :foreground  "#604000"
      :background  "#fff29a"
      )
     )))

 '(evil-goggles--pulse-face
   ((t
     (
      :background  "#bbeabb"
      )
     )))

 '(evil-goggles-change-face
   ((t
     (
      :inherit  diff-removed
      )
     )))

 '(evil-goggles-commentary-face
   ((t
     (
      :inherit (modus-themes-subtle-neutral modus-themes-slant)
      )
     )))

 '(evil-goggles-default-face
   ((t
     (
      :inherit  modus-themes-subtle-neutral
      )
     )))

 '(evil-goggles-delete-face
   ((t
     (
      :inherit  diff-removed
      )
     )))

 '(evil-goggles-fill-and-move-face
   ((t
     (
      :inherit  evil-goggles-default-face
      )
     )))

 '(evil-goggles-indent-face
   ((t
     (
      :inherit  evil-goggles-default-face
      )
     )))

 '(evil-goggles-join-face
   ((t
     (
      :inherit  modus-themes-subtle-green
      )
     )))

 '(evil-goggles-nerd-commenter-face
   ((t
     (
      :inherit  evil-goggles-commentary-face
      )
     )))

 '(evil-goggles-paste-face
   ((t
     (
      :inherit  diff-added
      )
     )))

 '(evil-goggles-record-macro-face
   ((t
     (
      :inherit  modus-themes-special-cold
      )
     )))

 '(evil-goggles-replace-with-register-face
   ((t
     (
      :inherit  modus-themes-refine-magenta
      )
     )))

 '(evil-goggles-set-marker-face
   ((t
     (
      :inherit  modus-themes-intense-magenta
      )
     )))

 '(evil-goggles-shift-face
   ((t
     (
      :inherit  evil-goggles-default-face
      )
     )))

 '(evil-goggles-surround-face
   ((t
     (
      :inherit  evil-goggles-default-face
      )
     )))

 '(evil-goggles-yank-face
   ((t
     (
      :foreground "#0030b4"
      :weight ultra-bold
      )
     )))

 '(evil-visual-mark-face
   ((t
     (
      :background "#0030b4"
      :foreground "#FFFFFF"
      :weight ultra-bold
      )
     )))

 '(eww-form-checkbox
   ((t
     (
      :inherit  eww-form-text
      )
     )))

 '(eww-form-file
   ((t
     (
      :inherit  eww-form-submit
      )
     )))

 '(eww-form-select
   ((t
     (
      :inherit  eww-form-submit
      )
     )))

 '(eww-form-submit
   ((t
     (
      :box (:line-width 2 :style released-button)
      :background  "#d7d7d7"
      )
     )))

 '(eww-form-text
   ((t
     (
      :box  "#d7d7d7"
      :background  "#f0f0f0"
      )
     )))

 '(eww-form-textarea
   ((t
     (
      :background  "#f0f0f0"
      )
     )))

 '(eww-invalid-certificate
   ((t
     (
      :foreground  "#7f1010"
      )
     )))

 '(eww-valid-certificate
   ((t
     (
      :foreground  "#003497"
      )
     )))

 '(eyebrowse-mode-line-active
   ((t
     (
      :foreground  "#0030b4"
      :inherit  bold
      )
     )))

 '(eyebrowse-mode-line-delimiters
   ((t
     (
      )
     )))

 '(eyebrowse-mode-line-inactive
   ((t
     (
      )
     )))

 '(eyebrowse-mode-line-separator
   ((t
     (
      )
     )))

 '(ffap
   ((t
     (
      :inherit  highlight
      )
     )))

 '(file-name-shadow
   ((t
     (
      :inherit shadow italic
      )
     )))

 '(fill-column-indicator
   ((t
     (
      :foreground  "#d7d7d7"
      )
     )))

 '(fixed-pitch
   ((t
     (
      :width  normal
      :height  100
      :weight  regular
      :slant  normal
      )
     )))

 '(fixed-pitch-serif
   ((t
     (
      )
     )))

 '(flycheck-color-mode-line-error-face
   ((t
     (
      :inherit  flycheck-fringe-error
      )
     )))

 '(flycheck-color-mode-line-info-face
   ((t
     (
      :inherit  flycheck-fringe-info
      )
     )))

 '(flycheck-color-mode-line-running-face
   ((t
     (
      :foreground  "#404148"
      :inherit  italic
      )
     )))

 '(flycheck-color-mode-line-success-face
   ((t
     (
      )
     )))

 '(flycheck-color-mode-line-warning-face
   ((t
     (
      :inherit  flycheck-fringe-warning
      )
     )))

 '(flycheck-delimited-error
   ((t
     (
      )
     )))

 '(flycheck-error
   ((t
     (
      :inherit  modus-themes-lang-error
      )
     )))

 '(flycheck-error-delimiter
   ((t
     (
      )
     )))

 '(flycheck-error-list-checker-name
   ((t
     (
      :foreground  "#5c2092"
      )
     )))

 '(flycheck-error-list-column-number
   ((t
     (
      :foreground  "#093060"
      )
     )))

 '(flycheck-error-list-error
   ((t
     (
      :foreground  "#a60000"
      :inherit  modus-themes-bold
      )
     )))

 '(flycheck-error-list-error-message
   ((t
     (
      )
     )))

 '(flycheck-error-list-filename
   ((t
     (
      :foreground  "#0031a9"
      )
     )))

 '(flycheck-error-list-highlight
   ((t
     (
      :inherit  modus-themes-hl-line
      )
     )))

 '(flycheck-error-list-id
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(flycheck-error-list-id-with-explainer
   ((t
     (
      :box  1
      :inherit  flycheck-error-list-id
      )
     )))

 '(flycheck-error-list-info
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(flycheck-error-list-line-number
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(flycheck-error-list-warning
   ((t
     (
      :foreground  "#813e00"
      )
     )))

 '(flycheck-fringe-error
   ((t
     (
      :inherit  modus-themes-fringe-red
      )
     )))

 '(flycheck-fringe-info
   ((t
     (
      :inherit  modus-themes-fringe-cyan
      )
     )))

 '(flycheck-fringe-warning
   ((t
     (
      :inherit  modus-themes-fringe-yellow
      )
     )))

 '(flycheck-info
   ((t
     (
      :inherit  modus-themes-lang-note
      )
     )))

 '(flycheck-inline-error
   ((t
     (
      :inherit  compilation-error
      )
     )))

 '(flycheck-inline-info
   ((t
     (
      :inherit  compilation-info
      )
     )))

 '(flycheck-inline-warning
   ((t
     (
      :inherit  compilation-warning
      )
     )))

 '(flycheck-verify-select-checker
   ((t
     (
      :box (:line-width 1 :color nil :style released-button)
      )
     )))

 '(flycheck-warning
   ((t
     (
      :inherit  modus-themes-lang-warning
      )
     )))

 '(flymake-error
   ((t
     (
      :inherit  modus-themes-lang-error
      )
     )))

 '(flymake-note
   ((t
     (
      :inherit  modus-themes-lang-note
      )
     )))

 '(flymake-warning
   ((t
     (
      :inherit  modus-themes-lang-warning
      )
     )))

 '(flyspell-duplicate
   ((t
     (
      :inherit  modus-themes-lang-warning
      )
     )))

 '(flyspell-incorrect
   ((t
     (
      :inherit  modus-themes-lang-error
      )
     )))

 '(font-lock-builtin-face
   ((t
     (
      :foreground  "#8f0075"
      :inherit  modus-themes-bold
      )
     )))

 '(font-lock-comment-delimiter-face
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(font-lock-comment-face
   ((t
     (
      :foreground  "#505050"
      :inherit  modus-themes-slant
      )
     )))

 '(font-lock-constant-face
   ((t
     (
      :foreground  "#0000c0"
      )
     )))

 '(font-lock-doc-face
   ((t
     (
      :foreground  "#2a486a"
      :inherit  modus-themes-slant
      )
     )))

 '(font-lock-doc-markup-face
   ((t
     (
      :inherit  font-lock-constant-face
      )
     )))

 '(font-lock-function-name-face
   ((t
     (
      :foreground  "#721045"
      )
     )))

 '(font-lock-keyword-face
   ((t
     (
      :foreground  "#5317ac"
      :inherit  modus-themes-bold
      )
     )))

 '(font-lock-negation-char-face
   ((t
     (
      :foreground  "#813e00"
      :inherit  modus-themes-bold
      )
     )))

 '(font-lock-preprocessor-face
   ((t
     (
      :foreground  "#a0132f"
      )
     )))

 '(font-lock-regexp-grouping-backslash
   ((t
     (
      :foreground  "#654d0f"
      :inherit  bold
      )
     )))

 '(font-lock-regexp-grouping-construct
   ((t
     (
      :foreground  "#8b1030"
      :inherit  bold
      )
     )))

 '(font-lock-string-face
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(font-lock-type-face
   ((t
     (
      :foreground  "#005a5f"
      :inherit  modus-themes-bold
      )
     )))

 '(font-lock-variable-name-face
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(font-lock-warning-face
   ((t
     (
      :foreground  "#702f00"
      :inherit  modus-themes-bold
      )
     )))

 '(forge-post-author
   ((t
     (
      :foreground  "#000000"
      :inherit  bold
      )
     )))

 '(forge-post-date
   ((t
     (
      :foreground  "#093060"
      )
     )))

 '(forge-topic-closed
   ((t
     (
      :inherit  shadow
      )
     )))

 '(forge-topic-merged
   ((t
     (
      :inherit  shadow
      )
     )))

 '(forge-topic-open
   ((t
     (
      :foreground  "#184034"
      )
     )))

 '(forge-topic-unmerged
   ((t
     (
      :foreground  "#721045"
      :inherit  modus-themes-slant
      )
     )))

 '(forge-topic-unread
   ((t
     (
      :foreground  "#000000"
      :inherit  bold
      )
     )))

 '(fringe
   ((t
     (
      :foreground  "#000000"
      :background  "#ffffff"
      :inherit  default
      )
     )))

 '(funk/modeline-buffer-file
   ((t
     (
      :inherit (mode-line-buffer-id regular)
      )
     )))

 '(funk/modeline-buffer-file-modified
   ((t
     (
      :foreground  "#c1c1ff"
      )
     )))

 '(funk/modeline-buffer-path
   ((t
     (
      :weight  bold
      :foreground  "#5317ac"
      )
     )))

 '(funk/modeline-project-dir
   ((t
     (
      :weight  bold
      :foreground  "#edc809"
      )
     )))

 '(funk/modeline-project-parent-dir
   ((t
     (
      :weight  bold
      :foreground  "#2544bb"
      )
     )))

 '(doom-modeline-buffer-file
   ((t
     (
      :inherit (mode-line-buffer-id regular)
      )
     )))

 '(doom-modeline-buffer-file-modified
   ((t
     (
      :foreground  "#c1c1ff"
      )
     )))

 '(doom-modeline-buffer-path
   ((t
     (
      :weight  bold
      :foreground  "#5317ac"
      )
     )))

 '(doom-modeline-project-dir
   ((t
     (
      :weight  bold
      :foreground  "#edc809"
      )
     )))

 '(doom-modeline-project-parent-dir
   ((t
     (
      :weight  bold
      :foreground  "#2544bb"
      )
     )))

 '(doom-modeline-urgent
   ((t
     (
      :foreground  "#edc809"
      :weight bold
      )
     )))

 '(doom-modeline-info
   ((t
     (
      :foreground  "jedc809"
      :weight bold
      )
     )))

 '(doom-modeline-evil-emacs-state
   ((t
     (
      :foreground  "#000000"
      :weight bold
      )
     )))

 '(doom-modeline-evil-insert-state
   ((t
     (
      :foreground  "#edc809"
      :weight bold
      )
     )))

 '(doom-modeline-evil-motion-state
   ((t
     (
      :foreground  "#edc809"
      :weight bold
      )
     )))

 '(doom-modeline-evil-normal-state
   ((t
     (
      :foreground  "#edc809"
      :weight bold
      )
     )))

 '(doom-modeline-evil-operator-state
   ((t
     (
      :foreground  "#edc809"
      :weight bold
      )
     )))

 '(doom-modeline-evil-visual-state
   ((t
     (
      :foreground  "#edc809"
      :weight bold
      )
     )))

 '(doom-modeline-evil-replace-state
   ((t
     (
      :foreground  "#edc809"
      :weight bold
      )
     )))

 '(git-commit-comment-action
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(git-commit-comment-branch-local
   ((t
     (
      :foreground  "#2544bb"
      :inherit  modus-themes-slant
      )
     )))

 '(git-commit-comment-branch-remote
   ((t
     (
      :foreground  "#8f0075"
      :inherit  modus-themes-slant
      )
     )))

 '(git-commit-comment-detached
   ((t
     (
      :foreground  "#30517f"
      :inherit  modus-themes-slant
      )
     )))

 '(git-commit-comment-file
   ((t
     (
      :foreground  "#093060"
      :inherit  modus-themes-slant
      )
     )))

 '(git-commit-comment-heading
   ((t
     (
      :foreground  "#282828"
      :inherit bold modus-themes-slant
      )
     )))

 '(git-commit-keyword
   ((t
     (
      :foreground  "#721045"
      )
     )))

 '(git-commit-known-pseudo-header
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(git-commit-nonempty-second-line
   ((t
     (
      :inherit  modus-themes-refine-yellow
      )
     )))

 '(git-commit-overlong-summary
   ((t
     (
      :inherit  modus-themes-refine-yellow
      )
     )))

 '(git-commit-pseudo-header
   ((t
     (
      :foreground  "#0031a9"
      )
     )))

 '(git-commit-summary
   ((t
     (
      :foreground  "#00538b"
      :inherit  bold
      )
     )))

 '(git-gutter+-added
   ((t
     (
      :foreground  "#edc809"
      :background  "#FFFFFF"
      :inherit  modus-themes-fringe-green
      )
     )))

 '(git-gutter+-commit-header-face
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(git-gutter+-deleted
   ((t
     (
      :foreground  "#FF6E67"
      :background  "#FFFFFF"
      :inherit  modus-themes-fringe-red
      )
     )))

 '(git-gutter+-modified
   ((t
     (
      :foreground  "#6666ff"
      :background  "#FFFFFF"
      :inherit  modus-themes-fringe-yellow
      )
     )))

 '(git-gutter+-separator
   ((t
     (
      :inherit  modus-themes-fringe-cyan
      )
     )))

 '(git-gutter+-unchanged
   ((t
     (
      :inherit  modus-themes-fringe-magenta
      )
     )))

 '(git-gutter:added
   ((t
     (
      :foreground  "#62c86a"
      :background  "#FFFFFF"
      )
     )))

 '(git-gutter:deleted
   ((t
     (
      :foreground  "#f08290"
      :background  "#FFFFFF"
      )
     )))

 '(git-gutter:modified
   ((t
     (
      :foreground  "#dbba3f"
      :background  "#FFFFFF"
      )
     )))

 '(git-gutter:separator
   ((t
     (
      :inherit  modus-themes-fringe-cyan
      )
     )))

 '(git-gutter:unchanged
   ((t
     (
      :inherit  modus-themes-fringe-magenta
      )
     )))

 '(git-timemachine-commit
   ((t
     (
      :foreground  "#702f00"
      :inherit  bold
      )
     )))

 '(git-timemachine-minibuffer-author-face
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(git-timemachine-minibuffer-detail-face
   ((t
     (
      :foreground  "#972500"
      )
     )))

 '(glyphless-char
   ((t
     (
      :height  0.6
      )
     )))

 '(gnus-group-mail-1
   ((t
     (
      :foreground  "#8f0075"
      :inherit  bold
      )
     )))

 '(gnus-group-mail-1-empty
   ((t
     (
      :foreground  "#8f0075"
      )
     )))

 '(gnus-group-mail-2
   ((t
     (
      :foreground  "#721045"
      :inherit  bold
      )
     )))

 '(gnus-group-mail-2-empty
   ((t
     (
      :foreground  "#721045"
      )
     )))

 '(gnus-group-mail-3
   ((t
     (
      :foreground  "#5317ac"
      :inherit  bold
      )
     )))

 '(gnus-group-mail-3-empty
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(gnus-group-mail-low
   ((t
     (
      :foreground  "#541f4f"
      :inherit  bold
      )
     )))

 '(gnus-group-mail-low-empty
   ((t
     (
      :foreground  "#541f4f"
      )
     )))

 '(gnus-group-news-1
   ((t
     (
      :foreground  "#005e00"
      :inherit  bold
      )
     )))

 '(gnus-group-news-1-empty
   ((t
     (
      :foreground  "#005e00"
      )
     )))

 '(gnus-group-news-2
   ((t
     (
      :foreground  "#00538b"
      :inherit  bold
      )
     )))

 '(gnus-group-news-2-empty
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(gnus-group-news-3
   ((t
     (
      :foreground  "#3f3000"
      :inherit  bold
      )
     )))

 '(gnus-group-news-3-empty
   ((t
     (
      :foreground  "#3f3000"
      )
     )))

 '(gnus-group-news-4
   ((t
     (
      :foreground  "#0f3360"
      :inherit  bold
      )
     )))

 '(gnus-group-news-4-empty
   ((t
     (
      :foreground  "#0f3360"
      )
     )))

 '(gnus-group-news-5
   ((t
     (
      :foreground  "#5f0000"
      :inherit  bold
      )
     )))

 '(gnus-group-news-5-empty
   ((t
     (
      :foreground  "#5f0000"
      )
     )))

 '(gnus-group-news-6
   ((t
     (
      :foreground  "#56576d"
      :inherit  bold
      )
     )))

 '(gnus-group-news-6-empty
   ((t
     (
      :foreground  "#56576d"
      )
     )))

 '(gnus-group-news-low
   ((t
     (
      :foreground  "#004000"
      :inherit  bold
      )
     )))

 '(gnus-group-news-low-empty
   ((t
     (
      :foreground  "#004000"
      )
     )))

 '(gnus-splash
   ((t
     (
      :inherit  shadow
      )
     )))

 '(gnus-summary-cancelled
   ((t
     (
      :extend  t
      :inherit  modus-themes-mark-alt
      )
     )))

 '(gnus-summary-high-ancient
   ((t
     (
      :foreground  "#505050"
      :inherit  bold
      )
     )))

 '(gnus-summary-high-read
   ((t
     (
      :foreground  "#093060"
      :inherit  bold
      )
     )))

 '(gnus-summary-high-ticked
   ((t
     (
      :foreground  "#a0132f"
      :inherit  bold
      )
     )))

 '(gnus-summary-high-undownloaded
   ((t
     (
      :foreground  "#813e00"
      :inherit  bold
      )
     )))

 '(gnus-summary-high-unread
   ((t
     (
      :foreground  "#000000"
      :inherit  bold
      )
     )))

 '(gnus-summary-low-ancient
   ((t
     (
      :foreground  "#505050"
      :inherit  italic
      )
     )))

 '(gnus-summary-low-read
   ((t
     (
      :foreground  "#505050"
      :inherit  italic
      )
     )))

 '(gnus-summary-low-ticked
   ((t
     (
      :foreground  "#780000"
      :inherit  italic
      )
     )))

 '(gnus-summary-low-undownloaded
   ((t
     (
      :foreground  "#604000"
      :inherit  italic
      )
     )))

 '(gnus-summary-low-unread
   ((t
     (
      :foreground  "#093060"
      :inherit  bold
      )
     )))

 '(gnus-summary-normal-ancient
   ((t
     (
      :extend  t
      :foreground  "#61284f"
      )
     )))

 '(gnus-summary-normal-read
   ((t
     (
      :extend  t
      :inherit  shadow
      )
     )))

 '(gnus-summary-normal-ticked
   ((t
     (
      :extend  t
      :foreground  "#a0132f"
      )
     )))

 '(gnus-summary-normal-undownloaded
   ((t
     (
      :extend  t
      :foreground  "#813e00"
      )
     )))

 '(gnus-summary-normal-unread
   ((t
     (
      :extend  t
      :foreground  "#000000"
      )
     )))

 '(gnus-summary-selected
   ((t
     (
      :extend  t
      :inherit  highlight
      )
     )))

 '(header-line
   ((t
     (
      :foreground  "#2a2a2a"
      :background  "#e5e5e5"
      )
     )))

 '(header-line-highlight
   ((t
     (
      :inherit  modus-themes-active-blue
      )
     )))

 '(helm-action
   ((t
     (
      :underline  t
      :extend  t
      )
     )))

 '(helm-buffer-archive
   ((t
     (
      :extend  t
      :foreground  "#00538b"
      :inherit  bold
      )
     )))

 '(helm-buffer-directory
   ((t
     (
      :extend  t
      :foreground  "#0031a9"
      :inherit  bold
      )
     )))

 '(helm-buffer-file
   ((t
     (
      :extend  t
      :foreground  "#000000"
      )
     )))

 '(helm-buffer-modified
   ((t
     (
      :extend  t
      :foreground  "#70480f"
      )
     )))

 '(helm-buffer-not-saved
   ((t
     (
      :extend  t
      :foreground  "#972500"
      )
     )))

 '(helm-buffer-process
   ((t
     (
      :extend  t
      :foreground  "#721045"
      )
     )))

 '(helm-buffer-saved-out
   ((t
     (
      :extend  t
      :foreground  "#a60000"
      :background  "#f0f0f0"
      :inherit  bold
      )
     )))

 '(helm-buffer-size
   ((t
     (
      :extend  t
      :inherit  shadow
      )
     )))

 '(helm-candidate-number
   ((t
     (
      :extend  t
      :foreground  "#003f8a"
      )
     )))

 '(helm-candidate-number-suspended
   ((t
     (
      :extend  t
      :foreground  "#702f00"
      )
     )))

 '(helm-delete-async-message
   ((t
     (
      :extend  t
      :foreground  "#5c2092"
      :inherit  bold
      )
     )))

 '(helm-eob-line
   ((t
     (
      :extend  t
      :foreground  "#000000"
      :background  "#ffffff"
      )
     )))

 '(helm-etags-file
   ((t
     (
      :underline  t
      :extend  t
      :foreground  "#282828"
      )
     )))

 '(helm-ff-backup-file
   ((t
     (
      :extend  t
      :inherit  shadow
      )
     )))

 '(helm-ff-denied
   ((t
     (
      :extend  t
      :inherit (modus-themes-intense-red nil)
      )
     )))

 '(helm-ff-directory
   ((t
     (
      :inherit  helm-buffer-directory
      )
     )))

 '(helm-ff-dirs
   ((t
     (
      :extend  t
      :foreground  "#0000c0"
      :inherit  bold
      )
     )))

 '(helm-ff-dotted-directory
   ((t
     (
      :extend  t
      :foreground  "#505050"
      :background  "#f0f0f0"
      :inherit  bold
      )
     )))

 '(helm-ff-dotted-symlink-directory
   ((t
     (
      :inherit button helm-ff-dotted-directory
      )
     )))

 '(helm-ff-executable
   ((t
     (
      :extend  t
      :foreground  "#8f0075"
      )
     )))

 '(helm-ff-file
   ((t
     (
      :extend  t
      :foreground  "#000000"
      )
     )))

 '(helm-ff-file-extension
   ((t
     (
      :extend  t
      :foreground  "#5d3026"
      )
     )))

 '(helm-ff-invalid-symlink
   ((t
     (
      :underline  t
      :extend  t
      :foreground  "#a60000"
      :inherit  button
      )
     )))

 '(helm-ff-pipe
   ((t
     (
      :extend  t
      :inherit (modus-themes-subtle-magenta nil)
      )
     )))

 '(helm-ff-prefix
   ((t
     (
      :extend  t
      :inherit (modus-themes-subtle-yellow nil)
      )
     )))

 '(helm-ff-rsync-progress
   ((t
     (
      :extend  t
      :inherit  font-lock-warning-face
      )
     )))

 '(helm-ff-socket
   ((t
     (
      :extend  t
      :foreground  "#a0132f"
      )
     )))

 '(helm-ff-suid
   ((t
     (
      :extend  t
      :inherit (modus-themes-refine-red nil)
      )
     )))

 '(helm-ff-symlink
   ((t
     (
      :underline  t
      :extend  t
      :foreground  "#00538b"
      :inherit  button
      )
     )))

 '(helm-ff-truename
   ((t
     (
      :extend  t
      :foreground  "#0000c0"
      )
     )))

 '(helm-grep-cmd-line
   ((t
     (
      :extend  t
      :foreground  "#863927"
      )
     )))

 '(helm-grep-file
   ((t
     (
      :extend  t
      :foreground  "#093060"
      :inherit  bold
      )
     )))

 '(helm-grep-finish
   ((t
     (
      :extend  t
      :foreground  "#004c2e"
      )
     )))

 '(helm-grep-lineno
   ((t
     (
      :extend  t
      :foreground  "#5d3026"
      )
     )))

 '(helm-grep-match
   ((t
     (
      :extend  t
      :inherit  modus-themes-special-calm
      )
     )))

 '(helm-header
   ((t
     (
      :extend  t
      :foreground  "#093060"
      :inherit  bold
      )
     )))

 '(helm-header-line-left-margin
   ((t
     (
      :extend  t
      :foreground  "#605b00"
      :inherit  bold
      )
     )))

 '(helm-helper
   ((t
     (
      :extend  t
      :inherit  helm-header
      )
     )))

 '(helm-history-deleted
   ((t
     (
      :extend  t
      :inherit (modus-themes-intense-red bold)
      )
     )))

 '(helm-history-remote
   ((t
     (
      :extend  t
      :foreground  "#a0132f"
      )
     )))

 '(helm-locate-finish
   ((t
     (
      :extend  t
      :foreground  "#004c2e"
      )
     )))

 '(helm-mark-prefix
   ((t
     (
      :inherit  default
      )
     )))

 '(helm-match
   ((t
     (
      :extend  t
      :inherit (modus-themes-refine-cyan bold)
      )
     )))

 '(helm-match-item
   ((t
     (
      :extend  t
      :inherit (modus-themes-subtle-cyan nil)
      )
     )))

 '(helm-minibuffer-prompt
   ((t
     (
      :extend  t
      :inherit  modus-themes-prompt
      )
     )))

 '(helm-moccur-buffer
   ((t
     (
      :underline  t
      :extend  t
      :foreground  "#005a5f"
      :inherit  button
      )
     )))

 '(helm-no-file-buffer-modified
   ((t
     (
      :extend  t
      :foreground  "orange"
      :background  "black"
      )
     )))

 '(helm-non-file-buffer
   ((t
     (
      :extend  t
      :inherit  shadow
      )
     )))

 '(helm-org-rifle-separator
   ((t
     (
      :foreground  "black"
      )
     )))

 '(helm-prefarg
   ((t
     (
      :extend  t
      :foreground  "#8a0000"
      )
     )))

 '(helm-resume-need-update
   ((t
     (
      :extend  t
      :inherit (modus-themes-refine-magenta nil)
      )
     )))

 '(helm-selection
   ((t
     (
      :extend  t
      :inherit (modus-themes-refine-blue bold)
      )
     )))

 '(helm-selection-line
   ((t
     (
      :extend  t
      :inherit  modus-themes-special-cold
      )
     )))

 '(helm-separator
   ((t
     (
      :extend  t
      :foreground  "#184034"
      )
     )))

 '(helm-source-header
   ((t
     (
      :extend  t
      :foreground  "#972500"
      :inherit  bold
      )
     )))

 '(helm-visible-mark
   ((t
     (
      :extend  t
      :inherit  modus-themes-subtle-cyan
      )
     )))

 '(help-argument-name
   ((t
     (
      :foreground  "#00538b"
      :inherit  modus-themes-slant
      )
     )))

 '(help-for-help-header
   ((t
     (
      :height  1.26
      )
     )))

 '(helpful-heading
   ((t
     (
      :inherit  modus-themes-heading-1
      )
     )))

 '(highlight
   ((t
     (
      :foreground  "#000000"
      :background  "#b5d0ff"
      )
     )))

 '(highlight-parentheses-highlight
   ((t
     (
      )
     )))

 '(highlight-indent-guides-stack-character-face ((t (:foreground "lightgray"))))


 '(highlight-indent-guides-character-face ((t (:foreground "lightgray"))))

 '(highlight-indent-guides-odd-face ((t (:foreground "lightgray"))))

 '(highlight-indent-guides-even-face ((t (:foreground "lightgray"))))

 '(highlight-indent-guides-top-odd-face ((t (:foreground "lightgray"))))

 '(highlight-indent-guides-top-even-face ((t (:foreground "lightgray"))))

 '(highlight-indent-guides-stack-odd-face ((t (:foreground "lightgray"))))

 '(highlight-indent-guides-stack-even-face ((t (:foreground "lightgray"))))

 '(highlight-indent-guides-top-character-face ((t (:foreground "lightgray"))))

 '(hl-DONE-TODO
   ((t
     (
      :weight  ultra-bold
      :foreground  "orange"
      )
     )))

 '(hl-line
   ((t
     (
      :extend  t
      :background  "#f2eff3"
      :inherit  modus-themes-hl-line
      )
     )))

 '(hl-note-TODO
   ((t
     (
      :foreground  "#FFFFFF"
      :background  "#6666ff"
      :inherit hl-todo
      )
     )))

 '(hl-todo
   ((t
     (
      :foreground  "#a0132f"
      :inherit bold modus-themes-slant
      )
     )))

 '(hl-todo-TODO
   ((t
     (
      :foreground  "#00ff00"
      :background  "#ff0000"
      :inherit hl-todo
      )
     )))

 '(holiday
   ((t
     (
      :foreground  "#8f0075"
      :background  "#fdf0ff"
      )
     )))

 '(homoglyph
   ((t
     (
      :foreground  "#702f00"
      )
     )))

 '(hydra-face-amaranth
   ((t
     (
      :foreground  "#70480f"
      :inherit  bold
      )
     )))

 '(hydra-face-blue
   ((t
     (
      :foreground  "#0031a9"
      :inherit  bold
      )
     )))

 '(hydra-face-pink
   ((t
     (
      :foreground  "#7b206f"
      :inherit  bold
      )
     )))

 '(hydra-face-red
   ((t
     (
      :foreground  "#7f1010"
      :inherit  bold
      )
     )))

 '(hydra-face-teal
   ((t
     (
      :foreground  "#005a5f"
      :inherit  bold
      )
     )))

 '(ibuffer-locked-buffer
   ((t
     (
      :foreground  "#5e3a20"
      )
     )))

 '(ido-first-match
   ((t
     (
      :foreground  "#721045"
      :inherit  bold
      )
     )))

 '(ido-incomplete-regexp
   ((t
     (
      :inherit  error
      )
     )))

 '(ido-indicator
   ((t
     (
      :inherit  modus-themes-subtle-yellow
      )
     )))

 '(ido-only-match
   ((t
     (
      :foreground  "#005e00"
      :inherit  bold
      )
     )))

 '(ido-subdir
   ((t
     (
      :foreground  "#0031a9"
      )
     )))

 '(ido-virtual
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(iedit-occurrence
   ((t
     (
      :foreground  "#002f88"
      :background  "#8fcfff"
      :weight bold
      )
     )))

 '(iedit-read-only-occurrence
   ((t
     (
      :inherit  modus-themes-intense-yellow
      )
     )))

 '(info-header-node
   ((t
     (
      :inherit shadow bold
      )
     )))

 '(info-header-xref
   ((t
     (
      :foreground  "#0030b4"
      )
     )))

 '(info-index-match
   ((t
     (
      :inherit  match
      )
     )))

 '(info-menu-header
   ((t
     (
      :inherit  modus-themes-heading-3
      )
     )))

 '(info-menu-star
   ((t
     (
      :foreground  "#a60000"
      )
     )))

 '(info-node
   ((t
     (
      :inherit  bold
      )
     )))

 '(info-title-1
   ((t
     (
      :inherit  modus-themes-heading-1
      )
     )))

 '(info-title-2
   ((t
     (
      :inherit  modus-themes-heading-2
      )
     )))

 '(info-title-3
   ((t
     (
      :inherit  modus-themes-heading-3
      )
     )))

 '(info-title-4
   ((t
     (
      :inherit  modus-themes-heading-4
      )
     )))

 '(info-xref
   ((t
     (
      :inherit  link
      )
     )))

 '(info-xref-visited
   ((t
     (
      :inherit link-visited info-xref
      )
     )))

 '(internal-border
   ((t
     (
      )
     )))

 '(isearch
   ((t
     (
      :foreground  "#000000"
      :background  "#5ada88"
      )
     )))

 '(isearch-fail
   ((t
     (
      :foreground  "#780000"
      :background  "#ffcccc"
      )
     )))

 '(isearch-group-1
   ((t
     (
      :inherit  modus-themes-refine-blue
      )
     )))

 '(isearch-group-2
   ((t
     (
      :inherit  modus-themes-refine-magenta
      )
     )))

 '(italic
   ((t
     (
      :slant  italic
      )
     )))

 '(ivy-action
   ((t
     (
      :foreground  "#972500"
      :inherit  bold
      )
     )))

 '(ivy-completions-annotations
   ((t
     (
      :inherit  completions-annotations
      )
     )))

 '(ivy-confirm-face
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(ivy-current-match
   ((t
     (
      :background  "#c1c1ff"
      )
     )))

 '(ivy-cursor
   ((t
     (
      :foreground  "#ffffff"
      :background  "#000000"
      )
     )))

 '(ivy-grep-info
   ((t
     (
      :foreground  "#30517f"
      )
     )))

 '(ivy-grep-line-number
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(ivy-highlight-face
   ((t
     (
      :foreground  "#721045"
      )
     )))

 '(ivy-match-required-face
   ((t
     (
      :inherit  error
      )
     )))

 '(ivy-minibuffer-match-face-1
   ((t
     (
      :inherit (modus-themes-intense-neutral bold)
      )
     )))

 '(ivy-minibuffer-match-face-2
   ((t
     (
      :inherit (modus-themes-refine-green bold)
      )
     )))

 '(ivy-minibuffer-match-face-3
   ((t
     (
      :inherit (modus-themes-refine-blue bold)
      )
     )))

 '(ivy-minibuffer-match-face-4
   ((t
     (
      :inherit (modus-themes-refine-magenta bold)
      )
     )))

 '(ivy-minibuffer-match-highlight
   ((t
     (
      :inherit (modus-themes-intense-cyan bold)
      )
     )))

 '(ivy-modified-buffer
   ((t
     (
      :foreground  "#813e00"
      :inherit  modus-themes-slant
      )
     )))

 '(ivy-modified-outside-buffer
   ((t
     (
      :foreground  "#70480f"
      :inherit  modus-themes-slant
      )
     )))

 '(ivy-org
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(ivy-prompt-match
   ((t
     (
      :inherit  ivy-current-match
      )
     )))

 '(ivy-remote
   ((t
     (
      :foreground  "#721045"
      )
     )))

 '(ivy-separator
   ((t
     (
      :inherit  shadow
      )
     )))

 '(ivy-subdir
   ((t
     (
      :foreground  "#0000c0"
      )
     )))

 '(ivy-virtual
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(ivy-yanked-word
   ((t
     (
      :inherit (modus-themes-refine-blue nil)
      )
     )))

 '(jupyter-eval-overlay
   ((t
     (
      :foreground  "#0031a9"
      :inherit  bold
      )
     )))

 '(jupyter-repl-input-prompt
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(jupyter-repl-output-prompt
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(jupyter-repl-traceback
   ((t
     (
      :inherit  modus-themes-intense-red
      )
     )))

 '(langtool-correction-face
   ((t
     (
      :weight  bold
      :foreground  "yellow"
      :background  "red1"
      )
     )))

 '(langtool-errline
   ((t
     (
      :background  "LightPink"
      )
     )))

 '(lazy-highlight
   ((t
     (
      :inherit  modus-themes-search-success-lazy
      )
     )))

 '(line-number
   ((t
     (
      :foreground  "#505050"
      :background  "#f8f8f8"
      :inherit  default
      )
     )))

 '(line-number-current-line
   ((t
     (
      :foreground  "#000000"
      :background  "#d7d7d7"
      :inherit bold default
      )
     )))

 '(line-number-major-tick
   ((t
     (
      :foreground  "#3f3000"
      :background  "#fff3da"
      :inherit bold default
      )
     )))

 '(line-number-minor-tick
   ((t
     (
      :foreground  "#505050"
      :background  "#efefef"
      :inherit bold default
      )
     )))

 '(link
   ((t
     (
      :weight bold
      :inherit  button
      )
     )))

 '(link-visited
   ((t
     (
      :inherit font-lock-comment-face
      )
     )))

 '(log-edit-header
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(log-edit-summary
   ((t
     (
      :foreground  "#0031a9"
      :inherit  bold
      )
     )))

 '(log-edit-unknown-header
   ((t
     (
      :inherit  shadow
      )
     )))

 '(log-view-commit-body
   ((t
     (
      :foreground  "#201f55"
      )
     )))

 '(log-view-file
   ((t
     (
      :extend  t
      :foreground  "#093060"
      :inherit  bold
      )
     )))

 '(log-view-message
   ((t
     (
      :extend  t
      :foreground  "#505050"
      :background  "#f0f0f0"
      )
     )))

 '(lsp-details-face
   ((t
     (
      :height  0.8
      :inherit  shadow
      )
     )))

 '(lsp-face-highlight-read
   ((t
     (
      :underline  t
      :inherit  modus-themes-subtle-blue
      )
     )))

 '(lsp-face-highlight-textual
   ((t
     (
      :inherit  modus-themes-subtle-blue
      )
     )))

 '(lsp-face-highlight-write
   ((t
     (
      :inherit (modus-themes-refine-blue bold)
      )
     )))

 '(lsp-face-rename
   ((t
     (
      :underline  t
      )
     )))

 '(lsp-installation-buffer-face
   ((t
     (
      :foreground  "green"
      )
     )))

 '(lsp-installation-finished-buffer-face
   ((t
     (
      :foreground  "orange"
      )
     )))

 '(lsp-rename-placeholder-face
   ((t
     (
      :inherit  font-lock-variable-name-face
      )
     )))

 '(lsp-signature-face
   ((t
     (
      :inherit  lsp-details-face
      )
     )))

 '(lsp-signature-posframe
   ((t
     (
      :inherit  tooltip
      )
     )))

 '(lsp-ui-doc-background
   ((t
     (
      :background  "#f0f0f0"
      )
     )))

 '(lsp-ui-doc-header
   ((t
     (
      :foreground  "#2a2a2a"
      :background  "#e5e5e5"
      )
     )))

 '(lsp-ui-doc-highlight-hover
   ((t
     (
      :inherit  region
      )
     )))

 '(lsp-ui-doc-url
   ((t
     (
      :inherit  button
      )
     )))

 '(lsp-ui-peek-filename
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(lsp-ui-peek-footer
   ((t
     (
      :foreground  "#2a2a2a"
      :background  "#e5e5e5"
      )
     )))

 '(lsp-ui-peek-header
   ((t
     (
      :foreground  "#2a2a2a"
      :background  "#e5e5e5"
      )
     )))

 '(lsp-ui-peek-highlight
   ((t
     (
      :inherit  modus-themes-subtle-blue
      )
     )))

 '(lsp-ui-peek-line-number
   ((t
     (
      :inherit  shadow
      )
     )))

 '(lsp-ui-peek-list
   ((t
     (
      :background  "#f8f8f8"
      )
     )))

 '(lsp-ui-peek-peek
   ((t
     (
      :background  "#f0f0f0"
      )
     )))

 '(lsp-ui-peek-selection
   ((t
     (
      :inherit  modus-themes-subtle-cyan
      )
     )))

 '(lsp-ui-sideline-code-action
   ((t
     (
      :foreground  "#813e00"
      )
     )))

 '(lsp-ui-sideline-current-symbol
   ((t
     (
      :height  0.99
      :box (:line-width -1 :style nil)
      :foreground  "#000000"
      :inherit  bold
      )
     )))

 '(lsp-ui-sideline-global
   ((t
     (
      )
     )))

 '(lsp-ui-sideline-symbol
   ((t
     (
      :height  0.99
      :box (:line-width -1 :style nil)
      :foreground  "#505050"
      :inherit  bold
      )
     )))

 '(lsp-ui-sideline-symbol-info
   ((t
     (
      :height  0.99
      :inherit  italic
      )
     )))

 '(lv-separator
   ((t
     (
      :background  "grey80"
      )
     )))

 '(magit-bisect-bad
   ((t
     (
      :foreground  "#a0132f"
      )
     )))

 '(magit-bisect-good
   ((t
     (
      :foreground  "#145c33"
      )
     )))

 '(magit-bisect-skip
   ((t
     (
      :foreground  "#863927"
      )
     )))

 '(magit-blame-date
   ((t
     (
      :foreground  "#0031a9"
      )
     )))

 '(magit-blame-dimmed
   ((t
     (
      :inherit shadow modus-themes-reset-hard
      )
     )))

 '(magit-blame-hash
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(magit-blame-heading
   ((t
     (
      :extend  t
      :background  "#f0f0f0"
      :inherit  modus-themes-reset-hard
      )
     )))

 '(magit-blame-highlight
   ((t
     (
      :inherit  modus-themes-nuanced-cyan
      )
     )))

 '(magit-blame-margin
   ((t
     (
      :inherit magit-blame-highlight modus-themes-reset-hard
      )
     )))

 '(magit-blame-name
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(magit-blame-summary
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(magit-branch-current
   ((t
     (
      :box  1
      :foreground  "#0000c0"
      )
     )))

 '(magit-branch-local
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(magit-branch-remote
   ((t
     (
      :foreground  "#8f0075"
      )
     )))

 '(magit-branch-remote-head
   ((t
     (
      :box  1
      :foreground  "#5317ac"
      )
     )))

 '(magit-branch-upstream
   ((t
     (
      :inherit  italic
      )
     )))

 '(magit-branch-warning
   ((t
     (
      :inherit  warning
      )
     )))

 '(magit-cherry-equivalent
   ((t
     (
      :foreground  "#a8007f"
      :background  "#ffffff"
      )
     )))

 '(magit-cherry-unmatched
   ((t
     (
      :foreground  "#005f88"
      :background  "#ffffff"
      )
     )))

 '(magit-diff-added
   ((t
     (
      :extend  t
      :foreground  "#004500"
      :background  "#d4fad4"
      )
     )))

 '(magit-diff-added-highlight
   ((t
     (
      :foreground  "#002c00"
      :background  "#bbeabb"
      )
     )))

 '(magit-diff-base
   ((t
     (
      :extend  t
      :foreground  "#524200"
      :background  "#fcefcf"
      )
     )))

 '(magit-diff-base-highlight
   ((t
     (
      :extend  t
      :inherit  modus-themes-diff-focus-changed
      )
     )))

 '(magit-diff-conflict-heading
   ((t
     (
      :inherit  magit-diff-hunk-heading
      )
     )))

 '(magit-diff-context
   ((t
     (
      :extend  t
      :foreground  "#56576d"
      )
     )))

 '(magit-diff-context-highlight
   ((t
     (
      :extend  t
      :foreground  "#404148"
      :background  "#efefef"
      )
     )))

 '(magit-diff-file-heading
   ((t
     (
      :extend  t
      :foreground  "#093060"
      :inherit  bold
      )
     )))

 '(magit-diff-file-heading-highlight
   ((t
     (
      :extend  t
      :inherit (modus-themes-special-cold bold)
      )
     )))

 '(magit-diff-file-heading-selection
   ((t
     (
      :extend  t
      :inherit  modus-themes-refine-cyan
      )
     )))

 '(magit-diff-hunk-heading
   ((t
     (
      :extend  t
      :foreground  "#404148"
      :background  "#d7d7d7"
      :inherit  bold
      )
     )))

 '(magit-diff-hunk-heading-highlight
   ((t
     (
      :extend  t
      :foreground  "#041645"
      :background  "#b7cfe0"
      :inherit  bold
      )
     )))

 '(magit-diff-hunk-heading-selection
   ((t
     (
      :extend  t
      :inherit  modus-themes-refine-blue
      )
     )))

 '(magit-diff-hunk-region
   ((t
     (
      :extend  t
      :inherit  bold
      )
     )))

 '(magit-diff-lines-boundary
   ((t
     (
      :extend  t
      :background  "#000000"
      )
     )))

 '(magit-diff-lines-heading
   ((t
     (
      :extend  t
      :inherit  modus-themes-refine-magenta
      )
     )))

 '(magit-diff-our
   ((t
     (
      :inherit  magit-diff-removed
      )
     )))

 '(magit-diff-our-highlight
   ((t
     (
      :inherit  magit-diff-removed-highlight
      )
     )))

 '(magit-diff-removed
   ((t
     (
      :extend  t
      :foreground  "#691616"
      :background  "#ffe8ef"
      )
     )))

 '(magit-diff-removed-highlight
   ((t
     (
      :foreground  "#4a0000"
      :background  "#efcbcf"
      )
     )))

 '(magit-diff-revision-summary
   ((t
     (
      :inherit  magit-diff-hunk-heading
      )
     )))

 '(magit-diff-revision-summary-highlight
   ((t
     (
      :inherit  magit-diff-hunk-heading-highlight
      )
     )))

 '(magit-diff-their
   ((t
     (
      :inherit  magit-diff-added
      )
     )))

 '(magit-diff-their-highlight
   ((t
     (
      :inherit  magit-diff-added-highlight
      )
     )))

 '(magit-diff-whitespace-warning
   ((t
     (
      :inherit  trailing-whitespace
      )
     )))

 '(magit-diffstat-added
   ((t
     (
      :foreground  "#005e00"
      )
     )))

 '(magit-diffstat-removed
   ((t
     (
      :foreground  "#a60000"
      )
     )))

 '(magit-dimmed
   ((t
     (
      :foreground  "#56576d"
      )
     )))

 '(magit-filename
   ((t
     (
      :foreground  "#093060"
      )
     )))

 '(magit-hash
   ((t
     (
      :inherit  shadow
      )
     )))

 '(magit-head
   ((t
     (
      :inherit  magit-branch-local
      )
     )))

 '(magit-header-line
   ((t
     (
      :foreground  "#5c2092"
      :inherit  bold
      )
     )))

 '(magit-header-line-key
   ((t
     (
      :inherit  modus-themes-key-binding
      )
     )))

 '(magit-header-line-log-select
   ((t
     (
      :foreground  "#000000"
      :inherit  bold
      )
     )))

 '(magit-keyword
   ((t
     (
      :foreground  "#721045"
      )
     )))

 '(magit-keyword-squash
   ((t
     (
      :foreground  "#863927"
      :inherit  bold
      )
     )))

 '(magit-log-author
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(magit-log-date
   ((t
     (
      :inherit  shadow
      )
     )))

 '(magit-log-graph
   ((t
     (
      :foreground  "#282828"
      )
     )))

 '(magit-mode-line-process
   ((t
     (
      :foreground  "#0030b4"
      :inherit  bold
      )
     )))

 '(magit-mode-line-process-error
   ((t
     (
      :foreground  "#8a0000"
      :inherit  bold
      )
     )))

 '(magit-process-ng
   ((t
     (
      :inherit  error
      )
     )))

 '(magit-process-ok
   ((t
     (
      :inherit  success
      )
     )))

 '(magit-reflog-amend
   ((t
     (
      :foreground  "#a8007f"
      :background  "#ffffff"
      )
     )))

 '(magit-reflog-checkout
   ((t
     (
      :foreground  "#1f1fce"
      :background  "#ffffff"
      )
     )))

 '(magit-reflog-cherry-pick
   ((t
     (
      :foreground  "#006800"
      :background  "#ffffff"
      )
     )))

 '(magit-reflog-commit
   ((t
     (
      :foreground  "#006800"
      :background  "#ffffff"
      )
     )))

 '(magit-reflog-merge
   ((t
     (
      :foreground  "#006800"
      :background  "#ffffff"
      )
     )))

 '(magit-reflog-other
   ((t
     (
      :foreground  "#005f88"
      :background  "#ffffff"
      )
     )))

 '(magit-reflog-rebase
   ((t
     (
      :foreground  "#a8007f"
      :background  "#ffffff"
      )
     )))

 '(magit-reflog-remote
   ((t
     (
      :foreground  "#005f88"
      :background  "#ffffff"
      )
     )))

 '(magit-reflog-reset
   ((t
     (
      :foreground  "#b60000"
      :background  "#ffffff"
      )
     )))

 '(magit-refname
   ((t
     (
      :inherit  shadow
      )
     )))

 '(magit-refname-pullreq
   ((t
     (
      :inherit  shadow
      )
     )))

 '(magit-refname-stash
   ((t
     (
      :inherit  shadow
      )
     )))

 '(magit-refname-wip
   ((t
     (
      :inherit  shadow
      )
     )))

 '(magit-section-heading
   ((t
     (
      :extend  t
      :foreground  "#00538b"
      :inherit  bold
      )
     )))

 '(magit-section-heading-selection
   ((t
     (
      :extend  t
      :inherit (modus-themes-refine-cyan bold)
      )
     )))

 '(magit-section-highlight
   ((t
     (
      :extend  t
      :background  "#f0f0f0"
      )
     )))

 '(magit-section-secondary-heading
   ((t
     (
      :weight  bold
      :extend  t
      )
     )))

 '(magit-sequence-done
   ((t
     (
      :foreground  "#005e00"
      )
     )))

 '(magit-sequence-drop
   ((t
     (
      :foreground  "#972500"
      )
     )))

 '(magit-sequence-exec
   ((t
     (
      :foreground  "#8f0075"
      )
     )))

 '(magit-sequence-head
   ((t
     (
      :foreground  "#30517f"
      )
     )))

 '(magit-sequence-onto
   ((t
     (
      :inherit  shadow
      )
     )))

 '(magit-sequence-part
   ((t
     (
      :foreground  "#70480f"
      )
     )))

 '(magit-sequence-pick
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(magit-sequence-stop
   ((t
     (
      :foreground  "#a60000"
      )
     )))

 '(magit-signature-bad
   ((t
     (
      :foreground  "#a60000"
      :inherit  bold
      )
     )))

 '(magit-signature-error
   ((t
     (
      :foreground  "#972500"
      )
     )))

 '(magit-signature-expired
   ((t
     (
      :foreground  "#813e00"
      )
     )))

 '(magit-signature-expired-key
   ((t
     (
      :foreground  "#813e00"
      )
     )))

 '(magit-signature-good
   ((t
     (
      :foreground  "#005e00"
      )
     )))

 '(magit-signature-revoked
   ((t
     (
      :foreground  "#721045"
      )
     )))

 '(magit-signature-untrusted
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(magit-tag
   ((t
     (
      :foreground  "#863927"
      )
     )))

 '(marginalia-archive
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(marginalia-char
   ((t
     (
      :foreground  "#721045"
      )
     )))

 '(marginalia-date
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(marginalia-documentation
   ((t
     (
      :foreground  "#2a486a"
      :inherit  modus-themes-slant
      )
     )))

 '(marginalia-file-name
   ((t
     (
      :foreground  "#003497"
      )
     )))

 '(marginalia-file-owner
   ((t
     (
      :foreground  "#7f1010"
      )
     )))

 '(marginalia-file-priv-dir
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(marginalia-file-priv-exec
   ((t
     (
      :foreground  "#8f0075"
      )
     )))

 '(marginalia-file-priv-link
   ((t
     (
      :foreground  "#0000c0"
      )
     )))

 '(marginalia-file-priv-no
   ((t
     (
      :foreground  "gray50"
      )
     )))

 '(marginalia-file-priv-other
   ((t
     (
      :foreground  "#813e00"
      )
     )))

 '(marginalia-file-priv-rare
   ((t
     (
      :foreground  "#a60000"
      )
     )))

 '(marginalia-file-priv-read
   ((t
     (
      :foreground  "#000000"
      )
     )))

 '(marginalia-file-priv-write
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(marginalia-function
   ((t
     (
      :foreground  "#7b206f"
      )
     )))

 '(marginalia-installed
   ((t
     (
      :inherit  success
      )
     )))

 '(marginalia-key
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(marginalia-lighter
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(marginalia-list
   ((t
     (
      :foreground  "#55348e"
      )
     )))

 '(marginalia-mode
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(marginalia-modified
   ((t
     (
      :foreground  "#7b206f"
      )
     )))

 '(marginalia-null
   ((t
     (
      :inherit  shadow
      )
     )))

 '(marginalia-number
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(marginalia-off
   ((t
     (
      :inherit  error
      )
     )))

 '(marginalia-on
   ((t
     (
      :inherit  success
      )
     )))

 '(marginalia-size
   ((t
     (
      :foreground  "#125458"
      )
     )))

 '(marginalia-string
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(marginalia-symbol
   ((t
     (
      :foreground  "#001087"
      )
     )))

 '(marginalia-true
   ((t
     (
      :foreground  "#000000"
      )
     )))

 '(marginalia-type
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(marginalia-value
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(marginalia-version
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(markdown-blockquote-face
   ((t
     (
      :foreground  "#093060"
      :inherit  modus-themes-slant
      )
     )))

 '(markdown-bold-face
   ((t
     (
      :inherit  bold
      )
     )))

 '(markdown-code-face
   ((t
     (
      :extend  t
      :background  "#f8f8f8"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(markdown-comment-face
   ((t
     (
      :inherit  font-lock-comment-face
      )
     )))

 '(markdown-footnote-marker-face
   ((t
     (
      :foreground  "#30517f"
      :inherit  bold
      )
     )))

 '(markdown-footnote-text-face
   ((t
     (
      :foreground  "#000000"
      :inherit  modus-themes-slant
      )
     )))

 '(markdown-gfm-checkbox-face
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(markdown-header-delimiter-face
   ((t
     (
      :weight  ultra-bold
      :foreground  "#5317ac"
      :inherit  modus-themes-bold
      )
     )))

 '(markdown-header-face
   ((t
     (
      :weight  ultra-bold
      :foreground  "#5317ac"
      )
     )))

 '(markdown-header-face-1
   ((t
     (
      :weight  ultra-bold
      :foreground  "#5317ac"
      :inherit  modus-themes-heading-1
      )
     )))

 '(markdown-header-face-2
   ((t
     (
      :weight  ultra-bold
      :foreground  "#5317ac"
      :inherit  modus-themes-heading-2
      )
     )))

 '(markdown-header-face-3
   ((t
     (
      :weight  ultra-bold
      :foreground  "#5317ac"
      :inherit  modus-themes-heading-3
      )
     )))

 '(markdown-header-face-4
   ((t
     (
      :inherit  modus-themes-heading-4
      )
     )))

 '(markdown-header-face-5
   ((t
     (
      :inherit  modus-themes-heading-5
      )
     )))

 '(markdown-header-face-6
   ((t
     (
      :inherit  modus-themes-heading-6
      )
     )))

 '(markdown-header-rule-face
   ((t
     (
      :foreground  "#5d3026"
      :inherit  bold
      )
     )))

 '(markdown-highlight-face
   ((t
     (
      :inherit  highlight
      )
     )))

 '(markdown-highlighting-face
   ((t
     (
      :foreground  "black"
      :background  "yellow"
      )
     )))

 '(markdown-hr-face
   ((t
     (
      :foreground  "#5d3026"
      :inherit  bold
      )
     )))

 '(markdown-html-attr-name-face
   ((t
     (
      :foreground  "#00538b"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(markdown-html-attr-value-face
   ((t
     (
      :foreground  "#0031a9"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(markdown-html-entity-face
   ((t
     (
      :foreground  "#00538b"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(markdown-html-tag-delimiter-face
   ((t
     (
      :foreground  "#184034"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(markdown-html-tag-name-face
   ((t
     (
      :foreground  "#8f0075"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(markdown-inline-code-face
   ((t
     (
      :foreground  "#61284f"
      :background  "#f0f0f0"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(markdown-italic-face
   ((t
     (
      :inherit  italic
      )
     )))

 '(markdown-language-info-face
   ((t
     (
      :foreground  "#093060"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(markdown-language-keyword-face
   ((t
     (
      :foreground  "#505050"
      :background  "#f0f0f0"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(markdown-line-break-face
   ((t
     (
      :underline  t
      :inherit  modus-themes-refine-cyan
      )
     )))

 '(markdown-link-face
   ((t
     (
      :inherit  button
      )
     )))

 '(markdown-link-title-face
   ((t
     (
      :foreground  "#093060"
      :inherit  modus-themes-slant
      )
     )))

 '(markdown-list-face
   ((t
     (
      :foreground  "#282828"
      )
     )))

 '(markdown-markup-face
   ((t
     (
      :inherit  shadow
      )
     )))

 '(markdown-math-face
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(markdown-metadata-key-face
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(markdown-metadata-value-face
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(markdown-missing-link-face
   ((t
     (
      :foreground  "#813e00"
      :inherit  bold
      )
     )))

 '(markdown-plain-url-face
   ((t
     (
      :inherit  markdown-link-face
      )
     )))

 '(markdown-pre-face
   ((t
     (
      :foreground  "#184034"
      :inherit  markdown-code-face
      )
     )))

 '(markdown-reference-face
   ((t
     (
      :inherit  markdown-markup-face
      )
     )))

 '(markdown-strike-through-face
   ((t
     (
      :strike-through  t
      )
     )))

 '(markdown-table-face
   ((t
     (
      :foreground  "#093060"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(markdown-url-face
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(match
   ((t
     (
      :inherit  modus-themes-special-calm
      )
     )))

 '(menu
   ((t
     (
      )
     )))

 '(message-cited-text-1
   ((t
     (
      :foreground  "#003497"
      )
     )))

 '(message-cited-text-2
   ((t
     (
      :foreground  "#104410"
      )
     )))

 '(message-cited-text-3
   ((t
     (
      :foreground  "#7f1010"
      )
     )))

 '(message-cited-text-4
   ((t
     (
      :foreground  "#5f4400"
      )
     )))

 '(message-header-cc
   ((t
     (
      :foreground  "#0000c0"
      )
     )))

 '(message-header-name
   ((t
     (
      :foreground  "#00538b"
      :inherit  bold
      )
     )))

 '(message-header-newsgroups
   ((t
     (
      :inherit  message-header-other
      )
     )))

 '(message-header-other
   ((t
     (
      :foreground  "#61284f"
      )
     )))

 '(message-header-subject
   ((t
     (
      :foreground  "#8f0075"
      :height 1.5
      :inherit  bold
      )
     )))

 '(message-header-to
   ((t
     (
      :foreground  "#5317ac"
      :inherit  bold
      )
     )))

 '(message-header-xheader
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(message-mml
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(message-separator
   ((t
     (
      :inherit  modus-themes-intense-neutral
      )
     )))

 '(message-signature-separator
   ((t
     (
      :weight  bold
      )
     )))

 '(minibuffer-prompt
   ((t
     (
      :inherit  modus-themes-prompt
      )
     )))

 '(mm-command-output
   ((t
     (
      :foreground  "#a0132f"
      )
     )))

 '(mode-line
   ((t
     (
      :foreground  "#FFFFFF"
      :background  "#6666ff"
      )
     )))

 '(mode-line-active
   ((t
     (
      :inherit  mode-line
      )
     )))

 '(mode-line-buffer-id
   ((t
     (
      :inherit  bold
      )
     )))

 '(mode-line-emphasis
   ((t
     (
      :foreground  "#0030b4"
      :inherit  bold
      )
     )))

 '(mode-line-highlight
   ((t
     (
      :box (:line-width -1 :style pressed-button)
      :inherit  modus-themes-active-blue
      )
     )))

 '(mode-line-inactive
   ((t
     (
      :foreground  "#FFFFFF"
      :background  "#c1c1ff"
      )
     )))

 '(modus-themes-active-blue
   ((t
     (
      :foreground  "#d7d7d7"
      :background  "#0030b4"
      )
     )))

 '(modus-themes-active-cyan
   ((t
     (
      :foreground  "#d7d7d7"
      :background  "#003f8a"
      )
     )))

 '(modus-themes-active-green
   ((t
     (
      :foreground  "#d7d7d7"
      :background  "#004c2e"
      )
     )))

 '(modus-themes-active-magenta
   ((t
     (
      :foreground  "#d7d7d7"
      :background  "#5c2092"
      )
     )))

 '(modus-themes-active-red
   ((t
     (
      :foreground  "#d7d7d7"
      :background  "#8a0000"
      )
     )))

 '(modus-themes-active-yellow
   ((t
     (
      :foreground  "#d7d7d7"
      :background  "#702f00"
      )
     )))

 '(modus-themes-bold
   ((t
     (
      )
     )))

 '(modus-themes-diff-added
   ((t
     (
      :foreground  "#002c00"
      :background  "#bbeabb"
      )
     )))

 '(modus-themes-diff-changed
   ((t
     (
      :foreground  "#392900"
      :background  "#ecdfbf"
      )
     )))

 '(modus-themes-diff-focus-added
   ((t
     (
      :foreground  "#002c00"
      :background  "#bbeabb"
      )
     )))

 '(modus-themes-diff-focus-changed
   ((t
     (
      :foreground  "#392900"
      :background  "#ecdfbf"
      )
     )))

 '(modus-themes-diff-focus-removed
   ((t
     (
      :foreground  "#4a0000"
      :background  "#efcbcf"
      )
     )))

 '(modus-themes-diff-heading
   ((t
     (
      :foreground  "#041645"
      :background  "#b7cfe0"
      )
     )))

 '(modus-themes-diff-refine-added
   ((t
     (
      :foreground  "#002a00"
      :background  "#94cf94"
      )
     )))

 '(modus-themes-diff-refine-changed
   ((t
     (
      :foreground  "#302010"
      :background  "#cccf8f"
      )
     )))

 '(modus-themes-diff-refine-removed
   ((t
     (
      :foreground  "#400000"
      :background  "#daa2b0"
      )
     )))

 '(modus-themes-diff-removed
   ((t
     (
      :foreground  "#4a0000"
      :background  "#efcbcf"
      )
     )))

 '(modus-themes-fixed-pitch
   ((t
     (
      )
     )))

 '(modus-themes-fringe-blue
   ((t
     (
      :foreground  "#000000"
      :background  "#82afff"
      )
     )))

 '(modus-themes-fringe-cyan
   ((t
     (
      :foreground  "#000000"
      :background  "#2fcddf"
      )
     )))

 '(modus-themes-fringe-green
   ((t
     (
      :foreground  "#000000"
      :background  "#62c86a"
      )
     )))

 '(modus-themes-fringe-magenta
   ((t
     (
      :foreground  "#000000"
      :background  "#e0a3ff"
      )
     )))

 '(modus-themes-fringe-red
   ((t
     (
      :foreground  "#000000"
      :background  "#f08290"
      )
     )))

 '(modus-themes-fringe-yellow
   ((t
     (
      :foreground  "#000000"
      :background  "#dbba3f"
      )
     )))

 '(modus-themes-graph-blue-0
   ((t
     (
      :background  "#55a2f0"
      )
     )))

 '(modus-themes-graph-blue-1
   ((t
     (
      :background  "#7fcfff"
      )
     )))

 '(modus-themes-graph-cyan-0
   ((t
     (
      :background  "#30d3f0"
      )
     )))

 '(modus-themes-graph-cyan-1
   ((t
     (
      :background  "#6fefff"
      )
     )))

 '(modus-themes-graph-green-0
   ((t
     (
      :background  "#49d239"
      )
     )))

 '(modus-themes-graph-green-1
   ((t
     (
      :background  "#6dec6d"
      )
     )))

 '(modus-themes-graph-magenta-0
   ((t
     (
      :background  "#ba86ef"
      )
     )))

 '(modus-themes-graph-magenta-1
   ((t
     (
      :background  "#e7afff"
      )
     )))

 '(modus-themes-graph-red-0
   ((t
     (
      :background  "#ef6f79"
      )
     )))

 '(modus-themes-graph-red-1
   ((t
     (
      :background  "#ff9f9f"
      )
     )))

 '(modus-themes-graph-yellow-0
   ((t
     (
      :background  "#efec08"
      )
     )))

 '(modus-themes-graph-yellow-1
   ((t
     (
      :background  "#dbff4e"
      )
     )))

 '(modus-themes-heading-1
   ((t
     (
      :foreground  "#000000"
      :inherit  bold
      )
     )))

 '(modus-themes-heading-2
   ((t
     (
      :foreground  "#5d3026"
      :inherit  bold
      )
     )))

 '(modus-themes-heading-3
   ((t
     (
      :foreground  "#093060"
      :inherit  bold
      )
     )))

 '(modus-themes-heading-4
   ((t
     (
      :foreground  "#184034"
      :inherit  bold
      )
     )))

 '(modus-themes-heading-5
   ((t
     (
      :foreground  "#61284f"
      :inherit  bold
      )
     )))

 '(modus-themes-heading-6
   ((t
     (
      :foreground  "#3f3000"
      :inherit  bold
      )
     )))

 '(modus-themes-heading-7
   ((t
     (
      :foreground  "#5f0000"
      :inherit  bold
      )
     )))

 '(modus-themes-heading-8
   ((t
     (
      :foreground  "#541f4f"
      :inherit  bold
      )
     )))

 '(modus-themes-hl-line
   ((t
     (
      :extend  t
      :background  "#f2eff3"
      )
     )))

 '(modus-themes-intense-blue
   ((t
     (
      :foreground  "#000000"
      :background  "#77baff"
      )
     )))

 '(modus-themes-intense-cyan
   ((t
     (
      :foreground  "#000000"
      :background  "#42cbd4"
      )
     )))

 '(modus-themes-intense-green
   ((t
     (
      :foreground  "#000000"
      :background  "#5ada88"
      )
     )))

 '(modus-themes-intense-magenta
   ((t
     (
      :foreground  "#000000"
      :background  "#d5baff"
      )
     )))

 '(modus-themes-intense-neutral
   ((t
     (
      :foreground  "#000000"
      :background  "#d7d7d7"
      )
     )))

 '(modus-themes-intense-red
   ((t
     (
      :foreground  "#000000"
      :background  "#ff9f9f"
      )
     )))

 '(modus-themes-intense-yellow
   ((t
     (
      :foreground  "#000000"
      :background  "#f5df23"
      )
     )))

 '(modus-themes-key-binding
   ((t
     (
      :inherit  help-key-binding
      )
     )))

 '(modus-themes-lang-error
   ((t
     (
      :underline (:color "#ef4f54" :style wave)
      )
     )))

 '(modus-themes-lang-note
   ((t
     (
      :underline (:color "#3f6fef" :style wave)
      )
     )))

 '(modus-themes-lang-warning
   ((t
     (
      :underline (:color "#cf9f00" :style wave)
      )
     )))

 '(modus-themes-mark-alt
   ((t
     (
      :foreground  "#782900"
      :background  "#f5d88f"
      :inherit  bold
      )
     )))

 '(modus-themes-mark-del
   ((t
     (
      :foreground  "#840040"
      :background  "#ffccbb"
      :inherit  bold
      )
     )))

 '(modus-themes-mark-sel
   ((t
     (
      :foreground  "#005040"
      :background  "#a0f0cf"
      :weight bold
      )
     )))

 '(modus-themes-mark-symbol
   ((t
     (
      :foreground  "#2544bb"
      :inherit  bold
      )
     )))

 '(modus-themes-nuanced-blue
   ((t
     (
      :extend  t
      :background  "#f3f3ff"
      )
     )))

 '(modus-themes-nuanced-cyan
   ((t
     (
      :extend  t
      :background  "#ebf6fa"
      )
     )))

 '(modus-themes-nuanced-green
   ((t
     (
      :extend  t
      :background  "#ecf7ed"
      )
     )))

 '(modus-themes-nuanced-magenta
   ((t
     (
      :extend  t
      :background  "#fdf0ff"
      )
     )))

 '(modus-themes-nuanced-red
   ((t
     (
      :extend  t
      :background  "#fff1f0"
      )
     )))

 '(modus-themes-nuanced-yellow
   ((t
     (
      :extend  t
      :background  "#fff3da"
      )
     )))

 '(modus-themes-prompt
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(modus-themes-pseudo-header
   ((t
     (
      :foreground  "#000000"
      :inherit  bold
      )
     )))

 '(modus-themes-refine-blue
   ((t
     (
      :foreground  "#002f88"
      :background  "#8fcfff"
      )
     )))

 '(modus-themes-refine-cyan
   ((t
     (
      :foreground  "#004850"
      :background  "#8eecf4"
      )
     )))

 '(modus-themes-refine-green
   ((t
     (
      :foreground  "#004c00"
      :background  "#aceaac"
      )
     )))

 '(modus-themes-refine-magenta
   ((t
     (
      :foreground  "#770077"
      :background  "#ffccff"
      )
     )))

 '(modus-themes-refine-red
   ((t
     (
      :foreground  "#780000"
      :background  "#ffcccc"
      )
     )))

 '(modus-themes-refine-yellow
   ((t
     (
      :foreground  "#604000"
      :background  "#fff29a"
      )
     )))

 '(modus-themes-reset-hard
   ((t
     (
      :inherit fixed-pitch modus-themes-reset-soft
      )
     )))

 '(modus-themes-reset-soft
   ((t
     (
      :weight  normal
      :slant  normal
      :foreground  "#000000"
      :background  "#ffffff"
      )
     )))

 '(modus-themes-search-success
   ((t
     (
      :inherit  modus-themes-intense-green
      )
     )))

 '(modus-themes-search-success-lazy
   ((t
     (
      :inherit  modus-themes-refine-cyan
      )
     )))

 '(modus-themes-search-success-modeline
   ((t
     (
      :foreground  "#004c2e"
      )
     )))

 '(modus-themes-slant
   ((t
     (
      :slant  normal
      :inherit  italic
      )
     )))

 '(modus-themes-special-calm
   ((t
     (
      :foreground  "#61284f"
      :background  "#f8ddea"
      )
     )))

 '(modus-themes-special-cold
   ((t
     (
      :foreground  "#093060"
      :background  "#dde3f4"
      )
     )))

 '(modus-themes-special-mild
   ((t
     (
      :foreground  "#184034"
      :background  "#c4ede0"
      )
     )))

 '(modus-themes-special-warm
   ((t
     (
      :foreground  "#5d3026"
      :background  "#f0e0d4"
      )
     )))

 '(modus-themes-subtle-blue
   ((t
     (
      :foreground  "#282828"
      :background  "#b5d0ff"
      )
     )))

 '(modus-themes-subtle-cyan
   ((t
     (
      :foreground  "#282828"
      :background  "#c0efff"
      )
     )))

 '(modus-themes-subtle-green
   ((t
     (
      :foreground  "#282828"
      :background  "#aecf90"
      )
     )))

 '(modus-themes-subtle-magenta
   ((t
     (
      :foreground  "#282828"
      :background  "#f0d3ff"
      )
     )))

 '(modus-themes-subtle-neutral
   ((t
     (
      :foreground  "#404148"
      :background  "#efefef"
      )
     )))

 '(modus-themes-subtle-red
   ((t
     (
      :foreground  "#282828"
      :background  "#f2b0a2"
      )
     )))

 '(modus-themes-subtle-yellow
   ((t
     (
      :foreground  "#282828"
      :background  "#e4c340"
      )
     )))

 '(modus-themes-variable-pitch
   ((t
     (
      )
     )))

 '(mouse
   ((t
     (
      )
     )))

 '(mouse-drag-and-drop-region
   ((t
     (
      :inherit  region
      )
     )))

 '(mu4e-header-highlight-face
   ((t
     (
      :inherit  pulse-highlight-face
      )
     )))

 '(next-error
   ((t
     (
      :extend  t
      :inherit  modus-themes-subtle-red
      )
     )))

 '(next-error-message
   ((t
     (
      :extend  t
      :inherit  highlight
      )
     )))

 '(nobreak-hyphen
   ((t
     (
      :foreground  "#8b1030"
      )
     )))

 '(nobreak-space
   ((t
     (
      :underline  t
      :foreground  "#8b1030"
      )
     )))

 '(org-agenda-calendar-event
   ((t
     (
      :inherit  shadow
      )
     )))

 '(org-agenda-calendar-sexp
   ((t
     (
      :inherit  shadow
      )
     )))

 '(org-agenda-clocking
   ((t
     (
      :extend  t
      :inherit  modus-themes-special-cold
      )
     )))

 '(org-agenda-column-dateline
   ((t
     (
      :background  "#f0f0f0"
      )
     )))

 '(org-agenda-current-time
   ((t
     (
      :foreground  "#001087"
      )
     )))

 '(org-agenda-date
   ((t
     (
      :foreground  "#00538b"
      :weight  ultra-bold
      :height 1.15
      )
     )))

 '(org-agenda-date-today
   ((t
     (
      :foreground  "#00538b"
      :background  "#efefef"
      :weight  ultra-bold
      :height 1.15
      )
     )))

 '(org-agenda-date-weekend
   ((t
     (
      :foreground  "#125458"
      :weight  ultra-bold
      :height 1.15
      )
     )))

 '(org-agenda-date-weekend-today
   ((t
     (
      :foreground  "#125458"
      :background  "#efefef"
      :weight  ultra-bold
      :height 1.15
      )
     )))

 '(org-agenda-diary
   ((t
     (
      :inherit  org-agenda-calendar-sexp
      )
     )))

 '(org-agenda-dimmed-todo-face
   ((t
     (
      :inherit  shadow
      )
     )))

 '(org-agenda-done
   ((t
     (
      :foreground  "#004000"
      )
     )))

 '(org-agenda-filter-category
   ((t
     (
      :foreground  "#003f8a"
      :inherit  bold
      )
     )))

 '(org-agenda-filter-effort
   ((t
     (
      :foreground  "#003f8a"
      :inherit  bold
      )
     )))

 '(org-agenda-filter-regexp
   ((t
     (
      :foreground  "#003f8a"
      :inherit  bold
      )
     )))

 '(org-agenda-filter-tags
   ((t
     (
      :foreground  "#003f8a"
      :inherit  bold
      )
     )))

 '(org-agenda-restriction-lock
   ((t
     (
      :foreground  "#282828"
      :background  "#f8f8f8"
      )
     )))

 '(org-agenda-structure
   ((t
     (
      :height  1.15
      :foreground  "#2544bb"
      :inherit  bold
      )
     )))

 '(org-agenda-structure-filter
   ((t
     (
      :foreground  "#813e00"
      :inherit  org-agenda-structure
      )
     )))

 '(org-agenda-structure-secondary
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(org-archived
   ((t
     (
      :foreground  "#505050"
      :background  "#f0f0f0"
      )
     )))

 '(org-block
   ((t
     (
      :extend  t
      :foreground  "#000000"
      :background  "#f0f0f0"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(org-block-begin-line
   ((t
     (
      :foreground  "#505050"
      :background  "#f0f0f0"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(org-block-end-line
   ((t
     (
      :inherit  org-block-begin-line
      )
     )))

 '(org-checkbox
   ((t
     (
      :box (:line-width 1 :color "#d7d7d7")
      :foreground  "#0a0a0a"
      :background  "#efefef"
      )
     )))

 '(org-checkbox-statistics-done
   ((t
     (
      :inherit  org-done
      )
     )))

 '(org-checkbox-statistics-todo
   ((t
     (
      :inherit  org-todo
      )
     )))

 '(org-cite
   ((t
     (
      :inherit  link
      )
     )))

 '(org-cite-key
   ((t
     (
      :inherit  link
      )
     )))

 '(org-clock-overlay
   ((t
     (
      :inherit  modus-themes-special-cold
      )
     )))

 '(org-code
   ((t
     (
      :extend  t
      :foreground  "#184034"
      :background  "#f0f0f0"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(org-column
   ((t
     (
      :background  "#f0f0f0"
      )
     )))

 '(org-column-title
   ((t
     (
      :underline  t
      :background  "#f0f0f0"
      :inherit  bold
      )
     )))

 '(org-date
   ((t
     (
      :underline  t
      :foreground  "#00538b"
      :inherit  button
      )
     )))

 '(org-date-selected
   ((t
     (
      :inverse-video  t
      :foreground  "#2544bb"
      :inherit  bold
      )
     )))

 '(org-default
   ((t
     (
      :inherit  default
      )
     )))

 '(org-dispatcher-highlight
   ((t
     (
      :inherit bold modus-themes-mark-alt
      )
     )))

 '(org-document-info
   ((t
     (
      :foreground  "#093060"
      )
     )))

 '(org-document-info-keyword
   ((t
     (
      :inherit shadow modus-themes-fixed-pitch
      )
     )))

 '(org-document-title
   ((t
     (
      :foreground  "#093060"
      :inherit modus-themes-variable-pitch
      :weight bold
      :height 1.2
      )
     )))

 '(org-done
   ((t
     (
      :weight  bold
      :foreground  "#005e00"
      :background  "#f2eff3"
      )
     )))

 '(org-drawer
   ((t
     (
      :inherit shadow modus-themes-fixed-pitch
      )
     )))

 '(org-ellipsis
   ((t
     (
      )
     )))

 '(org-footnote
   ((t
     (
      :underline  t
      :foreground  "#2544bb"
      :inherit  button
      )
     )))

 '(org-formula
   ((t
     (
      :foreground  "#972500"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(org-headline-done
   ((t
     (
      :foreground  "#004000"
      :inherit  modus-themes-variable-pitch
      )
     )))

 '(org-headline-todo
   ((t
     (
      :foreground  "#5f0000"
      :inherit  modus-themes-variable-pitch
      )
     )))

 '(org-hide
   ((t
     (
      :foreground  "#ffffff"
      )
     )))

 '(org-imminent-deadline
   ((t
     (
      :foreground  "#b60000"
      )
     )))

 '(org-latex-and-related
   ((t
     (
      :foreground  "#770077"
      )
     )))

 '(org-level-1
   ((t
     (
      :height 1.15
      :inherit  modus-themes-heading-1
      )
     )))

 '(org-level-2
   ((t
     (
      :height 1.12
      :inherit  modus-themes-heading-2
      )
     )))

 '(org-level-3
   ((t
     (
      :height 1.075
      :inherit  modus-themes-heading-3
      )
     )))

 '(org-level-4
   ((t
     (
      :height 1.05
      :inherit  modus-themes-heading-4
      )
     )))

 '(org-level-5
   ((t
     (
      :height  1.025
      :inherit  modus-themes-heading-5
      )
     )))

 '(org-level-6
   ((t
     (
      :inherit  modus-themes-heading-6
      )
     )))

 '(org-level-7
   ((t
     (
      :inherit  modus-themes-heading-7
      )
     )))

 '(org-level-8
   ((t
     (
      :inherit  modus-themes-heading-8
      )
     )))

 '(org-link
   ((t
     (
      :inherit  button
      )
     )))

 '(org-list-dt
   ((t
     (
      :inherit  bold
      )
     )))

 '(org-macro
   ((t
     (
      :foreground  "#0f3360"
      :background  "#ebf6fa"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(org-meta-line
   ((t
     (
      :inherit shadow modus-themes-fixed-pitch
      )
     )))

 '(org-mode-line-clock
   ((t
     (
      :foreground  "#000000"
      )
     )))

 '(org-mode-line-clock-overrun
   ((t
     (
      :foreground  "#8a0000"
      :inherit  bold
      )
     )))

 '(org-priority
   ((t
     (
      :foreground  "#721045"
      )
     )))

 '(org-property-value
   ((t
     (
      :foreground  "#093060"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(org-quote
   ((t
     (
      :foreground  "#093060"
      )
     )))

 '(org-scheduled
   ((t
     (
      :foreground  "#5f4400"
      )
     )))

 '(org-scheduled-previously
   ((t
     (
      :foreground  "#813e00"
      )
     )))

 '(org-scheduled-today
   ((t
     (
      :foreground  "#813e00"
      )
     )))

 '(org-sexp-date
   ((t
     (
      :inherit  org-date
      )
     )))

 '(org-special-keyword
   ((t
     (
      :inherit shadow modus-themes-fixed-pitch
      )
     )))

 '(org-table
   ((t
     (
      :foreground  "#093060"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(org-table-header
   ((t
     (
      :inherit fixed-pitch modus-themes-intense-neutral
      )
     )))

 '(org-tag
   ((t
     (
      :foreground  "#541f4f"
      )
     )))

 '(org-tag-group
   ((t
     (
      :foreground  "#0f3360"
      :inherit  bold
      )
     )))

 '(org-target
   ((t
     (
      :underline  t
      )
     )))

 '(org-time-grid
   ((t
     (
      :inherit  shadow
      )
     )))

 '(org-todo
   ((t
     (
      :weight  bold
      :foreground  "#6666ff"
      :background  "#F1FA8C"
      )
     )))

 '(org-upcoming-deadline
   ((t
     (
      :foreground  "#a0132f"
      )
     )))

 '(org-upcoming-distant-deadline
   ((t
     (
      :foreground  "#7f1010"
      )
     )))

 '(org-verbatim
   ((t
     (
      :foreground  "#61284f"
      :background  "#f0f0f0"
      :inherit  modus-themes-fixed-pitch
      )
     )))

 '(org-verse
   ((t
     (
      :inherit  org-quote
      )
     )))

 '(org-warning
   ((t
     (
      :foreground  "#a0132f"
      :inherit  bold
      )
     )))

 '(outline-1
   ((t
     (
      :inherit  (modus-themes-heading-1 bold)
      )
     )))

 '(outline-2
   ((t
     (
      :inherit  (modus-themes-heading-2 bold)
      )
     )))

 '(outline-3
   ((t
     (
      :inherit  (modus-themes-heading-3 bold)
      )
     )))

 '(outline-4
   ((t
     (
      :inherit  (modus-themes-heading-4 bold)
      )
     )))

 '(outline-5
   ((t
     (
      :inherit  (modus-themes-heading-5 bold)
      )
     )))

 '(outline-6
   ((t
     (
      :inherit  (modus-themes-heading-6 bold)
      )
     )))

 '(outline-7
   ((t
     (
      :inherit  (modus-themes-heading-7 bold)
      )
     )))

 '(outline-8
   ((t
     (
      :inherit  (modus-themes-heading-8 bold)
      )
     )))

 '(package-description
   ((t
     (
      :foreground  "#093060"
      )
     )))

 '(package-help-section-name
   ((t
     (
      :foreground  "#5317ac"
      :inherit  bold
      )
     )))

 '(package-name
   ((t
     (
      :inherit  button
      )
     )))

 '(package-status-avail-obso
   ((t
     (
      :foreground  "#a60000"
      :inherit  bold
      )
     )))

 '(package-status-available
   ((t
     (
      :foreground  "#184034"
      )
     )))

 '(package-status-built-in
   ((t
     (
      :foreground  "#721045"
      )
     )))

 '(package-status-dependency
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(package-status-disabled
   ((t
     (
      :inherit  modus-themes-subtle-red
      )
     )))

 '(package-status-external
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(package-status-held
   ((t
     (
      :foreground  "#70480f"
      )
     )))

 '(package-status-incompat
   ((t
     (
      :foreground  "#813e00"
      :inherit  bold
      )
     )))

 '(package-status-installed
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(package-status-new
   ((t
     (
      :foreground  "#005e00"
      :inherit  bold
      )
     )))

 '(package-status-unsigned
   ((t
     (
      :foreground  "#972500"
      :inherit  bold
      )
     )))

 '(popup-face
   ((t
     (
      :foreground  "#000000"
      :background  "#f0f0f0"
      )
     )))

 '(popup-isearch-match
   ((t
     (
      :inherit (modus-themes-refine-cyan bold)
      )
     )))

 '(popup-menu-face
   ((t
     (
      :inherit  popup-face
      )
     )))

 '(popup-menu-mouse-face
   ((t
     (
      :inherit  modus-themes-intense-blue
      )
     )))

 '(popup-menu-selection-face
   ((t
     (
      :inherit (modus-themes-subtle-cyan bold)
      )
     )))

 '(popup-menu-summary-face
   ((t
     (
      :inherit  popup-summary-face
      )
     )))

 '(popup-scroll-bar-background-face
   ((t
     (
      :background  "#d7d7d7"
      )
     )))

 '(popup-scroll-bar-foreground-face
   ((t
     (
      :foreground  "#0a0a0a"
      )
     )))

 '(popup-summary-face
   ((t
     (
      :foreground  "#404148"
      :background  "#d7d7d7"
      )
     )))

 '(popup-tip-face
   ((t
     (
      :inherit  modus-themes-refine-yellow
      )
     )))

 '(pretty-hydra-toggle-off-face
   ((t
     (
      :inherit quote font-lock-comment-face
      )
     )))

 '(pretty-hydra-toggle-on-face
   ((t
     (
      :inherit quote font-lock-keyword-face
      )
     )))

 '(pulse-highlight-face
   ((t
     (
      :extend  t
      :background  "#d0d6ff"
      )
     )))

 '(pulse-highlight-start-face
   ((t
     (
      :extend  t
      :background  "#d0d6ff"
      )
     )))

 '(query-replace
   ((t
     (
      :inherit (modus-themes-intense-yellow bold)
      )
     )))

 '(rainbow-delimiters-base-error-face
   ((t
     (
      :foreground  "#000000"
      :background  "#f2b0a2"
      )
     )))

 '(rainbow-delimiters-base-face
   ((t
     (
      :foreground  "#000000"
      )
     )))

 '(rainbow-delimiters-depth-1-face
   ((t
     (
      :foreground  "#000000"
      )
     )))

 '(rainbow-delimiters-depth-2-face
   ((t
     (
      :foreground  "#a8007f"
      )
     )))

 '(rainbow-delimiters-depth-3-face
   ((t
     (
      :foreground  "#005f88"
      )
     )))

 '(rainbow-delimiters-depth-4-face
   ((t
     (
      :foreground  "#904200"
      )
     )))

 '(rainbow-delimiters-depth-5-face
   ((t
     (
      :foreground  "#7f10d0"
      )
     )))

 '(rainbow-delimiters-depth-6-face
   ((t
     (
      :foreground  "#006800"
      )
     )))

 '(rainbow-delimiters-depth-7-face
   ((t
     (
      :foreground  "#b60000"
      )
     )))

 '(rainbow-delimiters-depth-8-face
   ((t
     (
      :foreground  "#1f1fce"
      )
     )))

 '(rainbow-delimiters-depth-9-face
   ((t
     (
      :foreground  "#605b00"
      )
     )))

 '(rainbow-delimiters-mismatched-face
   ((t
     (
      :inherit bold modus-themes-refine-yellow
      )
     )))

 '(rainbow-delimiters-unmatched-face
   ((t
     (
      :inherit bold modus-themes-refine-red
      )
     )))

 '(read-multiple-choice-face
   ((t
     (
      :weight  bold
      :inherit  underline
      )
     )))

 '(reb-match-0
   ((t
     (
      :inherit  modus-themes-refine-cyan
      )
     )))

 '(reb-match-1
   ((t
     (
      :inherit  modus-themes-subtle-magenta
      )
     )))

 '(reb-match-2
   ((t
     (
      :inherit  modus-themes-subtle-green
      )
     )))

 '(reb-match-3
   ((t
     (
      :inherit  modus-themes-refine-yellow
      )
     )))

 '(reb-regexp-grouping-backslash
   ((t
     (
      :inherit  font-lock-regexp-grouping-backslash
      )
     )))

 '(reb-regexp-grouping-construct
   ((t
     (
      :inherit  font-lock-regexp-grouping-construct
      )
     )))

 '(rectangle-preview
   ((t
     (
      :inherit  modus-themes-special-mild
      )
     )))

 '(region
   ((t
     (
      :background  "#bcbcbc"
      )
     )))

 '(rg-column-number-face
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(rg-context-face
   ((t
     (
      :foreground  "#56576d"
      )
     )))

 '(rg-error-face
   ((t
     (
      :foreground  "#a60000"
      :inherit  bold
      )
     )))

 '(rg-file-tag-face
   ((t
     (
      :foreground  "#093060"
      )
     )))

 '(rg-filename-face
   ((t
     (
      :foreground  "#093060"
      :inherit  bold
      )
     )))

 '(rg-info-face
   ((t
     (
      :inherit  compilation-info
      )
     )))

 '(rg-line-number-face
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(rg-literal-face
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(rg-match-face
   ((t
     (
      :foreground  "#61284f"
      :background  "#f8ddea"
      :weight ultra-bold
      )
     )))

 '(rg-match-position-face
   ((t
     (
      :inherit  default
      )
     )))

 '(rg-regexp-face
   ((t
     (
      :foreground  "#5c2092"
      )
     )))

 '(rg-toggle-off-face
   ((t
     (
      :foreground  "#404148"
      :inherit  bold
      )
     )))

 '(rg-toggle-on-face
   ((t
     (
      :foreground  "#003f8a"
      :inherit  bold
      )
     )))

 '(rg-warning-face
   ((t
     (
      :foreground  "#813e00"
      :inherit  bold
      )
     )))

 '(rxt-highlight-face
   ((t
     (
      :background  "#eee8d5"
      )
     )))

 '(scroll-bar
   ((t
     (
      :foreground  "black"
      )
     )))

 '(secondary-selection
   ((t
     (
      :extend  t
      :inherit  modus-themes-special-cold
      )
     )))

 '(selectrum-completion-annotation
   ((t
     (
      :inherit  completions-annotations
      )
     )))

 '(selectrum-completion-docsig
   ((t
     (
      :slant  italic
      :inherit  selectrum-completion-annotation
      )
     )))

 '(selectrum-current-candidate
   ((t
     (
      :extend  t
      :foreground  "#000000"
      :background  "#efefef"
      :inherit  bold
      )
     )))

 '(selectrum-group-separator
   ((t
     (
      :strike-through  t
      :inherit  shadow
      )
     )))

 '(selectrum-group-title
   ((t
     (
      :slant  italic
      :inherit  shadow
      )
     )))

 '(selectrum-mouse-highlight
   ((t
     (
      :inherit  highlight
      )
     )))

 '(selectrum-prescient-primary-highlight
   ((t
     (
      :foreground  "#8f0075"
      :inherit  bold
      )
     )))

 '(selectrum-prescient-secondary-highlight
   ((t
     (
      :foreground  "#005a5f"
      :inherit  bold
      )
     )))

 '(selectrum-quick-keys-highlight
   ((t
     (
      :inherit  modus-themes-refine-red
      )
     )))

 '(selectrum-quick-keys-match
   ((t
     (
      :inherit bold modus-themes-search-success
      )
     )))

 '(separator-line
   ((t
     (
      :height  0.1
      :background  "#a0a0a0"
      )
     )))

 '(shadow
   ((t
     (
      :foreground  "#505050"
      )
     )))

 '(show-paren-match
   ((t
     (
      :weight  ultra-bold
      :foreground  "#FFFFFF"
      :background  "#5317ac"
      )
     )))

 '(show-paren-match-expression
   ((t
     (
      :background  "#dff0ff"
      )
     )))

 '(show-paren-mismatch
   ((t
     (
      :inherit  modus-themes-intense-red
      )
     )))

 '(shr-abbreviation
   ((t
     (
      :inherit  modus-themes-lang-note
      )
     )))

 '(shr-h1
   ((t
     (
      :inherit  (modus-themes-heading-1 bold)
      )
     )))

 '(shr-h2
   ((t
     (
      :inherit  modus-themes-heading-2
      )
     )))

 '(shr-h3
   ((t
     (
      :inherit  modus-themes-heading-3
      )
     )))

 '(shr-h4
   ((t
     (
      :inherit  modus-themes-heading-4
      )
     )))

 '(shr-h5
   ((t
     (
      :inherit  modus-themes-heading-5
      )
     )))

 '(shr-h6
   ((t
     (
      :inherit  modus-themes-heading-6
      )
     )))

 '(shr-link
   ((t
     (
      :inherit  link
      )
     )))

 '(shr-selected-link
   ((t
     (
      :inherit  modus-themes-subtle-red
      )
     )))

 '(shr-strike-through
   ((t
     (
      :strike-through  t
      )
     )))

 '(shr-sup
   ((t
     (
      :height  0.8
      )
     )))

 '(shr-text
   ((t
     (
      :inherit  variable-pitch-text
      )
     )))

 '(smerge-base
   ((t
     (
      :extend  t
      :inherit  modus-themes-diff-changed
      )
     )))

 '(smerge-lower
   ((t
     (
      :extend  t
      :inherit  modus-themes-diff-added
      )
     )))

 '(smerge-markers
   ((t
     (
      :extend  t
      :inherit  modus-themes-diff-heading
      )
     )))

 '(smerge-refined-added
   ((t
     (
      :inherit  modus-themes-diff-refine-added
      )
     )))

 '(smerge-refined-changed
   ((t
     (
      )
     )))

 '(smerge-refined-removed
   ((t
     (
      :inherit  modus-themes-diff-refine-removed
      )
     )))

 '(smerge-upper
   ((t
     (
      :extend  t
      :inherit  modus-themes-diff-removed
      )
     )))

 '(success
   ((t
     (
      :foreground  "#005e00"
      :inherit  bold
      )
     )))

 '(swiper-background-match-face-1
   ((t
     (
      :inherit  modus-themes-subtle-neutral
      :background  "#c1c1ff"
      )
     )))

 '(swiper-background-match-face-2
   ((t
     (
      :inherit  modus-themes-refine-cyan
      :background  "#00fa9a"
      )
     )))

 '(swiper-background-match-face-3
   ((t
     (
      :inherit  modus-themes-refine-magenta
      :background  "#dda0dd"
      )
     )))

 '(swiper-background-match-face-4
   ((t
     (
      :inherit  modus-themes-refine-yellow
      :background  "#deb887"
      )
     )))

 '(swiper-line-face
   ((t
     (
      :inherit ivy-current-match
      )
     )))

 '(swiper-match-face-1
   ((t
     (
      :inherit (modus-themes-intense-neutral bold)
      )
     )))

 '(swiper-match-face-2
   ((t
     (
      :inherit (modus-themes-intense-green bold)
      )
     )))

 '(swiper-match-face-3
   ((t
     (
      :inherit  (modus-themes-intense-blue bold)
      )
     )))

 '(swiper-match-face-4
   ((t
     (
      :inherit (modus-themes-intense-red bold)
      )
     )))

 '(tab-bar
   ((t
     (
      :background  "#d7d7d7"
      :inherit
      )
     )))

 '(tab-bar-tab
   ((t
     (
      :box (:line-width 2 :color "#f6f6f6")
      :background  "#f6f6f6"
      :inherit  bold
      )
     )))

 '(tab-bar-tab-inactive
   ((t
     (
      :box (:line-width 2 :color "#b7b7b7")
      :foreground  "#282828"
      :background  "#b7b7b7"
      )
     )))

 '(tab-bar-tab-ungrouped
   ((t
     (
      :inherit shadow tab-bar-tab-inactive
      )
     )))

 '(tab-line
   ((t
     (
      :height  0.95
      :background  "#d7d7d7"
      :inherit
      )
     )))

 '(tabulated-list-fake-header
   ((t
     (
      :weight  bold
      :underline  t
      :overline  t
      )
     )))

 '(term
   ((t
     (
      :foreground  "#000000"
      :background  "#ffffff"
      )
     )))

 '(term-bold
   ((t
     (
      :inherit  bold
      )
     )))

 '(term-color-black
   ((t
     (
      :foreground  "gray35"
      :background  "gray35"
      )
     )))

 '(term-color-blue
   ((t
     (
      :foreground  "#0031a9"
      :background  "#0031a9"
      )
     )))

 '(term-color-bright-black
   ((t
     (
      :inherit  ansi-color-bright-black
      )
     )))

 '(term-color-bright-blue
   ((t
     (
      :inherit  ansi-color-bright-blue
      )
     )))

 '(term-color-bright-cyan
   ((t
     (
      :inherit  ansi-color-bright-cyan
      )
     )))

 '(term-color-bright-green
   ((t
     (
      :inherit  ansi-color-bright-green
      )
     )))

 '(term-color-bright-magenta
   ((t
     (
      :inherit  ansi-color-bright-magenta
      )
     )))

 '(term-color-bright-red
   ((t
     (
      :inherit  ansi-color-bright-red
      )
     )))

 '(term-color-bright-white
   ((t
     (
      :inherit  ansi-color-bright-white
      )
     )))

 '(term-color-bright-yellow
   ((t
     (
      :inherit  ansi-color-bright-yellow
      )
     )))

 '(term-color-cyan
   ((t
     (
      :foreground  "#00538b"
      :background  "#00538b"
      )
     )))

 '(term-color-green
   ((t
     (
      :foreground  "#005e00"
      :background  "#005e00"
      )
     )))

 '(term-color-magenta
   ((t
     (
      :foreground  "#721045"
      :background  "#721045"
      )
     )))

 '(term-color-red
   ((t
     (
      :foreground  "#a60000"
      :background  "#a60000"
      )
     )))

 '(term-color-white
   ((t
     (
      :foreground  "gray65"
      :background  "gray65"
      )
     )))

 '(term-color-yellow
   ((t
     (
      :foreground  "#813e00"
      :background  "#813e00"
      )
     )))

 '(term-faint
   ((t
     (
      :inherit  ansi-color-faint
      )
     )))

 '(term-fast-blink
   ((t
     (
      :inherit  ansi-color-fast-blink
      )
     )))

 '(term-italic
   ((t
     (
      :inherit  ansi-color-italic
      )
     )))

 '(term-slow-blink
   ((t
     (
      :inherit  ansi-color-slow-blink
      )
     )))

 '(term-underline
   ((t
     (
      :underline  t
      )
     )))

 '(tool-bar
   ((t
     (
      :box (:line-width 1 :style released-button)
      :foreground  "black"
      :background  "grey75"
      )
     )))

 '(tooltip
   ((t
     (
      :foreground  "#000000"
      :background  "#dde3f4"
      )
     )))

 '(trailing-whitespace
   ((t
     (
      :background  "#ff9f9f"
      )
     )))

 '(transient-active-infix
   ((t
     (
      :inherit  modus-themes-special-mild
      )
     )))

 '(transient-amaranth
   ((t
     (
      :foreground  "#70480f"
      :inherit  bold
      )
     )))

 '(transient-argument
   ((t
     (
      :foreground  "#005e00"
      :inherit  bold
      )
     )))

 '(transient-blue
   ((t
     (
      :foreground  "#0031a9"
      :inherit  bold
      )
     )))

 '(transient-disabled-suffix
   ((t
     (
      :inherit  modus-themes-intense-red
      )
     )))

 '(transient-enabled-suffix
   ((t
     (
      :inherit  modus-themes-subtle-green
      )
     )))

 '(transient-heading
   ((t
     (
      :foreground  "#000000"
      :inherit  bold
      )
     )))

 '(transient-higher-level
   ((t
     (
      :underline  t
      )
     )))

 '(transient-inactive-argument
   ((t
     (
      :inherit  shadow
      )
     )))

 '(transient-inactive-value
   ((t
     (
      :inherit  shadow
      )
     )))

 '(transient-inapt-suffix
   ((t
     (
      :slant  italic
      :inherit  shadow
      )
     )))

 '(transient-key
   ((t
     (
      :inherit  modus-themes-key-binding
      )
     )))

 '(transient-mismatched-key
   ((t
     (
      :underline  t
      )
     )))

 '(transient-nonstandard-key
   ((t
     (
      :underline  t
      )
     )))

 '(transient-pink
   ((t
     (
      :foreground  "#7b206f"
      :inherit  bold
      )
     )))

 '(transient-red
   ((t
     (
      :foreground  "#7f1010"
      :inherit  bold
      )
     )))

 '(transient-separator
   ((t
     (
      :extend  t
      :background  "grey80"
      )
     )))

 '(transient-teal
   ((t
     (
      :foreground  "#005a5f"
      :inherit  bold
      )
     )))

 '(transient-unreachable
   ((t
     (
      :foreground  "#56576d"
      )
     )))

 '(transient-unreachable-key
   ((t
     (
      :foreground  "#56576d"
      )
     )))

 '(transient-value
   ((t
     (
      :foreground  "#5317ac"
      :inherit  bold
      )
     )))

 '(tty-menu-disabled-face
   ((t
     (
      :foreground  "#505050"
      :background  "#f0f0f0"
      )
     )))

 '(tty-menu-enabled-face
   ((t
     (
      :foreground  "#000000"
      :background  "#f0f0f0"
      :inherit  bold
      )
     )))

 '(tty-menu-selected-face
   ((t
     (
      :inherit  modus-themes-intense-blue
      )
     )))

 '(underline
   ((t
     (
      :underline  t
      )
     )))

 '(undo-tree-visualizer-active-branch-face
   ((t
     (
      :foreground  "#000000"
      :inherit  bold
      )
     )))

 '(undo-tree-visualizer-current-face
   ((t
     (
      :foreground  "#1f1fce"
      )
     )))

 '(undo-tree-visualizer-default-face
   ((t
     (
      :inherit  shadow
      )
     )))

 '(undo-tree-visualizer-register-face
   ((t
     (
      :foreground  "#a8007f"
      )
     )))

 '(undo-tree-visualizer-unmodified-face
   ((t
     (
      :foreground  "#006800"
      )
     )))

 '(variable-pitch
   ((t
     (
      :width  normal
      :height  100
      :weight  regular
      :slant  normal
      )
     )))

 '(variable-pitch-text
   ((t
     (
      :height  1.1
      :inherit  variable-pitch
      )
     )))

 '(vc-conflict-state
   ((t
     (
      :foreground  "#8a0000"
      :inherit  bold
      )
     )))

 '(vc-dir-directory
   ((t
     (
      :foreground  "#0031a9"
      )
     )))

 '(vc-dir-file
   ((t
     (
      :foreground  "#000000"
      )
     )))

 '(vc-dir-header
   ((t
     (
      :foreground  "#005a5f"
      )
     )))

 '(vc-dir-header-value
   ((t
     (
      :foreground  "#5317ac"
      )
     )))

 '(vc-dir-mark-indicator
   ((t
     (
      :foreground  "#0000c0"
      )
     )))

 '(vc-dir-status-edited
   ((t
     (
      :foreground  "#813e00"
      )
     )))

 '(vc-dir-status-ignored
   ((t
     (
      :inherit  shadow
      )
     )))

 '(vc-dir-status-up-to-date
   ((t
     (
      :foreground  "#00538b"
      )
     )))

 '(vc-dir-status-warning
   ((t
     (
      :inherit  error
      )
     )))

 '(vc-edited-state
   ((t
     (
      :weight  bold
      :foreground  "#edc809"
      )
     )))

 '(vc-locally-added-state
   ((t
     (
      :foreground  "#003f8a"
      )
     )))

 '(vc-locked-state
   ((t
     (
      :foreground  "#0030b4"
      )
     )))

 '(vc-missing-state
   ((t
     (
      :foreground  "#5c2092"
      :inherit  modus-themes-slant
      )
     )))

 '(vc-needs-update-state
   ((t
     (
      :foreground  "#004c2e"
      :inherit  modus-themes-slant
      )
     )))

 '(vc-removed-state
   ((t
     (
      :foreground  "#8a0000"
      )
     )))

 '(vc-state-base
   ((t
     (
      :foreground  "#0a0a0a"
      )
     )))

 '(vc-up-to-date-state
   ((t
     (
      :weight  bold
      :foreground  "#FFFFFF"
      )
     )))

 '(vertical-border
   ((t
     (
      :weight  bold
      :foreground  "#888888"
      :background  "#888888"
      )
     )))

 '(vterm-color-black
   ((t
     (
      :foreground  "gray35"
      :background  "gray35"
      )
     )))

 '(vterm-color-blue
   ((t
     (
      :foreground  "#0031a9"
      :background  "#0031a9"
      )
     )))

 '(vterm-color-cyan
   ((t
     (
      :foreground  "#00538b"
      :background  "#00538b"
      )
     )))

 '(vterm-color-green
   ((t
     (
      :foreground  "#005e00"
      :background  "#005e00"
      )
     )))

 '(vterm-color-inverse-video
   ((t
     (
      :inverse-video  t
      :background  "#ffffff"
      )
     )))

 '(vterm-color-magenta
   ((t
     (
      :foreground  "#721045"
      :background  "#721045"
      )
     )))

 '(vterm-color-red
   ((t
     (
      :foreground  "#a60000"
      :background  "#a60000"
      )
     )))

 '(vterm-color-underline
   ((t
     (
      :underline  t
      :foreground  "#5d3026"
      )
     )))

 '(vterm-color-white
   ((t
     (
      :foreground  "gray65"
      :background  "gray65"
      )
     )))

 '(vterm-color-yellow
   ((t
     (
      :foreground  "#813e00"
      :background  "#813e00"
      )
     )))

 '(warning
   ((t
     (
      :foreground  "#813e00"
      :inherit  bold
      )
     )))

 '(wgrep-delete-face
   ((t
     (
      :inherit  modus-themes-refine-yellow
      )
     )))

 '(wgrep-done-face
   ((t
     (
      :inherit  modus-themes-refine-blue
      )
     )))

 '(wgrep-face
   ((t
     (
      :inherit  modus-themes-refine-green
      )
     )))

 '(wgrep-file-face
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(wgrep-reject-face
   ((t
     (
      :inherit (modus-themes-intense-red bold)
      )
     )))

 '(which-func
   ((t
     (
      :foreground  "#5c2092"
      )
     )))

 '(which-key-command-description-face
   ((t
     (
      :foreground  "#000000"
      )
     )))

 '(which-key-docstring-face
   ((t
     (
      :inherit  which-key-note-face
      )
     )))

 '(which-key-group-description-face
   ((t
     (
      :foreground  "#8f0075"
      )
     )))

 '(which-key-highlighted-command-face
   ((t
     (
      :underline  t
      :foreground  "#813e00"
      )
     )))

 '(which-key-key-face
   ((t
     (
      :inherit  modus-themes-key-binding
      )
     )))

 '(which-key-local-map-description-face
   ((t
     (
      :foreground  "#000000"
      )
     )))

 '(which-key-note-face
   ((t
     (
      :foreground  "#5d3026"
      )
     )))

 '(which-key-separator-face
   ((t
     (
      :inherit  shadow
      )
     )))

 '(which-key-special-key-face
   ((t
     (
      :foreground  "#904200"
      :inherit  bold
      )
     )))

 '(whitespace-big-indent
   ((t
     (
      :inherit  modus-themes-subtle-red
      :background  unspecified
      )
     )))

 '(whitespace-empty
   ((t
     (
      :extend  t
      :background  unspecified
      )
     )))

 '(whitespace-hspace
   ((t
     (
      :foreground  "#624956"
      :background  unspecified
      )
     )))

 '(whitespace-indentation
   ((t
     (
      :foreground  "#624956"
      :background  unspecified
      )
     )))

 '(whitespace-line
   ((t
     (
      :inherit  modus-themes-subtle-yellow
      :background  unspecified
      )
     )))

 '(whitespace-missing-newline-at-eof
   ((t
     (
      :foreground  "black"
      :background  unspecified
      )
     )))

 '(whitespace-newline
   ((t
     (
      :foreground  "lightgray"
      :background  unspecified
      )
     )))

 '(whitespace-space
   ((t
     (
      :foreground  "#624956"
      :background  unspecified
      )
     )))

 '(whitespace-space-after-tab
   ((t
     (
      :inherit  modus-themes-subtle-magenta
      :background  unspecified
      )
     )))

 '(whitespace-space-before-tab
   ((t
     (
      :inherit  modus-themes-subtle-cyan
      :background  unspecified
      )
     )))

 '(whitespace-tab
   ((t
     (
      :foreground  "#624956"
      :background  unspecified
      )
     )))

 '(whitespace-trailing
   ((t
     (
      :inherit  modus-themes-intense-red
      :background  unspecified
      )
     )))

 '(widget-button
   ((t
     (
      :foreground  "#2544bb"
      )
     )))

 '(widget-button-pressed
   ((t
     (
      :foreground  "#721045"
      :inherit  widget-button
      )
     )))

 '(widget-documentation
   ((t
     (
      :foreground  "#005e00"
      )
     )))

 '(widget-field
   ((t
     (
      :extend  t
      :foreground  "#282828"
      :background  "#f0f0f0"
      )
     )))

 '(widget-inactive
   ((t
     (
      :background  "#f8f8f8"
      :inherit  shadow
      )
     )))

 '(widget-single-line-field
   ((t
     (
      :inherit  widget-field
      )
     )))

 '(window-divider
   ((t
     (
      :weight  bold
      :foreground  "#888888"
      :background  "#888888"
      )
     )))

 '(window-divider-first-pixel
   ((t
     (
      :foreground  "#585858"
      )
     )))

 '(window-divider-last-pixel
   ((t
     (
      :foreground  "#585858"
      )
     )))

 '(xref-file-header
   ((t
     (
      :foreground  "#093060"
      :inherit  bold
      )
     )))

 '(xref-line-number
   ((t
     (
      :inherit  shadow
      )
     )))

 '(xref-match
   ((t
     (
      :inherit  match
      )
     )))

 '(yaml-tab-face
   ((t
     (
      :inherit  modus-themes-intense-red
      )
     )))

 '(yas--field-debug-face
   ((t
     (
      )
     )))

 '(yas-field-highlight-face
   ((t
     (
      :background  "#e8dfd1"
      )
     )))

 )

;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'funkday)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; Disable package.el in favor of straight.el
(setq package-enable-at-startup nil)
(setq straight-process-buffer " *straight-process*")

;; Bootstrap straight.el
(setq straight-repository-branch "develop")
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)

(straight-override-recipe '(compat :host github :repo "emacs-compat/compat"))

;; Don't my eyes on startup
(load-theme 'modus-vivendi t)

;; ;; NOTE 2022-06-17: If you want to insert time-stamps for each item on Messages buffer.
;; ;; BEG: Time-stamp on Messages buffer
;; (defun funk/current-time-microseconds ()
;;   "Return the current time formatted to include microseconds."
;;   (let* ((nowtime (current-time))
;;          (now-ms (nth 2 nowtime)))
;;     (concat (format-time-string "[%Y-%m-%dT%T" nowtime) (format ".%d]" now-ms))))
;;
;; (defun funk/ad-timestamp-message (FORMAT-STRING &rest args)
;;   "Advice to run before `message' that prepends a timestamp to each message.
;;
;; Activate this advice with:
;; (advice-add 'message :before 'funk/ad-timestamp-message)"
;;   (unless (string-equal FORMAT-STRING "%s%s")
;;     (let ((deactivate-mark nil)
;;           (inhibit-read-only t))
;;       (with-current-buffer "*Messages*"
;;         (goto-char (point-max))
;;         (if (not (bolp))
;;             (newline))
;;         (insert (funk/current-time-microseconds) " ")))))
;;
;; (advice-add 'message :before 'funk/ad-timestamp-message)
;; ;; END: Time-stamp on Messages buffer

(setq garbage-collection-messages t)
;; https://bling.github.io/blog/2016/01/18/why-are-you-changing-gc-cons-threshold/
(setq gc-cons-threshold (* 1024 1024 100))
(defun funk/minibuffer-setup-hook ()
  "Define garbage collection for run when in minibuffer."
  (setq gc-cons-threshold most-positive-fixnum))
(defun funk/minibuffer-exit-hook ()
  "Define garbage collection for run when outside minibuffer."
  (setq gc-cons-threshold 800000))
(add-hook 'minibuffer-setup-hook #'funk/minibuffer-setup-hook)
(add-hook 'minibuffer-exit-hook #'funk/minibuffer-exit-hook)

;; Compile warnings
;;  (setq warning-minimum-level :emergency)
(setq native-comp-async-report-warnings-errors 'silent) ;; native-comp warning
(setq byte-compile-warnings '(not free-vars unresolved noruntime lexical make-local))

(setq package-native-compile t
      native-comp-deferred-compilation t
      native-comp-async-report-warnings-errors nil)

;; remove old eln versions
;; (native-compile-prune-cache)

;; optimizations (froom Doom's core.el). See that file for descriptions.
(setq idle-update-delay 1.0)

(defun desktop-restore-frameset ()
  "Restore the state of a set of frames.
This function depends on the value of `desktop-saved-frameset'
being set (usually, by reading it from the desktop)."
  (when (desktop-restoring-frameset-p)
    (frameset-restore desktop-saved-frameset
		      :reuse-frames (eq desktop-restore-reuses-frames t)
		      :cleanup-frames (not (eq desktop-restore-reuses-frames 'keep))
		      :force-display desktop-restore-in-current-display
		      :force-onscreen (and desktop-restore-forces-onscreen
                                           (display-graphic-p)))
    ;; When at least one restored frame contains a tab bar,
    ;; enable `tab-bar-mode' that takes care about recalculating
    ;; the correct values of the frame parameter `tab-bar-lines'
    ;; (that depends on `tab-bar-show'), and also loads graphical buttons.
    (when (seq-some
           (lambda (frame)
             (menu-bar-positive-p (frame-parameter frame 'tab-bar-lines)))
           (frame-list))
      (tab-bar-mode -1))))

;;; init-mu4e.el --- mu4e configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; Notes about config

(defun me()
  (interactive)
  (insert "Karlo Guidoni <karloguidoni2@gmail.com>"))

;;;; Installation

;; In Arch Linux, you can get `mu' via: `yay -S mu'.
;;  NOTE 2022-07-06: Here, I'm running the version 1.6.11. There are
;;  so many change in the latest version which broken most of my
;;  configuration.

;; Initialize `mu' with (update too):
;; mu init --maildir=~/.local/share/mail/kguidonimartins --my-address=kguidonimartins@gmail.com

;; If you want setup two or more accounts, pass the basename for the
;; maildir then list all email accounts:
;; mu init --maildir=~/.local/share/mail --my-address=kguidonimartins@gmail.com --my-address=karloguidoni2@gmail.com

;; when the following error occur:
;; error: failed to open store @ /home/karlo/.cache/mu/xapian: Unable to get write lock on /home/karlo/.cache/mu/xapian: already locked
;; just run:
;; pkill -2 -u $UID mu
;; then:
;; mu init --maildir=~/.local/share/mail --my-address=kguidonimartins@gmail.com --my-address=karloguidoni2@gmail.com
;; then:
;; mu index

;;;; Gmail two factor authentication

;; Also, because the two factor authentication, create less secure app
;; at https://myaccount.google.com/apppasswords, copy the password and
;; insert when send the first email.

;; Other resources to setup `mu4e'
;; CHECK:
;; - https://github.com/panjie/mu4e-goodies
;; - https://www.reddit.com/r/emacs/comments/bfsck6/mu4e_for_dummies/
;; - https://pragmaticemacs.wordpress.com/mu4e-tutorials/

;;; BEG: mu4e config

(use-package mu4e
  :straight (:type built-in)
  :config
  (require 'mu4e)
  ;; just for `mu4e-headers-mark-all-unread-read' (mark as read all unread messages).
  (require 'mu4e-contrib)

;;;; contexts (accounts)

  (require 'smtpmail)
  (setq mu4e-contexts
        `(
          ;; University/Work account
          ,(make-mu4e-context
            :name "A kguidonimartins"
            :enter-func (lambda () (mu4e-message "Entering Work context"))
            :leave-func (lambda () (mu4e-message "Leaving Work context"))
            :match-func
            (lambda (msg)
              (when msg
                (string-prefix-p "/kguidonimartins" (mu4e-message-field msg :maildir))))
            :vars '((user-mail-address      . "kguidonimartins@gmail.com")
                    (user-full-name         . "Karlo Guidoni Martins")

                    (message-send-mail-function . smtpmail-send-it)
                    (smtpmail-smtp-server   . "smtp.gmail.com")
                    (smtpmail-smtp-service  . 465)
                    (smtpmail-stream-type   . ssl)
                    (smtpmail-smtp-user . "kguidonimartins")
                    (smtpmail-auth-credentials . (expand-file-name "~/.authinfo"))
                    (smtpmail-default-smtp-server . "smtp.gmail.com")
                    (smtpmail-debug-info . t)
                    (smtpmail-debug-verbose . t)

                    ;; (mu4e-compose-signature . (concat "Karlo Guidoni\n"))
                    (mu4e-drafts-folder     . "/kguidonimartins/[Gmail].Drafts")
                    (mu4e-sent-folder       . "/kguidonimartins/[Gmail].Sent Mail")
                    (mu4e-refile-folder     . "/kguidonimartins/[Gmail].All Mail")
                    (mu4e-trash-folder      . "/kguidonimartins/[Gmail].Trash")))
          ;; New account for 2022: migrating all services
          ,(make-mu4e-context
            :name "S karloguidoni2"
            :enter-func (lambda () (mu4e-message "Entering Personal context"))
            :leave-func (lambda () (mu4e-message "Leaving Personal context"))
            :match-func
            (lambda (msg)
              (when msg
                (string-prefix-p "/karloguidoni2" (mu4e-message-field msg :maildir))))
            :vars '((user-mail-address      . "karloguidoni2@gmail.com")
                    (user-full-name         . "Karlo Guidoni")

                    (message-send-mail-function . smtpmail-send-it)
                    (smtpmail-smtp-server   . "smtp.gmail.com")
                    (smtpmail-smtp-service  . 465)
                    (smtpmail-stream-type   . ssl)
                    (smtpmail-smtp-user . "karloguidoni2")
                    (smtpmail-auth-credentials . (expand-file-name "~/.authinfo"))
                    (smtpmail-default-smtp-server . "smtp.gmail.com")
                    (smtpmail-debug-info . t)
                    (smtpmail-debug-verbose . t)

                    ;; (mu4e-compose-signature . (concat "Karlo Guidoni\n"))
                    (mu4e-drafts-folder     . "/karloguidoni2/[Gmail].Drafts")
                    (mu4e-sent-folder       . "/karloguidoni2/[Gmail].Sent Mail")
                    (mu4e-refile-folder     . "/karloguidoni2/[Gmail].All Mail")
                    (mu4e-trash-folder      . "/karloguidoni2/[Gmail].Trash")))
          ;; businnes account
          ,(make-mu4e-context
            :name "D data.broomer"
            :enter-func (lambda () (mu4e-message "Entering businnes context"))
            :leave-func (lambda () (mu4e-message "Leaving businnes context"))
            :match-func
            (lambda (msg)
              (when msg
                (string-prefix-p "/data.broomer" (mu4e-message-field msg :maildir))))
            :vars '((user-mail-address      . "data.broomer@gmail.com")
                    (user-full-name         . "Karlo Guidoni")

                    (message-send-mail-function . smtpmail-send-it)
                    (smtpmail-smtp-server   . "smtp.gmail.com")
                    (smtpmail-smtp-service  . 465)
                    (smtpmail-stream-type   . ssl)
                    (smtpmail-smtp-user . "data.broomer")
                    (smtpmail-auth-credentials . (expand-file-name "~/.authinfo"))
                    (smtpmail-default-smtp-server . "smtp.gmail.com")
                    (smtpmail-debug-info . t)
                    (smtpmail-debug-verbose . t)

                    ;; (mu4e-compose-signature . (concat "Karlo Guidoni\n"))
                    (mu4e-drafts-folder     . "/data.broomer/[Gmail].Drafts")
                    (mu4e-sent-folder       . "/data.broomer/[Gmail].Sent Mail")
                    (mu4e-refile-folder     . "/data.broomer/[Gmail].All Mail")
                    (mu4e-trash-folder      . "/data.broomer/[Gmail].Trash")))
          )
        )

;;;; setqs

  ;; This allows me to use to select mailboxes
  (setq mu4e-completing-read-function 'ivy-completing-read)
  ;; Why would I want to leave my message open after I've sent it?
  (setq message-kill-buffer-on-exit t)
  ;; Don't ask to quit... why is this the default?
  (setq mu4e-confirm-quit nil)
  ;; This is set to 't' to avoid mail syncing issues when using mbsync
  (setq mu4e-change-filenames-when-moving t)
  ;; Refresh mail using isync every 10 minutes
  (setq mu4e-update-interval (* 10 60))
  ;; Define sync command
  (setq mu4e-get-mail-command "mbsync -a")
  ;; Define dirname for mail storage
  (setq mu4e-maildir "~/.local/share/mail")
  ;; auto refresh index on background
  (setq mu4e-index-update-in-background t)
  ;; auto refresh view
  (setq mu4e-headers-auto-update t)
  ;; always show info about syncing process
  (setq mu4e-hide-index-messages nil)
  ;; Make sure plain text mails flow correctly for recipients
  (setq mu4e-compose-format-flowed t)
  ;; Configure the function to use for sending mail
  (setq message-send-mail-function 'smtpmail-send-it)
  ;; enable inline images
  (setq mu4e-view-show-images t)
  ;; use imagemagick, if available
  (when (fboundp 'imagemagick-register-types)
    (imagemagick-register-types))
  ;; don't save message to Sent Messages, IMAP takes care of this
  (setq mu4e-sent-messages-behavior 'delete)
  ;; ;; NOTE: Only use this if you have set up a GPG key! Automatically sign all outgoing mails
  ;; (add-hook 'message-send-hook 'mml-secure-message-sign-pgpmime)
  ;; select the first context without asking
  (setq mu4e-context-policy 'pick-first)
  (eval-after-load 'mu4e #'(lambda() (mu4e 0)))
  ;; ask before compose
  (setq mu4e-compose-context-policy 'always-ask)
  ;; define download attachment folder
  (setq mu4e-attachment-dir  "~/")
  ;; don't include me on reply
  (setq mu4e-compose-dont-reply-to-self t)
  ;; from vxlabs config: (show full addresses in view message (instead of just names). toggle per name with M-RET
  (setq mu4e-view-show-addresses 't)
  ;; box shortcuts
  (setq mu4e-maildir-shortcuts
        '(("/kguidonimartins/INBOX"             . ?i)
          ("/kguidonimartins/[Gmail].Sent Mail" . ?s)
          ("/kguidonimartins/[Gmail].Trash"     . ?t)
          ("/kguidonimartins/[Gmail].Drafts"    . ?d)
          ("/kguidonimartins/[Gmail].All Mail"  . ?a)
          ("/karloguidoni2/INBOX"               . ?I)
          ("/karloguidoni2/[Gmail].Sent Mail"   . ?S)
          ("/karloguidoni2/[Gmail].Trash"       . ?T)
          ("/karloguidoni2/[Gmail].Drafts"      . ?D)
          ("/karloguidoni2/[Gmail].All Mail"    . ?A)
          ("/data.broomer/INBOX"               . ?Y)
          ("/data.broomer/[Gmail].Sent Mail"   . ?U)
          ("/data.broomer/[Gmail].Trash"       . ?O)
          ("/data.broomer/[Gmail].Drafts"      . ?P)
          ("/data.broomer/[Gmail].All Mail"    . ?Z)))
  ;; fields on view
  (setq mu4e-headers-fields '(
                              (:flags          . 6)
                              (:date           . 12)
                              (:from           . 30)
                              (:thread-subject . 110)
                              (:maildir        . 200)))
  ;; fancy characters (including icons)
  (setq mu4e-use-fancy-chars t)
  ;; change date-format
  (setq mu4e-headers-date-format "%Y/%m/%d")
  (setq
   mu4e~headers-mode-line-label "~~mu4e~~"
   mu4e-headers-include-related t
   mu4e-headers-skip-duplicates t
   mu4e-headers-thread-first-child-prefix   '("o " . "o ")
   mu4e-headers-thread-child-prefix         '("|>" . "|>")
   mu4e-headers-thread-duplicate-prefix     '("="  . "=")
   mu4e-headers-thread-connection-prefix    '("|"  . "|")
   mu4e-headers-threaded-label              '("T"  . "T")
   mu4e-headers-thread-root-prefix          '("* " . "* ")
   mu4e-headers-thread-blank-prefix         '(" "  . "  ")
   mu4e-headers-thread-orphan-prefix        '("<>" . "<>")
   mu4e-headers-thread-last-child-prefix    '("L"  . "L")
   mu4e-headers-thread-single-orphan-prefix '("<>" . "<>")
   ;; mu4e-html2text-command "w3m -dump -T text/html -cols 80 -o display_link_number=true -o auto_image=false -o display_image=false -o ignore_null_img_alt=true"
   mu4e-html2text-command "mu4e-shr2text"
   mu4e-view-prefer-html nil
   )
  ;; icons on view (needs `mu4e-use-fancy-chars')
  (setq
   mu4e-headers-draft-mark     '("D" . " ")
   mu4e-headers-flagged-mark   '("F" . " ")
   mu4e-headers-new-mark       '("N" . " ")
   mu4e-headers-passed-mark    '("P" . " ")
   mu4e-headers-replied-mark   '("R" . " ")
   mu4e-headers-seen-mark      '("S" . " ")
   mu4e-headers-trashed-mark   '("T" . " ")
   mu4e-headers-attach-mark    '("a" . " ")
   mu4e-headers-encrypted-mark '("x" . " ")
   mu4e-headers-signed-mark    '("s" . " ")
   mu4e-headers-unread-mark    '("u" . " ")
   mu4e-headers-list-mark      '("s" . " ")
   mu4e-headers-personal-mark  '("p" . " ")
   mu4e-headers-calendar-mark  '("c" . " "))

;;;; defuns

  (defun funk/mu4e-update-marks-and-index ()
    "Update view based on recent marks (read and delete)
and update mu index to redisplay new messages."
    (interactive)
    (progn
      (mu4e-headers-rerun-search)
      (mu4e-update-index)))

  (defun funk/mu4e-full-update ()
    "Run`funk/mu4e-update-marks-and-index' and update mail."
    (interactive)
    (progn
      (funk/mu4e-update-marks-and-index)
      (mu4e-update-mail-and-index t)))

  (defun funk/mu4e-jump-to-the-first-inbox ()
    (interactive)
    (progn
      (projectile-switch-project-by-name "~/")
      (kill-buffer "karlo/")
      (mu4e~headers-jump-to-maildir "/kguidonimartins/INBOX")))

  (defun funk/mu4e-set-account-for-compose-or-reply ()
    "Set the account for composing or replying a message."
    (if mu4e-compose-parent-message
        (let ((mail (cdr (car (mu4e-message-field mu4e-compose-parent-message :to)))))
          (if (member mail (mu4e-personal-addresses))
              (setq user-mail-address mail)
            (setq user-mail-address "kguidonimartins@gmail.com")))
      (ivy-read "Account: " (mu4e-personal-addresses))))

  (defun funk/fix-mu4e-display ()
    (interactive)
    (progn
      (visual-line-mode -1)
      (toggle-truncate-lines +1)))

  ;; ;; rewrite the original mode-line for mu4e-header
  ;; (defun mu4e~headers-update-mode-line ()
  ;;   "Update mode-line settings."
  ;;   (let* ((flagstr "")
  ;;          (name "mu4e-headers"))
  ;;     (setq mode-name name)
  ;;     (setq mu4e~headers-mode-line-label (concat flagstr "" mu4e--search-last-query))
  ;;     (make-local-variable 'global-mode-string)
  ;;     (add-to-list 'global-mode-string
  ;;                  `(:eval
  ;;                    (concat
  ;;                     (propertize
  ;;                      (mu4e~quote-for-modeline ,mu4e~headers-mode-line-label)
  ;;                      'face 'mu4e-modeline-face)
  ;;                     " "
  ;;                     (if (and mu4e-display-update-status-in-modeline
  ;;                              (buffer-live-p mu4e~update-buffer)
  ;;                              (process-live-p (get-buffer-process
  ;;                                               mu4e~update-buffer)))
  ;;                         (propertize " (updating)" 'face 'mu4e-modeline-face)
  ;;                       ""))))))

  (defun mu4e~headers-update-mode-line ()
    "Update mode-line settings."
    (let* ((flagstr
            (mapconcat
	     (lambda (flag-cell)
               (if (car flag-cell)
                   (if mu4e-use-fancy-chars
                       (cddr flag-cell) (cadr flag-cell) ) ""))
             `((,mu4e-search-full             . ,mu4e-headers-full-label)
               (,mu4e-headers-include-related . ,mu4e-headers-related-label)
               (,mu4e-search-threads          . ,mu4e-headers-threaded-label)
	       (,mu4e-headers-skip-duplicates . ,mu4e-headers-skip-duplicates-label))
             ""))
           (name "mu4e-headers"))

      (setq mode-name name)
      (setq mu4e~headers-mode-line-label (concat " " mu4e--search-last-query))

      (make-local-variable 'global-mode-string)

      (add-to-list 'global-mode-string
                   `(:eval
                     (concat
                      (propertize
                       (mu4e-quote-for-modeline ,mu4e~headers-mode-line-label)
                       'face 'mu4e-modeline-face)
                      " "
                      (if (and mu4e-display-update-status-in-modeline
                               (buffer-live-p mu4e--update-buffer)
                               (process-live-p (get-buffer-process
                                                mu4e--update-buffer)))
                          (propertize " (updating)" 'face 'mu4e-modeline-face)
                        ""))))))

  ;; NOTE 2022-07-06: Attach files `dired' into `mu4e'.
  ;; FROM: https://www.djcbsoftware.nl/code/mu/mu4e/Attaching-files-with-dired.html
  (require 'gnus-dired)
  ;; make the `gnus-dired-mail-buffers' function also work on
  ;; message-mode derived modes, such as mu4e-compose-mode
  (defun gnus-dired-mail-buffers ()
    "Return a list of active message buffers."
    (let (buffers)
      (save-current-buffer
        (dolist (buffer (buffer-list t))
          (set-buffer buffer)
          (when (and (derived-mode-p 'message-mode)
                     (null message-sent-message-via))
            (push (buffer-name buffer) buffers))))
      (nreverse buffers)))

  (setq gnus-dired-mail-mode 'mu4e-user-agent)
  (add-hook 'dired-mode-hook 'turn-on-gnus-dired-mode)

  (defun funk/dired-attach-files-into-mu4e ()
    "Wrapper around `gnus-dired-attach' for ensure marked files in
dired before attach to a message."
    (interactive)
    (if (eq major-mode 'dired-mode)
        (let ((files-to-attach (dired-get-marked-files)))
          (when (eql (length files-to-attach) 1)
            (progn
              (message "INFO: You can attach more files marking them")
              (sleep-for 2)))
          (gnus-dired-attach files-to-attach))
      (user-error "Not in dired")))

  (defun mml-attach-file--go-to-eob (orig-fun &rest args)
    "Go to the end of buffer before attaching files.
Always put attachments to the bottom of email
http://mbork.pl/2015-11-28_Fixing_mml-attach-file_using_advice
FROM:
https://github.com/panjie/mu4e-goodies/blob/254768ec469bb9de6c7051b62faeb5dda56d15fa/mu4e-goodies-hacks.el#L74-L87
"
    (save-excursion
      (save-restriction
        (widen)
        (goto-char (point-max))
        (apply orig-fun args))))

  (advice-add 'mml-attach-file :around #'mml-attach-file--go-to-eob)
  (advice-add 'mml-dnd-attach-file :around #'mml-attach-file--go-to-eob)

  (defun funk/mu4e-all-inboxes ()
    (interactive)
    (mu4e-headers-search-bookmark "maildir:/kguidonimartins/INBOX OR maildir:/karloguidoni2/INBOX OR maildir:/data.broomer/INBOX" nil))

;;;; keys

  (general-define-key
   :keymaps '(mu4e-main-mode-map)
   :states '(normal)
   "j" #'mu4e~headers-jump-to-maildir
   "b" #'mu4e-headers-search-bookmark)

  (general-define-key
   :keymaps '(mu4e-view-mode-map)
   :states '(normal)
   "SPC" nil)

  (general-define-key
   :keymaps '(mu4e-headers-mode-map)
   :states '(normal)
   "gr" 'funk/mu4e-update-marks-and-index
   "gu" 'funk/mu4e-full-update
   "/" 'swiper)

  (defalias 'mu 'funk/mu4e-jump-to-the-first-inbox)

  (global-set-key (kbd "C-c m") 'funk/mu4e-jump-to-the-first-inbox)

;;;; add-to-list

  ;; Include a bookmark to open all of my inboxes
  ;; (add-to-list 'mu4e-bookmarks
  ;;              (make-mu4e-bookmark
  ;;               :name "All Inboxes"
  ;;               :query "maildir:/kguidonimartins/INBOX OR maildir:/karloguidoni2/INBOX OR maildir:/data.broomer/INBOX"
  ;;               :key ?i))

  (add-to-list 'mu4e-header-info-custom
               '(:empty . (:name "Empty"
                                 :shortname ""
                                 :function (lambda (msg) "  "))))

;;;; hooks

  (add-hook 'mu4e-compose-pre-hook 'funk/mu4e-set-account-for-compose-or-reply)
  ;; HACK 2022-07-06: when composing with attachments, `openwith-mode' open files
  ;;                  right before sending the message (i.e. the message isn't sent!).
  (add-hook 'mu4e-compose-pre-hook (lambda ()
                                     (openwith-mode -1)
                                     (flyspell-mode)
                                     ;; (electric-pair-mode -1)
                                     ))
  (add-hook 'mu4e-compose-mode-hook (lambda () (electric-pair-mode -1)))
  (add-hook 'mu4e-headers-mode-hook 'funk/fix-mu4e-display)
  ;; NOTE: turn on this hook when working with `org-mime' and `htmlize'.
  ;; (add-hook 'mu4e-compose-mode-hook (lambda () (org-mime-edit-mail-in-org-mode)))


;;;; miscellaneous

  ;; attachment reminder based on
  ;; http://emacs-fu.blogspot.co.uk/2008/12/highlighting-todo-fixme-and-friends.html
  (set-face-attribute 'font-lock-warning-face nil :foreground "red" :weight 'bold :background "yellow")
  (add-hook 'mu4e-compose-mode-hook
            (defun contrib/mu4e-highlight-attachment-keywords ()
              "Flag attachment keywords"
              (font-lock-add-keywords nil
                                      '(("\\(attach\\|pdf\\|file\\|anexo\\|arquivo\\|planilha\\|documento\\)" 1 font-lock-warning-face t)))))

  (add-hook 'org-msg-mode-hook
            (defun contrib/mu4e-highlight-attachment-keywords ()
              "Flag attachment keywords"
              (font-lock-add-keywords nil
                                      '(("\\(attach\\|pdf\\|file\\|anexo\\|arquivo\\|planilha\\|documento\\)" 1 font-lock-warning-face t)))))

  )


;;; END: mu4e config

;;; org-mime and htmlize

(use-package org-mime
  :after mu4e

  ;; Composing with `org-mode'
  ;; Try `org-mime-edit-mail-in-org-mode'.
  ;; `org-mime-org-buffer-htmlize' : Send an entire org-mode buffer as an e-mail message
  ;; `org-mime-org-subtree-htmlize': Send a subtree of the current org-mode buffer as an e-mail message composing with org mode
  )

(use-package htmlize
  :after mu4e

  :config
  ;; NOTE 2022-07-10: `org-msg' works nice too. Let's see!
  ;; (add-hook 'message-send-hook 'org-mime-htmlize)
  ;; (add-hook 'message-send-hook 'org-mime-confirm-when-no-multipart)
  (setq org-mime-export-options '(
                                  :section-numbers nil
                                  :with-author nil
                                  :with-toc nil
                                  )))

;;; mu4e-column-faces

;; (use-package mu4e-column-faces
;;   :after mu4e
;;   :config (mu4e-column-faces-mode))

;;; org-msg

(use-package org-msg
  :after mu4e

  :config
  (setq mail-user-agent 'mu4e-user-agent)
  (setq org-msg-options "html-postamble:nil num:nil ^:{} toc:nil author:nil email:nil \\n:t tex:imagemagick"
        org-msg-startup "inlineimages"
        org-msg-default-alternatives '((new           . (text html))
                                       (reply-to-html . (text html))
                                       (reply-to-text . (text)))
        org-msg-convert-citation t
        org-msg-signature "

Att,

#+begin_signature
--
*Karlo Guidoni*
[[https://karloguidoni.com/][karloguidoni.com]]
#+end_signature")
  (org-msg-mode)
  (add-hook 'org-msg-edit-mode-hook
            (lambda ()
              (org-indent-mode -1)
              ;; (electric-pair-mode -1)
              (setq-local org-html-indent nil)
              ))
  ;; NOTE: the CSS is defined in `org-msg-default-style' but need to be changed
  ;; via `org-msg-enforce-css', like below:
  (setq org-msg-enforce-css
        (let* ((font-family '(font-family . "\"Arial\""))
               (font-size '(font-size . "10pt"))
               (font `(,font-family ,font-size))
               (line-height '(line-height . "10pt"))
               (bold '(font-weight . "bold"))
               (theme-color "#000000")
               (color `(color . ,theme-color))
               (table `(,@font (margin-top . "0px")))
               (ftl-number `(,@font ,color ,bold (text-align . "left")))
               (inline-modes '(asl c c++ conf cpp csv diff ditaa emacs-lisp
                                   fundamental ini json makefile man org plantuml
                                   python sh xml))
               (inline-src `((color . ,(face-foreground 'default))
                             (background-color . ,(face-background 'default))))
               (code-src
                (mapcar (lambda (mode)
                          `(code ,(intern (concat "src src-" (symbol-name mode)))
                                 ,inline-src))
                        inline-modes))
               (base-quote '((padding-left . "5px") (margin-left . "10px")
                             (margin-top . "10px") (margin-bottom . "0")
                             (font-style . "italic") (background . "#f9f9f9")))
               (quote-palette '("#324e72" "#6a3a4c" "#7a4900" "#ff34ff"
                                "#ff4a46" "#008941" "#006fa6" "#a30059"
                                "#ffdbe5" "#000000" "#0000a6" "#63ffac"))
               (quotes
                (mapcar (lambda (x)
                          (let ((c (nth x quote-palette)))
                            `(blockquote ,(intern (format "quote%d" (1+ x)))
                                         (,@base-quote
                                          (color . ,c)
                                          (border-left . ,(concat "3px solid "
                                                                  (org-msg-lighten c)))))))
                        (number-sequence 0 (1- (length quote-palette))))))
          `((del nil (,@font (color . "grey") (border-left . "none")
                             (text-decoration . "line-through") (margin-bottom . "0px")
                             (margin-top . "10px") (line-height . "11pt")))
            (a nil (,color))
            (a reply-header ((color . "black") (text-decoration . "none")))
            (div reply-header ((padding . "3.0pt 0in 0in 0in")
                               (border-top . "solid #e1e1e1 1.0pt")
                               (margin-bottom . "20px")))
            (span underline ((text-decoration . "underline")))
            (li nil (,@font ,line-height (margin-bottom . "0px")
                            (margin-top . "2px")))
            (nil org-ul ((list-style-type . "square")))
            (nil org-ol (,@font ,line-height (margin-bottom . "0px")
                                (margin-top . "0px") (margin-left . "30px")
                                (padding-top . "0px") (padding-left . "5px")))
            (nil signature (,@font (margin-bottom . "20px")))
            (blockquote quote0 ,(append base-quote '((border-left . "3px solid #ccc"))))
            ,@quotes
            (code nil (,font-size (font-family . "monospace") (background . "#FFFFFF")))
            ,@code-src
            (nil linenr ((padding-right . "1em")
                         (color . "black")
                         (background-color . "#aaaaaa")))
            (pre nil ((line-height . "12pt")
                      ,@inline-src
                      (margin . "0px")
                      (font-size . "9pt")
                      (font-family . "monospace")))
            (div org-src-container ((margin-top . "10px")))
            (nil figure-number ,ftl-number)
            (nil table-number)
            (caption nil ((text-align . "left")
                          (background . ,theme-color)
                          (color . "white")
                          ,bold))
            (nil t-above ((caption-side . "top")))
            (nil t-bottom ((caption-side . "bottom")))
            (nil listing-number ,ftl-number)
            (nil figure ,ftl-number)
            (nil org-src-name ,ftl-number)

            (table nil (,@table ,line-height (border-collapse . "collapse")))
            (th nil ((border . "1px solid white") (background-color . ,theme-color) (color . "white") (padding-left . "10px") (padding-right . "10px")))
            (td nil (,@table (padding-left . "10px") (padding-right . "10px") (background-color . "#f9f9f9") (border . "1px solid white"))) (td org-left ((text-align . "left")))
            (td org-right ((text-align . "right")))
            (td org-center ((text-align . "center")))

            (div outline-text-4 ((margin-left . "15px")))
            (div outline-4 ((margin-left . "10px")))
            (h4 nil ((margin-bottom . "0px") (font-size . "11pt") ,font-family))
            (h3 nil ((margin-bottom . "0px") (text-decoration . "none") ,color (font-size . "12pt") ,font-family))
            (h2 nil ((margin-top . "20px") (margin-bottom . "20px") (font-style . "italic") ,color (font-size . "13pt") ,font-family))
            (h1 nil ((margin-top . "20px") (margin-bottom . "0px") ,color (font-size . "12pt") ,font-family))
            (p nil ((text-decoration . "none") (margin-bottom . "0px") (margin-top . "10px") (line-height . "11pt") ,font-size ,font-family))
            (div nil (,@font (line-height . "11pt")))))))

;;; notmuch

(use-package notmuch
  :config
  (setq notmuch-search-oldest-first nil))

;;; mu4e-alert

;; (use-package mu4e-alert
;;   :config
;;   (mu4e-alert-set-default-style 'libnotify)
;;   (mu4e-alert-enable-notifications))

(provide 'init-mu4e)
;;; init-mu4e.el ends here

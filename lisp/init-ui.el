;;; init-ui.el --- Configuration for Emacs UI -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; dashboard

;; (use-package dashboard
;;   :config
;;   (dashboard-setup-startup-hook)
;;   (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
;;   ;; Content is not centered by default. To center, set
;;   (setq dashboard-center-content t)
;;   ;; To disable shortcut "jump" indicators for each section, set
;;   (setq dashboard-show-shortcuts t)
;;   (setq dashboard-startup-banner 'logo)
;;   (setq dashboard-items '(
;;                           (agenda . 10)
;;                           (projects . 10)
;;                           (recents  . 10)
;;                           ;; (bookmarks . 5)
;;                           ))
;;   (setq dashboard-set-heading-icons t)
;;   (setq dashboard-set-file-icons t)
;;   (setq dashboard-projects-switch-function 'counsel-projectile-switch-project-by-name))

;;; help
(use-package help
  :straight (:type built-in)
  :config
  (setq help-window-select t))


;;; goto-address-mode
(global-goto-address-mode +1)

;;; link-hint

;; (use-package link-hint
;;   :after evil
;;   ;; :config
;;   ;; (define-key evil-normal-state-map (kbd "SPC f") 'link-hint-open-link)
;;   )

;;; frame transparency
;; Make frame transparency overridable
(defvar funk/frame-transparency '(100 . 97)
  "Alist defining the transparency values for active and inactive frames.")

;; Set frame transparency
(set-frame-parameter nil 'alpha funk/frame-transparency)
(set-frame-parameter nil 'alpha-background 100)
(add-to-list 'default-frame-alist `(alpha . ,funk/frame-transparency))

(defun funk/toggle-alpha-background (&optional alpha)
  "Toggle alpha-background (i.e. make the background transparent
without touching on foreground transparency). Set 75 when ALPHA value
is not supplied. This is a toggle function: when setting ALPHA, run
it twice to take effect."
  (interactive)
  (if (eql (frame-parameter nil 'alpha-background) 100)
      (progn
        (set-frame-parameter nil 'alpha-background (if (eql alpha nil) 75 alpha))
        (message (format "Setting alpha-background to %d" (if (eql alpha nil) 75 alpha))))
    (progn
      (set-frame-parameter nil 'alpha-background 100)
      (message "Setting alpha-background to 100"))))


;; Set maximized frame
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;;; window divider

;; (setq window-divider-default-bottom-width 1
;;       window-divider-default-right-width 1)

;; (window-divider-mode)

;;; miscellaneous UI configurations

(use-package emacs
  :straight (:type built-in)
  :config

  (setq pixel-scroll-precision-mode t)

  ;; FROM: https://www.reddit.com/r/emacs/comments/v7n1nc/what_does_an_orange_small_dot_on_the_lefthand/
  ;; Remove a mark (a dot) in fringe when jumping the bookmarks.
  (setq bookmark-set-fringe-mark nil)

  (setq max-mini-window-height 10)
  (setq use-dialog-box nil)

  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  (tooltip-mode -1)
  (menu-bar-mode -1)

  (context-menu-mode)

  (setq-default cursor-in-non-selected-windows nil)

  (blink-cursor-mode +1)

  (defun funk-initial-buffer-choice ()
    (get-buffer "*Messages*"))
  (setq initial-buffer-choice 'funk-initial-buffer-choice)

  (setq use-short-answers t) ; emacs 28

  (setq inhibit-startup-message t)

  (setq visible-bell nil)

  (column-number-mode)
  (global-visual-line-mode t) ; word wrap; dont show arrow

  (setq global-display-fill-column-indicator-mode nil)
  (setq fill-column 79)

  ;; Disable line numbers
  (global-display-line-numbers-mode -1)
  ;; (setq display-line-numbers 'relative)

  (dolist (mode '(org-mode-hook
                  term-mode-hook
                  shell-mode-hook
                  treemacs-mode-hook
                  eshell-mode-hook
                  inferior-ess-mode-hook))
    (add-hook mode (lambda () (display-line-numbers-mode 0))))

  (setq confirm-kill-emacs 'y-or-n-p)

  ;; nice scrolling
  (setq scroll-step 1)
  (setq scroll-margin 0
        scroll-conservatively 100000
        scroll-preserve-screen-position 1
        scroll-preserve-screen-position 'always
        next-screen-context-lines 5)

  ;; Echo keystrokes a little faster
  (setq echo-keystrokes 0.1)


  ;; turn on highlight matching brackets
  (show-paren-mode 1)

  ;; highlight brackets
  ;; can be: parenthesis, expression, or mixed
  (setq show-paren-style 'parenthesis)

  ;; ;; maintain a header in all buffers
  ;; (defun funk/custom-header ()
  ;;   (setq header-line-format " "))
  ;; (add-hook 'buffer-list-update-hook 'funk/custom-header)

  ;; ;; set a internal border
  ;; (add-to-list 'default-frame-alist '(internal-border-width . 15))
  (set-fringe-mode 5)

  ;; remove truncate line indicator
  (setq-default fringe-indicator-alist (assq-delete-all 'truncation fringe-indicator-alist))

  ;; (setq-default fringe-indicator-alist
  ;;               '((continuation nil nil)
  ;;                 (truncation nil nil)
  ;;                 (overlay-arrow . nil)
  ;;                 (up . nil)
  ;;                 (down . nil)
  ;;                 (top nil nil)
  ;;                 (bottom nil nil nil nil)
  ;;                 (top-bottom nil nil nil nil)
  ;;                 (empty-line . nil)
  ;;                 (unknown . nil)))

  ;; don't truncate line in occur-mode
  (add-hook 'occur-mode-hook (lambda ()
                               (visual-line-mode -1)
                               (toggle-truncate-lines 1)
                               ))
  ;; title bar
  (defun frame-title-string ()
    "Return the file name of current buffer, using ~ if under home directory"
    (let ((ftname (or
                   (buffer-file-name (current-buffer))
                   (buffer-name)))

          (host (if (file-remote-p default-directory)
                    (tramp-file-name-host
                     (tramp-dissect-file-name default-directory))
                  (system-name))))

      (if (string-match (getenv "HOME") ftname)
          (setq fname (concat "[emacs@" host "] " (replace-match "~" t t ftname)))
        (setq fname (concat "[emacs@" host "] " ftname))
        fname)))

  (setq frame-title-format '((:eval (frame-title-string)) " %m ")))

;;; hl-block-mode

(use-package hl-block-mode
  :disabled
  :commands (hl-block-mode)
  :config
  (setq hl-block-bracket nil)    ;; Match all brackets.
  (setq hl-block-single-level t) ;; Only one pair of brackets.
  (setq hl-block-style 'bracket) ;; or color-tint
  :hook ((prog-mode ess-mode ess-r-mode emacs-lisp-mode) . hl-block-mode))

;;; highlight-parentheses

(use-package highlight-parentheses
  :config
  (highlight-parentheses-mode +1))

;;; beacon
;; (use-package beacon
;;   :disabled
;;   ;; cursor visual clues
;;   :config
;;   (beacon-mode -1))

;;; pulse (pulse region like beacon)
(defun pulse-line (&rest _)
  "Pulse the current line."
  (pulse-momentary-highlight-one-line (point)))

(dolist (command '(
                   scroll-up-command
                   scroll-down-command
                   recenter-top-bottom
                   other-window
                   evil-window-up
                   evil-window-down
                   evil-window-left
                   evil-window-right
                   switch-to-buffer
                   counsel-outline
                   consult-outline
                   consult-line
                   funk/consult-outline
                   funk/consult-line
                   swiper
                   swiper-all
                   ))
  (advice-add command :after #'pulse-line))

;;; all-the-icons
(use-package all-the-icons

  :init
  (when (and (not (member "all-the-icons" (font-family-list)))
             (window-system))
    (all-the-icons-install-fonts t))
  :config
  (setq all-the-icons-mode-icon-alist (remove '(messages-buffer-mode all-the-icons-faicon "file-o" :v-adjust 0.0 :face all-the-icons-dsilver) all-the-icons-mode-icon-alist))
  (add-to-list 'all-the-icons-mode-icon-alist '(messages-buffer-mode all-the-icons-faicon "stack-overflow" :v-adjust 0.0))
  (add-to-list 'all-the-icons-mode-icon-alist '(inferior-ess-mode all-the-icons-faicon "flask" :v-adjust 0.0))
  (add-to-list 'all-the-icons-mode-icon-alist '(inferior-ess-r-mode all-the-icons-faicon "flask" :v-adjust 0.0))
  (add-to-list 'all-the-icons-mode-icon-alist '(ess-r-mode all-the-icons-fileicon "R" :height 0.8))
  (add-to-list 'all-the-icons-mode-icon-alist '(ess-r-help-mode all-the-icons-material "help_outline"))
  ;; (add-to-list 'all-the-icons-mode-icon-alist '(jupyter-repl-mode  all-the-icons-faicon "leaf"))
  (add-to-list 'all-the-icons-mode-icon-alist '(debugger-mode  all-the-icons-material "error_outline"))
  (add-to-list 'all-the-icons-extension-icon-alist '("rproj" all-the-icons-material "fingerprint" :face all-the-icons-lblue))
  (add-to-list 'all-the-icons-extension-icon-alist '("qgz" all-the-icons-faicon "map-marker" :face all-the-icons-lblue))
  (add-to-list 'all-the-icons-extension-icon-alist '("" all-the-icons-faicon "map" :face all-the-icons-lblue))
  (add-to-list 'all-the-icons-extension-icon-alist '("rmd" all-the-icons-faicon "leaf" :face all-the-icons-lblue))
  (add-to-list 'all-the-icons-regexp-icon-alist '("^renv\\.lock" all-the-icons-fileicon "R" :face all-the-icons-lblue))
  (add-to-list 'all-the-icons-regexp-icon-alist '("^\\.Rprofile$" all-the-icons-fileicon "R" :face all-the-icons-lblue))
  (add-to-list 'all-the-icons-regexp-icon-alist '("^\\*Magit Repositories\\*$" all-the-icons-faicon "wrench" :face all-the-icons-lblue))
  (add-to-list 'all-the-icons-regexp-icon-alist '("^tags$" all-the-icons-faicon "tags" :face all-the-icons-lblue))
  (add-to-list 'all-the-icons-regexp-icon-alist '("^TAGS$" all-the-icons-faicon "tags" :face all-the-icons-lblue))
  )

;;; rainbow-mode
(use-package rainbow-mode

  :init
  (add-hook 'lisp-mode-hook #'rainbow-mode)
  (add-hook 'markdown-mode-hook #'rainbow-mode)
  (add-hook 'org-mode-hook #'rainbow-mode)
  (add-hook 'csv-mode-hook #'rainbow-mode)
  (add-hook 'ess-mode-hook #'rainbow-mode)
  (add-hook 'help-mode-hook #'rainbow-mode)
  (add-hook 'inferior-ess-mode-hook #'rainbow-mode)
  :hook prog-mode
  :config (setq-default rainbow-x-colors-major-mode-list '()))

;;; rainbow-delimiters
(use-package rainbow-delimiters

  :hook
  (prog-mode . rainbow-delimiters-mode)
  (ess-r-mode . rainbow-delimiters-mode)
  (ess-mode . rainbow-delimiters-mode))

;;; hl-line-mode
(use-package hl-line

  :straight (:type built-in)
  :config
  (add-hook 'evil-insert-state-exit-hook (lambda() (hl-line-mode -1)))
  (add-hook 'evil-insert-state-entry-hook (lambda() (hl-line-mode +1)))
  (add-hook 'org-agenda-mode-hook (lambda() (hl-line-mode +1)))
  (add-hook 'csv-mode-hook (lambda() (hl-line-mode +1))))

;;; dimmer
;; (use-package dimmer
;;   ;; indicates which buffer is currently active
;;   ;; by dimming the faces in the other buffers
;;   :init
;;   (dimmer-mode t)
;;   :config
;;   (setq-default dimmer-fraction 0.4)
;;   (dimmer-configure-hydra)
;;   (dimmer-configure-magit)
;;   (dimmer-configure-which-key)
;;   (dimmer-configure-company-box)
;;   (dimmer-configure-helm)
;;   (dimmer-configure-org)
;;   (setq dimmer-adjustment-mode :foreground)
;;   (add-to-list 'dimmer-buffer-exclusion-regexps "magit-diff:")
;;   (add-to-list 'dimmer-buffer-exclusion-regexps "*R:")
;;   (add-to-list 'dimmer-buffer-exclusion-regexps "*rg*")
;;   (add-to-list 'dimmer-buffer-exclusion-regexps "*Org Agenda*")
;;   (add-to-list 'dimmer-buffer-exclusion-regexps "*help\\[R\\:")
;;   (add-to-list 'dimmer-buffer-exclusion-regexps "*helpful")
;;   (add-to-list 'dimmer-buffer-exclusion-regexps "*Calendar*"))

;;; tab-bar
;; (defun funk/name-tab-by-project-or-default ()
;;   "Return project name if in a project, or default tab-bar name if not.
;; The default tab-bar name uses the buffer name."
;;   (let ((project-name (projectile-project-name)))
;;     (if (string= "-" project-name)
;;         (tab-bar-tab-name-current)
;;       (concat (projectile-project-name) ":::" (buffer-name) " "))))

;; (setq tab-bar-mode t)
;; (setq tab-bar-show t)
;; (setq tab-bar-new-tab-choice "*new-tab*")
;; (setq tab-bar-tab-name-function #'funk/name-tab-by-project-or-default)
(tab-bar-mode -1)

;;; centaur-tabs

;; (use-package centaur-tabs
;;   :init
;;   (centaur-tabs-mode t)
;;   :config
;;   (centaur-tabs-group-by-projectile-project)
;;   (defun centaur-tabs-hide-tab (x)
;;     "Do no to show buffer X in tabs."
;;     (let ((name (format "%s" x)))
;;       (or
;;        ;; Current window is not dedicated window.
;;        (window-dedicated-p (selected-window))
;;        ;; Buffer name not match below blacklist.
;;        (string-prefix-p "*epc" name)
;;        (string-prefix-p "*helm" name)
;;        (string-prefix-p "*Helm" name)
;;        (string-prefix-p "*Compile-Log*" name)
;;        (string-prefix-p "*lsp" name)
;;        (string-prefix-p "*company" name)
;;        (string-prefix-p "*Flycheck" name)
;;        (string-prefix-p "*tramp" name)
;;        (string-prefix-p " *Mini" name)
;;        (string-prefix-p "*help" name)
;;        (string-prefix-p "*straight" name)
;;        (string-prefix-p " *temp" name)
;;        (string-prefix-p "*Help" name)
;;        (string-prefix-p "*mybuf" name)
;;        (string-prefix-p "*which-key*" name)
;;        ;; ;; Is not magit buffer.
;;        ;; (and (string-prefix-p "magit" name)
;;        ;;      (not (file-name-extension name)))
;;        )))
;;   (setq centaur-tabs-style "bar"
;;         centaur-tabs-height 10
;;         centaur-tabs-plain-icons nil
;;         centaur-tabs-set-icons t
;;         centaur-tabs-set-modified-marker nil
;;         centaur-tabs-show-new-tab-button nil
;;         centaur-tabs-show-navigation-buttons nil
;;         centaur-tabs-set-bar 'under
;;         x-underline-at-descent-line t)
;;   (centaur-tabs-headline-match)
;;   (setq centaur-tabs-label-fixed-length 10)
;;   ;; (setq centaur-tabs-gray-out-icons 'buffer)
;;   (centaur-tabs-enable-buffer-reordering)
;;   (setq centaur-tabs-adjust-buffer-order t)
;;   (setq uniquify-separator "/")
;;   (setq uniquify-buffer-name-style 'forward)
;;
;;   (defun centaur-tabs-projectile-buffer-groups ()
;;     "Return the list of group names BUFFER belongs to."
;;     (if centaur-tabs-projectile-buffer-group-calc
;;         (symbol-value 'centaur-tabs-projectile-buffer-group-calc)
;;       (set (make-local-variable 'centaur-tabs-projectile-buffer-group-calc)
;;
;;            (cond
;;             ((string-equal "*Messages*" (buffer-name)) '("Misc"))
;;             ((string-equal "*Backtrace*" (buffer-name)) '("Misc"))
;;             ((string-match-p "*mu4e" (buffer-name)) '("Mail"))
;;             ((condition-case _err
;;                  (projectile-project-root)
;;                (error nil)) (list (projectile-project-name)))
;;             ((memq major-mode '(emacs-lisp-mode python-mode emacs-lisp-mode c-mode
;;                                                 c++-mode javascript-mode js-mode
;;                                                 js2-mode makefile-mode
;;                                                 lua-mode vala-mode)) '("Coding"))
;;             ((memq major-mode '(nxhtml-mode html-mode
;;                                             mhtml-mode css-mode)) '("HTML"))
;;             ((memq major-mode '(org-mode calendar-mode diary-mode)) '("Org"))
;;             ((memq major-mode '(dired-mode)) '("Dir"))
;;             (t '("Other"))))
;;       (symbol-value 'centaur-tabs-projectile-buffer-group-calc)))
;;
;;   (general-define-key
;;    :keymaps '(centaur-tabs-mode-map)
;;    :states '(normal)
;;    "gt" #'centaur-tabs-forward
;;    "gT" #'centaur-tabs-backward
;;    )
;;
;;   (add-hook 'dired-mode-hook 'centaur-tabs-local-mode)
;;   (add-hook 'which-key-mode-hook 'centaur-tabs-local-mode)
;;   :hook
;;   ((which-key-mode . centaur-tabs-local-mode)
;;    (dashboard-mode . centaur-tabs-local-mode)
;;    ;; (term-mode . centaur-tabs-local-mode)
;;    ;; (vterm-mode . centaur-tabs-local-mode)
;;    (calendar-mode . centaur-tabs-local-mode)
;;    (org-agenda-mode . centaur-tabs-local-mode)
;;    (helpful-mode . centaur-tabs-local-mode))
;;   ;; (magit-status-mode . centaur-tabs-local-mode)
;;   ;; :bind
;;   ;; ("C-<prior>" . centaur-tabs-backward)
;;   ;; ("C-<next>" . centaur-tabs-forward)
;;   ;; ("C-c t s" . centaur-tabs-counsel-switch-group)
;;   ;; ("C-c t p" . centaur-tabs-group-by-projectile-project)
;;   ;; ("C-c t g" . centaur-tabs-group-buffer-groups)
;;   ;; (:map evil-normal-state-map
;;   ;;       ("g t" . centaur-tabs-forward)
;;   ;;       ("g T" . centaur-tabs-backward))
;;   )

(use-package highlight-numbers
  :config
  (add-hook 'prog-mode-hook 'highlight-numbers-mode)
  (highlight-numbers-mode)
  )

(provide 'init-ui)
;;; init-ui.el ends here

;;; init-cloud.el --- Cloud services config -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; AWS

;; CHECK: https://github.com/sebasmonia/awscli-capf
;; CHECK: https://github.com/baron42bba/aws-snippets
;; CHECK: https://github.com/Yuki-Inoue/aws.el

(provide 'init-cloud)
;;; init-cloud.el ends here

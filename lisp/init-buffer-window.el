;;; init-buffer-window.el --- buffer and window configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; window management

(use-package emacs
  :config
  (defun funk/only-current-buffer ()
    "Kill all other buffers: like ':only' in vim."
    (interactive)
    (mapc 'save-buffer (cdr (buffer-list (current-buffer))))
    (mapc 'kill-buffer (cdr (buffer-list (current-buffer))))
    (delete-other-windows))

  (defun funk/clean-up-buffer-list ()
    (interactive)
    (tramp-cleanup-all-connections)
    ;; (ignore-errors (funk/save-and-kill-all-agenda-files-and-quit-agenda))
    (visit-initel)
    (funk/only-current-buffer)
    (switch-to-buffer "*Messages*")
    (mapc 'kill-buffer (cdr (buffer-list (current-buffer))))
    (cd "~/")
    (message "Cleaned buffer list!"))

  (defalias 'cb 'funk/clean-up-buffer-list)

  ;; custom functions to narrowing windows
  ;; this is the helper code from funk/common.el
  (defun funk/common-window-bounds ()
    "Determine start and end points in the window."
    (list (window-start) (window-end)))

  (defun funk/simple-narrow-visible-window ()
    "Narrow buffer to wisible window area.
Also check `funk/simple-narrow-dwim'."
    (interactive)
    (let* ((bounds (funk/common-window-bounds))
           (window-area (- (cadr bounds) (car bounds)))
           (buffer-area (- (point-max) (point-min))))
      (if (/= buffer-area window-area)
          (narrow-to-region (car bounds) (cadr bounds))
        (user-error "Buffer fits in the window; won't narrow"))))

  (defun funk/simple-narrow-dwim ()
    "Do-what-I-mean narrowing.
If region is active, narrow the buffer to the region's
boundaries.

If no region is active, narrow to the visible portion of the
window.

If narrowing is in effect, widen the view."
    (interactive)
    (unless mark-ring                  ; needed when entering a new buffer
      (push-mark (point) t nil))
    (cond
     ((and (use-region-p)
           (null (buffer-narrowed-p)))
      (let ((beg (region-beginning))
            (end (region-end)))
        (narrow-to-region beg end)))
     ((null (buffer-narrowed-p))
      (funk/simple-narrow-visible-window))
     (t
      (widen)
      (recenter))))

  ;; auto-save when switching buffers
  ;; https://stackoverflow.com/a/1413990/5974372
  (defadvice switch-to-buffer (before save-buffer-now activate)
    (when buffer-file-name (save-buffer)))
  (defadvice other-window (before other-window-now activate)
    (when buffer-file-name (save-buffer)))
  (defadvice other-frame (before other-frame-now activate)
    (when buffer-file-name (save-buffer)))

  ;; custom functions to switch between buffers
  (defun funk/switch-to-previous-buffer ()
    "Switch to previously open buffer.
Repeated invocations toggle between the two most recently open buffers."
    (interactive)
    (switch-to-buffer (other-buffer (current-buffer) 1)))

  ;; Split the windows sensibly.
  ;; https://github.com/sje30/emacs#alternative-c-x-3-and-c-x-2
  ;; https://gitlab.com/jabranham/emacs/blob/master/init.el#L2537
  (defun funk/split-below-last-buffer (prefix)
    "Split the window above/below and display the previous buffer.
If prefix arg is provided, show current buffer twice."
    (interactive "p")
    (split-window-below)
    (other-window 1 nil)
    (if (= prefix 1)
        (switch-to-next-buffer)))

  (defun funk/split-right-last-buffer (prefix)
    "Split the window left/right and display the previous buffer
If prefix arg is provided, show current buffer twice."
    (interactive "p")
    (split-window-right)
    (other-window 1 nil)
    (if (= prefix 1)
        (switch-to-next-buffer)))

  (defun funk/split-below-projectile-project-root ()
    "Split the window above/below and display the previous buffer.
If prefix arg is provided, show current buffer twice."
    (interactive)
    (split-window-below)
    (other-window 1 nil)
    (switch-to-buffer (dired (projectile-project-root))))

  ;; (advice-add 'funk/split-below-projectile-project-root :after #'ivy-switch-buffer)

  (defun funk/split-right-projectile-project-root ()
    "Split the window left/right and display the previous buffer
If prefix arg is provided, show current buffer twice."
    (interactive)
    (split-window-right)
    (other-window 1 nil)
    (switch-to-buffer (dired (projectile-project-root))))

  ;; (advice-add 'funk/split-right-projectile-project-root :after #'ivy-switch-buffer)

  (defun funk/split-top-projectile-project-root ()
    "Split the window above/top and display the previous buffer.
  If prefix arg is provided, show current buffer twice."
    (interactive)
    (split-window-below)
    (other-window 1 nil)
    (windmove-up)
    (switch-to-buffer (dired (projectile-project-root))))

  ;; (advice-add 'funk/split-top-projectile-project-root :after #'ivy-switch-buffer)

  (defun funk/split-left-projectile-project-root ()
    "Split the window left/left and display the previous buffer
  If prefix arg is provided, show current buffer twice."
    (interactive)
    (split-window-right)
    (other-window 1 nil)
    (windmove-left)
    (switch-to-buffer (dired (projectile-project-root))))

  ;; (advice-add 'funk/split-left-projectile-project-root :after #'ivy-switch-buffer)

  (setq switch-to-prev-buffer-skip 'this)

  ;; when opening multiple files
  ;; Removes *Completions* from buffer after you've opened a file.
  (add-hook 'minibuffer-exit-hook
            #'(lambda ()
                (let ((buffer "*Completions*"))
                  (and (get-buffer buffer)
                       (kill-buffer buffer)))))

  ;; Don't show *Buffer list* when opening multiple files at the same time.
  (setq inhibit-startup-buffer-menu t)

  ;; Show only one active window when opening multiple files at the same time.
  (add-hook 'window-setup-hook 'delete-other-windows)

  )

;;; eyebrowse

(use-package eyebrowse
  ;; workspace management (dont preserve buffer list by projects)
  :init
  (setq eyebrowse-keymap-prefix (kbd "C-c w"))
  :config
  (eyebrowse-mode)
  (setq eyebrowse-mode-line-style 'always)
  (setq eyebrowse-new-workspace 'dired-jump)
  (when (and (featurep 'desktop)
             (featurep 'eyebrowse))
    (dolist ($param '(eyebrowse-window-config
                      eyebrowse-current-slot
                      eyebrowse-last-slot))
      (add-to-list 'frameset-filter-alist '($param . :save))))

  (add-to-list 'window-persistent-parameters '(window-side . writable))
  (add-to-list 'window-persistent-parameters '(window-slot . writable)))

;; (use-package eyebrowse-projectile
;;   :after (eyebrowse projectile)
;;    FIXME 2022-07-17: change this the straight strategy
;;   :load-path (lambda() (expand-file-name "lisp/github/eyebrowse-projectile/eyebrowse-projectile.el" user-emacs-directory)))

;;; eyebrowse-restore

;; (use-package eyebrowse-restore
;;   :config
;;   (set-frame-parameter nil 'name "Main")
;;   (eyebrowse-restore-mode))

;;; beframe

(use-package beframe)

;;; popper

(use-package popper
  ;; manage popup windows
  :init
  (setq popper-reference-buffers
        '(
          ;; "\\*Messages\\*"
          "Output\\*$"
          "\\*ESS\\*"
          "\\*vterm"
          "^\\magit-process"
          ;; "^\\*Warnings\\*"
          ;; "^\\*Helpful\\*"
          "^\\*PDF-Occur\\*"
          "^\\*helm-ag\\*"
          ;; "^\\*R dired\\*"
          "^\\*Async Shell Command\\*"
          "^\\*Occur\\*"
          ;; "^\\*R:"
          "^\\*R Data View:"
          "^\\*compilation"
          "^\\*interpretation\\*$"
          "^\\*Compile-Log\\*$"
          "\\*HTTP Response\\*"
          "^\\*Flymake diagnostics?"
          "\\*Async-native-compile-log\\*"
          "^\\*R dired\\*$"
          ))
  (popper-mode +1)
  :config
  (setq popper-display-control nil)
  (setq popper-group-function #'popper-group-by-projectile)
  ;; NOTE: this is related to `vterm' and `multi-vterm' packages.
  ;; As `vterm' terminals are opened as a `POP' window, I cannot
  ;; figure out how to close the terminal window after `exit' command.
  ;; See: https://github.com/akermu/emacs-libvterm/issues/24
  ;; So, I mapped `(global-set-key (kbd "C-c C-x") 'popper-kill-latest-popup)'
  ;; and add a advice to auto-confirm the kill.
  (advice-add #'popper-kill-latest-popup :around #'auto-yes))

;;; shackle

(use-package shackle
  ;; windows management
  :config
  (setq shackle-lighter "")
  (setq shackle-select-reused-windows nil) ; default nil
  (setq shackle-default-alignment 'below) ; default below
  (setq shackle-rules
        ;;                          :regexp nil :select nil :inhibit-window-quit nil :size 0.00 :align nil :other nil :same|:popup
        '(("*undo-tree*"                                                             :size 0.25 :align right)
          ("*eshell*"                           :select t                                                  :other t)
          ("*Messages*"                         :select t   :align below                                   :other t)
          ("*Warnings*"                         :select nil :align below                                   :other t)
          ("*HTTP Response*"                    :select nil :align right                                   :other t)
          ("*Shell Command Output*"             :select nil)
          ("*Async Shell Command*"              :select nil :inhibit-window-quit nil :size 0.5  :align below)
          ("*PDF-Occur*"                        :select t   :inhibit-window-quit nil                       :other t)
          ;; ("*compilation*"                      :select nil :inhibit-window-quit nil :size 0.5  :align below)
          ("*interpretation*"                   :select nil :inhibit-window-quit nil :size 0.5  :align below)
          ("*helm-ag*"                          :select nil :inhibit-window-quit t                         :other t)
          ("*rg*"                               :select t   :inhibit-window-quit nil :size 0.5  :align left)
          (occur-mode                           :select t                                       :align t)
          ;; ("*Help*"                             :select t   :inhibit-window-quit t                         :other t)
          ("*Python Doc*"                       :select t)
          ("*Completions*"                                                           :size 0.3  :align t)
          ("*Occur*"                            :select t                            :size 0.5  :align left)
          ("*elfeed-entry*"                     :select t                            :size 0.5  :align right)
          ("*quarto-preview*"                   :select nil                          :size 0.5  :align below)
          ("*org-roam*"                         :select nil                          :size 0.3  :align left)
          ("*Register Preview*"                 :select t                            :size 0.5  :align below)
          ("^\\*vterm-proj*?"       :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^\\*vc-diff\\*"         :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^\\*VC-history\\*"      :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^magit-process:?"       :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^\\*Flymake diagnostics?" :regexp t :select t   :inhibit-window-quit nil :size 0.3  :align below :popup t)
          ("^\\*Fd\\*$"             :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^\\*vterm *?"           :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^\\*interpretation\\*$" :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^\\*compilation\\*$"    :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("^\\*Compile-Log\\*$"    :regexp t   :select t   :inhibit-window-quit nil :size 0.5  :align below)
          ("\\*[Wo]*Man.*\\*"       :regexp t   :select t   :inhibit-window-quit t                         :other t)
          ;; ("\\*poporg.*\\*"         :regexp t   :select t                                                  :other t)
          ("\\`\\*helm.*?\\*\\'"    :regexp t                                        :size 0.3  :align t)
          ("*Calendar*"                         :select t                            :size 0.3  :align below)
          ("*Org Select*"                       :select t                            :size 0.3  :align below)
          ("*info*"                             :select t   :inhibit-window-quit t                                    :same t)
          ;; (magit-status-mode                    :select t   :inhibit-window-quit t   :size 0.5  :align below          :same nil) ; use this for open magit below
          (magit-status-mode                    :select t   :inhibit-window-quit t                                    :same t)
          (magit-log-mode                       :select t   :inhibit-window-quit t                                    :same t)
          ("\\`\\*R Data View:.*?\\*\\'" :regexp t   :select t :inhibit-window-quit nil :size 0.5 :align above)
          ))
  (shackle-mode +1)
  )

;;; transpose-frame

(use-package transpose-frame
  :preface
  (defun funk/frame-flip (&optional arg)
    "Flip window layout.
With `\\[universal-argument]' prefix argument ARG, flip
vertically, else, flip horizontally."
    (interactive "P")
    (if arg
        (flip-frame)
      (flop-frame)))

  (defun funk/frame-rotate (&optional arg)
    "Rotate window layout.
With `\\[universal-argument]' prefix argument ARG, rotate
clockwise, else, rotate counterclockwise."
    (interactive "P")
    (if arg
        (rotate-frame-clockwise)
      (rotate-frame-anticlockwise)))

  :config
  (global-set-key (kbd "C-c w f") 'funk/frame-flip)
  (global-set-key (kbd "C-c w r") 'funk/frame-rotate)
  (global-set-key (kbd "C-c w t") 'transpose-frame))

;;; winner

(use-package winner
  ;; undo and redo window positions
  :config
  (global-set-key (kbd "C-c w u") 'winner-undo)
  (global-set-key (kbd "C-c w C-r") 'winner-redo)
  (winner-mode))

;;; zoom-window

(use-package zoom-window
  ;; like tmux `prefix-z', but better!
  :config
  (global-set-key (kbd "<f8>") 'zoom-window-next)
  (global-set-key (kbd "<f9>") 'zoom-window-zoom)
  (custom-set-variables
   '(zoom-window-mode-line-color "#FF6E67")))

;;; ibuffer

(use-package ibuffer
  :config
  (setq ibuffer-use-other-window nil)
  (setq ibuffer-movement-cycle nil)
  (setq ibuffer-default-sorting-mode 'filename/process)
  (setq ibuffer-use-header-line t)
  (setq ibuffer-default-shrink-to-minimum-size t)
  (setq ibuffer-old-time 48)
  (setq ibuffer-formats
        '((mark modified read-only locked " "
                (name 50 50 :left :elide)
                " "
                (size 9 -1 :right)
                " "
                (mode 16 16 :left :elide)
                " " filename-and-process)
          (mark " "
                (name 16 -1)
                " " filename
                project-relative-file)))
  (add-hook 'ibuffer-mode-hook #'hl-line-mode))

;;; all-the-icons-ibuffer

(use-package all-the-icons-ibuffer
  :config
  ;; The default icon size in ibuffer.
  (setq all-the-icons-ibuffer-icon-size 1.0)
  ;; The default vertical adjustment of the icon in ibuffer.
  (setq all-the-icons-ibuffer-icon-v-adjust 0.0)
  ;; Use human readable file size in ibuffer.
  (setq  all-the-icons-ibuffer-human-readable-size t)
  ;; Slow Rendering
  ;; If you experience a slow down in performance when rendering multiple icons simultaneously,
  ;; you can try setting the following variable
  (setq inhibit-compacting-font-caches t)
  (add-hook 'ibuffer-mode-hook #'all-the-icons-ibuffer-mode))

;;; uniquify

(use-package uniquify
  :straight (:type built-in)
  :config
  (setq uniquify-buffer-name-style 'forward)
  (setq uniquify-trailing-separator-p t))

;;; autorevert

(use-package autorevert
  :straight (:type built-in)
  :config
  (global-auto-revert-mode)
  (setq global-auto-revert-non-file-buffers t)
  (setq auto-revert-verbose nil)
  :delight auto-revert-mode)

;;; golden-ratio

(use-package golden-ratio
  :config
  (golden-ratio-mode +1)
  ;; Integrate with other functions.
  ;; FROM: https://github.com/roman/golden-ratio.el/issues/55
  (dolist (f '(evil-avy-goto-word-or-subword-1
               evil-avy-goto-line
               evil-window-delete
               evil-window-split
               evil-window-vsplit
               evil-window-left
               evil-window-right
               evil-window-up
               evil-window-down
               evil-window-bottom-right
               evil-window-top-left
               evil-window-mru
               evil-window-next
               evil-window-prev
               evil-window-new
               evil-window-vnew
               evil-window-rotate-upwards
               evil-window-rotate-downwards
               evil-window-move-very-top
               evil-window-move-far-left
               evil-window-move-far-right
               evil-window-move-very-bottom))
    (add-to-list 'golden-ratio-extra-commands f))
  ;; Ignore some buffer names.
  (dolist (b '("*side-by-side-1*"
               "*side-by-side-2*"
               ;; NOTE 2022-09-14: These org buffers are trick!
               ;; https://github.com/roman/golden-ratio.el/pull/15
               " *Org todo*"
               " *Org tags*"))
    (add-to-list 'golden-ratio-exclude-buffer-names b))
  ;; Ignore some modes.
  (dolist (m '(dirvish-mode dired-sidebar-mode org-agenda-mode ranger))
    (add-to-list 'golden-ratio-exclude-modes m))
  ;; Ignore buffer name based on regex.
  (dolist (r '("^\\*vterm-proj*?"
               "^\\FD####*?" ; that one is a strange dirvish buffer name
               "^\\*Org Agenda\\*$"
               "^\\*marginal notes\\*$"
               "^\\org-roam\\*$"
               ))
    (add-to-list 'golden-ratio-exclude-buffer-regexp r))
  ;; Don't touch which-key.
  ;; FROM: https://github.com/roman/golden-ratio.el/issues/82#issuecomment-806822915
  (with-eval-after-load "which-key"
    (add-to-list 'golden-ratio-inhibit-functions
                 (lambda ()
                   (and which-key--buffer
                        (window-live-p (get-buffer-window which-key--buffer)))))))


;;; display-buffer-alist (but see `shackle' and `popper')

(use-package emacs
  :straight (:type built-in)
  :config
  (setq display-buffer-alist
        `(
          ;; ("*R Data View:"
          ;;  (display-buffer-pop-up-frame)
          ;;  ;; (slot . 1)
          ;;  ;; (window-width . 0.5)
          ;;  ;; (reusable-frames . nil)
          ;;  )
          ("*help\\[R:"
           (display-buffer-reuse-window display-buffer-at-bottom)
           (slot . 1)
           (window-width . 0.5)
           (reusable-frames . 0)
           )
          ("*Python doc*"
           (display-buffer-reuse-window display-buffer-at-bottom)
           (slot . 1)
           (window-width . 0.5)
           (reusable-frames . 0)
           )
          ("*R dired*"
           (display-buffer-reuse-window display-buffer-at-bottom)
           (slot . 1)
           (window-width . 0.3)
           (window-height . 0.3)
           (reusable-frames . 0)
           )
          ("*Flymake diagnostics"
           (display-buffer-reuse-window display-buffer-at-bottom)
           (slot . 1)
           (window-width . 0.3)
           (window-height . 0.3)
           (reusable-frames . 0)
           )
          ("*R"
           (display-buffer-reuse-window display-buffer-in-side-window)
           (side . left)
           (slot . -1)
           (window-width . 0.35)
           (reusable-frames . nil)
           )
          ("*Python"
           (display-buffer-reuse-window display-buffer-in-side-window)
           (side . left)
           (slot . -1)
           (window-width . 0.35)
           (reusable-frames . nil)
           )
          ("*jupyter-repl"
           (display-buffer-reuse-window display-buffer-in-side-window)
           (side . left)
           (slot . -1)
           (window-width . 0.35)
           (reusable-frames . nil)
           )
          ("*shell*"
           (display-buffer-reuse-window display-buffer-in-side-window)
           (side . left)
           (slot . -1)
           (window-width . 0.35)
           (reusable-frames . nil)
           )
          ("*cider-repl"
           (display-buffer-reuse-window display-buffer-in-side-window)
           (side . left)
           (slot . -1)
           (window-width . 0.35)
           (reusable-frames . nil)
           )
          ;;         ("*eldoc*"
          ;;          (display-buffer-reuse-window display-buffer-at-bottom)
          ;;          (slot . 1)
          ;;          (window-width . 0.5)
          ;;          (reusable-frames . 0))
          ;;         ;; ("*Help"
          ;;         ;;  (display-buffer-reuse-window display-buffer-at-bottom)
          ;;         ;;  (slot . 1)
          ;;         ;;  (window-width . 0.5)
          ;;         ;;  (reusable-frames . 0))
          ;; ("*Help"
          ;;  (display-buffer-reuse-window display-buffer-in-side-window)
          ;;  (side . left)
          ;;  (slot . -1)
          ;;  (window-width . 0.50)
          ;;  (reusable-frames . nil)
          ;;  )
          ("*Help"
           (display-buffer-reuse-window display-buffer-at-bottom)
           (slot . 1)
           (window-width . 0.5)
           (reusable-frames . 0)
           )
          ("*Messages*"
           (display-buffer-reuse-window display-buffer-at-bottom)
           (slot . 1)
           (window-width . 0.5)
           (reusable-frames . 0)
           )
          ("*compilation"
           (display-buffer-reuse-window display-buffer-at-bottom)
           (slot . 1)
           (window-width . 0.5)
           (reusable-frames . 0)
           )
          ("\\*org-roam\\*"
           (display-buffer-in-direction)
           (direction . left)
           (window-width . 0.33)
           (left-fringe-width . 15)
           (right-fringe-width . 15)
           (window-height . fit-window-to-buffer)
           )

          ;; ("*Org Agenda*"
          ;;  (display-buffer-reuse-window display-buffer-at-bottom)
          ;;  (slot . 1)
          ;;  (window-width . 0.5)
          ;;  (reusable-frames . 0)
          ;;  )
          )
        )
  ;; (setq same-window-regexps '("*Org Agenda*"))

  )

;;; dont kill message buffer

(defun funk/unkillable-message ()
  "Never kill *Messages* buffer."
  (if (string= (buffer-name (current-buffer)) "*Messages*")
      (progn
        ;; (delete-window (get-buffer-window "*Messages*"))
        (message "From *Messages* buffer: I'm unkillable =p")
        nil
        nil)
    t))
(add-hook 'kill-buffer-query-functions #'funk/unkillable-message)

(provide 'init-buffer-window)
;;; init-buffer-window.el ends here

;;; init-session.el --- session settings -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; psession

(use-package psession
  :init
  (require 'psession)
  (psession-mode 1)
  (psession-savehist-mode +1)
  (psession-autosave-mode +1)
  (setq psession-save-buffers-unwanted-buffers-regexp "\\(\\.org\\|diary\\|\\.jpg\\|.*[.]csv$|\\.png\\|\\*image-native-display\\*\\)$"))

(provide 'init-session)
;;; init-session.el ends here

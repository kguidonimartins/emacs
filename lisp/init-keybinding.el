;;; init-keybinding.el --- keybindings configs -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; override some global keybinding

(use-package emacs
  ;; FROM: https://github.com/mpereira/.emacs.d/#mappings
  :after
  (evil evil-collection)

  :config

  ;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Remapping-Commands.html
  ;; (global-unset-key (kbd "C-d"))
  (global-unset-key (kbd "C-SPC"))
  (global-unset-key (kbd "C-,"))
  (global-unset-key (kbd "M-u"))
  (global-unset-key (kbd "M-TAB"))
  (global-unset-key (kbd "M-z"))
  (global-unset-key (kbd "M-."))

  ;; (general-define-key
  ;;  :keymaps '(emacs-lisp-mode-map markdown-mode-map ess-mode-map)
  ;;  :states '(normal emacs insert)
  ;;  "C-m" #'newline)

  (with-eval-after-load 'outline-minor-mode
    (general-define-key
     :keymaps '(outline-mode-map)
     :states '(normal)
     "C-j" nil
     "C-k" nil)
    (general-define-key
     :keymaps '(outline-minor-mode-map)
     :states '(normal)
     "C-j" nil
     "C-k" nil))

  (general-define-key
   :keymaps '(org-agenda-mode-map)
   :states '(normal motion emacs)
   "M-h" nil
   "M-j" nil
   "M-k" nil
   "M-l" nil)

  (general-define-key
   :keymaps '(override) ; check out `general-override-mode-map'.
   ;; Adding `nil' to the states makes these keybindings work on buffers where
   ;; they would usually not work, e.g. the *Messages* buffer or the
   ;; `undo-tree-visualize' buffer.
   :states '(normal visual insert nil)
   "M-,"   #'counsel-M-x
   "M-u"   #'universal-argument
   "M-h"   #'evil-window-left
   "M-j"   #'evil-window-down
   "M-k"   #'evil-window-up
   "M-l"   #'evil-window-right
   "M-TAB" #'evil-window-mru
   "C-y"   #'clipboard-yank
   "M-y"   #'funk/show-kill-ring
   "M-z"   #'funk/unwrap-lines
   )

  ;;   (defun easy-underscore (arg)
  ;;     "Convert all inputs of semicolon to an underscore.
  ;; If given ARG, then it will insert an acutal semicolon."
  ;;     (interactive "P")
  ;;     (if arg
  ;;         (insert ";")
  ;;       (insert "_")))
  ;; (global-set-key (kbd ";") 'easy-underscore)

  (defun scroll-buffer-down (&optional arg)
    "Scroll buffer by (optional) ARG paragraphs."
    (interactive "p")
    (forward-paragraph arg)
    (recenter))

  (defun scroll-buffer-up (&optional arg)
    "Scroll buffer by (optional) ARG paragraphs."
    (interactive "p")
    (backward-paragraph arg)
    (recenter))

  (global-set-key "\M-]" 'scroll-buffer-down)
  (global-set-key "\M-[" 'scroll-buffer-up)

  (add-hook 'image-mode-hook (lambda () (use-local-map nil))))

(use-package emacs
  :straight (:type built-in)
  :config
  ;; FROM: https://karthinks.com/software/an-elisp-editing-tip/
  ;; (global-set-key [remap eval-last-sexp] 'pp-eval-last-sexp)

  (global-set-key [remap describe-function] 'counsel-describe-function)
  (global-set-key [remap describe-command] 'helpful-command)
  (global-set-key [remap describe-variable] 'counsel-describe-variable)
  (global-set-key [remap describe-key] 'helpful-key)

  (with-eval-after-load 'projectile
    (global-set-key (kbd "C-c p") 'projectile-command-map)
    )

  (global-set-key (kbd "<escape>")       'keyboard-escape-quit)
  ;; (global-set-key (kbd "<f7>")        'counsel-projectile-switch-project)
  (global-set-key (kbd "C--")            'default-text-scale-decrease)
  (global-set-key (kbd "C-=")            'default-text-scale-increase)
  (global-set-key (kbd "C-)")            'funk/general-font-setup)
  (global-set-key (kbd "C-<tab>")           'next-buffer)
  (global-set-key (kbd "C-S-<iso-lefttab>") 'previous-buffer)
  (global-set-key (kbd "C-c a")          'org-agenda)
  (global-set-key (kbd "C-c c")          'org-capture)
  (global-set-key (kbd "C-c e")          'visit-initel)
  (global-set-key (kbd "C-c r")          'reload-initel)
  (global-set-key (kbd "C-c s")          'funk/simple-scratch-buffer)
  (global-set-key (kbd "C-v")            'vterm-yank)
  (global-set-key (kbd "C-x C-b")        'ibuffer)
  (global-set-key (kbd "C-x O")          'funk/only-current-buffer)
  (global-set-key (kbd "M-/")            'hippie-expand)
  (global-set-key (kbd "M-\\")           'comint-dynamic-complete-filename)

  (with-eval-after-load 'popper
    (global-set-key (kbd "C-`") 'popper-toggle-latest)
    (global-set-key (kbd "M-`") 'popper-cycle)
    (global-set-key (kbd "C-M-`") 'popper-toggle-type))

  (global-set-key (kbd "<f10>") 'funk/toggle-outline-minor-mode)

  )

;;; which-key

(use-package which-key
  :init
  (setq which-key-idle-delay 1)
  :config
  (which-key-mode)
  (setq which-key-side-window-max-height 0.5))

;;; general

(use-package general
  :config
  (general-auto-unbind-keys)
  (general-override-mode +1)

  (general-create-definer funk/global-primary-leader-key
    :keymaps '(normal insert visual emacs motion operator)
    :prefix ","
    :global-prefix "C-,")

  (general-create-definer funk/global-secondary-leader-key
    :keymaps '(normal insert visual emacs motion operator)
    :prefix "SPC"
    :non-normal-prefix "C-SPC")

  (general-create-definer funk/org-agenda-secondary-leader-key
    :keymaps '(org-agenda-mode-map)
    :prefix "SPC"
    :non-normal-prefix "C-SPC")

  (funk/global-secondary-leader-key
    "mm" '(funk/mu4e-jump-to-the-first-inbox :wk "jump to INBOX")
    "ma" '(funk/mu4e-all-inboxes             :wk "all INBOX")
    "a" '(funk/org-agenda-a                 :wk "jump to agenda")
    )

  ;; NOTE: keep a similar list for both leader keys.
  ;; thanks: https://github.com/noctuid/general.el/issues/501
  (defmacro funk/global-both-leader-keys (&rest args)
    `(progn
       (funk/global-primary-leader-key ,@args)
       (funk/global-secondary-leader-key ,@args)
       (funk/org-agenda-secondary-leader-key ,@args)))

  (funk/global-both-leader-keys
   ;;;; misc
   "."    '(counsel-projectile-switch-to-buffer :wk "switch open proj buf")
   "/"    '(funk/counsel-surfraw                :wk "www search this")
   ":"    '(counsel-switch-buffer               :wk "switch buf with preview")
   "`"    '(funk/vterm-project-vterm            :wk "vterm proj")
   "b"    '(counsel-bookmark                    :wk "bookmark")
   "B"    '(ibuffer                             :wk "ibuffer")
   "j"    '(counsel-bookmark                    :wk "bookmark")
   "l"    '(ivy-switch-buffer                   :wk "switch buf")
   "cr"   '(funk/create-readme-here             :wk "create README.txt")
   "hg"   '(global-hide-mode-line-mode          :wk "global hide mode-line")
   "hh"   '(funk/hydra-top-menu/body            :wk "hydra")
   "hm"   '(hide-mode-line-mode                 :wk "hide mode-line")
   "i"    '(funk/indent-region                  :wk "indent region")
   "x"    '(execute-extended-command            :wk "M-x")
   "ee"   '(eval-defun                          :wk "eval-defun")
   "er"   '(eyebrowse-rename-window-config      :wk "eyebrowse rename")
   "ew"   '(funk/eval-last-sexp                 :wk "like C-x C-e")
   "ed"   '(eval-defun                          :wk "eval-defun")
   "ei"   '(visit-initel                        :wk "visit init.el")

   ;;;; dotfiles related
   "d"    '(:ignore t                              :wk "dotfiles")
   "de"   '(visit-initel                           :wk "visit-initel")
   "df"   '(funk/counsel-git-emacs                 :wk "get emacs files")
   "dp"   '(funk/counsel-git-dotfiles-public       :wk "get .-public")
   "di"   '(funk/magit-status-for-dotfiles-private :wk "status .-private")
   "du"   '(funk/magit-status-for-dotfiles-public  :wk "status .-public")

   ;;;; fold related
   "fa"   '(outline-show-all               :wk "unfold all")
   "fh"   '(outline-hide-body              :wk "fold all")
   "ft"   '(funk/toggle-outline-minor-mode :wk "fold toogle")
   "fb"   '(bicycle-cycle                  :wk "fold toggle this header")
   "ff"   '(find-file                      :wk "find-file")
   "fo"   '(funk/consult-outline           :wk "find outline")
   "fs"   '(funk/selective-display-hide    :wk "selective-display-hide")
   "fu"   '(funk/selective-display-show    :wk "selective-display-show")

   ;;;; dired/files
   "f-"   '(funk/dired-replace-space-to-hyphen :wk "replace space by -")
   "f_"   '(funk/dired-replace-space-to-underscore :wk "replace space by _")
   "fA"   '(funk/dired-attach-files-into-mu4e :wk "attach file into mu4e")
   "fc"   '(funk/compile                      :wk "compile")
   "fd"   '(funk/dired-drag-and-drop          :wk "dired drag and drop")
   "fj"   '(dired-jump                        :wk "jump dir")
   "fp"   '(projectile-dired                  :wk "jump proj")
   "fr"   '(funk/rename-file-and-buffer       :wk "rename file and buf")
   "fi"   '(funk/insert-filename-here         :wk "insert file")
   "fO"   '(funk/only-current-buffer          :wk "only this buf")

   ;;;; git related
   "g"    '(:ignore t                    :wk "git things")
   "gc"   '(funk/git-clone-clipboard-url :wk "get repo from the clipboard")
   "gb"   '(browse-at-remote             :wk "find this line/region on online repo")
   "gB"   '(blamer-mode                  :wk "blamer-mode")
   "gf"   '(magit-file-dispatch          :wk "magit-file-dispatch")
   "gg"   '(magit-status                 :wk "git status")
   "gl"   '(magit-list-repositories      :wk "list git repos")
   "gm"   '(git-timemachine              :wk "git-timemachine")
   "gn"   '(git-gutter:next-hunk         :wk "next-hunk")
   "gp"   '(git-gutter:previous-hunk     :wk "previous-hunk")
   "gr"   '(funk/git-goto-remote         :wk "goto remote")
   "gR"   '(vc-region-history            :wk "region git history")
   "gs"   '(git-gutter:stage-hunk        :wk "stage-hunk")
   "gu"   '(git-gutter:revert-hunk       :wk "revert-hunk")
   "gv"   '(funk/diffview-vc-diff        :wk "file diffview")
   "gV"   '(funk/diffview-vc-root-diff   :wk "dir diffview")

   ;;;; narrow related
   "n"    '(:ignore t        :wk "narrow things")
   "nd"   '(narrow-to-defun  :wk "narrow-to-defun")
   "np"   '(narrow-to-page   :wk "narrow-to-page")
   "nr"   '(narrow-to-region :wk "narrow-to-region")
   "nu"   '(widen            :wk "undo narrow")

   ;;;; project related
   "p"    '(:ignore t                                      :wk "proj things")
   "p"    '(:ignore t                                      :wk "proj things")
   "po"   '(counsel-projectile-switch-project-open-project :wk "switch to open projs")
   "pp"   '(counsel-projectile-switch-project-open-project :wk "switch to open projs")
   "pk"   '(funk/save-and-kill-project-buffers             :wk "save n kill projs")
   "pK"   '(funk/projectile-kill-other-projects            :wk "kill all other projs")
   "pr"   '(projectile-recentf                             :wk "find recent proj files")
   "pR"   '(rg-dwim-current-file                           :wk "rg cur file")
   "pg"   '(counsel-projectile-rg                          :wk "ripgrep-all proj files")
   "pt"   '(funk/vterm-project-vterm                       :wk "vterm proj")
   "pF"   '(projectile-find-file-dwim                      :wk "find files")
   "pf"   '(funk/org-projectile-get-todo-file              :wk "get proj todo")
   "O"    '(occur                                          :wk "occur")
   "ps"   '(counsel-projectile-switch-project              :wk "swith proj")

   ;;;; roam related
   "r"   '(:ignore t               :wk "roam things")
   "rt"   '(org-roam-buffer-toggle :wk "backlinks buf")
   "rf"   '(org-roam-node-find     :wk "find node")
   "rg"   '(org-roam-graph         :wk "graph")
   "ri"   '(org-roam-node-insert   :wk "insert node")
   "rc"   '(org-roam-capture       :wk "capture")

   ;;;; scratch buffer things
   "s"    '(:ignore t                                :wk "scratch things")
   "ss"   '(funk/simple-scratch-buffer               :wk "scratch buf for the current mode")
   "sc"   '(funk/simple-scratch-buffer-select        :wk "select scratch buf mode")
   "sm"   '(funk/simple-markdown-mode-scratch-buffer :wk "markdown scratch buf mode")
   "so"   '(funk/simple-org-mode-scratch-buffer      :wk "org-mode scratch buf mode")
   "se"   '(funk/simple-elisp-mode-scratch-buffer    :wk "emacs-lisp-mode scratch buf mode")

   ;;;; todo related
   "t"    '(:ignore t                      :wk "todo things")
   "tn"   '(hl-todo-next                   :wk "next todo")
   "to"   '(hl-todo-occur                  :wk "find todo")
   "tp"   '(hl-todo-previous               :wk "prev todo")
   "tf"   '(funk/org-projectile-list-todo-file :wk "list proj's todo")
   "td"   '(funk/simple-insert-date        :wk "insert date")
   "tt"   '(funk/comment-timestamp-keyword :wk "insert comment KEYWORD YYYY-MM-DD:")

   ;;;; tree-sitter
   "ts"    '(:ignore t                      :wk "ts fold")
   "tsa"   '(ts-fold-open-all               :wk "open all")
   "tsc"   '(ts-fold-close-all              :wk "close all")
   "tst"   '(ts-fold-toggle                 :wk "toggle")
   "tsr"   '(ts-fold-open-recursively       :wk "open recurse")

   ;;;; workspace/window related
   "H"    '(funk/split-left-projectile-project-root  :wk "split left")
   "J"    '(funk/split-below-projectile-project-root :wk "split below")
   "K"    '(funk/split-top-projectile-project-root   :wk "split top")
   "L"    '(funk/split-right-projectile-project-root :wk "split right")
   "q"    '(q                                        :wk "kill buf and win")
   "w"    '(:ignore t                                :wk "workspace things")
   "wc"   '(eyebrowse-create-window-config           :wk "create workspace")
   "wn"   '(eyebrowse-next-window-config             :wk "next workspace")
   "wl"   '(eyebrowse-next-window-config             :wk "next workspace")
   "wh"   '(eyebrowse-prev-window-config             :wk "prev workspace")
   "wp"   '(eyebrowse-prev-window-config             :wk "prev workspace")
   "wu"   '(winner-undo                              :wk "undo window layout")
   "wr"   '(winner-redo                              :wk "redo window layout")
   "wz"   '(zoom-window-zoom                         :wk "zoom window")
   "ww"   '(zoom-window-next                         :wk "zoom window next")

   "\""   '(eyebrowse-close-window-config       :wk "close workspace")
   "1"    '(eyebrowse-switch-to-window-config-1 :wk "workspace 1")
   "2"    '(eyebrowse-switch-to-window-config-2 :wk "workspace 2")
   "3"    '(eyebrowse-switch-to-window-config-3 :wk "workspace 3")
   "4"    '(eyebrowse-switch-to-window-config-4 :wk "workspace 4")
   "5"    '(eyebrowse-switch-to-window-config-5 :wk "workspace 5")
   "6"    '(eyebrowse-switch-to-window-config-6 :wk "workspace 6")
   "7"    '(eyebrowse-switch-to-window-config-7 :wk "workspace 7")
   "8"    '(eyebrowse-switch-to-window-config-8 :wk "workspace 8")
   "9"    '(eyebrowse-switch-to-window-config-9 :wk "workspace 9")
   ">"    '(eyebrowse-next-window-config        :wk "next workspace")
   "<"    '(eyebrowse-prev-window-config        :wk "prev workspace")
   "RET"  '(eyebrowse-last-window-config        :wk "last workspace")
   "z"    '(darkroom-mode                       :wk "zen")

   ;;;; org related
   "o"    '(:ignore t                      :wk "org things")
   "oc"   '(funk/org-capture               :wk "org-capture")
   "oC"   '(org-projectile-project-todo-completing-read :wk "org proj capture")
   "og"   '(funk/open-calendar             :wk "open calendar")
   "oa"   '(:ignore t                      :wk "org agenda views")
   "oaa"  '(funk/org-agenda-a              :wk "weekly view")
   "oac"  '(funk/org-agenda-cc             :wk "today view")
   "oag"  '(funk/org-agenda-cg             :wk "GTD view")
   "of"   '(funk/office                    :wk "office")
   "oq"   '(funk/office-quit               :wk "office-quit")
   "os"  '(org-schedule                    :wk "org-schedule")
   "op"  '(org-gcal-post-at-point          :wk "org-gcal-post-at-point")
   "oS"  '(funk/org-screen-capture         :wk "org-screen-capture")
   "oi"  '(org-insert-structure-template   :wk "org src block")
   "ot"  '(org-todo                        :wk "org-todo")
   "oT"  '(org-set-tags-command            :wk "org-set-tags-command")
   "or"  '(org-refile                      :wk "org-refile")
   "od"  '(org-deadline                    :wk "org-deadline")
   "oP"  '(funk/org-schedule-and-gcal-post :wk "org-schedule-and-gcal-post")
   "oF"  '(funk/org-agenda-search-outline  :wk "org search outlines")
   "ol"  '(org-open-at-point               :wk "org open link")
   "oj"  '(funk/org-journal                :wk "org-journal")

   )
  )

(provide 'init-keybinding)
;;; init-keybinding.el ends here

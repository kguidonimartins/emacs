;;; init-help.el --- Help configs -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; helpful

(use-package helpful
  :after (counsel ivy)
  :config
  (setq counsel-describe-function-function #'helpful-callable)
  (setq counsel-describe-variable-function #'helpful-variable))

;;; elisp-demos

(use-package  elisp-demos
  :after (counsel ivy)
  :config
  (advice-add 'describe-function-1 :after #'elisp-demos-advice-describe-function-1)
  (advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update))

(provide 'init-help)
;;; init-help.el ends here

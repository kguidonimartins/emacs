;;; init-www.el --- Packages for searching things online -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; google-this
(use-package google-this
  :disabled

  :config
  (google-this-mode +1))

;;; engine-mode
(use-package engine-mode
  :disabled

  :hook (after-init . engine-mode)
  :config
  (defengine google
             "http://www.google.com/search?ie=utf-8&oe=utf-8&q=%s"
             :keybinding "g")

  (defengine google-images
             "http://www.google.com/images?hl=en&source=hp&biw=1440&bih=795&gbv=2&aq=f&aqi=&aql=&oq=&q=%s"
             :keybinding "i")

  (defengine google-maps
             "http://maps.google.com/maps?q=%s")

  (defengine wikipedia
             "http://www.wikipedia.org/search-redirect.php?language=en&go=Go&search=%s"
             :keybinding "w")

  (defengine wiktionary
             "https://www.wikipedia.org/search-redirect.php?family=wiktionary&language=en&go=Go&search=%s"
             :keybinding "d")

  (defengine youtube
             "http://www.youtube.com/results?aq=f&oq=&search_query=%s"
             :keybinding "v")

  (defengine rdocumentation
             "https://www.rdocumentation.org/search?q=%s"
             :keybinding "r")

  (defengine rstackoverflow
             "https://stackoverflow.com/search?q=[r]+%s"
             :keybinding "s")

  (global-set-key (kbd "C-x /") 'engine-mode-map)

  )

;;; counsel-surfraw
(use-package counsel-surfraw
  :after (ivy counsel)
  :straight (counsel-surfraw :local-repo "lisp/github/counsel-surfraw/" :type built-in)
  :config
  (defun funk/counsel-surfraw ()
    "Search for something online, using the surfraw command."
    (interactive)
    (setq-local thing (if (use-region-p)
                          (buffer-substring-no-properties
                           (region-beginning) (region-end))
                        (thing-at-point 'symbol)))
    (let ((search-for (read-string "Search for: " thing 'counsel-surfraw-search-history)))
      (ivy-read (format "Search for `%s` with: " search-for)
                #'counsel-surfraw-elvi
                :require-match t
                :history 'counsel-surfraw-engine-history
                :sort t
                :action
                (lambda (selected-elvis)
                  (browse-url (shell-command-to-string
                               (format "sr %s -p %s"
                                       (get-text-property 0 'elvis selected-elvis)
                                       search-for)))))))

  )


;;; google-translate
(use-package google-translate
  :disabled
  :config
  (setq google-translate-default-source-language "en")
  (setq google-translate-default-target-language "pt")
  (setq google-translate-show-phonetic t)
  (setq google-translate-pop-up-buffer-set-focus t)
  (defun google-translate--search-tkk () "Search TKK." (list 430675 2721866130))
  (setq google-translate-translation-directions-alist '(("en" . "pt") ("pt" . "en"))))

(global-set-key "\C-ct" 'google-translate-smooth-translate)

;;; eww-search-words
(use-package eww
  :disabled
  :commands (eww eww-search-words)
  :bind
  ;; If a webpage requires more than eww can handle, I can switch to the
  ;; system default by tapping &, but 0 is easier to type:
  (:map eww-mode-map
        ("0" . eww-browse-with-external-browser)))

;;; 0x0: easily upload files
(use-package 0x0
  )

;;; browser-rules

(use-package browse-rules
  :straight (:host github :repo "SidharthArya/browse-rules.el")
  :config
  (setq browse-url-browser-function 'browse-rules-url)
  (setq browse-rules '(
                       ;; "Regexp" external application or function format-string
                       ;; NOTE 2022-07-22: First define the default for all urls.
                       (".*" nil browse-url-default-browser "%s")
                       ;; NOTE 2022-07-22: Then, define the exceptions.
                       (".*meet.google*" t "google-chrome-stable" "%s")
                       ("https://accounts.google.com*" t "google-chrome-stable" "%s")
                       (".*docs.google*" t "google-chrome-stable" "%s")
                       (".*bbc.*" t "firefox" "about:reader?url=%s")
                       (".*gnu.org.*" nil eww "%s")
                       ("^file:///" nil eww "%s")
                       ("*localhost:*" nil eww "%s")
                       (".*microsoft-teams.*" t "google-chrome-stable" "%s")
                       (".*teams.microsoft.com.*" t "google-chrome-stable" "%s")
                       )))

(provide 'init-www)
;;; init-www.el ends here

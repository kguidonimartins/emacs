;;; init-terminal.el --- terminal configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; general configurations

(use-package emacs
  :straigth (:type built-in)
  :config
  (setq savehist-file (expand-file-name "var/savehist.el" user-emacs-directory))
  (setq history-length t)
  (setq history-delete-duplicates t)
  (setq savehist-additional-variables
        '(command-history
          compile-history
          evil-ex-history
          evil-jumps-history
          extended-command-history
          file-name-history
          helm-M-x-input-history
          helm-grep-history
          ido-file-history
          kill-ring
          log-edit-comment-ring
          magit-read-rev-history
          mark-ring
          minibuffer-history
          read-expression-history
          regexp-search-ring
          savehist-minibuffer-history-variables
          search-ring))
  (savehist-mode +1)
  (setq history-length 100)

  (defun turn-on-comint-history (history-file)
    (setq comint-input-ring-file-name history-file)
    (comint-read-input-ring 'silent))

  (add-hook 'shell-mode-hook
            (lambda ()
              (turn-on-comint-history (getenv "HISTFILE"))))

  (defun funk-shell-mode-hook ()
    (setq comint-input-ring-file-name "~/.local/share/zsh_history")
    (comint-read-input-ring t))

  (add-hook 'shell-mode-hook 'funk-shell-mode-hook))

;;; vterm

(use-package vterm
  
  :after projectile
  ;; :commands (vterm vterm-other-window)
  :config
  (setq vterm-kill-buffer-on-exit t)
  (setq vterm-max-scrollback 10000)
  (add-hook 'vterm-mode-hook
            (lambda ()
              (set (make-local-variable 'buffer-face-mode-face) 'fixed-pitch)
              (buffer-face-mode t)))
  (add-hook 'vterm-mode-hook 'evil-insert-state)
  ;; kill associated terminal buffer
  ;; from: https://github.com/akermu/emacs-libvterm/issues/24#issuecomment-907660950
  (add-hook 'vterm-exit-functions
            (lambda (_ _)
              (let* ((buffer (current-buffer))
                     (window (get-buffer-window buffer)))
                (when (not (one-window-p))
                  (delete-window window))
                (kill-buffer buffer))))
  (add-hook 'vterm-mode-hook
            (lambda ()
              (setq-local evil-insert-state-cursor 'bar)
              (evil-insert-state)))
  (add-hook 'vterm-mode-hook
            (lambda ()
              (read-only-mode -1))))

(defun funk/vterm-project-vterm ()
  "Create a new `vterm' for the current project or reopen the latest one."
  (interactive)
  (let* ((project-path (projectile-ensure-project (projectile-project-root)))
         (project-name (projectile-project-name))
         (vterm-project-name (concat "*vterm-proj: " project-name "*")))
    (if (buffer-live-p (get-buffer vterm-project-name))
        (progn
          (pop-to-buffer vterm-project-name)
          (message (format "Reopen vterm-project: %s" project-name)))
      (progn
        (vterm-other-window vterm-project-name)
        (vterm-send-string (concat "builtin cd " project-path))
        (vterm-send-return)
        (message (format "Created vterm-project: %s" project-name))))))

;;; essh

(use-package essh
  
  :straight (essh :local-repo "lisp/emacswiki/essh" :type built-in)
  :config
  (require 'essh))

;; https://stackoverflow.com/questions/3576192/how-to-implement-shell-command-on-region-to-string-emacs-elisp-function
;; https://www.nistara.net/post/emacs-send-line-or-region-to-shell/
;; https://stackoverflow.com/questions/40589463/emacs-ess-pipe-output-from-r-repl-into-buffer
;; https://stackoverflow.com/questions/6286579/emacs-shell-mode-how-to-send-region-to-shell
;; https://www.emacswiki.org/emacs/essh.el
;; (defun sh-send-line-or-region (&optional step)
;;   (interactive ())
;;   (let ((proc (get-process "vterm"))
;;         pbuf min max command)
;;     (unless proc
;;       (let ((currbuff (current-buffer)))
;;         (vterm)
;;         (switch-to-buffer currbuff)
;;         (setq proc (get-process "shell"))
;;         ))
;;     (setq pbuff (process-buffer proc))
;;     (if (use-region-p)
;;         (setq min (region-beginning)
;;               max (region-end))
;;       (setq min (point-at-bol)
;;             max (point-at-eol)))
;;     (setq command (concat (buffer-substring min max) "\n"))
;;     (with-current-buffer pbuff
;;       (goto-char (process-mark proc))
;;       (insert command)
;;       (move-marker (process-mark proc) (point))
;;       ) ;;pop-to-buffer does not work with save-current-buffer -- bug?
;;     (process-send-string  proc command)
;;     (display-buffer (process-buffer proc) t)
;;     (when step
;;       (goto-char max)
;;       (next-line))
;;     ))

;; (defun sh-send-line-or-region-and-step ()
;;   (interactive)
;;   (sh-send-line-or-region t))
;; (defun sh-switch-to-process-buffer ()
;;   (interactive)
;;   (pop-to-buffer (process-buffer (get-process "shell")) t))

;; (define-key sh-mode-map [(control ?j)] 'sh-send-line-or-region-and-step)
;; (define-key sh-mode-map [(control ?c) (control ?z)] 'sh-switch-to-process-buffer)

(provide 'init-terminal)
;;; init-terminal.el ends here

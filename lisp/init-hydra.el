;;; init-hydra.el --- hydra configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; hydra
;; https://github.com/abo-abo/hydra/wiki
(use-package hydra
  
  )

;;; major-mode-hydra
;; https://github.com/jerrypnz/major-mode-hydra.el
(use-package major-mode-hydra
  
  :after hydra
  :config

    ;;; functions to include icons on hydra panels
  ;; https://gist.github.com/mbuczko/e15d61363d31cf78ff17427072e0c325
  (defun with-faicon (icon str &optional height v-adjust)
    (s-concat (all-the-icons-faicon icon :v-adjust (or v-adjust 0) :height (or height 1)) " " str))

  (defun with-fileicon (icon str &optional height v-adjust)
    (s-concat (all-the-icons-fileicon icon :v-adjust (or v-adjust 0) :height (or height 1)) " " str))

  (defun with-octicon (icon str &optional height v-adjust)
    (s-concat (all-the-icons-octicon icon :v-adjust (or v-adjust 0) :height (or height 1)) " " str))

  (defun with-material (icon str &optional height v-adjust)
    (s-concat (all-the-icons-material icon :v-adjust (or v-adjust 0) :height (or height 1)) " " str))

  (defun with-mode-icon (mode str &optional height nospace face)
    (let* ((v-adjust (if (eq major-mode 'emacs-lisp-mode) 0.0 0.05))
           (args     `(:height ,(or height 1) :v-adjust ,v-adjust))
           (_         (when face
                        (lax-plist-put args :face face)))
           (icon     (apply #'all-the-icons-icon-for-mode mode args))
           (icon     (if (symbolp icon)
                         (apply #'all-the-icons-octicon "file-text" args)
                       icon)))
      (s-concat icon (if nospace "" " ") str)))


  (major-mode-hydra-define emacs-lisp-mode nil
    ("Eval"
     (("b" eval-buffer "buffer")
      ("e" eval-defun "defun")
      ("r" eval-region "region"))
     "REPL"
     (("I" ielm "ielm"))
     "Test"
     (("t" ert "prompt")
      ("T" (ert t) "all")
      ("F" (ert :failed) "failed"))
     "Doc"
     (("d" describe-foo-at-point "thing-at-pt")
      ("f" describe-function "function")
      ("v" describe-variable "variable")
      ("i" info-lookup-symbol "info lookup"))))


  (setq major-mode-hydra-title-generator
        '(lambda (mode)
           (s-concat "\n"
                     (s-repeat 10 " ")
                     (all-the-icons-icon-for-mode mode :v-adjust 0.05)
                     " "
                     (symbol-name mode)
                     " commands")))

  (defvar funk-toggles--title (with-faicon "toggle-on" "Toggles" 1 -0.05))

;;; hydra toogles
  (pretty-hydra-define funk-toggles
    (:color amaranth :quit-key "q" :title funk-toggles--title)
    ("Basic"
     (("n" linum-mode "line number" :toggle t)
      ("w" whitespace-mode "whitespace" :toggle t)
      ("W" whitespace-cleanup-mode "whitespace cleanup" :toggle t)
      ("r" rainbow-mode "rainbow" :toggle t)
      ("L" page-break-lines-mode "page break lines" :toggle t))
     "Highlight"
     (("s" symbol-overlay-mode "symbol" :toggle t)
      ("l" hl-line-mode "line" :toggle t)
      ("x" highlight-sexp-mode "sexp" :toggle t)
      ("t" hl-todo-mode "todo" :toggle t))
     "UI"
     (("d" funk-themes-toggle-light-dark "dark theme" :toggle funk-current-theme-dark-p))
     "Coding"
     (("p" smartparens-mode "smartparens" :toggle t)
      ("P" smartparens-strict-mode "smartparens strict" :toggle t)
      ("S" show-smartparens-mode "show smartparens" :toggle t)
      ("f" flycheck-mode "flycheck" :toggle t))
     "Emacs"
     (("D" toggle-debug-on-error "debug on error" :toggle (default-value 'debug-on-error))
      ("X" toggle-debug-on-quit "debug on quit" :toggle (default-value 'debug-on-quit)))))

  (defvar funk-window--title (with-faicon "windows" "Window Management" 1 -0.05))

;;; hydra window
  (pretty-hydra-define funk-window (:foreign-keys warn
                                                  :quit-key "q"
                                                  :title funk-window--title
                                                  )
    ("Actions"
     (("TAB" other-window "switch")
      ("x" ace-delete-window "delete")
      ("m" ace-delete-other-windows "maximize")
      ("s" ace-swap-window "swap")
      ("a" ace-select-window "select"))

     "Resize"
     (("h" evil-window-increase-width "←")
      ("j" evil-window-increase-height "↓")
      ("k" evil-window-decrease-height "↑")
      ("l" evil-window-decrease-width "→")
      ("=" balance-windows "balance")
      )

     "Split"
     (("b" split-window-right "horizontally")
      ("B" split-window-horizontally-instead "horizontally instead")
      ("v" split-window-below "vertically")
      ("V" split-window-vertically-instead "vertically instead"))

     "Layout"
     (
      ("f" funk/frame-flip "flip frame")
      ("F" funk/frame-rotate "rotate frame")
      ("u" winner-undo "undo layout")
      ("r" winner-redo "redo layout")
      ("z" zoom-window-zoom "zoom window")
      ("Z" zoom-window-next "next zoomed window")
      ("n" funk/simple-narrow-dwim "narrow window based on selected text")
      )
     ))


  ;; --------------------------------------------------------------------------------
  ;; FROM: https://github.com/geolessel/dotfiles/blob/master/emacs/emacs.d/README.org#hydra-menus
  ;; ** Hydra menus
  ;; *** Apropros

  ;; #+begin_src emacs-lisp
  ;; TODO: learning about apropos
  (defhydra funk/hydra-apropos-menu (:color blue :hint nil)
    "
_a_propos        _c_ommand
_d_ocumentation  _l_ibrary
_v_ariable       _u_ser-option
_i_nfo       valu_e_"
    ("a" counsel-apropos)
    ("d" apropos-documentation)
    ("v" apropos-variable)
    ("i" info-apropos)
    ("c" apropos-command)
    ("l" apropos-library)
    ("u" apropos-user-option)
    ("e" apropos-value))
  ;; #+end_src

  ;; *** Workspaces
  ;; #+begin_src emacs-lisp
  (pretty-hydra-define funk/hydra-workspace-menu (:exit t :quit-key "q")
    ("General"
     (("w" persp-switch "Switch/New")
      ("k" persp-kill "Kill")
      ("r" persp-rename "Rename")
      ("i" persp-import "Import")
      ("n" persp-next "Next")
      ("p" persp-prev "Prev"))
     "Buffers"
     (("b b" persp-counsel-switch-buffer "Switch to buffer in current perspective")
      ("b a" persp-add-buffer "Add buffer to current perspective")
      ("b k" persp-remove-buffer "Remove buffer from current perspective")
      ("b s" persp-set-buffer "Move buffer to current perspective"))
     "State Mgmt"
     (("W" persp-state-save "Write to disk")
      ("l" persp-state-load "Load from disk"))
     ))
  ;; #+end_src

  ;; *** Buffers/Files
  ;; #+begin_src emacs-lisp
  (pretty-hydra-define funk/hydra-buffer-menu (:exit t :quit-key "q")
    ("Buffers"
     (("b" counsel-switch-buffer "Switch")
      ("n" evil-buffer-new "New")
      ("R" rename-buffer "Rename buffer")
      ("k" kill-this-buffer "Kill this buffer")
      ("K" funk/only-current-buffer "Kill all other buffers"))
     "Files"
     (("r" funk/rename-file-and-visit "Rename this file"))
     "Views/Modes"
     (("i" ibuffer "ibuffer"))))
  ;; #+end_src

  ;; *** Preferences
  ;; =doom-modeline= has some variables you can set explictly, but no easy
  ;; way to toggle it on and off. So here are a few helper functions to add
  ;; the ability to toggle them.

  ;; #+begin_src emacs-lisp
  (defun funk/doom-modeline-toggle-word-count ()
    "Toggle doom-modeline's word count indicator on and off"
    (interactive)
    (if doom-modeline-enable-word-count
        (progn
          (setq doom-modeline-enable-word-count nil)
          (message "Word count turned off"))
      (progn
        (setq doom-modeline-enable-word-count t)
        (message "Word count turned on for modes %s" doom-modeline-continuous-word-count-modes))))

  (defun funk/persp-mode-toggle-modestring ()
    "Toggle the list of perspective names in the modeline (off/single/all)"
    (interactive)
    ;; it is off
    (if (not persp-show-modestring)
        ;; turn it on (short)
        (progn
          (setq persp-modestring-short t)
          (persp-turn-on-modestring)
          (message "Perspective names turned on (short)"))
      ;; it is on (short)
      (if persp-modestring-short
          (progn
            (setq persp-modestring-short nil)
            (persp-turn-on-modestring)
            (message "Perspective names turned on (long)"))
        ;; it is on (long)
        (progn
          (persp-turn-off-modestring)
          (message "Perspective names turned off")))))
  ;; #+end_src

  ;; #+begin_src emacs-lisp

  (defvar funk/hydra-prefs-menu--title (with-faicon "wrench" "Preferences" 1 -0.05))

  (pretty-hydra-define funk/hydra-prefs-menu (:quit-key "q" :title funk/hydra-prefs-menu--title)
    ("Display"
     (("n" linum-mode "line number" :toggle t)
      ("w" whitespace-mode "whitespace" :toggle t)
      ("h" global-hl-line-mode "highlight line" :toggle t)
      ("H" highlight-indent-guides-mode "highlight indents" :toggle t)
      ("+" default-text-scale-increase "increase font size")
      ("-" default-text-scale-decrease "decrease font size")
      ("0" (funk/general-font-setup) "reset font size"))
     ""
     (("d" diff-hl-mode "diff-hl" :toggle t)
      ("c" global-display-fill-column-indicator-mode "show fill column" :toggle t)
      (")" funk/toggle-show-paren-style "show-paren style")
      ("t" toggle-truncate-lines "toggle wrap lines" :toggle t)
      ("v" visual-line-mode "visual wrap lines" :toggle t)
      )
     "Editing"
     (("p" electric-pair-mode "electric-pair" :toggle t)
      ("f" auto-fill-mode "auto-fill"))
     "Modeline"
     (("m c" column-number-mode "column number" :toggle t)
      ("m l" line-number-mode "line number" :toggle t)
      ("m w" funk/doom-modeline-toggle-word-count "word count" :toggle doom-modeline-enable-word-count)
      ("m p" funk/persp-mode-toggle-modestring "perspective list" :toggle persp-show-modestring))
     ))
  ;; #+end_src

  ;; *** Projectile

  ;; There's so much stuff in =projectile=. Who can keep track of it
  ;; all? Now I don't have to!

  ;; #+begin_src emacs-lisp
  ;; TODO: add more commands for hydra projectile


  (defvar funk-projectile--title (with-faicon "cogs" "Projectile" 1 -0.05))

  (pretty-hydra-define funk/hydra-projectile (:exit t :quit-key "q" :title funk-projectile--title)
    (
     "Files"
     (("f" counsel-projectile-find-file "Find file")
      (">" projectile-toggle-between-implementation-and-test
       "Go to test/impl")
      ("d" projectile-display-buffer "Display buffer")
      ("D" counsel-projectile-dired "dired"))

     "Searching"
     (("/" projectile-ag "ag")
      ("?" counsel-projectile-ag "ag (with counsel)")
      ("g" projectile-grep "grep")
      ("r" prejectile-ripgrep "ripgrep"))

     "Management"
     (("p" counsel-projectile-switch-project "Switch project")
      ("i" projectile-ibuffer "ibuffer")
      ("b" counsel-projectile-switch-to-buffer "Switch to buffer")
      ("t" projectile-test-project "Test project"))

     "Commands"
     (("v" projectile-run-vterm "vterm")
      ("c" projectile-run-command-in-root "Run command in root"))
     ))


  (defvar funk-git--title (with-faicon "git" "Magit" 1 -0.05))

  (pretty-hydra-define funk/hydra-git (:exit t :quit-key "q" :title funk-git--title)

    (

     "Status"
     (
      ("s" magit-status "Status")
      ("r" magit-list-repositories "Repos list")
      )

     "Hunks"
     (
      ("n" git-gutter:next-hunk "Next hunk")
      ("p" git-gutter:previous-hunk "Previous hunk")
      ("u" git-gutter:revert-hunk "Undo hunks changes")
      )

     "Commit"
     (
      ("o" vc-msg-show "Show commit msg at point")
      )

     "Browse"
     (
      ("b" browse-at-remote "See file location at remote")
      )

     )
    )

  ;; TODO 2021-08-18: include dired-hacks in the hydra-dired
  (defvar funk-dired--title (with-faicon "files-o" "File Management" 1 -0.05))

  (pretty-hydra-define
    funk/hydra-dired (
                      :exit t
                      :quit-key "q"
                      :foreign-keys warn
                      :title funk-dired--title
                      )
    (

     "Finding"
     (
      ("fd" counsel-projectile-find-dir "find directories")
      ("ff" counsel-projectile-find-file "find files")
      ("fi" dired-narrow "filter and narrow dired buffer")
      )

     "Marks"
     (
      ("E" dired-mark-extension "mark by extension")
      ("U" dired-unmark-all-marks "unmark all")
      ("fm" dired-mark-files-regexp "mark files by regex")
      ("m" dired-mark "mark files")
      ("u" dired-unmark "unmark")
      )

     "Actions"
     (
      ("!" dired-do-shell-command "guess shell command")
      ("(" dired-hide-details-mode "show details")
      ("+" dired-create-directory "make a directory")
      ("C" dired-do-copy "copy to")        ;; Copy all marked files
      ("D" dired-do-delete "delete")
      ("R" dired-do-rename "move|rename")
      ("RET" funk/open-with "open with")
      ("Z" dired-do-compress "un|compress files")
      ("c" dired-do-compress-to "compress to")
      ("gr" revert-buffer "refresh dired")
      ("o" dired-find-file-other-window "display in anothter window")
      ("A" funk/dired-attach-files-into-mu4e "Attach files to mu4e")
      )


     "Subdirectories"
     (
      ("i" dired-maybe-insert-subdir "view subdir below")
      ("k" (dired-do-kill-lines 0) "remove subdir (cursor on header)")
      ("l" dired-do-redisplay "refresh subdir content")
      ("t" dired-toggle-marks "invert marks")
      ("w" dired-kill-subdir "remove subdir")
      )

     ;; "Others 2"
     ;; (
     ;;  ("Q" dired-do-find-regexp-and-replace)
     ;;  ("r" dired-do-rsynch)
     ;;  ("S" dired-do-symlink)
     ;;  ("s" dired-sort-toggle-or-edit)
     ;;  ("v" dired-view-file)      ;; q to exit, s to search, = gets line #
     ;;  ("Y" dired-do-relsymlink)
     ;;  ("z" diredp-compress-this-file)
     ;;  ("q" nil)
     ;;  )

     ))


  (defvar funk-mu4e--title (with-octicon "mail" "Mail" 1 -0.05))

  (pretty-hydra-define
    funk/hydra-mu4e (
                     :exit t
                     :quit-key "q"
                     :foreign-keys warn
                     :title funk-mu4e--title
                     )
    (

     "Finding"
     (
      ("j" mu4e-headers-next)
      ("k" mu4e-headers-prev)
      )

     "Marks"
     (
      )

     "Actions"
     (
      )

     ))

  (pretty-hydra-define funk/hydra-top-menu
    (:title "The world's your oyster"
            :quit-key "q"
            :foreign-keys warn
            :exit t)
    ("Working"
     (
      ;; ("s" funk/hydra-workspace-menu/body "Workspaces")
      ("w" funk-window/body "Window")
      ;; ("b" funk/hydra-buffer-menu/body "Buffers")
      ("p" funk/hydra-projectile/body "Projectile")
      ("d" funk/hydra-dired/body "Dired")
      ("g" funk/hydra-git/body "Git")
      ("a" funk/hydra-org-agenda/body "Org-agenda")
      ("m" funk/hydra-mu4e/body "Mu4e")
      ;; ("o" funk/hydra-org-agenda-view/body "Org agenda view")
      )

     "Getting Help"
     (("h" funk/hydra-apropos-menu/body "Apropos"))

     "Customizing"
     (("," funk/hydra-prefs-menu/body "Preferences"))))

  (defvar funk/hydra-org-agenda--title (with-fileicon "org" "Org Agenda" 1 -0.05))
  (pretty-hydra-define funk/hydra-org-agenda
    (:color amaranth :quit-key "q" :title funk/hydra-org-agenda--title)
    (
     "Go to"
     ;; open
     (
      ("TAB  "     org-agenda-goto "Goto on right")
      ("RET  "     org-agenda-switch-to "Full goto")
      ("C-RET"   org-agenda-recenter "View on right")
      ;; motion
      ("j    " org-agenda-next-line)
      ("k    " org-agenda-previous-line)
      ("gj   "  org-agenda-next-item          "Next item")
      ("gk   "  org-agenda-previous-item      "Previous item")
      ("C-j  " org-agenda-next-date-line     "Next date")
      ("C-k  " org-agenda-previous-date-line "Previous date")
      ("<    "   org-agenda-earlier            "Previous week")
      (">    "   org-agenda-later              "Next week")
      ;; refresh
      ("gr   " org-agenda-redo "Refresh view")
      ("gR   " org-agenda-redo-all "Refresh all views")
      )
     "Manipulation"
     (
      ("t  " org-agenda-todo "Change TODOs")
      ("H  " org-agenda-do-date-earlier "Move to yesterday")
      ("J  " org-agenda-priority-down "Down priority")
      ("K  " org-agenda-priority-up "Up priority")
      ("L  " org-agenda-do-date-later "Move to tomorrow")
      ("M-j" org-agenda-drag-line-forward "Move item down")
      ("M-k" org-agenda-drag-line-backward "Move item up")
      ;; actions
      ("dd " org-agenda-kill "Delete item")
      ("da " org-agenda-archive-default-with-confirmation "Archive item")
      ("ct " org-agenda-set-tags "Change tags")
      ("cs " org-agenda-schedule "Change SCHEDULE")
      ("ce " org-agenda-set-effort "Set effort")
      ("cT " org-timer-set-timer "Set timer")
      ("a  " org-agenda-add-note "Add note")
      ("A  " org-agenda-append-agenda "Append view")
      )
     "Mark"
     (
      ("m" org-agenda-bulk-toggle "Mark item")
      ("~" org-agenda-bulk-toggle-all "Revert marks")
      ("*" org-agenda-bulk-mark-all "Mark all items")
      ("%" org-agenda-bulk-mark-regexp "Mark regex")
      ("U" org-agenda-bulk-unmark-all "Unmark all")
      ("x" org-agenda-bulk-action "Action on marks")
      )
     "Display"
     (
      ("vD" org-agenda-toggle-deadlines "Toggle DEADLINEs" :toggle t)
      ("vi" org-agenda-toggle-diary "Toggle diary" :toggle t)
      ("vf" org-agenda-fortnight-view "View two weeks" :toggle t)
      ("ve" org-agenda-entry-text-mode "View items' text" :toggle t)
      ("vg" org-agenda-toggle-time-grid "Toggle time grid" :toggle t)
      ("vd" org-agenda-day-view "View a day" :toggle t)
      ("vw" org-agenda-week-view "View a week" :toggle t)
      ("vm" org-agenda-month-view "View a month" :toggle t)
      ("vy" org-agenda-year-view "View a year" :toggle t)
      ("vb" org-agenda-dim-blocked-tasks "Dim blocker TODOs" :toggle t)
      ("C " org-agenda-capture "Capture")
      ("F " org-agenda-follow-mode "Follow mode" :toggle t)
      )
     "Filter"
     (
      ("sc" org-agenda-filter-by-category "Filter by category")
      ("sr" org-agenda-filter-by-regexp "Filter by regex")
      ("se" org-agenda-filter-by-effort "Filter by effeort")
      ("st" org-agenda-filter-by-tag "Filter by tag")
      ("s^" org-agenda-filter-by-top-headline "Filter by top-headline")
      ("ss" org-agenda-limit-interactively "Interative filter")
      ("S" org-agenda-filter-remove-all "Remove all filters")
      )
     "Clock actions"
     (
      ("I" org-agenda-clock-in "Clock in")
      ("O" org-agenda-clock-out "Clock out")
      ("cg" org-agenda-clock-goto "Goto cloked task")
      ("cc" org-agenda-clock-cancel "Cancel clock")
      ("cr" org-agenda-clockreport-mode "Report clock" :toggle t)
      )
     )
    )
  )

(provide 'init-hydra)
;;; init-hydra.el ends here

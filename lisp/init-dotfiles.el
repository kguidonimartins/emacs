;;; init-dotfiles.el --- dotfiles configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; Quickly find my dotfiles from anywhere
(use-package emacs
  :straight (:type built-in)
  :after (ivy counsel magit projectile)
  :config

  (defvar funk/counsel-git-cmd-dotfiles "git --git-dir='/home/karlo/dotfiles-public/' --work-tree='/home/karlo/' ls-files -z --full-name --"
    "Command for `counsel-git'.")

  (defun funk/counsel-git-cands-dotfiles (dir)
    (let ((default-directory dir))
      (split-string
       (shell-command-to-string funk/counsel-git-cmd-dotfiles)
       "\0"
       t)))

  (defun funk/counsel-git-dotfiles-public (&optional initial-input)
    "Quick find my public dotfiles (bare repository) from anywhere."
    (interactive)
    ;; FIXME 2022-12-03: For while because some `consult' functions
    ;; keep suffering from interference of prescient even after
    ;; disabling.
    (ivy-prescient-mode +1)
    (counsel-require-program funk/counsel-git-cmd-dotfiles)
    (let ((default-directory "/home/karlo"))
      (ivy-read "Find file: " (funk/counsel-git-cands-dotfiles default-directory)
                :initial-input initial-input
                :action #'counsel-git-action
                :caller 'counsel-git)))

  (defvar funk/counsel-git-cmd-emacs "git ls-files -z --full-name -- /home/karlo/.emacs.d.vanilla/"
    "Command for `counsel-git'.")

  (defun funk/counsel-git-cands-emacs (dir)
    (let ((default-directory dir))
      (split-string
       (shell-command-to-string funk/counsel-git-cmd-emacs)
       "\0"
       t)))

  (defun funk/counsel-git-emacs (&optional initial-input)
    "Quick find my emacs files from anywhere."
    (interactive)
    ;; FIXME 2022-12-03: For while because some `consult' functions
    ;; keep suffering from interference of prescient even after
    ;; disabling.
    (ivy-prescient-mode +1)
    (counsel-require-program funk/counsel-git-cmd-emacs)
    (let ((default-directory "/home/karlo/.emacs.d.vanilla/"))
      (ivy-read "Find file: " (funk/counsel-git-cands-emacs default-directory)
                :initial-input initial-input
                :action #'counsel-git-action
                :caller 'counsel-git)))

  (defun ~/magit-process-environment-for-dotfiles-private (env)
    "Add GIT_DIR and GIT_WORK_TREE to ENV when in a special directory.
Manage my dotfiles-private in a bare repository.
From: https://github.com/magit/magit/issues/460 (@cpitclaudel)."
    (let ((default (file-name-as-directory (expand-file-name default-directory)))
          (home (expand-file-name "~/")))
      (when (string= default home)
        (let ((gitdir (expand-file-name "~/dotfiles-private")))
          (push (format "GIT_WORK_TREE=%s" home) env)
          (push (format "GIT_DIR=%s" gitdir) env))))
    env)

  (defun funk/magit-status-for-dotfiles-private ()
    "Check `magit-status' for the ~/dotfiles-private."
    (interactive)
    (progn
      (advice-add 'magit-process-environment :filter-return #'~/magit-process-environment-for-dotfiles-private)
      (magit-status "~/")))


  (defun ~/magit-process-environment-for-dotfiles-public (env)
    "Add GIT_DIR and GIT_WORK_TREE to ENV when in a special directory.
Manage my dotfiles-public in a bare repository.
From: https://github.com/magit/magit/issues/460 (@cpitclaudel)."
    (let ((default (file-name-as-directory (expand-file-name default-directory)))
          (home (expand-file-name "~/")))
      (when (string= default home)
        (let ((gitdir (expand-file-name "~/dotfiles-public")))
          (push (format "GIT_WORK_TREE=%s" home) env)
          (push (format "GIT_DIR=%s" gitdir) env))))
    env)

  (defun funk/magit-status-for-dotfiles-public ()
    "Check `magit-status' for the ~/dotfiles-public."
    (interactive)
    (progn
      (advice-add 'magit-process-environment :filter-return #'~/magit-process-environment-for-dotfiles-public)
      (magit-status "~/")))

  (add-hook 'projectile-after-switch-project-hook #'(lambda () (progn
                                                                 (advice-remove 'magit-process-environment '~/magit-process-environment-for-dotfiles-private)
                                                                 (advice-remove 'magit-process-environment '~/magit-process-environment-for-dotfiles-public))))

  )


(provide 'init-dotfiles)
;;; init-dotfiles.el ends here

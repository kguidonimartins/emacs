;;; init-ssh.el --- ssh connection configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; tramp

(use-package tramp
  
  :straight (:type built-in)
  :config
  (setq tramp-verbose 1) ;; or 6 to debug issues
  (setq tramp-ssh-controlmaster-options "")
  (setq vc-ignore-dir-regexp
        (format "\\(%s\\)\\|\\(%s\\)"
                vc-ignore-dir-regexp
                tramp-file-name-regexp))

  (defun funk/remote-mode-line-function ()
    (when (file-remote-p default-directory)
      (setq mode-line-format
            (format-mode-line mode-line-format
                              '((t (:foreground "black" :weight extra-bold))))))
    )

  (defun funk/sudo-mode-line-function ()
    (when (string-match "^/su\\(do\\)?:" default-directory)
      (setq mode-line-format
            (format-mode-line mode-line-format
                              '((t (:foreground "black")))))))

  (defun turn-off-modes-in-tramp ()
    (interactive)
    (if (in-slow-ssh)
        (progn
          (ivy-mode -1)
          (ivy-rich-mode -1)
          (projectile-mode -1)
          (all-the-icons-dired-mode -1)
          (all-the-icons-ivy-rich-mode -1)
          ;; (company-mode -1)
          )))

  (add-hook 'find-file-hook
            'funk/remote-mode-line-function)
  (add-hook 'dired-mode-hook
            'funk/remote-mode-line-function)
  (add-hook 'find-file-hook
            'funk/sudo-mode-line-function)
  (add-hook 'dired-mode-hook
            'funk/sudo-mode-line-function)
  (add-hook 'find-file-hook
            'turn-off-modes-in-tramp)
  (add-hook 'dired-mode-hook
            'turn-off-modes-in-tramp)

  (defun turn-on-modes-after-leaves-tramp ()
    (interactive)
    (progn
      (ivy-mode +1)
      (ivy-rich-mode +1)
      (projectile-mode +1)
      (all-the-icons-dired-mode +1)
      (all-the-icons-ivy-rich-mode +1)
      ;; (company-mode +1)
      ))

  (add-hook 'tramp-cleanup-all-connections-hook 'turn-on-modes-after-leaves-tramp))

(eval-after-load 'tramp '(setenv "SHELL" "/bin/bash"))

;;; counsel-tramp

(use-package counsel-tramp

  :after (ivy counsel tramp)
  :preface
  (defvar private-hosts-config-file
    (concat (getenv "HOME") "/.ssh/hosts-list.el"))

  (load private-hosts-config-file)

  (defun funk/counsel-tramp-hosts-list ()
    (interactive)
    (counsel-find-file (ivy-read "Tramp: " (funk/hosts-list))))

  :config
  (add-hook 'counsel-tramp-pre-command-hook #'(lambda ()
                                                (global-aggressive-indent-mode 0)
                                                (projectile-mode 0)
                                                (setq make-backup-files nil)
                                                (setq create-lockfiles nil)
                                                ))
  (add-hook 'counsel-tramp-quit-hook #'(lambda ()
                                         (global-aggressive-indent-mode 1)
                                         (projectile-mode 1)
                                         (setq make-backup-files t)
                                         (setq create-lockfiles t)
                                         ))
  )

(provide 'init-ssh)
;;; init-ssh.el ends here

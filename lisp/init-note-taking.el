;;; init-note-taking.el --- Configs for note taking -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package denote
  :config
  ;; Remember to check the doc strings of those variables.
  (setq denote-directory (expand-file-name "~/google-drive/kguidonimartins/denote/"))
  (setq denote-known-keywords
        '("rstats" "emacs" "shell" "programming"))
  (setq denote-infer-keywords t)
  (setq denote-sort-keywords t)
  (setq denote-file-type 'markdown-yaml) ; Org is the default, set others here

  ;; We allow multi-word keywords by default.  The author's personal
  ;; preference is for single-word keywords for a more rigid workflow.
  (setq denote-allow-multi-word-keywords t)

  (setq denote-front-matter-date-format nil) ; change this to `org-timestamp' or custom string

  ;; You will not need to `require' all those individually once the
  ;; package is available.
  (require 'denote-retrieve)
  (require 'denote-link)
  (require 'denote-dired)
  (setq denote-dired-rename-expert nil)

  ;; Generic:
  (add-hook 'dired-mode-hook #'denote-dired-mode)
  ;;
  ;; OR better:
  ;; (add-hook 'dired-mode-hook #'denote-dired-mode-in-directories)
  )


(provide 'init-note-taking)
;;; init-note-taking.el ends here

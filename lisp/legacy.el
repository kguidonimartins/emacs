;; ;; NOTE 2021-11-21: parse the org-store-link to automatically generate todo heading while capturing.

;; ;; workaround to get the comment at point
;; ;; https://www.reddit.com/r/orgmode/comments/k2yado/orgprojectile_capture_template_question/
;; ;; for now, check `org-projectile'
;; (defun get-annotation ()
;;   (plist-get org-store-link-plist :annotation))
;; (defun remove-starting-brackets (s)
;;   (require 'subr-x)
;;   (string-remove-prefix "\[\[" s))
;; (defun remove-trailing-brackets (s)
;;   (require 'subr-x)
;;   (string-remove-suffix "\]\]" s))
;; (defun remove-brackets (s)
;;   (remove-trailing-brackets (remove-starting-brackets s)))
;; (defun description-text ()
;;   (remove-trailing-brackets
;;    (nth 1
;;         (split-string (get-annotation) "::"))))

;; ;; BEG testing ---------------------------------------------------------------------
;; ;; quero remover os caracteres de comentário do link estocado pelo org-mode quando é colado no buffer

;; (defun give-me-comment-starters-ender-for-a-mode (mode)
;;   "Returns a cons containing comment-start comment-end of arg MODE"
;;   (with-temp-buffer
;;     (funcall mode)
;;     (cons comment-start comment-end)))

;; (defun comment-starter-ender-return-test (&optional arg)
;;   (interactive)
;;   (let ((erg (give-me-comment-starters-ender-for-a-mode (or arg 'emacs-lisp-mode))))
;;     (message "%s" erg)))

;; (defun buffer-mode (buffer-or-string)
;;   "Returns the major mode associated with a buffer."
;;   (with-current-buffer buffer-or-string major-mode))

;; (defun get-comment-string-current-buffer()
;;   (comment-starter-ender-return-test (buffer-mode (current-buffer)))
;;   )

;; (defun start-comment-string()
;;   (require 'subr-x)
;;   (string-remove-prefix "\("
;;                         (nth 0
;;                              (split-string (get-comment-string-current-buffer) " " ))
;;                         ))

;; (defun end-comment-string()
;;   (require 'subr-x)
;;   (string-remove-suffix "\)"
;;                         (nth 4
;;                              (split-string (get-comment-string-current-buffer) " " ))
;;                         ))

;; ;; (start-comment-string)
;; ;; (end-comment-string)

;; (defun clean-todo-string (string)
;;   (let ((final ""))
;;     (setq final
;;           (string-replace (start-comment-string) "" string))

;;     (if (end-comment-string)
;;         (setq final
;;               (string-replace (end-comment-string) "" string)
;;               ))

;;     (if (string-match ":" string)
;;         (setq final
;;               (string-replace ":" "" string)))

;;     (if (string-match "TODO" string) nil
;;       (setq final
;;             (concat "TODO " string))
;;       )

;;     (string-trim final)

;;     ))

;; ;; (clean-todo-string (description-text))
;; ;; END testing---------------------------------------------------------------------

;; ;; NOTE: capture templates

;; ("e" "Errands - general tasks"
;;  entry (file+olp "~/google-drive/kguidonimartins/org/tasks.org" "Errands")
;;  "* TODO %? :errands:\n %^{Isso é pra quando?}T \n :PROPERTIES:\n :CATEGORY: errands\n :END:" :empty-lines 1)

;; ;; ("w" "Workflows")
;; ;; ("we" "Checking Email" entry (file+olp+datetree "~/google-drive/kguidonimartins/org/journal.org")
;; ;;  "* Checking Email :email:\n\n%?" :clock-in :clock-resume :empty-lines 1)

;; ;; ("a" "Appointment" entry (file+olp+datetree  "~/google-drive/kguidonimartins/org/appointments.org" )
;; ;;      "* %?\n\n%^T\n\n:PROPERTIES:\n\n:END:\n\n")

;; ("@" "Mail messages")
;; ("@e" "Inbox [mu4e]" entry (file+olp "~/google-drive/kguidonimartins/org/email.org")
;;  "* CHECK %?\n  %U\n  %a\n  %i" :clock-in :clock-resume :empty-lines 1)

;; ;;;; funk/theme-dark
;; (defun funk/theme-dark ()
;;   "Apply the dark theme based on doom-dracula."
;;   (interactive)
;;   (mapc #'disable-theme custom-enabled-themes)
;;   (load-theme 'doom-dracula t)
;;   (custom-set-faces
;;    '(header-line ((t (:background "#132738"))))
;;    ;; '(font-lock-comment-face ((t (:foreground "#343746"))))
;;    '(aw-leading-char-face ((t (:inherit ace-jump-face-foreground :height 10.0))))
;;    '(blamer-face                        ((t          (:background "#132738"             :foreground "#7a88cf" :height 90))))
;;    '(cursor                             ((t          (:background "#f0cc09"))))
;;    '(eyebrowse-mode-line-active         ((t          (:foreground "#F1FA8C" :weight ultra-bold))))
;;    '(default                            ((t          (:background "#132738" :foreground "#FFFFFF"))))
;;    '(diff-hl-change                     ((t          (:background "#132738" :foreground "#6666ff"))))
;;    '(diff-hl-delete                     ((t          (:background "#132738" :foreground "#FF6E67"))))
;;    '(dired-subtree-depth-1-face         ((t          (:background "#132738"))))
;;    '(dired-subtree-depth-2-face         ((t          (:background "#132738"))))
;;    '(dired-subtree-depth-3-face         ((t          (:background "#132738"))))
;;    '(dired-subtree-depth-4-face         ((t          (:background "#132738"))))
;;    '(dired-subtree-depth-5-face         ((t          (:background "#132738"))))
;;    '(dired-subtree-depth-6-face         ((t          (:background "#132738"))))
;;    '(ess-R-font-lock-keywords           ((t          (:foreground "#bc6ec5" :weight                 bold))))
;;    '(ess-function-call-face             ((t          (:foreground "#bc6ec5" :weight                 bold))))
;;    '(font-lock-function-name-face       ((t          (:foreground "#bc6ec5" :weight                 ultra-bold))))
;;    '(font-lock-string-face              ((t          (:foreground "#F1FA8C" :background unspecified))))
;;    '(font-lock-keyword-face             ((t (:foreground "#ff79c6" :background unspecified))))
;;    '(fringe                             ((t (:inherit default))))
;;    '(git-gutter+-added                  ((t          (:foreground "#edc809" :background "#132738"))))
;;    '(git-gutter+-deleted                ((t          (:foreground "#FF6E67" :background "#132738"))))
;;    '(git-gutter+-modified               ((t          (:foreground "#6666ff" :background "#132738"))))
;;    '(hl-line                            ((t          (:background "#212026"))))
;;    '(ivy-org                            ((t          (:inherit    default))))
;;    '(markdown-header-delimiter-face ((t (:foreground "#F1FA8C"    :weight               bold))))
;;    '(markdown-header-face               ((t          (:foreground "#bc6ec5" :weight                 bold))))
;;    '(markdown-italic-face               ((t          (:foreground "#c6c6c6"))))
;;    '(mode-line                          ((t          (:background "#2A265A" :foreground "#FFFFFF"))))
;;    '(mode-line-inactive                 ((t          (:background "#2A265A" :foreground "#bc6ec5"))))
;;    '(mu4e-header-highlight-face         ((t          (:inherit hl-line))))
;;    '(show-paren-match                   ((t          (:background "#FFFFFF" :foreground "#5317ac" :weight ultra-bold))))
;;    '(vc-edited-state                    ((t          (:foreground "#FF6E67" :background unspecified   :weight bold))))
;;    '(vc-up-to-date-state                ((t          (:foreground "#edc809" :background unspecified   :weight bold))))
;;    '(vertical-border                    ((t          (:foreground "#2A265A" :background "#2A265A"   :weight normal))))
;;    '(window-divider                     ((t          (:foreground "#2A265A" :background "#2A265A"   :weight normal))))
;;    ;; '(outline-1                      ((t (:background "#132738" :foreground "#ffffff" :underline t :width ultra-expanded))))
;;    ;; '(outline-2                      ((t (:background "#132738" :foreground "#ffffff" :underline t))))
;;    ;; '(outline-3                      ((t (:background "#132738" :foreground "#ffffff"))))
;;    ;; '(outline-4                      ((t (:background "#132738" :foreground "#ffffff"))))
;;    ;; '(funk/modeline-buffer-path      ((t (:foreground "#bc6ec5" :background "#2A265A" :weight     ultra-bold))))
;;    ;; '(funk/modeline-buffer-file      ((t (:foreground "#f8f8f2" :background "#2A265A" :weight     ultra-bold))))
;;    )
;;   ;; midnight colors for pdf-view
;;   (setq pdf-view-midnight-colors '("#f8f8f2" . "#132738"))
;;   (add-hook 'pdf-tools-enabled-hook 'pdf-view-midnight-minor-mode)
;;   ;; set hl-todo-colors
;;   (funk/set-hl-todo)
;;   (setq company-quickhelp-color-background "#132738")
;;   (setq company-quickhelp-color-foreground "#FFFFFF")
;;   (setq hl-block-bracket-face '(t (:background "#5317ac" :foreground "#FFFFFF" :weight ultra-bold)))
;;
;;
;;   (defun funk/csv-mode-faces ()
;;     (face-remap-add-relative 'hl-line '(:background "#343746"))
;;     (face-remap-add-relative 'header-line '(:background "#343746")))
;;
;;   )
;;
;; ;;;; funk/theme-grey
;; (defun funk/theme-grey ()
;;   "Apply the grey theme based on doom-dracula."
;;   (interactive)
;;   (mapc #'disable-theme custom-enabled-themes)
;;   (load-theme 'doom-dracula t)
;;   (custom-set-faces
;;    ;; '(default ((t (:background "#1b1b1b" :foreground "#FFFFFF"))))
;;    ;; '(header-line ((t (:background "#1b1b1b"))))
;;    ;; '(font-lock-comment-face ((t (:foreground "#343746"))))
;;    '(aw-leading-char-face ((t (:inherit ace-jump-face-foreground :height 10.0))))
;;    '(cursor                         ((t (:background "#f0cc09"    ))))
;;    '(default                        ((t (:background "#1b1b1b"    :foreground "#f8f8f2"))))
;;    '(diff-hl-margin-change          ((t (:background "#1b1b1b"    :foreground "#6666ff"))))
;;    '(diff-hl-margin-delete          ((t (:background "#1b1b1b"    :foreground "#FF6E67"))))
;;    '(diff-hl-margin-ignored         ((t (:background "#1b1b1b"    :foreground nil))))
;;    '(diff-hl-margin-insert          ((t (:background "#1b1b1b"    :foreground "#edc809"))))
;;    '(diff-hl-margin-unknown         ((t (:background "#1b1b1b"    :foreground "#FF6E67"))))
;;    '(dired-subtree-depth-1-face     ((t (:background "#1b1b1b"))))
;;    '(dired-subtree-depth-2-face     ((t (:background "#1b1b1b"))))
;;    '(dired-subtree-depth-3-face     ((t (:background "#1b1b1b"))))
;;    '(dired-subtree-depth-4-face     ((t (:background "#1b1b1b"))))
;;    '(dired-subtree-depth-5-face     ((t (:background "#1b1b1b"))))
;;    '(dired-subtree-depth-6-face     ((t (:background "#1b1b1b"))))
;;    '(ess-R-font-lock-keywords       ((t (:foreground "#bc6ec5"    :weight     bold))))
;;    '(ess-function-call-face         ((t (:foreground "#bc6ec5"    :weight     bold))))
;;    '(font-lock-function-name-face   ((t (:foreground "#bc6ec5"    :weight     ultra-bold))))
;;    '(font-lock-keyword-face         ((t (:foreground "#F1FA8C"))))
;;    '(font-lock-string-face          ((t (:foreground "#F1FA8C"))))
;;    '(fringe                         ((t (:background "#1b1b1b"    :foreground "#f8f8f2"))))
;;    '(git-gutter+-added              ((t (:foreground "#edc809"    :background "#1b1b1b"))))
;;    '(git-gutter+-deleted            ((t (:foreground "#FF6E67"    :background "#1b1b1b"))))
;;    '(git-gutter+-modified           ((t (:foreground "#6666ff"    :background "#1b1b1b"))))
;;    '(hl-line                        ((t (:background "#212026"))))
;;    '(markdown-header-delimiter-face ((t (:foreground "#F1FA8C"    :weight     bold))))
;;    '(markdown-header-face           ((t (:foreground "#bc6ec5"    :weight     bold))))
;;    '(markdown-italic-face           ((t (:foreground "#c6c6c6"))))
;;    '(mode-line                      ((t (:background "#3A3A3A"    :foreground "#FFFFFF"))))
;;    '(mode-line-inactive             ((t (:background "#3A3A3A"))))
;;    '(window-divider                 ((t (:foreground "#3A3A3A"    :weight     bold))))
;;    )
;;   ;; midnight colors for pdf-view
;;   (setq pdf-view-midnight-colors '("#f8f8f2" . "#1b1b1b"))
;;   (add-hook 'pdf-tools-enabled-hook 'pdf-view-midnight-minor-mode)
;;   ;; set hl-todo-colors
;;   (funk/set-hl-todo)
;;   )
;;
;; ;;;; funk/theme-light
;; (defun funk/theme-light ()
;;   "Apply the light theme based on modus-operandi."
;;   (interactive)
;;   (mapc #'disable-theme custom-enabled-themes)
;;   (load-theme 'modus-operandi t) ;; light
;;   (custom-set-faces
;;    ;; '(header-line ((t (:background "#FFFFFF"))))
;;    '(aw-leading-char-face ((t (:inherit ace-jump-face-foreground :height 7.0))))
;;    '(blamer-face                            ((t          (:background "#FFFFFF"             :foreground "#7a88cf" :height 90))))
;;    '(cursor                                 ((t          (:background "#f0cc09"             ))))
;;    '(default                                ((t          (:background "#FFFFFF"             :foreground "#000000"))))
;;    '(diff-hl-margin-change                  ((t          (:background nil                   :foreground "#6666ff"))))
;;    '(diff-hl-margin-delete                  ((t          (:background nil                   :foreground "#FF6E67"))))
;;    '(diff-hl-margin-ignored                 ((t          (:background nil                   :foreground nil))))
;;    '(diff-hl-margin-insert                  ((t          (:background nil                   :foreground "#edc809"))))
;;    '(diff-hl-margin-unknown                 ((t          (:background nil                   :foreground "#FF6E67"))))
;;    '(doom-modeline-bar                      ((t          (:foreground "#6666ff"))))
;;    '(doom-modeline-buffer-file              ((t          (:foreground "#FFFFFF"))))
;;    '(doom-modeline-buffer-major-mode        ((t          (:foreground "#FFFFFF"))))
;;    '(doom-modeline-buffer-minor-mode        ((t          (:foreground "#FFFFFF"))))
;;    '(doom-modeline-buffer-modified          ((t          (:foreground "#132738"))))
;;    '(doom-modeline-buffer-path              ((t          (:foreground "#FFFFFF"))))
;;    '(doom-modeline-evil-emacs-state         ((t          (:foreground "#FFFFFF"))))
;;    '(doom-modeline-evil-insert-state        ((t          (:foreground "#FFFFFF"))))
;;    '(doom-modeline-evil-normal-state        ((t          (:foreground "#FFFFFF"))))
;;    '(doom-modeline-info                     ((t          (:foreground "#FFFFFF"))))
;;    '(ess-function-call-face                 ((t          (:foreground "#5317ac"             :weight               bold))))
;;    '(font-lock-keyword-face                 ((t          (:foreground "#5317ac"))))
;;    '(font-lock-string-face                  ((t          (:background unspecified             :foreground "#2544bb"))))
;;    '(forge-topic-label                      ((t          (:box        nil))))
;;    '(fringe                                 ((t          (:inherit default))))
;;    ;; '(funk/modeline-buffer-file              ((nil        (:foreground "#FFFFFF" :weight                 bold))))
;;    '(funk/modeline-buffer-path              ((nil        (:foreground "#5317ac" :weight                 bold))))
;;    '(funk/modeline-project-dir              ((nil        (:foreground "#edc809" :weight                 bold))))
;;    '(funk/modeline-project-parent-dir       ((nil        (:foreground "#2544bb"              :weight                 bold))))
;;    '(funk/modeline-project-root-dir         ((nil        (:foreground "#FFFFFF" :weight                 regular))))
;;    '(git-gutter+-added                      ((t          (:foreground "#edc809"             :background "#FFFFFF"))))
;;    '(git-gutter+-deleted                    ((t          (:foreground "#FF6E67"             :background "#FFFFFF"))))
;;    '(git-gutter+-modified                   ((t          (:foreground "#6666ff"             :background "#FFFFFF"))))
;;    '(hl-line                                ((t          (:background "#f2eff3"             :extend               t))))
;;    '(markdown-header-delimiter-face         ((t          (:foreground "#5317ac"             :weight               ultra-bold))))
;;    '(markdown-header-face                   ((t          (:foreground "#5317ac"             :weight               ultra-bold))))
;;    '(markdown-header-face-1                 ((t          (:foreground "#5317ac"             :weight               ultra-bold))))
;;    '(markdown-header-face-2                 ((t          (:foreground "#5317ac"             :weight               ultra-bold))))
;;    '(markdown-header-face-3                 ((t          (:foreground "#5317ac"             :weight               ultra-bold))))
;;    '(mode-line                              ((t          (:background "#6666ff"             :foreground "#FFFFFF"  :box   nil))))
;;    '(mode-line-inactive                     ((t          (:background "#c1c1ff"             :foreground "#FFFFFF"  :box   nil))))
;;    '(funk/modeline-buffer-file-modified     ((t          (:foreground "#c1c1ff"))))
;;    '(org-block                              ((t          (:background "#f0f0f0"))))
;;    '(org-done                               ((t          (:background "#f2eff3"             :foreground "#005e00"  :bold t))))
;;    '(org-todo                               ((t          (:background "#F1FA8C"             :foreground "#6666ff"  :bold t))))
;;    '(show-paren-match                       ((t          (:background "#5317ac" :foreground "#FFFFFF" :weight ultra-bold))))
;;    '(vc-edited-state                        ((t          (:foreground "#edc809" :background unspecified   :weight bold))))
;;    '(vc-up-to-date-state                    ((t          (:foreground "#FFFFFF" :background unspecified   :weight bold))))
;;    '(vertical-border                        ((t          (:foreground "#888888" :background "#888888"   :weight    bold))))
;;    '(window-divider                         ((t          (:foreground "#888888" :background "#888888"   :weight    bold))))
;;    )
;;   ;; ;; inverted midnight colors for pdf-view
;;   ;; (setq pdf-view-midnight-colors '("#000000" . "#FFFFFF"))
;;   ;; (add-hook 'pdf-tools-enabled-hook 'pdf-view-midnight-minor-mode)
;;   (setq hl-block-bracket-face '(t (:background "#f0f0f0" :foreground "#5317ac" :weight ultra-bold)))
;;   ;; set hl-todo-colors
;;   (funk/set-hl-todo)
;;
;;   (defun funk/csv-mode-faces ()
;;     (face-remap-add-relative 'hl-line '(:background "#c1c1ff"))
;;     (face-remap-add-relative 'header-line '(:background "#c1c1ff")))
;;
;;   )


;; ;; custom theme path
;; (get-theme-files "themes")
;;
;; ;; (load-theme 'funkday t)
;;
;; ;; ;; TODO: preciso fazer o meu próprio tema: funk-theme
;; ;; ;;       talvez esse tema possa me ajudar a desenvolver isso:
;; ;; ;;       https://gitlab.com/aimebertrand/timu-spacegrey-theme
;; ;; ;; REVIEW 2021-07-29: check also themes by: https://github.com/guidoschmidt/circadian.el
;; ;; ;; CHECK: `list-faces-display'
;; ;; ;; CHECK: https://emacs.stackexchange.com/a/52461/31478
;; ;; (use-package timu-spacegrey-theme
;; ;;   :init
;; ;;   (setq timu-spacegrey-flavour "dark")
;; ;;   :config
;; ;;   (load-theme 'timu-spacegrey t))
;;
;; ;; (use-package night-owl-theme)
;; ;; (load-theme 'night-owl t)
;;
;; ;; ;; CHECK: https://www.youtube.com/watch?v=kCCIudu53Zg
;; ;; ;; autothemer
;; ;; ;; https://github.com/jasonm23/autothemer
;; ;; (use-package autothemer)
;;
;; ;; https://github.com/daylerees/colour-schemes/blob/master/emacs/rainbow-theme.el


;;; init-bibtex.el --- bibtex configuration -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; library variables

;; (defvar funk/bibtex-bib-paths
;;   '(
;;     ;; "~/google-drive/kguidonimartins/UFG/tese-karlo/qualification/bibliography/bibliography.bib"
;;     ;; "~/google-drive/kguidonimartins/UFG/tese-karlo/qualification/bibliography/manual_entry.bib"
;;     "~/google-drive/kguidonimartins/UFG/tese-karlo/cap_02/bibliography/bibliography.bib"
;;     "~/google-drive/kguidonimartins/UFG/tese-karlo/cap_02/bibliography/manual_entry.bib"
;;     ;; "~/google-drive/kguidonimartins/Zotero_Library_bib/26-02-2021_Zotero-Library.bib"
;;     )
;;   )

;; (defvar funk/bibtex-bib-paths (s-split "\n" (shell-command-to-string "fd --hidden --no-ignore --exclude=.git --type f --glob \"*.bib\" ~/google-drive/kguidonimartins/UFG")))


(defvar funk/bibtex-bib-paths '(
                                "~/google-drive/kguidonimartins/UFG/tese-karlo/fire-fd-data/bibliography/bibliography-paper.bib"
                                "~/google-drive/kguidonimartins/UFG/tese-karlo/fire-fd-data/bibliography/manual_entry.bib"
                                "~/google-drive/kguidonimartins/UFG/tese-karlo/fire-fd-data/bibliography/bibliography.bib"
                                "~/google-drive/kguidonimartins/UFG/tese-karlo/fire-fd-data/bibliography/bibliography-used.bib"
                                "~/google-drive/kguidonimartins/UFG/tese-karlo/qualification/bibliography/bibliography.bib"
                                ))


(defvar funk/bibtex-pdf-paths '("~/google-drive/kguidonimartins/UFG/tese-karlo/fire-fd-data/bibliography/articles"
                                "~/google-drive/kguidonimartins/UFG/tese-karlo/fire-fd-data/bibliography/books"))

;;; bibtex-completion
(use-package bibtex-completion

  :config
  (setq bibtex-completion-bibliography funk/bibtex-bib-paths)
  (setq bibtex-completion-library-path funk/bibtex-pdf-paths)
  (setq bibtex-completion-pdf-field '"pdf")
  ;; Customize layout of search results
  ;; first add journal and booktitle to the search fields
  (setq bibtex-completion-additional-search-fields '(journal booktitle))
  (setq bibtex-completion-display-formats
        '((article       . "${=has-pdf=:1}${=has-note=:1} ${=type=:3} ${year:4} ${author:36} ${title:*} ${journal:40}")
          (inbook        . "${=has-pdf=:1}${=has-note=:1} ${=type=:3} ${year:4} ${author:36} ${title:*} Chapter ${chapter:32}")
          (incollection  . "${=has-pdf=:1}${=has-note=:1} ${=type=:3} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
          (inproceedings . "${=has-pdf=:1}${=has-note=:1} ${=type=:3} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
          (t             . "${=has-pdf=:1}${=has-note=:1} ${=type=:3} ${year:4} ${author:36} ${title:*}")))
  ;; Symbols used for indicating the availability of notes and PDF files
  (setq bibtex-completion-pdf-symbol "⌘")
  (setq bibtex-completion-notes-symbol "✎"))

;;; citar
(use-package citar

  :bind (("C-c b" . citar-insert-citation)
         :map minibuffer-local-map
         ("M-b" . citar-insert-preset))
  :after (embark bibtex-completion)
  :config
  ;; ;; Make the 'citar' bindings and targets available to `embark'.
  ;; (add-to-list 'embark-target-finders 'citar-citation-key-at-point)
  ;; (add-to-list 'embark-keymap-alist '(bib-reference . citar-map))
  ;; (add-to-list 'embark-keymap-alist '(citation-key . citar-buffer-map))
  ;; (setq citar-at-point-function 'embark-act)
  (setq citar-bibliography funk/bibtex-bib-paths)
  ;; use consult-completing-read for enhanced interface
  (advice-add #'completing-read-multiple :override #'selectrum-completing-read-multiple)
  (setq citar-symbols
        `((file ,(all-the-icons-faicon "file-o" :face 'all-the-icons-green :v-adjust -0.1) . " ")
          (note ,(all-the-icons-material "speaker_notes" :face 'all-the-icons-blue :v-adjust -0.3) . " ")
          (link ,(all-the-icons-octicon "link" :face 'all-the-icons-orange :v-adjust 0.01) . " ")))
  (setq citar-symbol-separator "  ")
  ;; Here we define a face to dim non 'active' icons, but preserve alignment
  (defface citar-icon-dim
    '((((background dark)) :foreground "#282c34")
      (((background light)) :foreground "#fafafa"))
    "Face for obscuring/dimming icons"
    :group 'all-the-icons-faces)

  )

;; use consult-completing-read for enhanced interface
(advice-add #'completing-read-multiple :override #'consult-completing-read-multiple)

;;; ivy-bibtex
(use-package ivy-bibtex

  :after (ivy bibtex-completion)
  :config

  (defun bibtex-completion-open-pdf-external (keys &optional fallback-action)
    (let ((bibtex-completion-pdf-open-function
           (lambda (fpath) (start-process "zathura" "*helm-bibtex-evince*" "/usr/bin/zathura" fpath))))
      (bibtex-completion-open-pdf keys fallback-action)))

  (defun bibtex-completion-open-doi-at-scihub (keys)
    "Open the DOI associated with entries in KEYS in at SciHub.
Check also: https://github.com/dangom/org-thesis/blob/074c653187a8e788d7d07e77add0e8bdb37f49b3/org-init.el#L415"
    (dolist (key keys)
      (let* ((entry (bibtex-completion-get-entry key))
             (doi (bibtex-completion-get-value "doi" entry))
             (browse-url-browser-function
              (or bibtex-completion-browser-function
                  browse-url-browser-function)))
        (if doi (browse-url
                 (s-concat "https://sci-hub.se/" doi))
          (message "No URL or DOI found for this entry: %s"
                   key)))))

  (ivy-bibtex-ivify-action bibtex-completion-open-pdf-external ivy-bibtex-open-pdf-external)
  (ivy-bibtex-ivify-action bibtex-completion-open-doi-at-scihub ivy-bibtex-doi-at-scihub)
  (ivy-add-actions
   'ivy-bibtex
   '(("P" ivy-bibtex-open-pdf-external "Open PDF file in external viewer (if present)")))
  (ivy-add-actions
   'ivy-bibtex
   '(("p" ivy-bibtex-open-any "Open PDF, URL, or DOI" ivy-bibtex-open-any)
     ("e" ivy-bibtex-edit-notes "Edit notes" ivy-bibtex-edit-notes)
     ("s" ivy-bibtex-doi-at-scihub "Open DOI at scihub" ivy-bibtex-doi-at-scihub)
     )))

;; ;;; consult-bibtex
;; (use-package consult-bibtex
;;   :ensure nil
;;   :after (consult embark)
;;   :quelpa (consult-bibtex :fetcher github
;;                           :repo "mohkale/consult-bibtex")
;;   :config
;;   (with-eval-after-load 'embark
;;     (add-to-list 'embark-keymap-alist '(bibtex-completion . consult-bibtex-embark-map))))


(provide 'init-bibtex)
;;; init-bibtex.el ends here

;;; init-org-roam.el --- Roam configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Problems:
;; https://org-roam.discourse.group/t/org-roam-migrate-wizard-selecting-deleted-buffer/1781/2

(use-package org-roam
  :init
  (setq org-roam-v2-ack t)
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         :map org-mode-map
         ("C-M-i" . completion-at-point))
  :config
  (setq org-roam-directory "~/google-drive/kguidonimartins/roam")
  (setq org-roam-index-file "~/google-drive/kguidonimartins/roam/index.org")
  ;; (setq org-roam-db-location "~/google-drive/kguidonimartins/roam/db/org-roam.db")
  (setq org-roam-node-display-template "${title:*} ${tags:50}")
  (setq org-roam-completion-everywhere t)
  (org-roam-setup)
  (setq org-roam-db-update-method 'immediate)
  (setq org-roam-mode-sections
        (list #'org-roam-backlinks-section
              #'org-roam-reflinks-section
              #'org-roam-unlinked-references-section
              ))
  (setq org-roam-capture-templates
        '(("d" "default" plain
           "%?"
           :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+date: %U\n")
           :unnarrowed t)

          ("b" "book notes" plain
           (file "~/google-drive/kguidonimartins/roam/templates/book-notes.org")
           :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
           :unnarrowed t)

          ("l" "programming language" plain
           "* Characteristics\n\n- Family: %?\n- Inspired by: \n\n* Reference:\n\n"
           :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
           :unnarrowed t)

          ("p" "project" plain "* Goals\n\n%?\n\n* Tasks\n\n** TODO Add initial tasks\n\n* Dates\n\n"
           :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+filetags: Project")
           :unnarrowed t)))
  ;; If using org-roam-protocol
  (defun funk/org-roam-set-fringe ()
    "Custom `org-roam-buffer' behaviours."
    (setq-local left-fringe-width 15
                right-fringe-width 15))
  ;; (add-hook org-roam-mode-hook' 'funk/org-roam-set-fringe)
  (advice-add 'org-roam-buffer-display-dedicated :before 'funk/org-roam-set-fringe)
  (require 'org-roam-protocol))

(provide 'init-org-roam)
;;; init-org-roam.el ends here

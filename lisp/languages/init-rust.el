;;; init-rust.el --- Rust config -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:


;;; rustic
(use-package rustic
  :config
  (require 'lsp-rust)
  (setq lsp-rust-analyzer-completion-add-call-parenthesis nil))

(provide 'init-rust)
;;; init-rust.el ends here

;;; init-javascript.el --- javascript config -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; typescript-mode

(use-package typescript-mode)

(provide 'init-javascript)
;;; init-javascript.el ends here

;;; init-sql.el --- SQL config -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; https://about.gitlab.com/handbook/business-technology/data-team/platform/sql-style-guide/
;; https://github.com/mattm/sql-style-guide
;; https://github.com/purcell/sqlformat
;; https://bitspook.in/blog/using-org-mode-as-an-sql-playground/
;; https://bitspook.in/blog/using-org-mode-as-an-sql-playground/

(provide 'init-sql)
;;; init-sql.el ends here

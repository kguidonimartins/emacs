;;; init-markdown.el --- markdown configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; markdown-mode

(use-package text-mode
  :straight (:type built-in)
  :mode (("\\.txt\\'" . text-mode)))

(use-package markdown-mode
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :config
  (setq markdown-enable-math t)

  (defvar markdown-mode-keywords nil)
  (setq markdown-mode-keywords
        '(("\\[@[^]]+\\]" . font-lock-keyword-face)
          ("@.+" . font-lock-keyword-face)
          )
        )

  (font-lock-add-keywords
   'markdown-mode
   markdown-mode-keywords
   )

  (setq markdown-header-scaling t)

  ;; (set-face-italic-p 'italic nil)
  ;; (set-face-bold-p   'bold nil)

  (general-define-key
   :keymaps '(markdown-mode-map)
   :states '(normal emacs insert)
   "C-m" #'newline)

  )

;;; pandoc-mode
;; REVIEW 2021-07-29: https://github.com/joostkremers/pandoc-mode/blob/master/pandoc-mode.el

;;; imenu-list
;; from: https://jblevins.org/log/markdown-imenu
;; (use-package imenu-list
;;   :config
;;   (setq imenu-list-focus-after-activation t
;;         imenu-list-auto-resize nil)
;;   (add-hook 'markdown-mode-hook 'imenu-add-menubar-index)
;;   (setq imenu-auto-rescan t)
;;   )

(provide 'init-markdown)
;;; init-markdown.el ends here

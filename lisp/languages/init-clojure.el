;;; init-clojure.el --- clojure configs -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package flycheck-clj-kondo)

;; then install the checker as soon as `clojure-mode' is loaded
(use-package clojure-mode
  ;; :ensure-system-package
  ;; (clojure . "yay -S --noconfirm clojure leiningen clj-kondo-bin cljstyle-bin")
  :config
  (require 'flycheck-clj-kondo))

(use-package cider
  :config
  (general-define-key
   :keymaps '(cider-mode-map)
   :states '(normal)
   ",," #'cider-eval-defun-at-point
   "SPC SPC" #'cider-eval-defun-at-point))

(use-package clj-refactor
  ;; :ensure-system-package
  ;; (cljstyle . "yay -S --noconfirm cljstyle-bin")
  )

;; (use-package lsp-mode
;;   :ensure t
;;   :hook ((clojure-mode . lsp))
;;   :commands lsp
;;   :custom
;;   ((lsp-clojure-server-command '("java" "-jar" "/home/user/clj-kondo/clj-kondo-lsp-server.jar")))
;;   :config
;;   (dolist (m '(clojure-mode
;;                clojurescript-mode))
;;     (add-to-list 'lsp-language-id-configuration `(,m . "clojure"))))


(provide 'init-clojure)
;;; init-clojure.el ends here

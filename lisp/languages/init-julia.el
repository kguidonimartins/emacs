;;; init-julia.el --- Julia development configs -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; * [[https://github.com/frederic-santos/ob-ess-julia][frederic-santos/ob-ess-julia: A lightweight Julia support for org mode using Emacs Speaks Statistics]]
;; * [[https://sntag.github.io/post/2020-04-13-getting-emacs-and-julia-to-work/][Getting Emacs and Julia to work · SNTagore's blog]]
;; * [[https://juliahub.com/ui/Packages/EmacsVterm/yLklC/0.2.0][EmacsVterm · JuliaHub]]
;; * [[https://discourse.julialang.org/t/emacs-based-workflow/19400][Emacs-based workflow - Tooling - JuliaLang]]
;; * [[https://github.com/jinzhu/zeal-at-point][jinzhu/zeal-at-point: Search the word at point with Zeal (Emacs)]]
;; * [[https://hershsingh.net/blog/emacs-julia/][Setting up a Julia workflow in Emacs]]

(provide 'init-julia)
;;; init-julia.el ends here

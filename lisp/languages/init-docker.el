;;; init-docker.el --- Docker config -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; ESS-mode and docker
;; * [[https://gtown-ds.netlify.app/2017/08/16/docker-emacs/][Running R on a Docker container through Emacs · Steve Lane]]
;; * [[https://github.com/rocker-org/rocker/wiki/Allowing-GUI-windows][Allowing GUI windows · rocker-org/rocker Wiki]]
;; * [[https://github.com/emacs-ess/ESS/issues/1059][ESS over Tramp on docker container does not forward plots anymore · Issue #1059 · emacs-ess/ESS]]
;; * [[https://www.stat.purdue.edu/~sguha/r_and_ess_remote.html][Using ESS With a Remote Session]]
;; * [[https://guangchuangyu.github.io/2013/11/run-remote-r-in-emacs-with-ess/][Run remote R in Emacs with ESS]]

(provide 'init-docker)
;;; init-docker.el ends here

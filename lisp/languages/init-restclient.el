;;; init-restclient.el --- Config for restclient -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; restclient (like postman)
;; https://github.com/pashky/restclient.el

(use-package restclient

  :config
  (add-to-list 'auto-mode-alist '("\\.http" . restclient-mode))
  (defun funk/restclient-eval-request-and-stay ()
    (interactive)
    (restclient-http-send-current nil t))
  (general-define-key
   :keymaps '(restclient-mode-map)
   :states '(normal)
   ",,"      #'funk/restclient-eval-request-and-stay
   "SPC SPC" #'funk/restclient-eval-request-and-stay
   "C-c C-c" #'funk/restclient-eval-request-and-stay))

;; https://github.com/iquiw/company-restclient
(use-package company-restclient

  :after restclient)

;; https://github.com/alf/ob-restclient.el


(provide 'init-restclient)
;;; init-restclient.el ends here

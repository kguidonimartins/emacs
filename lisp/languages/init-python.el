;;; init-python.el --- python configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; CHECK: https://github.com/nryotaro/pyvenv-auto
;; CHECK: https://github.com/SystemCrafters/rational-emacs/blob/master/modules/rational-python.el
;; CHECK: https://github.com/galaunay/poetry.el

;;; python
(use-package python
  
  ;; :ensure-system-package
  ;; (pipx . "yay -S --noconfirm python-pipx")
  :config
  (global-unset-key (kbd "C-c C-p"))
  :config
  (setq python-shell-interpreter "ipython"
        python-shell-interpreter-args "-i --pprint"
        python-shell-prompt-detect-failure-warning nil)
  (setq python-indent-guess-indent-offset t)
  (setq python-indent-guess-indent-offset-verbose nil)
  (add-to-list 'python-shell-completion-native-disabled-interpreters "ipython")
  )

;;; conda
(use-package conda
  :after python

  :init
  (setq conda-anaconda-home (expand-file-name "~/.local/lib/miniconda3"))
  (setq conda-env-home-directory (expand-file-name "~/.local/lib/miniconda3"))
  (setq conda-env-subdirectory "envs")
  :config
  (conda-env-initialize-interactive-shells)
  (conda-env-initialize-eshell))

;;; anaconda
;; (use-package anaconda-mode
;;   :config
;;   (add-hook 'python-mode-hook 'anaconda-mode)
;;   (add-hook 'python-mode-hook 'anaconda-eldoc-mode))


;;; pyvenv
(use-package pyvenv
  :after python

  )


;;; black
(use-package python-black

  :after python
  )

;;; isort
(use-package py-isort
  :after python

  )

(provide 'init-python)
;;; init-python.el ends here

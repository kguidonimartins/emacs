;;; init-emacs-lisp.el --- emacs-lisp configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; (use-package rainbow-blocks
;;   :hook
;;   (emacs-lisp-mode . rainbow-blocks-mode))

;; REVIEW: https://github.com/noctuid/lispyville

;; (use-package lispyville
;;              :config
;;              (add-hook 'emacs-lisp-mode-hook #'lispyville-mode)
;;              (add-hook 'emacs-lisp-mode-hook #'clojure-mode))


(use-package emacs
  :config
  (general-define-key
   :keymaps '(emacs-lisp-mode-map)
   :states '(normal)
   "C-c C-b" #'eval-buffer))

(use-package elisp-refs)

(defun funk/eval-last-sexp ()
  (interactive)
  (progn
    (call-interactively 'evil-append)
    (call-interactively 'eval-last-sexp)
    (evil-normal-state)))

(provide 'init-emacs-lisp)
;;; init-emacs-lisp.el ends here

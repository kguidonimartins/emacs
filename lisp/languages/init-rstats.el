;;; init-rstats.el --- rstats configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; ESS (ess-mode)
(use-package ess
  :after (evil key-chord general)
  ;; :ensure-system-package
  ;; (R . "yay -S --noconfirm r ccache udunits geos proj gdal gcc gcc-fortran glibc gcc-fortran make openblas imagemagick")
  :init
  ;; (require 'ess-site)
  (progn
    (require 'ess-site)
    (setq abbrev-mode t)
    (define-skeleton ess-abbrev-fl "Docstring." nil "filter(" _ ")")
    (define-skeleton ess-abbrev-sl "Docstring." nil "select(" _ ")")
    (define-skeleton ess-abbrev-gb "Docstring." nil "group_by(" _ ")")
    (define-skeleton ess-abbrev-mt "Docstring." nil "mutate(" _ ")")
    (define-skeleton ess-abbrev-hr "Docstring." nil "here(\"" _ "\")")
    ;; (define-skeleton ess-abbrev-st "Docstring." nil "st_drop_geometry()")
    (define-skeleton ess-abbrev-sd "Docstring." nil "str_detect(" _ ")")
    (define-skeleton ess-abbrev-sr "Docstring." nil "str_replace(" _ ")")
    (define-skeleton ess-abbrev-rs "Docstring." nil "read_sf(" _ ")")
    (define-skeleton ess-abbrev-rc "Docstring." nil "read_csv(" _ ")")
    (define-skeleton ess-abbrev-wc "Docstring." nil "write_csv(" _ ")")
    (define-skeleton ess-abbrev-t  "Docstring." nil "TRUE")
    (define-skeleton ess-abbrev-f  "Docstring." nil "FALSE")
    (define-skeleton ess-abbrev-vi "Docstring." nil "misc::view_vd()")
    (define-skeleton ess-abbrev-vt "Docstring." nil "misc::view_vd(title = \".\")")
    (define-abbrev-table 'ess-mode-abbrev-table
      '(
        ("pp" "%>%" nil 0)
        ("fl" "" ess-abbrev-fl)
        ("sl" "" ess-abbrev-sl)
        ("gb" "" ess-abbrev-gb)
        ("mt" "" ess-abbrev-mt)
        ("hr" "" ess-abbrev-hr)
        ;; ("st "" ess-abbrev-st)
        ("sd" "" ess-abbrev-sd)
        ("sr" "" ess-abbrev-sr)
        ("rs" "" ess-abbrev-rs)
        ("rc" "" ess-abbrev-rc)
        ("wc" "" ess-abbrev-wc)
        ;; ("t" ""  ess-abbrev-t)
        ;; ("f" ""  ess-abbrev-f)
        ("vv" "" ess-abbrev-vi)
        ("vt" "" ess-abbrev-vt)
        )
      )
    (dolist (hook '(ess-mode-hook inferior-ess-mode-hook))
      (add-hook hook
                (lambda ()
                  (make-local-variable 'local-abbrev-table)
                  (setq local-abbrev-table ess-mode-abbrev-table)
                  )
                )
      ))
  :ensure ess
  :config

  (custom-set-faces
   '(ess-function-call-face ((t (:weight bold :inherit font-lock-function-name-face))))
   '(ess-keyword-face ((t (:inherit ess-function-call-face))))
   '(ess-modifiers-face ((t (:inherit ess-function-call-face))))
   )


  (general-create-definer funk/ess-primary-leader-key
    :keymaps '(ess-mode-map)
    :prefix ","
    :global-prefix "C-,")

  (general-create-definer funk/ess-secondary-leader-key
    :keymaps '(ess-mode-map)
    :prefix "SPC"
    :non-normal-prefix "C-SPC")

  ;; NOTE: keep a similar list for both leader keys.
  ;; thanks: https://github.com/noctuid/general.el/issues/501
  (defmacro funk/ess-both-leader-keys (&rest args)
    `(progn
       (funk/ess-primary-leader-key ,@args)
       (funk/ess-secondary-leader-key ,@args)))

  (funk/ess-both-leader-keys
   :states '(normal)
   ","   #'funk/ess-eval-and-recenter
   "SPC" #'funk/ess-eval-and-recenter
   "dd"  #'ess-rdired
   "de"  #'funk/get-ess-inferior-buffer
   "ea"  #'ess-switch-process
   "ec"  #'funk/polymode-eval-R-chunk
   "ef"  #'funk/ess-select-bracket-block-recursive
   ;; "ei"  #'funk/add-in-here
   "en"  #'funk/polymode-eval-R-chunk-and-next
   "ew"  #'funk/ess-eval-word
   "q"   #'funk/ess-quit-kill-inferior
   "rp"  #'funk/ess-baseplot-by-wrap-object
   "ev"  #'ess-view-data-print
   "eq"  #'ess-abort
   "es"  #'ess-switch-to-inferior-or-script-buffer
   "v"  #'funk/ess-eval-misc-view
   )

  ;; (general-define-key
  ;;  :keymaps '(ess-mode-map)
  ;;  :states '(normal)
  ;;  ",," #'funk/ess-eval-and-recenter
  ;;  "SPC SPC" #'funk/ess-eval-and-recenter
  ;;  ",ef" #'funk/ess-select-bracket-block-recursive
  ;;  "SPC ef" #'funk/ess-select-bracket-block-recursive
  ;;  ",ec" #'funk/polymode-eval-R-chunk
  ;;  "SPC ec" #'funk/polymode-eval-R-chunk
  ;;  ",ei" #'funk/add-in-here
  ;;  "SPC ei" #'funk/add-in-here
  ;;  ",en" #'funk/polymode-eval-R-chunk-and-next
  ;;  "SPC en" #'funk/polymode-eval-R-chunk-and-next
  ;;  ",ew" #'funk/ess-eval-word
  ;;  "SPC ew" #'funk/ess-eval-word
  ;;  ",q" #'funk/ess-quit-kill-inferior
  ;;  "SPC q" #'funk/ess-quit-kill-inferior
  ;;  ",rp" #'funk/ess-baseplot-by-wrap-object
  ;;  "SPC rp" #'funk/ess-baseplot-by-wrap-object
  ;;  "C-]" #'funk/ess-xref-find-etags
  ;;  ",m" #'funk/add-pipe
  ;;  ",de" #'funk/get-ess-inferior-buffer
  ;;  ",ea" #'ess-switch-process
  ;;  ",dd" #'ess-rdired
  ;;  ",a" #'funk/ess-assign-and-indent-region
  ;;  "SPC a" #'funk/ess-assign-and-indent-region
  ;;  )

  (general-define-key
   :keymaps '(ess-mode-map)
   :states '(motion normal visual)
   :prefix "SPC"
   "rb" #'jupyter-eval-buffer
   "rr" #'jupyter-eval-line-or-region

   "rz" #'jupyter-repl-associate-buffer
   "rZ" #'jupyter-repl-restart-kernel)

  (general-define-key
   :keymaps '(ess-mode-map)
   :states '(visual)
   ",," #'funk/ess-eval-and-recenter
   "SPC SPC" #'funk/ess-eval-and-recenter
   "K" #'ess-display-help-on-object
   )

  (general-define-key
   :keymaps '(ess-mode-map)
   :states '(normal)
   "K" #'ess-display-help-on-object
   )

  (key-chord-define ess-mode-map ",m" 'funk/add-pipe)
  (key-chord-define ess-mode-map ",k" 'funk/add-pipe-inline)
  (key-chord-define ess-mode-map ",j" 'funk/add-pipe-here)
  (key-chord-define ess-mode-map ",a" 'funk/ess-assign-and-indent-region)
  (key-chord-define ess-mode-map ",k" 'funk/add-pipe-inline)

  ;; fonts
  (setq ess-R-font-lock-keywords
        '((ess-R-fl-keyword:modifiers . t)
          (ess-R-fl-keyword:fun-defs . t)
          (ess-R-fl-keyword:keywords . t)
          (ess-R-fl-keyword:assign-ops . t)
          (ess-R-fl-keyword:constants . t)
          (ess-fl-keyword:fun-calls . t)
          (ess-fl-keyword:numbers . t)
          (ess-fl-keyword:operators . t)
          (ess-fl-keyword:delimiters . t)
          (ess-fl-keyword:= . t)
          (ess-R-fl-keyword:F&T . t)
          (ess-R-fl-keyword:%op% . t)))
  (setq inferior-ess-r-font-lock-keywords
        '((ess-S-fl-keyword:prompt . t)
          (ess-R-fl-keyword:messages . t)
          (ess-R-fl-keyword:modifiers . t)
          (ess-R-fl-keyword:fun-defs . t)
          (ess-R-fl-keyword:keywords . t)
          (ess-R-fl-keyword:assign-ops . t)
          (ess-R-fl-keyword:constants . t)
          (ess-fl-keyword:matrix-labels . t)
          (ess-fl-keyword:fun-calls . t)
          (ess-fl-keyword:numbers . t)
          (ess-fl-keyword:operators . t)
          (ess-fl-keyword:delimiters . t)
          (ess-fl-keyword:= . t)
          (ess-R-fl-keyword:F&T . t)))
  ;; others
  (setq ansi-color-for-comint-mode 'filter)
  (setq comint-move-point-for-output t)
  (setq comint-scroll-to-bottom-on-input t)
  (setq comint-scroll-to-bottom-on-output t)
  (setq ess-ask-for-ess-directory nil)
  (setq ess-directory-function #'projectile-project-root)
  (setq ess-auto-width 'window)
  (setq ess-eval-visibly 'nowait)
  (setq ess-execute-in-process-buffer t)
  (setq ess-history-directory (expand-file-name "ess-history/" no-littering-var-directory))
  (setq ess-indent-with-fancy-comments nil)
  (setq ess-local-process-name "R")
  ;; NOTE 2022-06-23: stop being the "white space" police.
  (setq ess-nuke-trailing-whitespace-p nil)
  (setq ess-pdf-viewer-pref "emacsclient")
  (setq ess-plain-first-buffername nil)
  (setq ess-style 'RStudio)
  (setq ess-tab-complete-in-script t)
  (setq ess-use-ido nil)
  (setq ess-write-to-dribble nil)
  (setq inferior-R-args "--no-restore --no-save --quiet")
  (setq inferior-ess-fix-misaligned-output t)
  (setq ess-use-flymake nil)
  ;; package dev
  (setq ess-roxy-str "#'")
  ;; (setq ess-r--company-meta nil)
  (setq ess-roxy-fill-param-p t)
  (setq ess-gen-proc-buffer-name-function 'ess-gen-proc-buffer-name:project-or-directory)

  (define-key ess-mode-map (kbd "C-S-<return>") 'funk/ess-eval-pipe-through-line)
  (define-key ess-mode-map (kbd "C-M-S-<return>") 'funk/ess-eval-pipe-through-line-and-assign)
  (define-key ess-mode-map (kbd "C-c C-c") 'funk/ess-eval-and-recenter)

  (add-hook 'inferior-ess-mode-hook
            #'(lambda()
                ;; (linum-mode 0)
                (setq-local left-fringe-width 5
                            right-fringe-width 5)
                (local-set-key [C-up] 'comint-previous-input)
                (local-set-key [C-down] 'comint-next-input)))

  (add-hook 'ess-mode-hook
            #'(lambda()
                (local-set-key [(shift return)] 'funk/ess-eval)))

  (add-hook 'ess-post-run-hook 'ess-set-language t)

  (add-hook 'ess-mode-hook #'electric-operator-mode)

  (add-to-list 'auto-mode-alist '("DESCRIPTION" . ess-r-mode))
  (add-to-list 'auto-mode-alist '("Rprofile" . ess-r-mode))
  (add-to-list 'auto-mode-alist '("Renviron" . ess-r-mode))

  (define-derived-mode ess-rdired-mode tabulated-list-mode "Rdired"
    "Major mode for output from `ess-rdired'.
`ess-rdired' provides a dired-like mode for R objects.  It shows the
list of current objects in the current environment, one-per-line.  You
can then examine these objects, plot them, and so on."
    :group 'ess-R
    (setq mode-name (concat "RDired " ess-local-process-name))
    (setq tabulated-list-format
          `[("Name" 40 t)
            ("Class" 20 t)
            ("Length" 20 ess-rdired--length-predicate)
            ("Size" 20 ess-rdired--size-predicate)])
    (add-hook 'tabulated-list-revert-hook #'ess-rdired-refresh nil t)
    (when (and (not ess-rdired-auto-update-timer)
               ess-rdired-auto-update-interval)
      (setq ess-rdired-auto-update-timer
            (run-at-time t ess-rdired-auto-update-interval #'ess-rdired-refresh)))
    (add-hook 'kill-buffer-hook #'ess-rdired-cancel-auto-update-timer nil t)
    (tabulated-list-init-header))

  ;; funk-rstats

  ;; Custom variables
  (defvar funk/ess-pipe-character "%>%" "Preferred pipe used.")
  (defvar funk/ess-busy-message '("        " " *BUSY* " "        ") "Message to be showed in the inferior-ess-mode mode-line.")

  ;; Launch new R instances
  (defun funk/ess-start-R ()
    (interactive)
    (if (not (member "*R*" (mapcar (function buffer-name) (buffer-list))))
        (progn
          (delete-other-windows)
          (setq w1 (selected-window))
          (setq w1name (buffer-name))
          (setq w2 (split-window-left w1 nil t))
          (R)
          (set-window-buffer w2 "*R*")
          (set-window-buffer w1 w1name))))

  (defun funk/ess-eval ()
    (interactive)
    (funk/ess-start-R)
    (if (and transient-mark-mode mark-active)
        (call-interactively 'ess-eval-region)
      (call-interactively 'ess-eval-region-or-function-or-paragraph)))

  ;; cause "Shift+Enter" to send the current line to *R*
  (defun funk/ess-eval ()
    (interactive)
    (if (and transient-mark-mode mark-active)
        (call-interactively 'ess-eval-region)
      (call-interactively 'ess-eval-region-or-function-or-paragraph)))

  ;; default ESS to use R
  (defun ess-set-language ()
    (setq-default ess-language "R")
    (setq ess-language "R"))

  ;; Explicitly show busy processes on the mode-line of the inferior-ess-mode.
  (setq ess-busy-strings funk/ess-busy-message)

  ;; Format the mode-line of the inferior-ess-mode
  (defun funk/ess-display-host-name ()
    "Get the hostname to display on the mode-line of the inferior-ess-mode."
    (interactive)
    (let ((host (if (file-remote-p default-directory)
                    (tramp-file-name-host (tramp-dissect-file-name default-directory))
                  (system-name))))
      (concat " @" host)))

  (defun funk/ess-inferior-mode-line ()
    "Format the mode-line of the inferior-ess-mode."
    (setq-local mode-line-format '("%e"
                                   " "
                                   (:eval (eyebrowse-mode-line-indicator))
                                   " "
                                   mode-line-buffer-identification
                                   minions-mode-line-modes
                                   mode-line-misc-info
                                   (:eval (funk/ess-display-host-name))
                                   mode-line-end-spaces))
    (setq-local mode-line-process ""))
  (add-hook 'inferior-ess-mode-hook 'funk/ess-inferior-mode-line)

  ;; Custom actions during/after the code evaluation

  (evil-define-motion funk/evil-find-char (count char)
    "This is based on `evil-find-char'. The change can be checked below.
@@ -1,4 +1,4 @@
-(evil-define-motion funk/evil-find-char (count char)
+(evil-define-motion evil-find-char (count char)
   \"Move to the next COUNT'th occurrence of CHAR.
 Movement is restricted to the current line unless `evil-cross-lines' is non-nil.\"
   :type inclusive
@@ -28,4 +28,4 @@ Movement is restricted to the current line unless `evil-cross-lines' is non-nil.
                                          (line-beginning-position)))
                                   t count)
                 (when fwd (backward-char)))
-        (message \"Cant find\")))))
+        (user-error \"Can't find %c\" char)))))"
    :type inclusive
    (interactive "<c><C>")
    (setq count (or count 1))
    (let ((fwd (> count 0))
          (visual (and evil-respect-visual-line-mode
                       visual-line-mode)))
      (setq evil-last-find (list #'evil-find-char char fwd))
      (when fwd (evil-forward-char 1 evil-cross-lines))
      (let ((case-fold-search nil))
        (unless (prog1
                    (search-forward (char-to-string char)
                                    (cond (evil-cross-lines
                                           nil)
                                          ((and fwd visual)
                                           (save-excursion
                                             (end-of-visual-line)
                                             (point)))
                                          (fwd
                                           (line-end-position))
                                          (visual
                                           (save-excursion
                                             (beginning-of-visual-line)
                                             (point)))
                                          (t
                                           (line-beginning-position)))
                                    t count)
                  (when fwd (backward-char)))
          (message "Cant find")))))

  (defun funk/ess-select-bracket-block ()
    "Select code inside a non-nested bracket {}.
Works with:
  if () {

  }

or

  for () {

  }
"
    (interactive)
    (progn
      (beginning-of-line)
      (evil-visual-char)
      (funk/evil-find-char 1 ?\{)
      (evilmi-jump-items)))

  (defun funk/ess-select-bracket-block-recursive ()
    "Select code inside a nested bracket {}.
  Works with:

  if () {

  } else if () {

  } else {

  }
  "
    (interactive)
    (while (search-forward "{" (line-end-position) t)
      (funk/ess-select-bracket-block)))

  (defun funk/ess-eval-bracket-block ()
    "Eval code through a nested bracket {}."
    (interactive)
    (progn
      (funk/ess-select-bracket-block-recursive)
      (let* (
             (beg (region-beginning))
             (end (region-end))
             )
        (pulse-momentary-highlight-region beg end)
        (ess-eval-region beg end nil)
        (ess-eval-line)
        (ess-next-code-line)
        (message "Finish evalution of a for/if/while/function block.")
        )
      )
    )

  (defun current-line-empty-p ()
    (save-excursion
      (beginning-of-line)
      (looking-at-p "[[:blank:]]*$")))


  (defun funk/ess-eval-and-recenter ()
    "Smart code evaluation. If at the beginning of a nested bracket code block,
then eval the entire block and recenter the window, else run
ess-eval-region-or-function-or-paragraph-and-step.
This also set `ess-r-evaluation-env' to nil. Because my projects
always uses a package structure, this setting allows multiline evaluation
outside a package namespace."
    (interactive)
    (setq ess-r-evaluation-env nil)
    (when (not (eql (line-beginning-position) (line-end-position)))
      (beginning-of-line))
    (when (current-line-empty-p)
      (ess-next-code-line))
    (if (eq (funk/evil-find-char 1 ?\{) nil)
        (progn
          (funk/ess-eval-bracket-block)
          (recenter))
      (progn
        (ess-eval-region-or-function-or-paragraph-and-step)
        (recenter))
      )
    )


  ;; Quick launch R for test
  (defun funk/quick-r ()
    "Launch a new R instance with few opinated sample code."
    (interactive)
    (eyebrowse-create-window-config)
    (eyebrowse-rename-window-config (eyebrowse--get 'current-slot) "quick-r")
    (projectile-switch-project-by-name "~/google-drive/kguidonimartins/git-repos/quick-r")
    (progn
      (find-file (shell-command-to-string "echo -n quick-r_$(date '+%Y%m%d-%H%M%S').R"))
      (insert "if (!require(\"tidyverse\")) install.packages(\"tidyverse\")\nif (!require(\"here\")) install.packages(\"here\")\nif (!require(\"fs\")) install.packages(\"fs\")"))
    (evil-insert 0)
    (newline)
    (newline)
    (ess-eval-buffer))

  (defalias 'qr 'funk/quick-r)
  (evil-ex-define-cmd "qr" #'funk/quick-r)

  ;; Smart code evaluation through a pipe chain
  (defun funk/ess-eval-pipe-through-line (vis &optional assign)
    "Like `ess-eval-paragraph' but only evaluates up to the pipe on this line.

  If no pipe, evaluate paragraph through the end of current line.
  Prefix arg VIS toggles visibility of ess-code as for `ess-eval-region'.
  The optional ASSIGN argument is useful in some cases. At the code examples
  below, the | is the current cursor position and * is the position
  up to which the code is evaluated.

  If ASSIGN is nil, the code is evaluated is this way:

  carb2 <-
  * mtcars |>
  | select(carb, mpg) |>
  filter(carb == 2)

  Which is the same as:

  mtcars |>
  select(carb, mpg)

  Another example:

  carb2 <-
  * mtcars |>
  select(carb, mpg) |>
  | filter(carb == 2)

  Which is the same as:

  mtcars |>
  select(carb, mpg) |>
  filter(carb == 2)

  If ASSIGN is non-nil, the code is evaluated is this way:

  * carb2 <-
  mtcars |>
  | select(carb, mpg) |>
  filter(carb == 2)

  Which is the same as:

  carb2 <-
  mtcars |>
  select(carb, mpg)

  You can define smart keys for these commands, for example:
  C-S-RET   for `funk/ess-eval-pipe-through-line' and
  C-S-M-RET for`funk/ess-eval-pipe-through-line-and-assign'.

  Refer to `funk/ess-eval-pipe-through-line-and-assign'."
    (interactive "P")
    (save-excursion
      (let ((end (progn
                   (funk/ess-beginning-of-pipe-or-end-of-line)
                   (point)))
            (beg (progn (backward-paragraph)
                        (ess-skip-blanks-forward 'multiline)
                        (if (null assign)
                            (funk/ess-skip-assign-arrow))
                        (point))))
        (ess-eval-region beg end vis))))


  (defun funk/add-pipe ()
    "Add a pipe operator |> at the end of the current line.
  Don't add one if the end of line already has one.  Ensure one
  space to the left and start a newline with indentation."
    (interactive)
    (end-of-line)
    (unless (looking-back funk/ess-pipe-character nil)
      (just-one-space 1)
      (insert funk/ess-pipe-character))
    (newline-and-indent))

  (defun funk/ess-eval-pipe-through-line-and-assign ()
    "When the ASSIGN argument of `funk/ess-eval-pipe-through-line' is non-nil."
    (interactive)
    (funk/ess-eval-pipe-through-line t t))

  (defun funk/ess-skip-assign-arrow ()
    "Find the `<-' character and forward one character."
    (interactive)
    (progn
      (if (search-forward "<-" nil t)
          (forward-char 1))))

  (defun funk/ess-beginning-of-pipe-or-end-of-line ()
    "Find point position of end of line or beginning of pipe |>."
    (if (search-forward funk/ess-pipe-character (line-end-position) t)
        (goto-char (match-beginning 0))
      (end-of-line)))

  (defun funk/add-pipe-inline ()
    "Add a pipe operator |> at the end of the current line.
  Don't add one if the end of line already has one."
    (interactive)
    (end-of-line)
    (unless (looking-back funk/ess-pipe-character nil)
      (just-one-space 1)
      (insert funk/ess-pipe-character)))

  (defun funk/add-pipe-here ()
    "Add a pipe operator |> at the current position."
    (interactive)
    (just-one-space 1)
    (insert funk/ess-pipe-character))

  (defun funk/add-in-here ()
    "Add a pipe operator %in% at the current position."
    (interactive)
    (just-one-space 1)
    (insert "%in%"))

  ;; Custom code wrapper
  (defun funk/ess-baseplot-by-wrap-object ()
    (interactive)
    (beginning-of-line)
    (insert "plot(")
    (end-of-line)
    (insert ", colNA = \"blue\")"))

  (defun funk/insert-fig-ref ()
    (interactive)
    ;; (just-one-space 1)
    (insert "`r figure_ref(FIG_)`")
    (backward-char 2))

  (defun funk/insert-tab-ref ()
    (interactive)
    ;; (just-one-space 1)
    (insert "`r table_ref(TAB_)`")
    (backward-char 2))

  ;; compile rmarkdown to HTML or PDF with M-n s
  ;; use YAML in Rmd doc to specify the usual options
  ;; which can be seen at http://rmarkdown.rstudio.com/
  ;; thanks http://roughtheory.com/posts/ess-rmarkdown.html
  (defun funk/render-rmarkdown ()
    "Compile R markdown (.Rmd). Should work for any output type."
    (interactive)
    ;; Check if attached R-session
    (condition-case nil
        (ess-get-process)
      (error
       (ess-switch-process)))
    (let* ((rmd-buf (current-buffer)))
      (save-excursion
        (let* ((sprocess (ess-get-process ess-current-process-name))
               (sbuffer (process-buffer sprocess))
               (buf-coding (symbol-name buffer-file-coding-system))
               (R-cmd
                (format "library(rmarkdown); rmarkdown::render(\"%s\")"
                        buffer-file-name)))
          (message "Running rmarkdown on %s" buffer-file-name)
          (ess-execute R-cmd 'buffer nil nil)
          (switch-to-buffer rmd-buf)
          (ess-show-buffer (buffer-name sbuffer) nil)))))

  ;; get the magrittr pipe with CTRL-shift-m
  (defun funk/then_R_operator ()
    "R - |> operator or 'then' pipe operator"
    (interactive)
    (just-one-space 1)
    (insert funk/ess-pipe-character)
    (reindent-then-newline-and-indent))

  ;; ;; FROM: https://github.com/walmes/emacs/blob/master/funcs.el#L381
  ;; ;;----------------------------------------------------------------------
  ;; ;; Functions related do Rmd files.

  ;; Goes to next chunk.
  (defun funk/polymode-next-chunk ()
    "Go to next chunk. This function is not general because is
   assumes all chunks are of R language."
    (interactive)
    (search-forward-regexp "^```{.*}$" nil t)
    (forward-line 1))

  ;; Goes to previous chunk.
  (defun funk/polymode-previous-chunk ()
    "Go to previous chunk. This function is not general because is
   assumes all chunks are of R language."
    (interactive)
    (search-backward-regexp "^```$" nil t)
    (search-backward-regexp "^```{.*}$" nil t)
    (forward-line 1))

  ;; Evals current R chunk.
  (defun funk/polymode-eval-R-chunk ()
    "Evals all code in R chunks in a polymode document (Rmd files)."
    (interactive)
    (if (derived-mode-p 'ess-mode)
        (let ((ptn (point))
              (beg (progn
                     (search-backward-regexp "^```{r.*}$" nil t)
                     (forward-line 1)
                     (line-beginning-position)))
              (end (progn
                     (search-forward-regexp "^```$" nil t)
                     (forward-line -1)
                     (line-end-position))))
          (ess-eval-region beg end nil)
          (goto-char ptn))
      (message "ess-mode weren't detected.")))

  ;; Evals R chunk and goes to next chunk.
  (defun funk/polymode-eval-R-chunk-and-next ()
    "Evals a R chunk and move point to next chunk."
    (interactive)
    (funk/polymode-eval-R-chunk)
    (funk/polymode-next-chunk))

  ;; Mark a word at a point.
  ;; http://www.emacswiki.org/emacs/ess-edit.el
  (defun ess-edit-word-at-point ()
    (save-excursion
      (buffer-substring
       (+ (point) (skip-chars-backward "a-zA-Z0-9._"))
       (+ (point) (skip-chars-forward "a-zA-Z0-9._")))))

  ;; Eval any word where the cursor is (objects, functions, etc).
  (defun funk/ess-eval-word ()
    (interactive)
    (let ((x (ess-edit-word-at-point)))
      (ess-eval-linewise (concat x))))

  (defun funk/ess-eval-misc-view ()
    (interactive)
    (let ((x (ess-edit-word-at-point)))
      (ess-eval-linewise (concat x " %>% misc::view_vd(title='.')"))))

  ;; folding rmd text and code-------------------------------------------------------
  ;; FROM: https://github.com/polymode/polymode/issues/128

  (defun funk/rmd-fold-region (beg end &optional msg)
    "Eval all spans within region defined by BEG and END.
MSG is a message to be passed to `polymode-eval-region-function';
defaults to \"Eval region\"."
    (interactive "r")
    (save-excursion
      (let* ((base (pm-base-buffer))
                                        ; (host-fun (buffer-local-value 'polymode-eval-region-function (pm-base-buffer)))
             (host-fun (buffer-local-value 'polymode-eval-region-function base))
             (msg (or msg "Eval region"))
             )
        (if host-fun
            (pm-map-over-spans
             (lambda (span)
               (when (eq (car span) 'body)
                 (with-current-buffer base
                   (ignore-errors (vimish-fold (max beg (nth 1 span)) (min end (nth 2 span)))))))
             beg end)
          (pm-map-over-spans
           (lambda (span)
             (when (eq (car span) 'body)
               (setq mapped t)
               (when polymode-eval-region-function
                 (setq evalled t)
                 (ignore-errors (vimish-fold
                                 (max beg (nth 1 span))
                                 (min end (nth 2 span))
                                 )))))
           beg end)
          ))))

  (defun funk/rmd-fold-codes ()
    "Eval all inner chunks in the buffer. "
    (interactive)
    (vimish-fold-delete-all)
    (funk/rmd-fold-region (point-min) (point-max) "Eval buffer")
    (polymode-previous-chunk 1)
    (next-line 2)
    )

  (defun re-seq (regexp string n)
    "Get a list of all regexp matches in a string"
    (save-match-data
      (let ((pos 0)
            matches)
        (while (string-match regexp string pos)
          (add-to-list 'matches (+ n (string-match regexp string pos)) t)
          (setq pos (match-end 0)))
        matches)))


  (defun funk/rmd-fold-text ()
    (interactive)
    (vimish-fold-delete-all)
    (let ((begs (re-seq "```{" (buffer-string) 0))
          (ends (re-seq "```$" (buffer-string) 0)))
      (add-to-list 'begs (point-max) t)
      (while ends  (progn
                     (ignore-errors (vimish-fold (nth 1 begs) (nth 0 ends)))
                     (pop begs)
                     (pop ends))))
    (beginning-of-buffer)
    (polymode-next-chunk 2))

  ;; https://emacs.stackexchange.com/questions/63308/clear-console-output-on-r-buffers-when-using-ess
  (defun funk/r-clear-buffer ()
    (interactive)
    (let ((r-repl-buffer (seq-find (lambda (buf)
                                     (string-prefix-p "*R" (buffer-name buf)))
                                   (buffer-list))))
      (if r-repl-buffer
          (with-current-buffer r-repl-buffer
            (comint-clear-buffer))
        (user-error "No R REPL buffers found"))))


  (defun funk/ess-quit-kill-inferior ()
    (interactive)
    (let* ((proj-name (projectile-project-name))
           (ess-current-proc (concat "*" ess-local-process-name))
           (proj-proc (concat ess-current-proc ":" proj-name "*"))
           (r-repl-buffer (seq-find (lambda (buf)
                                      (string-prefix-p ess-current-proc (buffer-name buf)))
                                    (buffer-list)))
           (r-repl-buffer-name (buffer-name r-repl-buffer)))
      (if (string-equal r-repl-buffer-name proj-proc)
          (with-current-buffer r-repl-buffer
            (message (format "Killing R REPL: %s" r-repl-buffer))
            (kill-buffer r-repl-buffer))
        (user-error "No R REPL process found for %s or this buffer is not member of the project %s" (buffer-name) r-repl-buffer-name))))

  (defun ess-abort ()
    (interactive)
    (interrupt-process (ess-get-process)))
  (define-key ess-mode-map (kbd "C-c C-a") 'ess-abort)
  (define-key inferior-ess-mode-map (kbd "C-c C-a") 'ess-abort)


  (define-key ess-mode-map (kbd "C-c l") #'funk/r-clear-buffer)

  ;; FROM: https://github.com/emacs-ess/ESS/issues/686#issuecomment-475331573
  (defun funk/ess-xref-find-etags ()
    (interactive)
    (let ((xref-backend-functions '(etags--xref-backend))
          (thing (xref--read-identifier "Find definitions of: ")))
      (xref--find-definitions (replace-regexp-in-string "^\\.\\|\\.$\\|^[^/]+/" "" thing) nil)))
  ;; meta get tag
  (define-key ess-mode-map (kbd "M-g M-t") 'funk/ess-xref-find-etags)

  (defun funk/ess-styler-region ()
    "Emacsy way of ~/.local/bin/tools/r/rstylerfile.
Adaptated from: https://emacs.stackexchange.com/a/50455/31478"
    (interactive)
    (let ((tempfile (make-temp-file nil nil ".R"))
          (reg-beg (region-beginning))
          (reg-end (region-end)))
      (if (region-active-p)
          (progn
            ;; save user selected region to a tempfile
            (write-region reg-beg reg-end tempfile)
            ;; Run the command synchronously (i.e., blocking Emacs) and
            ;; do not popup the *Shell Command Output* buffer
            (save-window-excursion
              (shell-command (format "Rscript -e 'grkstyle::grk_style_file(\"%s\")'" tempfile) "*styler R file output*" nil))
            ;; kill the user selected region and replace the original
            ;; text using the formated text in the tempfile
            (kill-region reg-beg reg-end)
            (insert-file-contents tempfile)
            (message "INFO from `funk/ess-styler': Check results in the *styler R file output* buffer."))
        (message "Error: Please, select a region first!"))))

  (defun funk/ess-indent-region-and-styler ()
    "Works like `vip=' in evil normal mode, but it is binded to `,I'"
    (interactive)
    (save-excursion
      (progn
        (call-interactively 'evil-visual-char)
        (call-interactively 'evil-inner-paragraph)
        (funk/ess-styler-region)
        (indent-region (region-beginning) (region-end))
        ))
    )

  (defun funk/get-ess-inferior-buffer ()
    (interactive)
    (display-buffer (ess-get-process-buffer)))

  (defun funk/ess-assign-and-indent-region ()
    (interactive)
    (progn
      (ignore-errors (ess-cycle-assign))
      (funk/indent-region)))

  ;; compilation!!!
  ;; NOTE 2022-07-28: There is a lot going on here. First, I want the
  ;; ability of running some R process in compilation mode (e.g. test,
  ;; and devtools). So there is a PR on the ESS repo about this, but the
  ;; PR is quite old (2018!). Then, I copy and paste some code here that
  ;; help me to develop some funcionality!
  ;; https://github.com/emacs-ess/ESS/issues/528
  ;; https://github.com/emacs-ess/ESS/pull/531

  (defun ess-r-eval-in-compile-buffer (expr)
    "Evalate R expression EXPR in a standalone `compilation-mode' buffer."
    (let* ((procname inferior-ess-r-program-name)
           (command (format "%s --slave --no-readline -e \"%s\"" procname expr)))
      (compilation-start command nil
                         (lambda (name-of-mode)
                           (concat "*" (downcase name-of-mode) "*"))
                         ess-r-error-regexp-alist)))

  (defun my-ess-rmarkdown-render ()
    (interactive)
    (let ((cmd (format "rmarkdown::render('%s')" buffer-file-name)))
      (ess-r-eval-in-compile-buffer cmd)))

  (defun funk/ess-devtools::test_active_file ()
    (interactive)
    (let ((cmd (format "devtools::test_active_file('%s')" buffer-file-name)))
      (ess-r-eval-in-compile-buffer cmd)))

  ;; NOTE 2022-07-28: So, some funcionality can be used via `transient' interface, why not!?
  ;; SEE: https://github.com/ShuguangSun/rutils.el

  ;; (require 'transient)
  ;; (transient-define-prefix rutils-devtools ()
  ;;   "R devtools menu."
  ;;   ;; ["Arguments"
  ;;   ;;  (rutils-renv:--reuse-project)]
  ;;   [["Dev"
  ;;     ("b" "Build" ess-r-devtools-build)
  ;;     ("c" "check" ess-r-devtools-check-package)
  ;;     ("d" "document" ess-r-devtools-document-package)
  ;;     ("l" "load" ess-r-devtools-load-package)
  ;;     ("t" "test" ess-r-devtools-test-package)
  ;;     ("u" "unload" ess-r-devtools-unload-package)]
  ;;    ["Install"
  ;;     ("i" "install-package" ess-r-devtools-install-package)
  ;;     ("I" "install-github" ess-r-devtools-install-github)]
  ;;    ["Create"
  ;;     ("C" "Create" ess-r-devtools-create-package)]
  ;;    ["Excute"
  ;;     ("A" "Ask excute-command:" ess-r-devtools-execute-command)
  ;;     ("E" "excute-command:" ignore)]
  ;;    ["Test"
  ;;     ("f" "Test file" funk/ess-devtools::test_active_file)
  ;;     ]
  ;;    ["Misc"
  ;;     ("W" "check-with-winbuilder" ess-r-devtools-check-with-winbuilder)]
  ;;    ])

  ;; REVIEW:
  ;; ** [[https://github.com/atheriel/r-pkgdev.el/blob/master/r-pkgdev.el][r-pkgdev.el/r-pkgdev.el at master · atheriel/r-pkgdev.el]]
  ;; ** [[https://github.com/ShuguangSun/rutils.el/blob/main/rutils-devtools.el][rutils.el/rutils-devtools.el at main · ShuguangSun/rutils.el]]

  ;; ADVICES

  (advice-add 'ess-eval-region-or-function-or-paragraph-and-step :before #'funk/try-expand-outline)

  (advice-add #'funk/ess-quit-kill-inferior :around #'auto-yes)

  (defun funk/ess-advice-before-eval-for-multiple-r-processes ()
    "Check if the current buffer is associated with the
`ess-local-process-name' and if it is a member of the project running
the `ess-local-process-name'. If the previous condition is nil, prompt
for switch process with `ess-switch-process'. This function avoids the
automatic association of a new buffer with the wrong
`ess-local-process-name'. Also, this function is intended to be used
in an `advice-add'."
    (interactive)
    (let* ((proj-name (projectile-project-name))
           (buf-name (buffer-name))
           (ess-current-proc (concat "*" ess-local-process-name))
           (proj-proc (concat ess-current-proc ":" proj-name "*"))
           (r-repl-buffer (seq-find (lambda (buf)
                                      (string-prefix-p ess-current-proc (buffer-name buf)))
                                    (buffer-list)))
           (r-repl-buffer-name (buffer-name r-repl-buffer)))
      (cond ((or (string-equal r-repl-buffer-name proj-proc) ess-local-process-name)
             (message "Ok project buffer and associated R process."))
            (t (if (yes-or-no-p (format "The buffer %s is not associated with any R process. Want choose one?" buf-name))
                   (ess-switch-process)
                 (error "This %s buffer is not associated with an R process. Try eval the expression again." buf-name))))))

  (dolist (f '(ess-eval-region-or-line-visibly-and-step
               funk/ess-eval
               funk/ess-eval-and-recenter))
    (advice-add f :before 'funk/ess-advice-before-eval-for-multiple-r-processes))

  )

;;; RMarkdown via polymode

(use-package poly-R
  :after (markdown ess))

(use-package poly-markdown
  :after (markdown ess))

(use-package polymode
  :after (markdown ess)
  :config
  (add-to-list 'auto-mode-alist '("\\.Rmd" . poly-markdown+r-mode))
  (add-to-list 'auto-mode-alist '("\\.rmd" . poly-markdown+r-mode))
  (define-key polymode-mode-map "\M-n s" 'funk/ess-rmarkdown))

(use-package quarto-mode
  :after (markdown ess)
  ;; https://github.com/quarto-dev/quarto-emacs
  :config
  (add-to-list 'auto-mode-alist '("\\.qmd" . poly-quarto-mode)))

;;; ess-view-data
(use-package ess-view-data
  :after ess

  :config
  (setq ess-view-data-read-string #'ivy-completing-read)
  (evil-define-key 'normal ess-view-data-mode-map
    (kbd "q") 'popper-kill-latest-popup)

  (general-define-key
   :keymaps '(ess-view-data-mode-map)
   :states '(normal)
   "C-j" #'ess-view-data-goto-next-page
   "C-k" #'ess-view-data-goto-previous-page
   "C-c v" #'ess-view-data-print))

;;; ess-r-insert-obj

(use-package ess-r-insert-obj
  :after ess

  :config
  (setq ess-r-insert-obj-read-string #'ivy-completing-read)
  (defun funk/ess-r-insert-obj-col-name ()
    (interactive)
    (setq current-prefix-arg '(16))
    (call-interactively 'ess-r-insert-obj-col-name))
  (general-define-key
   :keymaps '(ess-r-mode-map)
   :states '(insert)
   "C-S-l" #'funk/ess-r-insert-obj-col-name))



(provide 'init-rstats)
;;; init-rstats.el ends here

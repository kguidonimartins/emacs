;;; init-csv.el --- csv-mode configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; csv-mode
(use-package csv-mode
  ;; adapted from:
  ;; https://www.reddit.com/r/emacs/comments/k12lh4/configuring_csvmode_with_usepackage/
  :mode ("\\.tsv$" "\\.csv$")
  :config

  (defun funk/csv-highlight (&optional separator)
    (require 'cl)
    (require 'color)
    (interactive (list (when current-prefix-arg (read-char "Separator: "))))
    (font-lock-mode 1)
    (let* ((separator (or separator ?\,))
           (n (count-matches (string separator) (point-at-bol) (point-at-eol)))
           (colors (cl-loop for i from 0 to 1.0 by (/ 2.0 n)
                            collect (apply #'color-rgb-to-hex
                                           (color-hsl-to-rgb i 0.3 0.5)))))
      (cl-loop for i from 2 to n by 2
               for c in colors
               for r = (format "^\\([^%c\n]+%c\\)\\{%d\\}" separator separator i)
               do (font-lock-add-keywords nil `((,r (1 '(face (:foreground ,c)))))))))

  (defun funk/fix-csv-display ()
    (interactive)
    ;; (funk/csv-highlight)
    (visual-line-mode -1)
    (toggle-truncate-lines +1)
    (csv-align-mode t)
    (csv-header-line t)
    (if (derived-mode-p 'csv-mode)
        (hl-line-mode +1)))

  (defun funk/csv-forward-field()
    (interactive)
    (call-interactively 'csv-forward-field)
    (call-interactively 'evil-forward-char))

  (defun funk/ensure-hl-mode-for-csv-mode ()
    (if (derived-mode-p 'csv-mode)
        (hl-line-mode +1)))

  (setq csv-comment-start "##")

  (add-hook 'csv-mode-hook #'(lambda ()
                               (progn
                                 (beginning-of-buffer)
                                 (beginning-of-line)
                                 ;; (funk/csv-mode-faces)
                                 (run-at-time 1 nil 'funk/fix-csv-display)
                                 (evil-define-key '(normal visual) csv-mode-map
                                   (kbd "w") 'funk/csv-forward-field
                                   (kbd "b") 'csv-backward-field
                                   )
                                 (run-with-idle-timer 1 t 'funk/ensure-hl-mode-for-csv-mode)
                                 (setq-local evil-move-cursor-back t)
                                 )
                               ))
  (add-hook 'csv-align-mode-hook #'(lambda () (hl-line-mode +1))))


(provide 'init-csv)
;;; init-csv.el ends here

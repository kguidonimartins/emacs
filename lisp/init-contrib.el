;;; init-contrib.el --- Code based on contributions -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(defun contrib/run-async-shell-command-and-kill-buffer-after-n-seconds (cmd n)
  "Create a new buffer, run the CMD in an async-shell-command and kill
the async output buffer after N seconds. CMD needs to be a string indicating
the script in your $PATH to be run. The CMD string will be used to generate
the buffer name.
FROM:
https://stackoverflow.com/a/6918728/5974372.
An alternative using `shell-command-sentinel' instead a timer:
https://stackoverflow.com/a/34859026/5974372."
  (interactive)
  (save-window-excursion
    (let* ((buf-name (format "*async-shell %s*" cmd))
           (buf (generate-new-buffer buf-name)))
      (async-shell-command cmd buf)
      (run-with-timer n nil (lambda (buf)
                              (kill-buffer buf)
                              (message (format "Killing %s buffer." buf-name))) buf))))

(defun contrib/async-shell-command-no-window (command)
  "Run `async-shell-command' but suppress the popup window only
for the current command.
FROM: https://stackoverflow.com/a/47910509/5974372"
  (interactive)
  (let ((display-buffer-alist (list (cons "\\*Async Shell Command\\*.*" (cons #'display-buffer-no-window nil)))))
    (async-shell-command command)))

(defun funk/format-function-parameters ()
  "Turn the list of function parameters into multiline.
Works print well for tidyverse function as mutate and select, for example:

Turn this:
#+begin_src R
  mtcars |>
    select(mpg, cyl, am)
#+end_src

Into this:
#+begin_src R
  mtcars |>
    select(
           mpg,
           cyl,
           am
    )
#+end_src

FROM: https://github.com/abrochard/emacs-config/blob/master/configuration.org#format-long-function-parameter-list-into-multiline=
"
  (interactive)
  (beginning-of-line)
  (search-forward "(" (line-end-position))
  (newline-and-indent)
  (while (search-forward "," (line-end-position) t)
    (newline-and-indent))
  (end-of-line)
  (c-hungry-delete-forward)
  (insert " ")
  (search-backward ")")
  (newline-and-indent))

(defun funk/occur-non-ascii ()
  "Find any non-ascii characters in the current buffer.
FROM: https://www.emacswiki.org/emacs/FindingNonAsciiCharacters
"
  (interactive)
  (occur "[^[:ascii:]]"))


(provide 'init-contrib)
;;; init-contrib.el ends here

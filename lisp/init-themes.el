;;; init-themes.el --- emacs themes configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; https://stackoverflow.com/a/15595000/5974372

(get-theme-files "themes")

(defadvice load-theme
    (before theme-dont-propagate activate)
  (mapc #'disable-theme custom-enabled-themes))

;;; themes

(use-package catppuccin-theme)

(use-package doom-themes)

(use-package zeno-theme)

(use-package almost-mono-themes)

(use-package timu-spacegrey-theme
  :init
  (setq timu-spacegrey-flavour "dark"))

(use-package night-owl-theme)

(use-package solarized-theme)

(use-package abyss-theme)

(use-package timu-rouge-theme)

(use-package spacemacs-theme
  :config
  (custom-theme-set-faces
   'spacemacs-dark
   '(outline-1 ((t (:height 1.100 :weight bold))))
   '(outline-2 ((t (:height 1.075 :weight bold))))
   '(outline-3 ((t (:height 1.050 :weight bold))))
   '(outline-4 ((t (:height 1.025 :weight bold))))
   '(outline-5 ((t (:height 1.000 :weight bold))))
   '(outline-6 ((t (:height 1.000 :weight bold))))
   '(outline-7 ((t (:height 1.000 :weight bold))))
   '(outline-8 ((t (:height 1.000 :weight bold))))
   )
  (custom-theme-set-faces
   'spacemacs-light
   '(outline-1 ((t (:height 1.100 :weight bold))))
   '(outline-2 ((t (:height 1.075 :weight bold))))
   '(outline-3 ((t (:height 1.050 :weight bold))))
   '(outline-4 ((t (:height 1.025 :weight bold))))
   '(outline-5 ((t (:height 1.000 :weight bold))))
   '(outline-6 ((t (:height 1.000 :weight bold))))
   '(outline-7 ((t (:height 1.000 :weight bold))))
   '(outline-8 ((t (:height 1.000 :weight bold))))
   )
  )

(use-package afternoon-theme)

(use-package light-soap-theme)

(use-package nofrils-acme-theme)

(use-package ubuntu-theme)

(use-package  underwater-theme)

(use-package color-theme-sanityinc-tomorrow)

;;; hl-todo
(use-package hl-todo
  :hook ((prog-mode . hl-todo-mode)
         (emacs-lisp-mode . hl-todo-mode)
         (ess-mode . hl-todo-mode)
         (markdown-mode . hl-todo-mode)))

(defun funk/set-hl-todo ()
  "Set main colors for hl-todo keywords."
  (interactive)

  (defface hl-todo-TODO
    '((t :background "#ff0000" :foreground "#00ff00" :inherit (hl-todo)))
    "Face for highlighting the TODO keyword.")

  (defface hl-note-TODO
    '((t :background "#6666ff" :foreground "#FFFFFF" :inherit (hl-todo)))
    "Face for highlighting the NOTE keyword.")

  (defface hl-DONE-TODO
    '((t :foreground "orange" :weight ultra-bold :background unspecified))
    "Face for highlighting the DONE keyword.")

  ;; TODOOOO 2022-07-16: Testing urgency todos
  (setq hl-todo-highlight-punctuation ":"
        hl-todo-keyword-faces
        `(
          ("UPDATE"     hl-todo-TODO)
          ("TODO"       hl-todo-TODO)
          ("TODOO"       hl-todo-TODO)
          ("TODOOO"       hl-todo-TODO)
          ("TODOOOO"       hl-todo-TODO)
          ("TODOOOOO"       hl-todo-TODO)
          ("FIXME"      error bold)
          ("FIXMEE"      error bold)
          ("FIXMEEE"      error bold)
          ("FIXMEEEE"      error bold)
          ("FIXMEEEEE"      error bold)
          ("CANCELLED" error bold)
          ("HACK"       error bold)
          ("IDEA"       font-lock-doc-face bold)
          ("SEE"        font-lock-constant-face bold)
          ("REVIEW"     font-lock-doc-face bold)
          ("NOTE"       hl-note-TODO)
          ("CHECK"      font-lock-doc-face bold)
          ("REFS"       font-lock-constant-face bold)
          ("DUMP"       font-lock-doc-face bold)
          ("COMMENT"    font-lock-doc-face bold)
          ("DEPRECATED" font-lock-doc-face bold)
          ("DONE"       hl-DONE-TODO)
          ("TEMP"       font-lock-constant-face bold)
          )))

;;; funk/theme-dark
(defun funk/theme-dark ()
  (interactive)
  (mapc #'disable-theme custom-enabled-themes)
  (load-theme 'funknight t)
  ;; midnight colors for pdf-view
  (setq pdf-view-midnight-colors '("#f8f8f2" . "#132738"))
  (add-hook 'pdf-tools-enabled-hook 'pdf-view-midnight-minor-mode)
  ;; set hl-todo-colors
  (funk/set-hl-todo)
  (setq company-quickhelp-color-background "#132738")
  (setq company-quickhelp-color-foreground "#FFFFFF")
  (setq hl-block-bracket-face '(t (:background "#5317ac" :foreground "#FFFFFF" :weight ultra-bold)))

  (defun funk/csv-mode-faces ()
    (face-remap-add-relative 'hl-line '(:background "#343746"))
    (face-remap-add-relative 'header-line '(:background "#343746")))
  )

;;; funk/theme-light
(defun funk/theme-light ()
  (interactive)
  (mapc #'disable-theme custom-enabled-themes)
  (load-theme 'funkday t)
  ;; ;; inverted midnight colors for pdf-view
  ;; (setq pdf-view-midnight-colors '("#000000" . "#FFFFFF"))
  ;; (add-hook 'pdf-tools-enabled-hook 'pdf-view-midnight-minor-mode)
  (setq hl-block-bracket-face '(t (:background "#f0f0f0" :foreground "#5317ac" :weight ultra-bold)))
  ;; set hl-todo-colors
  (funk/set-hl-todo)

  (defun funk/csv-mode-faces ()
    (face-remap-add-relative 'hl-line '(:background "#c1c1ff"))
    (face-remap-add-relative 'header-line '(:background "#c1c1ff")))
  )

;;; funk/theme-cream
(defun funk/theme-cream ()
  (interactive)
  (mapc #'disable-theme custom-enabled-themes)
  (load-theme 'solarized-light t)
  (custom-set-faces
   '(default ((t (:background "#fafad4"))))
   '(fringe ((t (:background "#fafad4"))))
   '(outline-1 ((t (:height 1.1   :weight bold :inherit unspecified))))
   '(outline-2 ((t (:height 1.075 :weight bold :inherit unspecified))))
   '(outline-3 ((t (:height 1.050 :weight bold :inherit unspecified))))
   '(outline-4 ((t (:height 1.025 :weight bold :inherit unspecified))))
   )
  )

;;;###autoload
(defun funk/mono-syntax()
  "Disable `git-gutter-mode' and `hl-todo-mode' in the current buffer."
  (interactive)
  (git-gutter+-mode -1)
  (hl-todo-mode -1))

;;; decide which theme use based on time
(let ((now (nth 2 (parse-time-string (current-time-string))))
      (down (string-to-number (format-time-string "16:30:00 %H"))))
  (if (< now down) (funk/theme-light) (funk/theme-dark)))
(run-at-time "16:30" nil #'funk/theme-dark)

;;; choose my preferred themes

(defun funk/select-themes ()
  "Select my preferred theme."
  (interactive)
  ;; (let ((theme (completing-read "Choose your theme: "
  ;;                               '(
  ;;                                 afternoon
  ;;                                 almost-mono-black
  ;;                                 almost-mono-cream
  ;;                                 almost-mono-gray
  ;;                                 almost-mono-white
  ;;                                 doom-dracula
  ;;                                 doom-palenight
  ;;                                 doom-tokyo-night
  ;;                                 funkday
  ;;                                 funknight
  ;;                                 modus-operandi
  ;;                                 modus-vivendi
  ;;                                 night-owl
  ;;                                 solarized-dark
  ;;                                 solarized-light
  ;;                                 spacemacs-dark
  ;;                                 spacemacs-light
  ;;                                 timu-spacegrey
  ;;                                 zeno
  ;;                                 ))))
  ;;   (load-theme (intern theme) t))

  (setq theme-table (make-hash-table :test 'equal))

  (setq theme-list '((" dark: afternoon"                   . afternoon)
                     (" dark: almost-mono-black"           . almost-mono-black)
                     (" dark: almost-mono-gray"            . almost-mono-gray)
                     (" dark: doom-dracula"                . doom-dracula)
                     (" dark: doom-palenight"              . doom-palenight)
                     (" dark: doom-tokyo-night"            . doom-tokyo-night)
                     (" dark: funknight"                   . funknight)
                     (" dark: funkmidnight"                . funkmidnight)
                     (" dark: modus-vivendi"               . modus-vivendi)
                     (" dark: night-owl"                   . night-owl)
                     (" dark: solarized-dark"              . solarized-dark)
                     (" dark: spacemacs-dark"              . spacemacs-dark)
                     (" dark: timu-spacegrey"              . timu-spacegrey)
                     (" dark: zeno"                        . zeno)
                     (" dark: doom-one"                    . doom-one)
                     (" dark: doom-gruvbox"                . doom-gruvbox)
                     (" dark: sanityinc-tomorrow-bright"   . sanityinc-tomorrow-bright)
                     (" dark: sanityinc-tomorrow-night"    . sanityinc-tomorrow-night)
                     (" dark: sanityinc-tomorrow-blue"     . sanityinc-tomorrow-blue)
                     (" dark: sanityinc-tomorrow-eighties" . sanityinc-tomorrow-eighties)
                     (" dark: timu-rouge"                  . timu-rouge)
                     (" dark: catppuccin"                  . catppuccin)
                     ("light: sanityinc-tomorrow-day"      . sanityinc-tomorrow-day)
                     ("light: almost-mono-cream"           . almost-mono-cream)
                     ("light: doom-gruvbox-light"          . doom-gruvbox-light)
                     ("light: light-soap"                  . light-soap)
                     ("light: nofrils-acme"                . nofrils-acme)
                     ("light: almost-mono-white"           . almost-mono-white)
                     ("light: funkday"                     . funkday)
                     ("light: modus-operandi"              . modus-operandi)
                     ("light: solarized-light"             . solarized-light)
                     ("light: spacemacs-light"             . spacemacs-light)))

  (cl-loop for theme_ in theme-list
           do (puthash (car theme_) (cdr theme_) theme-table))

  (let* ((table-key (completing-read "Choose your theme: " theme-table))
         (theme (gethash table-key theme-table)))
    (progn
      (load-theme theme t)
      (custom-set-faces
       '(evil-goggles-yank-face ((t (:weight ultra-bold))))
       '(dired-subtree-depth-1-face ((t (:background unspecified))))
       '(dired-subtree-depth-2-face ((t (:background unspecified))))
       '(dired-subtree-depth-3-face ((t (:background unspecified))))
       '(dired-subtree-depth-4-face ((t (:background unspecified))))
       '(dired-subtree-depth-5-face ((t (:background unspecified))))
       '(dired-subtree-depth-6-face ((t (:background unspecified))))
       '(ansi-color-blue ((t (:foreground  "#FFFFFF" :background  unspecified))))
       '(ansi-color-bold ((t (:weight bold :inherit default))))
       '(git-gutter+-modified ((t (:background unspecified))))
       '(git-gutter:modified ((t (:background unspecified))))
       '(git-gutter+-added ((t (:background unspecified))))
       '(git-gutter:added ((t (:background unspecified))))
       '(git-gutter+-deleted ((t (:background unspecified))))
       '(git-gutter:deleted ((t (:background unspecified))))
       '(git-gutter:added ((t (:foreground  "#62c86a" :background unspecified))))
       '(git-gutter:deleted ((t (:foreground  "#f08290" :background unspecified))))
       '(git-gutter:modified ((t (:foreground  "#dbba3f" :background unspecified))))
       '(diff-hl-change          ((t (:background unspecified))))
       '(diff-hl-delete          ((t (:background unspecified))))
       '(diff-hl-margin-change   ((t (:background unspecified))))
       '(diff-hl-margin-delete   ((t (:background unspecified))))
       '(diff-hl-margin-ignored  ((t (:background unspecified))))
       '(diff-hl-margin-insert   ((t (:background unspecified))))
       '(diff-hl-margin-unknown  ((t (:background unspecified))))
       '(diff-hl-margin-change   ((t (:background unspecified))))
       '(diff-hl-margin-delete   ((t (:background unspecified))))
       '(diff-hl-margin-ignored  ((t (:background unspecified))))
       '(diff-hl-margin-insert   ((t (:background unspecified))))
       '(diff-hl-margin-unknown  ((t (:background unspecified))))
       '(outline-1 ((t (:height 1.100 :weight bold))))
       '(outline-2 ((t (:height 1.075 :weight bold))))
       '(outline-3 ((t (:height 1.050 :weight bold))))
       '(outline-4 ((t (:height 1.025 :weight bold))))
       '(outline-5 ((t (:height 1.000 :weight bold))))
       '(outline-6 ((t (:height 1.000 :weight bold))))
       '(outline-7 ((t (:height 1.000 :weight bold))))
       '(outline-8 ((t (:height 1.000 :weight bold))))
       '(whitespace-big-indent             ((t (:background unspecified))))
       '(whitespace-empty                  ((t (:background unspecified))))
       '(whitespace-hspace                 ((t (:background unspecified))))
       '(whitespace-indentation            ((t (:background unspecified))))
       '(whitespace-line                   ((t (:background unspecified))))
       '(whitespace-missing-newline-at-eof ((t (:background unspecified))))
       '(whitespace-newline                ((t (:background unspecified))))
       '(whitespace-space                  ((t (:background unspecified))))
       '(whitespace-space-after-tab        ((t (:background unspecified))))
       '(whitespace-space-before-tab       ((t (:background unspecified))))
       '(whitespace-tab                    ((t (:background unspecified))))
       '(whitespace-trailing               ((t (:background unspecified))))
       '(ivy-org ((t (:height 1.00))))
       '(ivy-current-match ((t (:weight bold))))
       '(ivy-minibuffer-match-face-1 ((t (:weight bold))))
       '(ivy-minibuffer-match-face-2 ((t (:weight bold))))
       '(ivy-minibuffer-match-face-3 ((t (:weight bold))))
       '(ivy-minibuffer-match-face-4 ((t (:weight bold))))
       '(ivy-match-required-face ((t (:weight bold))))
       '(ess-function-call-face ((t (:weight bold :inherit font-lock-function-name-face))))
       '(ess-keyword-face ((t (:inherit ess-function-call-face))))
       '(ess-modifiers-face ((t (:inherit ess-function-call-face))))
       )
      )
    ))
(global-set-key (kbd "<f6>") 'funk/select-themes)


;;; describe faces unreachable by the cursor
;; based on: https://emacs.stackexchange.com/a/19585/13444
;; based on: https://emacs.stackexchange.com/a/35449/31478
(defun funk/describe-char-at-mouse-click (click-event)
  "`describe-char' at CLICK-EVENT's position.
CLICK-EVENT should be a mouse-click event."
  (interactive "e")
  (run-hooks 'mouse-leave-buffer-hook)
  (let ((pos (cadr (event-start click-event))))
    (describe-char pos)))

;; <d>escribe
(global-set-key (kbd "C-c d <down-mouse-1>")
                #'funk/describe-char-at-mouse-click)

(provide 'init-themes)
;;; init-themes.el ends here

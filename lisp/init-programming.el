;;; -*- lexical-binding: t; -*-

;; highlight error in compilation
(setq next-error-message-highlight t)

;; Check (on save) whether the file edited contains a shebang, if yes,
;; make it executable from
;; http://mbork.pl/2015-01-10_A_few_random_Emacs_tips
(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

(setq require-final-newline nil)

;;; editorconfig
(use-package editorconfig
  
  :config
  (editorconfig-mode 1))

;;; yaml-mode

(use-package yaml-pro

  :straight (:host github :repo "zkry/yaml-pro" :files ("*.el"))
  :config
  (add-hook 'yaml-mode-hook #'yaml-pro-mode))

;;; debug
;; (use-package dap-mode
;;   ;; Uncomment the config below if you want all UI panes to be hidden by default!
;;   ;; :custom
;;   ;; (lsp-enable-dap-auto-configure nil)
;;   ;; :config
;;   ;; (dap-ui-mode 1)
;;   :config
;;   ;; Set up Node debugging
;;   (require 'dap-node)
;;   (dap-node-setup) ;; Automatically installs Node debug adapter if needed
;;   ;; Bind `C-c l d` to `dap-hydra` for easy access
;;   (general-define-key
;;    :keymaps 'lsp-mode-map
;;    :prefix lsp-keymap-prefix
;;    "d" '(dap-hydra t :wk "debugger")))

;;; wakatime-mode
(use-package wakatime-mode
  
  ;; :ensure-system-package
  ;; ((wakatime . "yay -S --noconfirm wakatime-cli-bin"))
  :diminish wakatime-mode
  :if (executable-find "wakatime")
  :init
  (setq wakatime-api-key (funk-auth 'wakatimekey))
  (setq wakatime-cli-path (executable-find "wakatime"))
  :config
  (global-wakatime-mode +1)
  ;; global-wakatime-mode breaks recovering autosaves for some reason
  (advice-add 'recover-this-file :around
              (lambda (oldfn &rest args)
                (let ((wakatime-was-enabled global-wakatime-mode))
                  (when wakatime-was-enabled
                    (global-wakatime-mode -1))
                  (apply oldfn args)
                  (when wakatime-was-enabled
                    (global-wakatime-mode))))))

;;; copy-as-format

;; (use-package copy-as-format
;;   :init
;;   (setq copy-as-format-default "github"))

;;; xref
(use-package xref
  
  :config
  (add-to-list 'xref-prompt-for-identifier #'xref-find-references 'append))



(use-package counsel-etags
  :disabled
  :after counsel)


;; (use-package eldoc-box
;;   :hook ((text-mode prog-mode ess-r-mode) . eldoc-box-hover-mode))


;;; dumb-jump

;;   :config
;;   (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))

;; ;;; expand-region
;; (use-package expand-region
;;   :disabled
;;   :bind ("C-+" . er/expand-region))

;;; citre (ctags)
;; (use-package citre
;;   :config
;;   (require 'citre)
;;   (require 'citre-config)
;;   (setq
;;    citre-use-project-root-when-creating-tags t
;;    citre-prompt-language-for-ctags-command t
;;    citre-auto-enable-citre-mode-modes '(prog-mode ess-mode)))

;;; tree-sitter

;; (use-package tree-sitter
;;   :ensure tree-sitter-langs
;;   :diminish
;;   :hook ((after-init . global-tree-sitter-mode)
;;          (tree-sitter-after-on . tree-sitter-hl-mode)))

;;; misc
(advice-add 'compile-goto-error :after #'funk/try-expand-outline)

;;; activity-watch-mode

;; (use-package activity-watch-mode
;;   :after magit
;;   :config
;;   (global-activity-watch-mode))

;;; abbrev-mode
(use-package abbrev-mode
  
  :straight (:type built-in)
  :init
  (setq-default abbrev-mode t)
  ;; a hook funtion that sets the abbrev-table to org-mode-abbrev-table
  ;; whenever the major mode is a text mode
  (defun funk/set-text-mode-abbrev-table ()
    (if (derived-mode-p 'text-mode)
        (setq local-abbrev-table org-mode-abbrev-table)))
  (defun funk/set-ess-mode-abbrev-table ()
    (if (derived-mode-p 'ess-mode)
        (setq local-abbrev-table global-abbrev-table)))
  :commands abbrev-mode
  :hook
  (abbrev-mode . funk/set-text-mode-abbrev-table)
  (abbrev-mode . funk/set-ess-mode-abbrev-table)
  :config
  (setq abbrev-file-name "~/.emacs.d.vanilla/etc/abbrev.el")
  (read-abbrev-file "~/.emacs.d.vanilla/etc/abbrev.el")
  (setq save-abbrevs 'silent))

;;; zeal-at-point
(use-package zeal-at-point
  :disabled
  :config
  ;; Use multiple docsets
  (add-to-list 'zeal-at-point-mode-alist '(python-mode . ("python" "django" "pandas" "numpy"))))

;;; calculation on region

;; FROM: https://www.reddit.com/r/emacs/comments/uywjyx/comment/iaiw3pf/?utm_source=share&utm_medium=web2x&context=3
(defun funk/calc-eval-region (beg end)
  "Calculate the region and insert it into the current buffer."
  (interactive (if (use-region-p)
                   (list (region-beginning) (region-end))
                 (list (point-at-bol) (point-at-eol))))
  (let* ((expr (buffer-substring-no-properties beg end))
         (result (calc-eval expr))
         (my-beg beg)
         (my-end end) map)
    (if (not (listp result))
        (insert-result beg end result))))

(defun insert-result (beg end result)
  "Insert the result into the current buffer.

There are two ways to do that:
Either by replacing the term (which is defined by beg-end)
or by inserting after it."
  (let ((choice (nth 1 (read-multiple-choice
                        (format "What should be done with the result (%s)?" result)
                        '((?i "insert" "insert after the term")
                          (?r "replace" "replace the term"))))))
    (pcase choice
      ("insert" (progn
                  (goto-char end)
                  (insert " = " result)))
      ("replace" (progn
                   (kill-region beg end)
                   (goto-char beg)
                   (insert result))))))

;; https://github.com/Atreyagaurav/units-mode

;;; compilation

;; You can use `compile-command' on the file header to remember which command to use :)
;; For example, in my dwm config.h file, I have:
;; // -*- compile-command: "sudo make install" -*-
;; Then, when I run, M-x `compile', the correct command will run on the compilation buffer.
;; But, sometimes, these commands needs the user password and the ordinary compilation buffer is read-only.
;; To solve this, you can run C-u M-x compile. Or
(defun funk/compile ()
  (interactive)
  (setq current-prefix-arg '(4))
  (call-interactively 'compile))

(use-package ansi-color
  
  ;; FROM: https://stackoverflow.com/questions/13397737/ansi-coloring-in-compilation-mode
  :config
  (defun my-colorize-compilation-buffer ()
    (when (eq major-mode 'compilation-mode)
      (ansi-color-apply-on-region compilation-filter-start (point-max))))
  :hook
  (compilation-filter . ansi-color-compilation-filter)
  (compilation-filter . my-colorize-compilation-buffer))

;; Turn on auto-revert-tail-mode for log files
(add-to-list 'auto-mode-alist '("\\.log\\'" . auto-revert-tail-mode))
;; Compilation output
;; https://stackoverflow.com/questions/4657142/how-do-i-encourage-emacs-to-follow-the-compilation-buffer
(setq compilation-scroll-output t)
;; https://www.reddit.com/r/emacs/comments/c7j59/til_about_autorevertmode_and_autoreverttailmode/

;;; time-stamp

(use-package time-stamp
  
  ;; enabling the time-stamp feature in files with
  ;; a time-stamp place holder.
  ;; FROM: https://www.emacswiki.org/emacs/TimeStamp
  ;;       https://org-roam.discourse.group/t/update-a-field-last-modified-at-save/321/27
  :straight (:type built-in)
  :config
  (defun contrib/time-stamp-group-undo ()
    "Exclude a `time-stamp' modification from the `buffer-undo-list'.
This workaround is necessary because the `time-stamp' execution counts
as a modification on the buffer (changing the `buffer-undo-list'!). Thus,
right before a `time-stamp' execution, a nil entry is inserted in
the `buffer-undo-list'.
This is the scenario:
https://emacs.stackexchange.com/q/4217/31478
And this is the solution:
https://emacs.stackexchange.com/a/4235/31478."
    (if (eq nil (car buffer-undo-list))
        (setq buffer-undo-list (cdr buffer-undo-list)))
    (time-stamp))
  ;; NOTE 2022-07-01: `before-save-hook' is a good alternative here.
  (add-hook 'before-save-hook 'contrib/time-stamp-group-undo)
  )

;;; whitespace-mode

(use-package whitespace-mode
  :straight (:type built-in)
  :hook ((prog-mode ess-mode ess-r-mode emacs-lisp-mode markdown-mode) . whitespace-mode)
  :init
  ;; (global-whitespace-mode)
  ;; Display whitespace characters globally
  (setq whitespace-line-column 120)

  ;; Customize Whitespace Characters
  ;;  - Newline: \u00AC = ¬
  ;;  - Tab:     \u2192 = →
  ;;             \u00BB = »
  ;;             \u25B6 = ▶
  (setq-default whitespace-display-mappings
                (quote ((newline-mark ?\n [?\u00AC ?\n] [?\u00AC ?\n])
                        (tab-mark     ?\t [?\u25B6 ?\t] [?\u00BB ?\t] [?\\ ?\t]))))

  (setq-default whitespace-style
                (quote (face tabs trailing space-before-tab newline
                             indentation space-after-tab tab-mark newline-mark
                             empty)))

  ;; ;; Remove trailing white spaces
  ;; ;; NOTE 2022-06-23: stop being the "white space" police.
  ;; (add-hook 'before-save-hook 'whitespace-cleanup)

  )

;;; ws-butler

(use-package ws-butler
  ;; NOTE 2022-06-23: stop being the "white space" police.
  ;; NOTE 2022-06-23: an unobtrusive way to trim spaces from end of line.
  ;; FROM: https://github.com/lewang/ws-butler#history
  ;; QUOTE: > I started by trimming all spaces at EOL in source code in
  ;;          a "write-file-hook" when I started programming. It seemed like a
  ;;          great idea. Then I got a job working on a code base where no one
  ;;          else trimmed spaces, so my commits became super noisy...
  :config
  (ws-butler-global-mode +1))

;;; keyfrep
(use-package keyfreq
  :disabled
  ;; register the frequency of commands.
  :config
  (keyfreq-mode 1)
  (keyfreq-autosave-mode 1))

;;; highlight-symbol

(use-package highlight-symbol
  
  :config
  (set-face-attribute 'highlight-symbol-face nil
                      :underline t)
  (setq highlight-symbol-idle-delay 0)
  (setq highlight-symbol-on-navigation-p t)
  ;; NOTE 2022-08-04: This is useful when exploring new code bases.
  ;; (add-hook 'prog-mode-hook #'highlight-symbol-mode)
  ;; NOTE 2022-08-01: Defines `M-n' and `M-p' to navigate between the
  ;; highlighted symbols.
  (add-hook 'prog-mode-hook #'highlight-symbol-nav-mode))

(provide 'init-programming)
;;; init-programming.el ends here

;;; init-backup.el --- backup settings -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; backups

(use-package emacs
  :straight (:type built-in)
  :hook ((ess-r-mode text-mode prog-mode ess-mode emacs-lisp-mode markdown-mode org-mode) . auto-save-mode)
  :config
  ;; nice backup setup:
  ;; https://stackoverflow.com/questions/151945/how-do-i-control-how-emacs-makes-backup-files
  (setq funk/backup-dir
        (let ((dir (concat user-emacs-directory
                           "var/backup/")))
          (make-directory dir :parents)
          dir))

  (setq funk/backup-dir-per-save
        (let ((dir (concat user-emacs-directory
                           "var/backup/per-save/")))
          (make-directory dir :parents)
          dir))

  (setq backup-directory-alist `((".*" . ,funk/backup-dir)))

  (setq make-backup-files t
        backup-by-copying t
        version-control t
        kept-new-versions 10
        kept-old-versions 2
        delete-old-versions t
        backup-by-copying t
        auto-save-default t
        auto-save-timeout 5
        auto-save-interval 200)

  ;; also backup versioned files
  (setq vc-make-backup-files t)

  ;; Default and per-save backups go here:
  (setq backup-directory-alist `(("" . ,funk/backup-dir-per-save)))

  (defun force-backup-of-buffer ()
    (setq funk/backup-dir-per-session
          (let ((dir (concat user-emacs-directory
                             "var/backup/per-session/")))
            (make-directory dir :parents)
            dir))
    ;; Make a special "per session" backup at the first save of each
    ;; emacs session.
    (when (not buffer-backed-up)
      ;; Override the default parameters for per-session backups.
      (let ((backup-directory-alist `(("" . ,funk/backup-dir-per-session)))
            (kept-new-versions 3))
        (backup-buffer)))
    ;; Make a "per save" backup on each save.  The first save results in
    ;; both a per-session and a per-save backup, to keep the numbering
    ;; of per-save backups consistent.
    (let ((buffer-backed-up nil))
      (backup-buffer)))

  (add-hook 'before-save-hook  'force-backup-of-buffer)
  (auto-save-mode +1))

;;; real-auto-save

(use-package real-auto-save
  :config
  (setq real-auto-save-interval 2)
  (add-hook 'org-mode-hook 'real-auto-save-mode)
  (add-hook 'emacs-lisp-mode-hook 'real-auto-save-mode)
  (add-hook 'prog-mode-hook 'real-auto-save-mode)
  (add-hook 'markdown-mode-hook 'real-auto-save-mode)
  (add-hook 'text-mode-hook 'real-auto-save-mode)
  (add-hook 'ess-mode-hook 'real-auto-save-mode))

(provide 'init-backup)
;;; init-backup.el ends here

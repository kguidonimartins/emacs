;;; company-quarto.el --- Company backend for quarto (.qmd) documents -*- lexical-binding: t -*-

;; Author: Karlo Guidoni Martins
;; Maintainer: Karlo Guidoni Martins
;; Version: version
;; Package-Requires: (cl-lib company quarto-emacs)
;; Homepage: homepage
;; Keywords: keywords

;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; company-quarto provides backend for completion of chunk options (or
;; lua filters) on quarto documents (.qmd files).  For now, its only
;; provides completion for built-in chunk-options (see:
;; https://quarto.org/docs/extensions/filters.html#filter-extensions).

;;; Code:

(require 'cl-lib)
(require 'company)
(require 'quarto-emacs)

;; source the completion data
(require 'company-quarto-data)

(defun quarto-mode--is-start-of-block-p (para)
  "Return non-nil if PARA is the start of a pandoc block."
  (string-match "^```{*?" para))

(defun quarto-mode--is-end-of-block-p (para)
  "Return non-nil if PARA is the end of a pandoc block."
  (string-match "```$" para))

(defun quarto-mode--is-block-p ()
  "Return non-nil if thing at point is a quarto block."
  (let ((this-para (thing-at-point 'paragraph t)))
    (or (quarto-mode--is-start-of-block-p this-para)
        (quarto-mode--is-end-of-block-p this-para))))

(defconst company-quarto-regex-comment-chunk
  "^\\#\\|"
  "Grab code comment.")

(defun company-quarto-annotation (s)
  (format " [%s]" (get-text-property 0 :initials s)))

(defun company-quarto-doc (s)
  (get-text-property 0 :doc s))

(defun company-quarto-fuzzy-match (prefix candidate )
  (cl-subsetp (string-to-list prefix)
              (string-to-list candidate)))

(defun company-quarto-backend (command &optional arg &rest ignored)
  (interactive (list 'interactive))

  (cl-case command
    (interactive (company-begin-backend 'company-quarto-backend))
    (prefix (company-grab-symbol))
    (candidates
     (remove-if-not
      (lambda (c) (company-quarto-fuzzy-match arg c))
      company-quarto-completions))
    (doc-buffer (company-doc-buffer (company-quarto-doc arg)))
    (annotation (company-quarto-annotation arg))
    (kind 'keyword)
    (no-cache 't)))

(provide 'company-quarto)
;;; company-quarto.el ends here

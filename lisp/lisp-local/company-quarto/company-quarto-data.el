(defconst company-quarto-completions
  '(
        #("child" 0 1
        (:initials
        "chunk-option: include"
        :doc
        "One or more paths of child documents to be knitted and input into the main document."))
        #("file" 0 1
        (:initials
        "chunk-option: include"
        :doc
        "File containing code to execute for this chunk"))
        #("code" 0 1
        (:initials
        "chunk-option: include"
        :doc
        "String containing code to execute for this chunk"))
        #("purl" 0 1
        (:initials
        "chunk-option: include"
        :doc
        "Include chunk when extracting code with `knitr::purl()`"))
        #("eval" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "Evaluate code cells (if `false` just echos the code into output).

- `true` (default): evaluate code cell
- `false`: don't evaluate code cell
- `[...]`: A list of positive or negative line numbers to selectively include or exclude lines 
  (explicit inclusion/excusion of lines is available only when using the knitr engine)
"))
        #("echo" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "Include cell source code in rendered output.

- `true` (default): include source code in output
- `false`: do not include source code in output
- `fenced`: in addition to echoing, include the cell delimiter as part of the output.
- `[...]`: A list of positive or negative line numbers to selectively include or exclude lines
  (explicit inclusion/excusion of lines is available only when using the knitr engine)
"))
        #("code-fold" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "Collapse code into an HTML `<details>` tag so the user can display it on-demand.

- `true`: collapse code
- `false` (default): do not collapse code
- `show`: use the `<details>` tag, but show the expanded code initially.
"))
        #("code-summary" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "Summary text to use for code blocks collapsed using `code-fold`"))
        #("code-overflow" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "Choose how to handle code overflow, when code lines are too wide for their container. One of:

- `scroll`
- `wrap`
"))
        #("code-line-numbers" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "Include line numbers in code block output (`true` or `false`).

For revealjs output only, you can also specify a string to highlight
specific lines (and/or animate between sets of highlighted lines).

* Sets of lines are denoted with commas:
  * `3,4,5`
  * `1,10,12`
* Ranges can be denoted with dashes and combined with commas:
  * `1-3,5` 
  * `5-10,12,14`
* Finally, animation steps are separated by `|`:
  * `1-3|1-3,5` first shows `1-3`, then `1-3,5`
  * `|5|5-10,12` first shows no numbering, then 5, then lines 5-10
    and 12
"))
        #("lst-label" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "Unique label for code listing (used in cross references)"))
        #("lst-cap" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "Caption for code listing"))
        #("tidy" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "Whether to reformat R code."))
        #("tidy-opts" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "List of options to pass to `tidy` handler"))
        #("collapse" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "Collapse all the source and output blocks from one code chunk into a single block
"))
        #("prompt" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "Whether to add the prompt characters in R
code. See `prompt` and `continue` on the help page `?base::options`. Note
that adding prompts can make it difficult for readers to copy R code from
the output, so `prompt: false` may be a better choice. This option may not
work well when the `engine` is not `R`
([#1274](https://github.com/yihui/knitr/issues/1274)).
"))
        #("highlight" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "Whether to syntax highlight the source code"))
        #("class-source" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "Class name(s) for source code blocks"))
        #("attr-source" 0 1
        (:initials
        "chunk-option: codeoutput"
        :doc
        "Attribute(s) for source code blocks"))
        #("fig-width" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Default width for figures"))
        #("fig-height" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Default height for figures"))
        #("fig-cap" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Figure caption"))
        #("fig-subcap" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Figure subcaptions"))
        #("fig-link" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Hyperlink target for the figure"))
        #("fig-align" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Figure horizontal alignment (`default`, `left`, `right`, or `center`)"))
        #("fig-alt" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Alternative text to be used in the `alt` attribute of HTML images.
"))
        #("fig-env" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "LaTeX environment for figure output"))
        #("fig-pos" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "LaTeX figure position arrangement to be used in `\begin{figure}[]`.

Computational figure output that is accompanied by the code 
that produced it is given a default value of `fig-pos=\"H\"` (so 
that the code and figure are not inordinately separated).
"))
        #("fig-scap" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "A short caption (only used in LaTeX output). A short caption is inserted in `\caption[]`, 
and usually displayed in the “List of Figures” of a PDF document.
"))
        #("fig-format" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Default output format for figures (`retina`, `png`, `jpeg`, `svg`, or `pdf`)"))
        #("fig-dpi" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Default DPI for figures"))
        #("fig-asp" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "The aspect ratio of the plot, i.e., the ratio of height/width. When `fig-asp` is specified, the height of a plot 
(the option `fig-height`) is calculated from `fig-width * fig-asp`.
"))
        #("out-width" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Width of the plot in the output document, which can be different from its physical `fig-width`,
i.e., plots can be scaled in the output document.
Depending on the output format, this option can take special values.
For example, for LaTeX output, it can be `.8\\linewidth`, `3in`, or `8cm`;
for HTML, it can be `300px` or `50%`.
"))
        #("out-height" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Height of the plot in the output document, which can be different from its physical `fig-height`, 
i.e., plots can be scaled in the output document.
Depending on the output format, this option can take special values.
For example, for LaTeX output, it can be `3in`, or `8cm`;
for HTML, it can be `300px`.
"))
        #("fig-keep" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "How plots in chunks should be kept. Possible values are as follows:

-   `high`: Only keep high-level plots (merge low-level changes into
    high-level plots).
-   `none`: Discard all plots.
-   `all`: Keep all plots (low-level plot changes may produce new plots).
-   `first`: Only keep the first plot.
-   `last`: Only keep the last plot.
-   A numeric vector: In this case, the values are indices of (low-level) plots
    to keep.
"))
        #("fig-show" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "How to show/arrange the plots. Possible values are as follows:

-   `asis`: Show plots exactly in places where they were generated (as if
    the code were run in an R terminal).
-   `hold`: Hold all plots and output them at the end of a code chunk.
-   `animate`: Concatenate all plots into an animation if there are multiple
    plots in a chunk.
-   `hide`: Generate plot files but hide them in the output document.
"))
        #("out-extra" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Additional raw LaTeX or HTML options to be applied to figures"))
        #("external" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Externalize tikz graphics (pre-compile to PDF)"))
        #("sanitize" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "sanitize tikz graphics (escape special LaTeX characters)."))
        #("interval" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Time interval (number of seconds) between animation frames."))
        #("aniopts" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Extra options for animations; see the documentation of the LaTeX [**animate**
package.](http://ctan.org/pkg/animate)
"))
        #("animation-hook" 0 1
        (:initials
        "chunk-option: figure"
        :doc
        "Hook function to create animations in HTML output. 

The default hook (`ffmpeg`) uses FFmpeg to convert images to a WebM video.

Another hook function is `gifski` based on the
[**gifski**](https://cran.r-project.org/package=gifski) package to
create GIF animations.
"))
        #("output" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Include the results of executing the code in the output. Possible values:

- `true`: Include results.
- `false`: Do not include results.
- `asis`: Treat output as raw markdown with no enclosing containers.
"))
        #("warning" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Include warnings in rendered output."))
        #("error" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Include errors in the output (note that this implies that errors executing code
will not halt processing of the document).
"))
        #("include" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Catch all for preventing any output (code or results) from being included in output.
"))
        #("panel" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Panel type for cell output (`tabset`, `input`, `sidebar`, `fill`, `center`)"))
        #("output-location" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Location of output relative to the code that generated it. The possible values are as follows:

- `default`: Normal flow of the slide after the code
- `fragment`: In a fragment (not visible until you advance)
- `slide`: On a new slide after the curent one
- 'column': In an adjacent column 
- `column-fragment`:   In an adjacent column (not visible until you advance)

Note that this option is supported only for the `revealjs` format.
"))
        #("message" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Include messages in rendered output."))
        #("results" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "How to display text results. Note that this option only applies to normal text output (not warnings,
messages, or errors). The possible values are as follows:

- `markup`: Mark up text output with the appropriate environments
  depending on the output format. For example, if the text
  output is a character string `\"[1] 1 2 3\"`, the actual output that
  **knitr** produces will be:

  ```` md
  ```
  [1] 1 2 3
  ```
  ````

  In this case, `results: markup` means to put the text output in fenced
  code blocks (```` ``` ````).

- `asis`: Write text output as-is, i.e., write the raw text results
  directly into the output document without any markups.

  ```` md
  ```{r}
  #| results: asis
  cat(\"I'm raw **Markdown** content.\n\")
  ```
  ````

- `hold`: Hold all pieces of text output in a chunk and flush them to the
  end of the chunk.

- `hide` (or `false`): Hide text output.
"))
        #("comment" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Prefix to be added before each line of text output.
By default, the text output is commented out by `##`, so if
readers want to copy and run the source code from the output document, they
can select and copy everything from the chunk, since the text output is
masked in comments (and will be ignored when running the copied text). Set
`comment: ''` to remove the default `##`.
"))
        #("class-output" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Class name(s) for text/console output"))
        #("attr-output" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Attribute(s) for text/console output"))
        #("class-warning" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Class name(s) for warning output"))
        #("attr-warning" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Attribute(s) for warning output"))
        #("class-message" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Class name(s) for message output"))
        #("attr-message" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Attribute(s) for message output"))
        #("class-error" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Class name(s) for error output"))
        #("attr-error" 0 1
        (:initials
        "chunk-option: textoutput"
        :doc
        "Attribute(s) for error output"))
        #("label" 0 1
        (:initials
        "chunk-option: attributes"
        :doc
        "Unique label for code cell. Used when other code needs to refer to the cell 
(e.g. for cross references `fig-samples` or `tbl-summary`)
"))
        #("classes" 0 1
        (:initials
        "chunk-option: attributes"
        :doc
        "Classes to apply to cell container"))
        #("tags" 0 1
        (:initials
        "chunk-option: attributes"
        :doc
        "Array of tags for notebook cell"))
        #("id" 0 1
        (:initials
        "chunk-option: attributes"
        :doc
        "Notebook cell identifier. Note that if there is no cell `id` then `label` 
will be used as the cell `id` if it is present.
See <https://jupyter.org/enhancement-proposals/62-cell-id/cell-id.html>
for additional details on cell ids.
"))
        #("cache" 0 1
        (:initials
        "chunk-option: cache"
        :doc
        "Whether to cache a code chunk. When evaluating
code chunks for the second time, the cached chunks are skipped (unless they
have been modified), but the objects created in these chunks are loaded from
previously saved databases (`.rdb` and `.rdx` files), and these files are
saved when a chunk is evaluated for the first time, or when cached files are
not found (e.g., you may have removed them by hand). Note that the filename
consists of the chunk label with an MD5 digest of the R code and chunk
options of the code chunk, which means any changes in the chunk will produce
a different MD5 digest, and hence invalidate the cache.
"))
        #("cache-path" 0 1
        (:initials
        "chunk-option: cache"
        :doc
        "A prefix to be used to generate the paths of cache files"))
        #("cache-vars" 0 1
        (:initials
        "chunk-option: cache"
        :doc
        "Variable names to be saved in
the cache database. By default, all variables created in the current chunks
are identified and saved, but you may want to manually specify the variables
to be saved, because the automatic detection of variables may not be robust,
or you may want to save only a subset of variables.
"))
        #("cache-globals" 0 1
        (:initials
        "chunk-option: cache"
        :doc
        "Variables names that are not created from the current chunk.

This option is mainly for `autodep: true` to work more precisely---a chunk
`B` depends on chunk `A` when any of `B`'s global variables are `A`'s local 
variables. In case the automatic detection of global variables in a chunk 
fails, you may manually specify the names of global variables via this option.
In addition, `cache-globals: false` means detecting all variables in a code
chunk, no matter if they are global or local variables.
"))
        #("cache-lazy" 0 1
        (:initials
        "chunk-option: cache"
        :doc
        "Whether to `lazyLoad()` or directly `load()` objects. For very large objects, 
lazyloading may not work, so `cache-lazy: false` may be desirable (see
[#572](https://github.com/yihui/knitr/issues/572)).
"))
        #("cache-rebuild" 0 1
        (:initials
        "chunk-option: cache"
        :doc
        "Force rebuild of cache for chunk"))
        #("cache-comments" 0 1
        (:initials
        "chunk-option: cache"
        :doc
        "Prevent comment changes from invalidating the cache for a chunk"))
        #("dependson" 0 1
        (:initials
        "chunk-option: cache"
        :doc
        "Explicitly specify cache dependencies for this chunk (one or more chunk labels)
"))
        #("autodep" 0 1
        (:initials
        "chunk-option: cache"
        :doc
        "Detect cache dependencies automatically via usage of global variables"))
        #("column" 0 1
        (:initials
        "chunk-option: pagelayout"
        :doc
        "[Page column](https://quarto.org/docs/authoring/article-layout.html) for output"))
        #("fig-column" 0 1
        (:initials
        "chunk-option: pagelayout"
        :doc
        "[Page column](https://quarto.org/docs/authoring/article-layout.html) for figure output"))
        #("tbl-column" 0 1
        (:initials
        "chunk-option: pagelayout"
        :doc
        "[Page column](https://quarto.org/docs/authoring/article-layout.html) for table output"))
        #("cap-location" 0 1
        (:initials
        "chunk-option: pagelayout"
        :doc
        "Where to place figure and table captions (`top`, `bottom`, or `margin`)"))
        #("fig-cap-location" 0 1
        (:initials
        "chunk-option: pagelayout"
        :doc
        "Where to place figure captions (`top`, `bottom`, or `margin`)"))
        #("tbl-cap-location" 0 1
        (:initials
        "chunk-option: pagelayout"
        :doc
        "Where to place table captions (`top`, `bottom`, or `margin`)"))
        #("layout" 0 1
        (:initials
        "chunk-option: layout"
        :doc
        "2d-array of widths where the first dimension specifies columns and the second rows.

For example, to layout the first two output blocks side-by-side on the top with the third
block spanning the full width below, use `[[3,3], [1]]`.

Use negative values to create margin. For example, to create space between the 
output blocks in the top row of the previous example, use `[[3,-1, 3], [1]]`.
"))
        #("layout-ncol" 0 1
        (:initials
        "chunk-option: layout"
        :doc
        "Layout output blocks into columns"))
        #("layout-nrow" 0 1
        (:initials
        "chunk-option: layout"
        :doc
        "Layout output blocks into rows"))
        #("layout-align" 0 1
        (:initials
        "chunk-option: layout"
        :doc
        "Horizontal alignment for layout content (`default`, `left`, `right`, or `center`)"))
        #("layout-valign" 0 1
        (:initials
        "chunk-option: layout"
        :doc
        "Vertical alignment for layout content (`default`, `top`, `center`, or `bottom`)"))
        #("tbl-cap" 0 1
        (:initials
        "chunk-option: table"
        :doc
        "Table caption"))
        #("tbl-subcap" 0 1
        (:initials
        "chunk-option: table"
        :doc
        "Table subcaptions"))
        #("tbl-colwidths" 0 1
        (:initials
        "chunk-option: table"
        :doc
        "Apply explicit table column widths for markdown grid tables and pipe
tables that are more than `columns` characters wide (72 by default). 

Some formats (e.g. HTML) do an excellent job automatically sizing
table columns and so don't benefit much from column width specifications.
Other formats (e.g. LaTeX) require table column sizes in order to 
correctly flow longer cell content (this is a major reason why tables 
> 72 columns wide are assigned explicit widths by Pandoc).

This can be specified as:

- `auto`: Apply markdown table column widths except when there is a
  hyperlink in the table (which tends to throw off automatic
  calculation of column widths based on the markdown text width of cells).
  (`auto` is the default for HTML output formats)

- `true`: Always apply markdown table widths (`true` is the default
  for all non-HTML formats)

- `false`: Never apply markdown table widths.

- An array of numbers (e.g. `[40, 30, 30]`): Array of explicit width percentages.
"))
))


(provide 'company-quarto-data)


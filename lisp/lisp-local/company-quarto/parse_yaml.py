# https://stackoverflow.com/questions/35968189/retrieving-data-from-a-yaml-file-based-on-a-python-list

import glob
import os

import numpy as np
import pandas as pd
from yaml import safe_load

filename = "company-quarto-data.el"
path = r"/home/karlo/downloads/quick-clone-repos/quarto-cli/src/resources/schema/"

all_files = glob.glob(os.path.join(path, "*cell*.yml"))

all_files

dfs = list()
for f in all_files:
    context = os.path.basename(f).replace("cell-", "chunk-option: ").replace(".yml", "")
    with open(f, "r") as f:
        df = pd.json_normalize(safe_load(f))
        df["context"] = context
    # .stem is method for pathlib objects to get the filename w/o the extension
    dfs.append(df)

dfs

fds = pd.concat(dfs, ignore_index=True)

fds

fds.to_csv("./parsed_yaml_all.csv")

for index, row in fds.iterrows():
    field_name = row["name"]
    field_context = row["context"]
    field_description = row["description.long"]
    print(
        f"""\
    #(\"{field_name}\" 0 1
    (:initials
    \"{field_context}\"
    :doc
    \"{field_name}\"
    :summary
    \"{field_description}\"))"""
    )

if os.path.exists(filename):
    os.remove(filename)

with open(filename, "a"):
    os.utime(filename, None)

header = f"""\
(defconst company-quarto-completions
  '(
"""

bottom = f"""\
))
\n
(provide 'company-quarto-data)
"""

with open(filename, "a") as el:
    el.write(header)

with open(filename, "a") as el:
    for index, row in fds.iterrows():
        field_name = row["name"]
        field_context = row["context"]
        field_description = row["description.long"]
        if str(field_description) == "nan":
            field_description = row["description.short"]
        if str(field_description) == "nan":
            field_description = row["description"]
        if str(field_description) == "nan":
            field_description = "Not available"
        field_description = field_description.replace('"', '\\"')
        elsexp = f"""\
        #(\"{field_name}\" 0 1
        (:initials
        \"{field_context}\"
        :doc
        \"{field_description}\"))"""
        el.write(elsexp + "\n")

with open(filename, "a") as el:
    el.write(bottom + "\n")

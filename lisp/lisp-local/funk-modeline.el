;; -*- lexical-binding: t; -*-

(require 'cl-lib)
(require 'subr-x)
(require 'dash)
(require 'all-the-icons)
(use-package shrink-path)
(require 'shrink-path)
(require 'projectile)

(defface funk/modeline-project-parent-dir
  '((t (:inherit (font-lock-comment-face bold))))
  "Face used for the project parent directory of the mode-line buffer path.")

(defface funk/modeline-project-dir
  '((t (:inherit (font-lock-string-face bold))))
  "Face used for the project directory of the mode-line buffer path.")

(defface funk/modeline-buffer-path
  '((t (:inherit (funk/modeline-project-dir))))
  "Face used for the dirname part of the buffer path.")

(defface funk/modeline-buffer-file
  '((t (:inherit (mode-line-buffer-id regular))))
  "Face used for the filename part of the mode-line buffer path.")

(defface funk/modeline-buffer-file-modified
  '((t (:foreground "#FF6E67" :inherit funk/modeline-buffer-file)))
  "Face used for the filename part of the mode-line buffer path
when modified.")

(defun funk/modeline--buffer-file-name (file-path
                                        _true-file-path
                                        &optional
                                        truncate-project-root-parent
                                        truncate-project-relative-path
                                        hide-project-root-parent)
  "Propertized variable `buffer-file-name' given by FILE-PATH.
If TRUNCATE-PROJECT-ROOT-PARENT is non-nil will be saved by truncating project
root parent down fish-shell style.
Example:
  ~/Projects/FOSS/emacs/lisp/comint.el => ~/P/F/emacs/lisp/comint.el
If TRUNCATE-PROJECT-RELATIVE-PATH is non-nil will be saved by truncating project
relative path down fish-shell style.
Example:
  ~/Projects/FOSS/emacs/lisp/comint.el => ~/Projects/FOSS/emacs/l/comint.el
If HIDE-PROJECT-ROOT-PARENT is non-nil will hide project root parent.
Example:
  ~/Projects/FOSS/emacs/lisp/comint.el => emacs/lisp/comint.el"
  (let ((project-root (file-local-name (projectile-project-root))))
    (concat
     ;; Project root parent
     (unless hide-project-root-parent
       (when-let (root-path-parent
                  (file-name-directory (directory-file-name project-root)))
         (propertize
          (if (and truncate-project-root-parent
                   (not (string-empty-p root-path-parent))
                   (not (string= root-path-parent "/")))
              (shrink-path--dirs-internal root-path-parent t)
            (abbreviate-file-name root-path-parent))
          'face 'funk/modeline-project-parent-dir)))
     ;; Project directory
     (propertize
      (concat (file-name-nondirectory (directory-file-name project-root)) "/")
      'face 'funk/modeline-project-dir)
     ;; relative path
     (propertize
      (when-let (relative-path (file-relative-name
                                (or (file-name-directory file-path) "./")
                                project-root))
        (if (string= relative-path "./")
            ""
          (if truncate-project-relative-path
              (substring (shrink-path--dirs-internal relative-path t) 1)
            relative-path)))
      'face 'funk/modeline-buffer-path)
     ;; File name
     (if (buffer-modified-p)
         (propertize (file-name-nondirectory file-path)
                     'face 'funk/modeline-buffer-file-modified)
       (propertize (file-name-nondirectory file-path)
                   'face 'funk/modeline-buffer-file)))))

;; testing out
;; (funk/modeline--buffer-file-name buffer-file-name buffer-file-truename 'shrink)
;; (funk/modeline--buffer-file-name buffer-file-name buffer-file-truename 'shrink 'shrink)

(defun funk/set-funk-modeline ()
  (ignore-errors
    (funk/modeline--buffer-file-name buffer-file-name buffer-file-truename 'shrink)))

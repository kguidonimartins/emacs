;;; init-system.el --- Emacs integration with other OS packages -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;;; Ensure system packages
;; Basically, you can install all system packages through the package manager of
;; your OS. For example, install R and dependencies, rga and dependencies, system
;; fonts, docker and dependencies, exa, rip, terminal emulators, texlive...
;; See the list of all my installed packages at:
;; https://gitlab.com/kguidonimartins/dotfiles-public/-/blob/main/.local/share/miscellaneous/arch-pkg-list.csv

;; (use-package use-package-ensure-system-package
;;   :config
;;   (use-package system-requirements
;;     :no-require t
;;     :ensure nil
;;     :if (executable-find "yay")
;;     ;; :ensure-system-package
;;     ((pandoc . "yay -S --noconfirm pandoc biber bibtool readability-cli texlive-core texlive-latexextra zathura zathura-pdf-mupdf")
;;      (make   . "yay -S --noconfirm make ctags tmux grep sed sad entr surfraw wakatime-cli-bin shellcheck")
;;      (rsync  . "yay -S --noconfirm rsync vimiv rm-improved ripgrep ripgrep-all fd unzip gzip unrar exa dragon-drag-and-drop")
;;      (languagetool . "yay -S --noconfirm languagetool proselint hunspell hunspell-en_us hunspell-pt-br aspell-pt aspell-en"))))

;;;; Set PATH
(use-package exec-path-from-shell
  
  :config
  (exec-path-from-shell-initialize))

;;; conf-mode
(use-package emacs
  :config
  (add-to-list 'auto-mode-alist '("/\\.[^/]*rc" . conf-mode) t)
  (add-to-list 'auto-mode-alist '(".info" . makefile-mode) t)
  (setq native-comp-async-report-warnings-errors nil))

;;; turn shell scripts into interactive emacs commands

(use-package dash
  ;; HACK 2022-07-12: https://sachachua.com/dotemacs/index.html#org1100e7a
  :config
  (defmacro funk-convert-shell-scripts-to-interactive-commands (directory)
    "Make the shell scripts in DIRECTORY available as interactive commands."
    (cons 'progn
          (-map
           (lambda (filename)
             (let ((function-name (intern (concat "my-shell/" (file-name-nondirectory filename)))))
               `(defun ,function-name (&rest args)
                  (interactive)
                  (cond
                   ((not (called-interactively-p 'any))
                    (shell-command-to-string (mapconcat 'shell-quote-argument (cons ,filename args) " ")))
                   ((region-active-p)
                    (apply 'call-process-region (point) (mark) ,filename nil (if current-prefix-arg t nil) t args))
                   (t
                    (apply 'call-process ,filename nil (if current-prefix-arg t nil) nil args))))))
           (-filter (-not #'file-directory-p)
                    (-filter #'file-executable-p (directory-files directory t))))))
  (funk-convert-shell-scripts-to-interactive-commands "~/.local/bin/tools/emacs"))

(provide 'init-system)
;;; init-system.el ends here

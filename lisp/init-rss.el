;;; init-rss.el --- RSS config -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; elfeed

(use-package elfeed
  :config
  ;; (setq elfeed-feeds
  ;;       '(
  ;;         ("https://www.reddit.com/r/Rlanguage.rss" rstats)
  ;;         ("https://www.reddit.com/r/dataengineering.rss" dataengineering)
  ;;         ("https://www.reddit.com/r/emacs.rss" emacs)
  ;;         ("https://www.reddit.com/r/evilmode.rss" evil-mode)
  ;;         ("https://www.reddit.com/r/orgmode.rss" org-mode)
  ;;         ("https://www.reddit.com/r/planetemacs.rss" planetemacs)
  ;;         ))

  (defun funk/elfeed-mark-all-as-read ()
    (interactive)
    (mark-whole-buffer)
    (elfeed-search-untag-all-unread))

  (defun funk/elfeed-unwrap-lines ()
    (visual-line-mode -1)
    (toggle-truncate-lines +1))
  (advice-add 'elfeed :after 'funk/elfeed-unwrap-lines)
  (advice-add 'elfeed :after 'elfeed-update)

  (defun elfeed-show-entry (entry)
    "Display ENTRY in the current buffer.
    FROM: https://github.com/skeeto/elfeed/issues/103"
    (let ((title (elfeed-entry-title entry)))
      (split-window-right)
      (other-window 1 nil)
      (switch-to-buffer (get-buffer-create (format "*elfeed %s*" title)))
      (unless (eq major-mode 'elfeed-show-mode)
        (elfeed-show-mode))
      (setq elfeed-show-entry entry)
      (elfeed-show-refresh)))
  (defun elfeed-kill-buffer ()
    "Kill the current buffer."
    (interactive)
    (evil-window-left 1)
    (delete-other-windows))

  (setq elfeed-show-entry-switch #'pop-to-buffer)

  (defun elfeed-show-eww-open (&optional use-generic-p)
    "open with eww"
    (interactive "P")
    (let ((browse-url-browser-function #'eww-browse-url))
      (elfeed-show-visit use-generic-p)))

  (defun elfeed-show-browser-open (&optional use-generic-p)
    "open with eww"
    (interactive "P")
    (let ((browse-url-browser-function #'browse-url))
      (elfeed-show-visit use-generic-p)))

  (defun elfeed-search-eww-open (&optional use-generic-p)
    "open with eww"
    (interactive "P")
    (let ((browse-url-browser-function #'eww-browse-url))
      (elfeed-search-browse-url use-generic-p)))

  (define-key elfeed-show-mode-map (kbd "C-<return>") 'elfeed-show-eww-open)
  (define-key elfeed-show-mode-map (kbd "S-<return>") 'elfeed-show-browser-open)
  (define-key elfeed-search-mode-map (kbd "C-<return>") 'elfeed-search-eww-open)

  )

;; NOTE 2022-07-01: Nice configs here:
;; https://www.reddit.com/r/emacs/comments/vo2szt/for_digital_minimalists_rssatom_emacs_and_elfeed/
;; https://blog.dornea.nu/2022/06/29/rss/atom-emacs-and-elfeed/

;;; elfeed-org
(use-package elfeed-org
  ;; :after elfeed
  
  :config
  (setq rmh-elfeed-org-files (list "~/.emacs.d.vanilla/etc/elfeed/elfeed.org"))
  ;; (elfeed-org)
  )

;;; https://github.com/karthink/elfeed-tube

;;; pocket-reader

(use-package pocket-reader
  
  :config
  (general-define-key
   :keymaps '(pocket-reader-mode-map)
   :states '(normal)
   "RET" 'pocket-reader-open-url
   "TAB" 'pocket-reader-pop-to-url
   "a"   'pocket-reader-toggle-archived
   "C-<return>"   'pocket-reader-open-in-external-browser
   "c"   'pocket-reader-copy-url
   "d"   'pocket-reader ; Return to default view
   "D"   'pocket-reader-delete
   "e"   'pocket-reader-excerpt
   "E"   'pocket-reader-excerpt-all
   "*"   'pocket-reader-toggle-favorite
   "f"   'pocket-reader-toggle-favorite
   "F"   'pocket-reader-show-unread-favorites
   "go"   'pocket-reader-resort
   "gr"   'pocket-reader-refresh
   "s"   'pocket-reader-search
   "m"   'pocket-reader-toggle-mark
   "M"   'pocket-reader-mark-all
   "u"   'pocket-reader-unmark-all
   "o"   'pocket-reader-more
   "l"   'pocket-reader-limit
   "R"   'pocket-reader-random-item
   "ta"  'pocket-reader-add-tags
   "tr"  'pocket-reader-remove-tags
   "tt"  'pocket-reader-set-tags
   "ts"  'pocket-reader-tag-search)
  )

(provide 'init-rss)
;;; init-rss.el ends here

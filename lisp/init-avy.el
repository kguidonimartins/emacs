;;; init-avy.el --- avy config -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; avy

(use-package avy
  :config
  (setq avy-background t)
  (setq avy-all-windows 'all-frames)
  (setq avy-keys '(?a ?s ?d ?f ?j ?k ?l ?:))
  :bind
  (("C-'" . avy-goto-char)
   ("C-:" . avy-goto-char-timer)
   ("C-\"" . avy-goto-char-2)))

(provide 'init-avy)
;;; init-avy.el ends here

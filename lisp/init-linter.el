;;; init-linter.el --- Config for linters and formatters -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:


;;; format-all
;; like vim's ale
(use-package format-all
  :config
  (define-format-all-formatter styler
                               (:executable "Rscript")
                               (:install "Rscript -e \"install.packages('styler')\"")
                               (:languages "R")
                               (:features)
                               (:format
                                (format-all--buffer-easy executable "-e"
                                                         (concat
                                                          "options(styler.addins_style_transformer = \"grkstyle::grk_style_transformer()\");"
                                                          "options(grkstyle.use_tabs = list(indent_by = 2,indent_character = \" \"));"
                                                          " con <- file('stdin');"
                                                          " out <- grkstyle::grk_style_text(readLines(con));"
                                                          " close(con);"
                                                          " out")))))


;;; flymake
(use-package flymake)

(use-package flymake-shellcheck
  
  :commands flymake-shellcheck-load
  :init
  (add-hook 'sh-mode-hook 'flymake-shellcheck-load))

;;; flycheck
(use-package flycheck)

;;; flycheck-color-mode-line
(use-package flycheck-color-mode-line
  :after (flycheck)
  :config
  (eval-after-load "flycheck"
    '(add-hook 'flycheck-mode-hook 'flycheck-color-mode-line-mode)))

;;; flycheck-inline
(use-package flycheck-inline
  :after (flycheck)
  :config
  (with-eval-after-load 'flycheck
    (add-hook 'flycheck-mode-hook #'flycheck-inline-mode)))

;;; flycheck-languagetool
(use-package flycheck-languagetool
  :after (flycheck langtool)
  :hook
  (text-mode . (lambda ()
                 (require 'flycheck-languagetool)))
  (markdown-mode . (lambda ()
                     (require 'flycheck-languagetool)))
  (poly-markdown+r-mode . (lambda ()
                            (require 'flycheck-languagetool)))
  :init
  (setq flycheck-languagetool-commandline-jar "/usr/bin/languagetool"))


(provide 'init-linter)
;;; init-linter.el ends here

;;; init-ai.el --- ChatGPT and friends confifs -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;;; gptel
(use-package gptel
  :straight (gptel :type git
                   :host github
                   :repo "karthink/gptel")
  :config
  (setq gptel-api-key (funk-auth 'chatgpt)))

;;; https://github.com/zerolfx/copilot.el

(provide 'init-ai)
;;; init-ai.el ends here

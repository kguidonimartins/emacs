;;; init-tree-sitter.el --- tree-sitter config -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; FIXME 2022-12-05: Official tree-sitter feature in emacs
;; https://www.reddit.com/r/emacs/comments/zbpa42/how_to_use_emacs_29_treesitter/
;; https://git.savannah.gnu.org/cgit/emacs.git/tree/admin/notes/tree-sitter/starter-guide?h=feature/tree-sitter

(use-package tree-sitter)
(use-package tree-sitter-langs)

(require 'tree-sitter)
(require 'tree-sitter-langs)

(global-tree-sitter-mode +1)

(tree-sitter-require 'r)
(tree-sitter-require 'python)

;; ;; make sure I do not re-add the submodule multiple times
;; (unless (f-exists-p (concat tree-sitter-langs-git-dir "/repos/r"))
;;   (let ((default-directory tree-sitter-langs-git-dir))
;;     (tree-sitter-langs--call
;;      "git" "submodule" "add" "https://github.com/r-lib/tree-sitter-r" "repos/r")))

;; ;; compile only if haven't done so
;; (unless (--filter (string= (f-base it) "r") (f-entries (tree-sitter-langs--bin-dir)))
;;   ;; do not display compile information in a new buffer
;;   (cl-letf (((symbol-function 'tree-sitter-langs--buffer) (lambda (&rest _) nil)))
;;     (tree-sitter-langs-compile 'r)))

;; (unless (assq 'ess-r-mode tree-sitter-major-mode-language-alist)
;;   (add-to-list 'tree-sitter-major-mode-language-alist '(ess-r-mode . r)))

;; (defun funk/tree-sitter-load-r ()
;;   (interactive)
;;   ;; register ess-r-mode with r grammar
;;   (unless (assq 'ess-r-mode tree-sitter-major-mode-language-alist)
;;     (add-to-list 'tree-sitter-major-mode-language-alist '(ess-r-mode . r)))
;;   ;; register tree-sitter
;;   (tree-sitter-require 'r))

(add-hook 'ess-r-mode 'tree-sitter-mode)
(add-hook 'ess-mode 'tree-sitter-mode)

(use-package evil-textobj-tree-sitter
  :config
  ;; bind `function.outer`(entire function block) to `f` for use in things like `vaf`, `yaf`
  (define-key evil-outer-text-objects-map "f" (evil-textobj-tree-sitter-get-textobj "function.outer"))
  ;; bind `function.inner`(function block without name and args) to `f` for use in things like `vif`, `yif`
  (define-key evil-inner-text-objects-map "f" (evil-textobj-tree-sitter-get-textobj "function.inner"))
  (add-to-list 'evil-textobj-tree-sitter-major-mode-language-alist '(ess-r-mode . r)))

;; ;;; tree-sitter for R
;; https://github.com/junyi-hou/tree-sitter-fold
;; https://github.com/junyi-hou/dotfiles/blob/main/main.org#rtree-sitter

;; (use-package tree-sitter-ess-r
;;   :straight (:host github :repo "ShuguangSun/tree-sitter-ess-r"))

(use-package ts-fold
  :straight (ts-fold :type git :host github :repo "emacs-tree-sitter/ts-fold"))

;; (use-package ts-fold-indicators
;;   :straight (ts-fold-indicators :type git :host github :repo "emacs-tree-sitter/ts-fold")
;;   :config
;;   (add-hook 'tree-sitter-after-on-hook #ts-fold-indicators-mode))

;; (add-hook 'tree-sitter-mode-hook 'ts-fold-indicators-mode)

(provide 'init-tree-sitter)
;;; init-tree-sitter.el ends here

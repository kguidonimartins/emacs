;;; init-completion-et-al.el --- vertico, consult, and friends configs -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; REVIEW: https://www.reddit.com/r/emacs/comments/ol2luk/from_ivy_counsel_to_vertico_consult/
;; REVIEW: https://kristofferbalintona.me/posts/202202211546/

;;; vertico

(use-package vertico
  ;; Special recipe to load extensions conveniently
  :straight (:files (:defaults "extensions/*"))
  :bind (:map vertico-map
              ("C-j" . vertico-next)
              ("C-k" . vertico-previous)
              :map minibuffer-local-map
              ("C-w" . backward-kill-word))
  :config
  (setq vertico-cycle t)
  (setq vertico-multiform-categories
        '(
          (consult-grep buffer)
          ;; (consult-ripgrep buffer)
          (consult-line unobtrusive)
          (find-file reverse)
          ))
  ;; Prefix the current candidate with “» ”. From
  ;; https://github.com/minad/vertico/wiki#prefix-current-candidate-with-arrow
  (advice-add #'vertico--format-candidate :around
              (lambda (orig cand prefix suffix index _start)
                (setq cand (funcall orig cand prefix suffix index _start))
                (concat
                 (if (= vertico--index index)
                     (propertize "» " 'face 'vertico-current)
                   "  ")
                 cand)))

  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy) ; Correct file path when changed
  :init
  (vertico-mode))

;;; vertico-prescient

(use-package vertico-prescient
  :after (vertico consult)
  :config
  (setq vertico-prescient-override-sorting nil)
  (setq vertico-prescient-enable-filtering nil)
  (setq vertico-prescient-enable-sorting nil)
  (vertico-prescient-mode +1))

;;; savehist

(use-package savehist
  :init
  (savehist-mode +1))

;;; marginalia

(use-package marginalia

  :config
  (setq marginalia-annotators '(marginalia-annotators-heavy))
  (setq marginalia-max-relative-age 0)
  (setq marginalia-align 'right)
  :init
  (marginalia-mode))

;;; embark

(use-package embark

  ;; REVIEW: https://karthinks.com/software/fifteen-ways-to-use-embark/

  :bind
  (("M-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :init

  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  :config

  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none))))

  ;; (defun embark-which-key-indicator ()
  ;;     "An embark indicator that displays keymaps using which-key.
  ;; The which-key help message will show the type and value of the
  ;; current target followed by an ellipsis if there are further
  ;; targets."
  ;;     (lambda (&optional keymap targets prefix)
  ;;       (if (null keymap)
  ;;           (which-key--hide-popup-ignore-command)
  ;;         (which-key--show-keymap
  ;;          (if (eq (caar targets) 'embark-become)
  ;;              "Become"
  ;;            (format "Act on %s '%s'%s"
  ;;                    (plist-get (car targets) :type)
  ;;                    (embark--truncate-target (plist-get (car targets) :target))
  ;;                    (if (cdr targets) "…" "")))
  ;;          (if prefix
  ;;              (pcase (lookup-key keymap prefix 'accept-default)
  ;;                ((and (pred keymapp) km) km)
  ;;                (_ (key-binding prefix 'accept-default)))
  ;;            keymap)
  ;;          nil nil t))))

  ;; (setq embark-indicators
  ;;       '(embark-which-key-indicator
  ;;         embark-highlight-indicator
  ;;         embark-isearch-highlight-indicator))
  )


;;; consult

(use-package consult
  :config
  (setq consult-line-start-from-top nil)
  (setq consult-narrow-key "/")
  (defun funk/consult-outline ()
    ;; FIXME 2022-12-03: For while because some `consult' functions
    ;; keep suffering from interference of prescient even after
    ;; disabling.
    (interactive)
    (ivy-prescient-mode +1)
    (progn
      (ivy-prescient-mode -1)
      (consult-outline)
      (ivy-prescient-mode +1)))
  (defun funk/consult-line ()
    ;; FIXME 2022-12-03: For while because some `consult' functions
    ;; keep suffering from interference of prescient even after
    ;; disabling.
    (interactive)
    (ivy-prescient-mode +1)
    (progn
      (ivy-prescient-mode -1)
      (consult-line)
      (ivy-prescient-mode +1)))
  (global-set-key (kbd "C-.") 'funk/consult-outline)
  (advice-add 'funk/consult-outline :after  #'(lambda () (call-interactively 'evil-scroll-line-to-center))))

(use-package consult-dir)

(use-package consult-jump-project
  :straight (consult-jump-project :type git :host github :repo "jdtsmith/consult-jump-project")
  :custom (consult-jump-direct-jump-modes '(dired-mode))
  :bind ("C-x p j" . consult-jump-project)
  :config
  (defun consult-jump-project--projects ()
    "Return list of (other) project roots.
The list is sorted by last file mod date among recently saved
files. Save details.
;; NOTE 2022-12-11: Error related in the following closed issue persists: https://github.com/jdtsmith/consult-jump-project/issues/2#issue-1318340792
;; NOTE 2022-12-11: Also, I change the `project-known-project-roots' function by the `projectile-known-projects' variable.
"
    (let* ((projects (seq-filter
                      (lambda (dir)
                        (not (string-prefix-p
                              (expand-file-name dir) default-directory)))
                      projectile-known-projects))
           (details (seq-map #'consult-jump-project--details projects)))
      (setq consult-jump-project--max-age
            (seq-max (or '(0) (seq-map (lambda (x) (nth 3 x)) details))))
      (seq-map #'car
               (setq consult-jump-project--details
                     (sort details (lambda (a b)
                                     (or (not (nth 3 b))
                                         (< (nth 3 a) (nth 3 b)))))))))
  )

;; Consult users will also want the embark-consult package.
(use-package embark-consult

  :after (embark consult)
  ;; if you want to have consult previews as you move around an
  ;; auto-updating embark collect buffer
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package all-the-icons-completion
  :after (all-the-icons marginalia)
  :config
  (add-hook 'marginalia-mode-hook
            #'all-the-icons-completion-marginalia-setup)
  (all-the-icons-completion-mode))

;;;; consult-projectile
(use-package consult-projectile)

(provide 'init-completion-et-al)
;;; init-completion-et-al.el ends here

;;; init-indent.el --- indentation configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; indentation guides

(use-package highlight-indent-guides
  :config

  (setq highlight-indent-guides-method 'character)
  (setq highlight-indent-guides-character ?\xFFE8)
  ;; (setq highlight-indent-guides-method 'bitmap)
  (setq highlight-indent-guides-responsive nil)
  (add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
  (add-hook 'ess-mode-hook 'highlight-indent-guides-mode)
  (setq highlight-indent-guides-auto-odd-face-perc 20)
  (setq highlight-indent-guides-auto-even-face-perc 20)
  (setq highlight-indent-guides-auto-character-face-perc 20))



;; (use-package indent-guide
;;   :config
;;   (indent-guide-global-mode))

;;; aggressive-indent
(use-package aggressive-indent
  :hook
  ((emacs-lisp-mode) . aggressive-indent-mode)
  :config
  ;; tab complete and indent
  (setq tab-always-indent 'complete)
  (setq-default indent-tabs-mode nil))

;;; electric-operator

(use-package electric-operator
  ;; Electric operator will turn ~a=10*5+2~ into ~a = 10 * 5 + 2~, so let's
  ;; enable it for R:
  :after (ess python)
  :hook
  ((ess-r-mode python-mode) . electric-operator-mode)
  ((ess-r-mode python-mode) . electric-pair-mode)
  :config
  (setq electric-pair-preserve-balance nil)
  (setq electric-operator-R-named-argument-style 'spaced)
  (electric-operator-add-rules-for-mode 'ess-r-mode (cons "in" nil) (cons "%" nil)))

;;; electric-pair
(use-package electric
  :init
  (progn
    (electric-pair-mode 1))
  :config
  ;; ;; see: https://emacs.stackexchange.com/questions/13603/auctex-disable-electric-pair-mode-in-minibuffer-during-macro-definition
  ;; (defun pvj/inhibit-electric-pair-mode (char)
  ;;   (minibufferp))
  ;; (setq electric-pair-inhibit-predicate #'pvj/inhibit-electric-pair-mode)

  ;; ;; see: https://www.reddit.com/r/emacs/comments/lgyqxy/turn_off_electricpair_in_search/
  ;; Disable pairs when entering minibuffer
  (add-hook 'minibuffer-setup-hook (lambda () (electric-pair-mode -1)))
  ;; Renable pairs when existing minibuffer
  (add-hook 'minibuffer-exit-hook (lambda () (electric-pair-mode +1)))
  (setq-default electric-pair-inhibit-predicate 'electric-pair-conservative-inhibit)
  ;; (setq-default electric-pair-inhibit-predicate
  ;;               (lambda (c)
  ;;                 (if (looking-at "[ \n\t]")
  ;;                     (electric-pair-default-inhibit c)
  ;;                   t)))
  ;; append new pair when in org-mode
  ;; from: https://www.reddit.com/r/emacs/comments/wrcvap/use_hook_to_alter_global_variables_but_only_for/
  ;; (add-hook 'org-mode-hook (lambda ()
  ;;                            (setq-local electric-pair-pairs (append electric-pair-pairs '((?\* . ?\*))))))
  ;; inhibit some pair when in org-mode
  ;; from: https://stackoverflow.com/questions/69655134/emacs-electric-pair-mode-disable-specific-pairs
  (add-hook 'org-mode-hook (lambda ()
                             (setq-local electric-pair-inhibit-predicate
                                         `(lambda (c)
                                            (if (char-equal c ?<) t (,electric-pair-inhibit-predicate c))))))
  )

(provide 'init-indent)
;;; init-indent.el ends here

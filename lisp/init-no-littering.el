;;; init-no-littering.el --- Keep the `user-emacs-directory' clean -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; no-littering

(use-package no-littering
  ;; NOTE: If you want to move everything out of the ~/.emacs.d folder
  ;; reliably, set `user-emacs-directory` before loading no-littering!
  :config
  ;; no-littering doesn't set this by default so we must place
  ;; auto save files in the same path as it uses for sessions
  (setq auto-save-file-name-transforms `((".*" ,(no-littering-expand-var-file-name "auto-save/") t))))


(provide 'init-no-littering)
;;; init-no-littering.el ends here

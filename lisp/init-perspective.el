;;; init-perspective.el --- Config for perspective -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; perspective
;; ;; nice window management (preserve buffer list by project)
;; ;; https://github.com/nex3/perspective-el#customization
(use-package perspective
  :config
  (setq persp-mode-prefix-key (kbd "M-p"))
  (setq persp-state-default-file (expand-file-name "var/persp-auto-save.el" user-emacs-directory))
  (persp-mode +1))

;; (use-package  persp-projectile
;;   :config
;;   (require 'persp-projectile))

;;; persp-projectile
(use-package persp-projectile
  :after (perspective))

(define-key projectile-mode-map (kbd "C-a s") 'projectile-persp-switch-project)
(add-hook 'kill-emacs-hook #'persp-state-save)

(defvar persp-mode-functions-to-advise
  '(next-buffer
    previous-buffer
    counsel-switch-buffer
    ivy-switch-buffer
    counsel-projectile-switch-to-buffer)
  "List of functions which need additional advising when using `persp-mode'.")

(defun persp-mode-wrapper (wrapped-buffer-command &rest r)
  "Wrapper for commands which need advising for use with `persp-mode'.
Only for use with `advice-add'."
  (with-persp-buffer-list () (apply wrapped-buffer-command r)))

(defun persp-mode-setup-advice ()
  "Adds or removes advice on functions in `persp-mode-functions-to-advise'."
  (cl-loop for func in persp-mode-functions-to-advise
           do (if persp-mode
                  (advice-add func :around #'persp-mode-wrapper)
                (advice-remove func #'persp-mode-wrapper))))

(add-hook 'persp-mode-hook #'persp-mode-setup-advice)

;; ;; keep eyebrowse window's configuration by perspective
;; (require 'eyepersp)


(provide 'init-perspective)
;;; init-perspective.el ends here

;;; init-scratch.el --- Configs for scratch buffers -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; create scratch based on the current major mode
(defcustom funk/simple-scratch-buffer-default-mode 'markdown-mode
  "Default major mode for `funk/simple-scratch-buffer'."
  :type 'symbol
  :group 'funk/simple)

;; The idea is based on the `scratch.el' package by Ian Eure:
;; <https://github.com/ieure/scratch-el>.

;; Adapted from the `scratch.el' package by Ian Eure.
(defun funk/simple--scratch-list-modes ()
  "List known major modes."
  (cl-loop for sym the symbols of obarray
           for name = (symbol-name sym)
           when (and (functionp sym)
                     (not (member sym minor-mode-list))
                     (string-match "-mode$" name)
                     (not (string-match "--" name)))
           collect name))

(defun funk/clean-symbol (s)
  (string-replace "-mode" "" (symbol-name s)))


(defvar scratch-front-matter
  "-*- mode: %s; -*-
scratch-mode: %s
project:      %s
created-at:   %s
\n\n")

(defun funk/simple--scratch-buffer-setup (region &optional mode)
  "Add contents to `scratch' buffer and name it accordingly.

  REGION is added to the contents to the new buffer.

  Use the current buffer's major mode by default.  With optional
  MODE use that major mode instead."
  (let* ((major (or mode major-mode))
         (time-string (shell-command-to-string "echo -n $(date '+%Y%m%d-%H%M%S')"))
         (fmajor (if (not mode)
                     (funk/clean-symbol (symbol-value (intern "major-mode")))
                   (funk/clean-symbol mode)))
         (proj (if (or (equal (projectile-project-name) "karlo")
                       (equal (projectile-project-name) "-"))
                   "unknown"
                 (projectile-project-name)))
         (string (format scratch-front-matter fmajor major proj time-string))
         (text (concat string region))
         (buf (format "*Scratch for %s %s %s*" major proj time-string))
         ;; (dir (concat "~/.local/share/scratch-buffers/" proj))
         (dir (concat (projectile-project-root) "/.git/info/scratch"))
         (fbuf (concat dir "/" time-string "__" fmajor)))
    ;; (message (format "%s %s" major fmajor))
    (with-current-buffer
        (get-buffer-create buf)
      (funcall major)
      (save-excursion
        (insert text)
        (goto-char (point-min))
        (comment-region (point-at-bol) (progn (vertical-motion 3) (point-at-eol))))
      (vertical-motion 4))
    (pop-to-buffer buf)
    (make-directory dir :parents)
    (write-file fbuf)
    (when (equal region "")
      (goto-line 5)
      (evil-insert 0)
      (newline))))

;;;###autoload
(defun funk/simple-scratch-buffer (&optional arg)
  "Produce a bespoke scratch buffer matching current major mode.

With optional ARG as a prefix argument (\\[universal-argument]),
use `funk/simple-scratch-buffer-default-mode'.

With ARG as a double prefix argument, prompt for a major mode
with completion.

If region is active, copy its contents to the new scratch
buffer."
  (interactive "P")
  (let* ((default-mode funk/simple-scratch-buffer-default-mode)
         (modes (funk/simple--scratch-list-modes))
         (region (with-current-buffer (current-buffer)
                   (if (region-active-p)
                       (buffer-substring-no-properties
                        (region-beginning)
                        (region-end))
                     "")))
         )
    (pcase (prefix-numeric-value arg)
      (16 (progn
            (setq choose-mode (intern (completing-read "Select major mode: " modes nil t)))
            (funk/simple--scratch-buffer-setup region choose-mode)))
      (4 (funk/simple--scratch-buffer-setup region default-mode))
      (_ (funk/simple--scratch-buffer-setup region)))))

(defun funk/simple-markdown-mode-scratch-buffer ()
  (interactive)
  (funk/simple-scratch-buffer '(4)))

(defalias 'sm 'funk/simple-markdown-mode-scratch-buffer)

(defun funk/simple-scratch-buffer-select ()
  (interactive)
  (funk/simple-scratch-buffer '(16)))

(defalias 'sc 'funk/simple-scratch-buffer-select)

(defun funk/simple-elisp-mode-scratch-buffer ()
  (interactive)
  (funk/simple--scratch-buffer-setup nil 'emacs-lisp-mode))

(defun funk/simple-org-mode-scratch-buffer ()
  (interactive)
  (funk/simple--scratch-buffer-setup nil 'org-mode))

;;; scratch-palette

;; (use-package scratch-palette
;;   :config
;;   (setq scratch-palette-directory "~/.local/share/scratch-buffers/"))

(provide 'init-scratch)
;;; init-scratch.el ends here

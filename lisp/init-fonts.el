;;; init-fonts.el --- font configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:


;;; default-text-scale
(use-package default-text-scale

  )

;;; general fonts
(use-package emacs
  :straight (:type built-in)
  :config
  (defvar funk/font-default-font-size 100)
  (defvar funk/font-default-font-type "JetBrains Mono Nerd Font")

  (defun funk/general-font-setup()
    (interactive)
    (set-face-attribute 'default nil :height funk/font-default-font-size :weight 'regular)
    (set-face-attribute 'fixed-pitch nil :height funk/font-default-font-size)
    (set-face-attribute 'variable-pitch nil :height funk/font-default-font-size :weight 'regular)
    (message "Set font size to %s" funk/font-default-font-size))

  (funk/general-font-setup)

  (defun funk/set-font-faces ()
    (set-face-attribute 'default nil :font funk/font-default-font-type :height funk/font-default-font-size)
    (set-face-attribute 'fixed-pitch nil :font funk/font-default-font-type :height funk/font-default-font-size)
    (set-face-attribute 'variable-pitch nil :font funk/font-default-font-type :height funk/font-default-font-size :weight 'regular))

  (if (daemonp)
      (add-hook 'after-make-frame-functions
                (lambda (frame)
                  (setq doom-modeline-icon t)
                  (with-selected-frame frame
                    (funk/set-font-faces))))
    (funk/set-font-faces))
  (require 'server)
  (unless (server-running-p)
    (server-start))
  (require 'org-protocol)

  ;; (custom-theme-set-faces
  ;;  'user
  ;;  ;; install `ETBembo' font with: `yay -S fonts-et-book'
  ;;  '(variable-pitch ((t (:family "ETBembo" :height 120))))
  ;;  '(fixed-pitch ((t ( :family "JetBrains Mono Nerd Font" :height 100)))))

  )

;;; ligatures
;; SOURCE: https://alpha2phi.medium.com/ligature-fonts-for-terminal-vs-code-neovim-and-emacs-1187c6987491
(use-package ligature
  :straight (ligature
             :type git
             :host github
             :branch "master"
             :repo "mickeynp/ligature.el")
  :config
  ;; Enable the "www" ligature in every possible major mode
  (ligature-set-ligatures 't '("www"))
  ;; Enable traditional ligature support in eww-mode, if the
  ;; `variable-pitch' face supports it
  (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
  ;; Enable all Cascadia Code ligatures in programming modes
  (ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "~~>" "***" "||=" "||>"
                                       ":::" "::=" "=:=" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                       "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "-<<"
                                       "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                       "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "#_(" "..<"
                                       "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~@" "~="
                                       "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                       "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" ">:"
                                       ">=" ">>" ">-" "-~" "-|" "->" "-<" "<~" "<*" "<|" "<:"
                                       "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                       "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                       "?=" "?." "??" "/*" "/=" "/>" "//" "(*" "*)"
                                       ))
  (ligature-set-ligatures 'markdown-mode '("|||>" "<|||" "<==>" "~~>" "***" "||=" "||>"
                                           ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                           "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-<<"
                                           "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                           "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "#_(" "..<"
                                           "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~@" "~="
                                           "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                           "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" ">:"
                                           ">=" ">>" ">-" "-~" "-|" "-<" "<~" "<*" "<|" "<:"
                                           "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                           "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                           "?=" "?." "??" "/*" "/=" "/>" "//" "(*" "*)"
                                           ))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  (global-ligature-mode t))

;;; pretty symbols
;; (defun funk/pretty-symbols ()
;;   (prettify-symbols-mode +1)
;;   (setq prettify-symbols-alist
;;         '(("lambda" . 955)
;;           ("map" . 8614)
;;           ("%>%" . ?▷)
;;           ("function" . ?ƒ)
;;           ("defun" . ?ƒ))))
;; (add-hook 'emacs-lisp-mode-hook 'funk/pretty-symbols)
;; (add-hook 'clojure-mode-hook 'funk/pretty-symbols)
;; (add-hook 'ess-mode-hook 'funk/pretty-symbols)


(provide 'init-fonts)
;;; init-fonts.el ends here

;;; init-git.el --- Magit configurations -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; custom functions

(use-package emacs
  :config
  (defun funk/git-clone-clipboard-url ()
    "Clone git URL in clipboard asynchronously and open in dired when finished."
    (interactive)
    (cl-assert (string-match-p (regexp-opt-group '("git@" "https")) (current-kill 0)) nil "No URL in clipboard")
    (let* ((url (current-kill 0))
           (download-dir (expand-file-name "~/downloads/quick-clone-repos"))
           (project-dir (concat (file-name-as-directory download-dir)
                                (file-name-base url)))
           (default-directory download-dir)
           (command (format "git clone %s" url))
           (buffer (generate-new-buffer (format "*%s*" command)))
           (proc))
      (make-directory download-dir :parents)
      (when (file-exists-p project-dir)
        (if (y-or-n-p (format "%s exists. delete?" (file-name-base url)))
            (delete-directory project-dir t)
          (user-error "Bailed")))
      (switch-to-buffer buffer)
      (setq proc (start-process-shell-command (nth 0 (split-string command)) buffer command))
      (with-current-buffer buffer
        (setq default-directory download-dir)
        (shell-command-save-pos-or-erase)
        (require 'shell)
        (shell-mode)
        (view-mode +1))
      (set-process-sentinel proc (lambda (process state)
                                   (let ((output (with-current-buffer (process-buffer process)
                                                   (buffer-string))))
                                     (kill-buffer (process-buffer process))
                                     (if (= (process-exit-status process) 0)
                                         (progn
                                           (message "finished: %s" command)
                                           (dired project-dir))
                                       (user-error (format "%s\n%s" command output))))))
      (set-process-filter proc #'comint-output-filter))))

;;; magit

(use-package magit
  ;; CHECK 2022-07-06: working with pull requests
  ;; https://scripter.co/view-github-pull-requests-in-magit/
  :after projectile
  :init
  (setq magit-define-global-key-bindings nil)
  (setq magit-repolist-columns
        '(("Name"        30 magit-repolist-column-ident ())
          ("Status"      10 magit-repolist-column-flag)
          ("⇣"            3 magit-repolist-column-unpulled-from-upstream
           ((:right-align t)
            (:help-echo "Upstream changes not in branch")))
          ("⇡"            3 magit-repolist-column-unpushed-to-upstream
           ((:right-align t)
            (:help-echo "Local changes not in upstream")))
          ("Version"     25 magit-repolist-column-version ())
          ("Path"         99 magit-repolist-column-path ())))

  (setq magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)

  :config

  ;; magit will hide recent commit section if there are commits not
  ;; pushed onto the remote branch. I want recent commit section to be
  ;; visible at all time.

  (magit-add-section-hook 'magit-status-sections-hook
                          'magit-insert-recent-commits
                          'magit-insert-unpushed-to-upstream-or-recent
                          'replace)

  (magit-add-section-hook 'magit-status-sections-hook
                          'magit-insert-unpushed-to-upstream
                          'magit-insert-unpushed-to-pushremote
                          'append)


  (progn
    (let ((project-dirs (bound-and-true-p projectile-known-projects)))
      (setq magit-repository-directories
            (mapcar (lambda (path) `(,path . 1)) project-dirs))))

  (defun funk/magit-repolist-sort-columns ()
    "Sort 'Status' column in `magit'-tabulated-list."
    (setq-local tabulated-list-sort-key (cons "Status" t)))

  (general-define-key
   :keymaps 'transient-base-map
   "<escape>" 'transient-quit-one)

  (general-define-key
   :keymaps 'magit-mode-map
   "SPC" nil)

  (defun funk/magit-mode-set-fringe ()
    "Custom `magit-mode' behaviours."
    (setq-local left-fringe-width 15
                right-fringe-width 15))

  (add-hook 'magit-mode-hook 'funk/magit-mode-set-fringe)

  (setq magit-ediff-dwim-show-on-hunks t)

  (setq ediff-split-window-function 'split-window-horizontally)

  (setq magit-after-save-refresh-buffers t)


  (add-hook 'projectile-after-switch-project-hook #'(lambda () (progn
                                                                 (let ((project-dirs (bound-and-true-p projectile-known-projects)))
                                                                   (setq magit-repository-directories
                                                                         (mapcar (lambda (path) `(,path . 1)) project-dirs))))))

  (add-hook 'magit-repolist-mode-hook 'funk/magit-repolist-sort-columns))

;;; forge

(use-package forge
  :after magit
  
  ;; NOTE: Make sure to configure a GitHub token before using this package!
  ;; - https://magit.vc/manual/forge/Token-Creation.html#Token-Creation
  ;; - https://magit.vc/manual/ghub/Getting-Started.html#Getting-Started
  :after magit)

;;; git-gutter+

(use-package git-gutter
  
  :config
  (global-git-gutter-mode)
  (setq git-gutter:update-interval 0.02))

(use-package git-gutter-fringe
  
  ;; based on:
  ;; https://www.reddit.com/r/emacs/comments/suxc9b/modern_gitgutter_in_emacs/
  :config
  ;; (setq-default left-fringe-width  20)
  ;; (setq-default right-fringe-width 20)
  (define-fringe-bitmap 'git-gutter-fr:added
    [#b11000000] nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:modified
    [#b11000000] nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:deleted
    [
     #b00000000
     #b00000000
     #b00000000
     #b00000000
     #b00000000
     #b00000000
     #b11000000
     #b11000000
     #b00000000
     #b00000000
     #b00000000
     #b00000000
     #b00000000
     #b00000000
     ]))

;;; magit-todos

(use-package magit-todos
  :after magit
  :config
  (setq magit-todos-keyword-suffix "\\(?:([^)]+)\\)?:?") ; make colon optional
  (setq magit-todos-exclude-globs '("*.map" "*.html" "var/*" "elpa/*" "elpy" "dump.Rmd" "manuscript/" "*TinyTeX*" "*.csv"))
  ;; (setq magit-todos-group-by '(magit-todos-item-filename magit-todos-item-keyword magit-todos-item-first-path-component))
  (setq magit-todos-group-by '(magit-todos-item-keyword))
  (magit-todos-mode +1))

;;; git-commit

(use-package git-commit
  :after magit
  
  :config
  (defun funk/check-git-spelling (force)
    "Check spelling of commit message.
When FORCE is truthy, continue commit unconditionally."
    (let ((tick (buffer-chars-modified-tick))
          (result
           (let ((ispell-skip-region-alist ; Dynamic variable
                  (cons (list (rx line-start "#") #'forward-line) ; Comment
                        ispell-skip-region-alist)))
             (ispell-buffer))))
      (cond
       (force
        t)
       ;; When spell check was completed, result is truthy
       (result
        ;; When nothing was corrected, character tick counter is
        ;; unchanged
        (or (= (buffer-chars-modified-tick) tick)
            (y-or-n-p "Spelling checked.  Commit? "))))))
  (global-git-commit-mode)
  (add-hook 'git-commit-finish-query-functions #'funk/check-git-spelling))

;;; browse-at-remote

(use-package browse-at-remote
  :config
  (defun funk/git-goto-remote ()
    "Go to url of the current repo."
    (interactive)
    (browse-url
     (string-replace ".git" ""
                     (string-replace "git@" "http://"
                                     (string-replace ":" "/"
                                                     (shell-command-to-string "git remote -v | grep fetch | head -1 | cut -f2 | cut -d' ' -f1")))))))

;;; git-modes

(use-package git-modes
  :after magit
  )

;;; blamer

(use-package blamer
  
  :config
  (setq blamer-idle-time 0.7)
  (setq blamer-min-offset 80)
  (setq blamer-author-formatter " ✎ %s ")
  (setq blamer-datetime-formatter "[%s]")
  (setq blamer-commit-formatter " ● %s")
  (setq blamer-prettify-time-p t)
  (setq blamer-uncommitted-changes-message "NO COMMITTED"))

;;; git-timemachine

;; NOTE 2022-11-09: updated by hand; manually `git pull'ing
(use-package git-timemachine)

;;; diff-hl

(use-package diff-hl
  :after dired
  
  :init
  :defines diff-hl-margin-symbols-alist
  :config
  (custom-set-faces
   '(diff-hl-change          ((t (:background unspecified))))
   '(diff-hl-delete          ((t (:background unspecified))))
   '(diff-hl-margin-change   ((t (:background unspecified))))
   '(diff-hl-margin-delete   ((t (:background unspecified))))
   '(diff-hl-margin-ignored  ((t (:background unspecified))))
   '(diff-hl-margin-insert   ((t (:background unspecified))))
   '(diff-hl-margin-unknown  ((t (:background unspecified))))
   '(diff-hl-margin-change   ((t (:background unspecified))))
   '(diff-hl-margin-delete   ((t (:background unspecified))))
   '(diff-hl-margin-ignored  ((t (:background unspecified))))
   '(diff-hl-margin-insert   ((t (:background unspecified))))
   '(diff-hl-margin-unknown  ((t (:background unspecified)))))
  (setq diff-hl-margin-symbols-alist
        '((insert . "+")
          (delete . "-")
          (change . "~")
          (unknown . "*")
          (ignored . "i")))
  (setq diff-hl-dired-extra-indicators nil)
  (diff-hl-margin-mode)
  (add-hook 'dired-mode-hook 'diff-hl-dired-mode)
  (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh))

;;; code-review

(use-package code-review :disabled)

;;; diffview

(use-package diffview
  
  :config
  (defun funk/diffview-next-file-and-evil-zt ()
    "Go to the next file in a diffview window and scroll line to the top."
    (interactive)
    (diffview--next-file)
    (call-interactively 'evil-scroll-line-to-top)
    (diffview--align-windows))

  (defun funk/diffview-prev-file-and-evil-zt ()
    "Go to the previous file in a diffview window and scroll line to the top."
    (interactive)
    (diffview--prev-file)
    (call-interactively 'evil-scroll-line-to-top)
    (diffview--align-windows))

  (defun funk/diffview-quit ()
    "Quit both the diffview and vc-diff windows."
    (interactive)
    (progn
      (diffview--quit)
      (quit-window)
      (kill-buffer "*vc-diff*")))

  (defun funk/diffview-vc-diff ()
    "Show the diffview of the current file."
    (interactive)
    (vc-diff)
    (diffview-current))

  (defun funk/diffview-vc-root-diff ()
    "Show the diffview of the current git working tree."
    (interactive)
    (vc-root-diff nil)
    (diffview-current))

  (general-define-key
   :keymaps '(diffview--mode-map)
   :states '(normal)
   "=" #'diffview--align-windows
   "C-j" #'funk/diffview-next-file-and-evil-zt
   "C-k" #'funk/diffview-prev-file-and-evil-zt
   "q"   #'funk/diffview-quit))

;;; vc

(use-package vc
  :straight (:type built-in)
  :custom
  (vc-follow-symlinks t "Don't ask when following symlinks"))

(provide 'init-git)
;;; init-git.el ends here

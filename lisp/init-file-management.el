;;; -*- lexical-binding: t; -*-

;;; dired

;; Thumbnails images
;; https://www.youtube.com/watch?v=NrY3t3W0_cM
;; Mark files with =m= and open thumbnails with =C-t d=

(use-package dired
  :straight (:type built-in)
  :commands (dired dired-jump)
  :custom ((dired-listing-switches "-AhFlv --group-directories-first"))
  :config

  (defun funk/dired-replace-space-to-hyphen ()
    "In dired, rename current or marked files by replacing space to hyphen -.
If not in `dired', do nothing.
URL `http://xahlee.info/emacs/emacs/elisp_dired_rename_space_to_underscore.html'
Version 2016-10-04 2019-11-24"
    (interactive)
    (require 'dired-aux)
    (if (eq major-mode 'dired-mode)
        (progn
          (mapc (lambda (x)
                  (when (string-match " " x )
                    (dired-rename-file x (replace-regexp-in-string " " "-" x) nil)))
                (dired-get-marked-files ))
          (revert-buffer))
      (user-error "Not in dired")))

  (defun funk/dired-replace-space-to-underscore ()
    "In dired, rename current or marked files by replacing space to underscore _.
If not in `dired', do nothing.
URL `http://xahlee.info/emacs/emacs/elisp_dired_rename_space_to_underscore.html'
Version 2016-10-04 2019-11-24"
    (interactive)
    (require 'dired-aux)
    (if (eq major-mode 'dired-mode)
        (progn
          (mapc (lambda (x)
                  (when (string-match " " x )
                    (dired-rename-file x (replace-regexp-in-string " " "_" x) nil)))
                (dired-get-marked-files ))
          (revert-buffer))
      (user-error "Not in dired")))

  (defun funk/dired-copy-filename-at-point ()
    "When in a dired buffer, copy only the filename to the clipboard.
For example: a path like `/home/user/.config/emacs/lisp/functions/funk.el'
will be copied as `funk.el' only."
    (interactive)
    (dired-copy-filename-as-kill))

  (defun funk/dired-copy-full-path-at-point ()
    "When in a dired buffer, copy only the full path to the clipboard.
For example: a path like `/home/user/.config/emacs/lisp/functions/funk.el'
will be copied exactly as it is."
    (interactive)
    (dired-copy-filename-as-kill 0))

  (defun funk/dired-copy-tilda-path-at-point ()
    "When in a dired buffer, copy only the path with ~/ to the clipboard.
For example: a path like `/home/user/.emacs.d/lisp/functions/funk.el'
will be copied as `~/.emacs.d/lisp/functions/funk.el'."
    (interactive)
    (dired-copy-filename-as-kill 0)
    (setq filename-from-kill-ring (current-kill 0))
    (when (string-match (getenv "HOME") filename-from-kill-ring)
      (setq filename-home-tilda (replace-match "~" t t filename-from-kill-ring))
      (let ((x-select-enable-clipboard t)) (kill-new filename-home-tilda))
      (message filename-home-tilda)))

  (defun funk/dired-copy-from-project-path-at-point ()
    "When in a dired buffer, copy only the path from the project root to the clipboard.
For example: a path like `/home/user/.config/emacs/lisp/functions/funk.el'
will be copied as `lisp/functions/funk.el'."
    (interactive)
    (dired-copy-filename-as-kill 0)
    (setq filename-from-kill-ring (current-kill 0))
    (when (string-match (projectile-project-root) filename-from-kill-ring)
      (setq filename-without-project-root (replace-match "" t t filename-from-kill-ring))
      (let ((x-select-enable-clipboard t)) (kill-new filename-without-project-root))
      (message filename-without-project-root)))

  (defun funk/open-with (arg)
    "Open visited file in default external program.
When in dired mode, open file under the cursor.
With a prefix ARG always prompt for command to use.
FROM: https://github.com/bbatsov/crux/blob/20c07848049716a0e1aa2560e23b5f4149f2a74f/crux.el#L161"
    (interactive "P")
    (let* ((current-file-name
            (if (or (eq major-mode 'dired-mode) (eq major-mode 'dirvish-mode))
                (dired-get-file-for-visit)
              buffer-file-name))
           (open (pcase system-type
                   (`darwin "open")
                   ((or `gnu `gnu/linux `gnu/kfreebsd) "xdg-open")))
           (program (if (or arg (not open))
                        (read-shell-command "Open current file with: ")
                      open)))
      (call-process program nil 0 nil current-file-name)))

  (defun funk/git-add-files(files)
    "Run git add with the input file.
@see: https://gist.github.com/justinhj/5945047"
    (interactive)
    (shell-command (format "git add -f %s" files)))

  (defun funk/dired-git-add-marked-files()
    "For each marked file in a dired buffer add it to the index.
@see: https://gist.github.com/justinhj/5945047"
    (interactive)
    (if (eq major-mode 'dired-mode)
        (let ((filenames (dired-get-marked-files))
              (files ""))
          (dolist (fn filenames)
            (setq fn (shell-quote-argument fn))
            (setq files (concat files " " fn)))
          (funk/git-add-files files))
      (error (format "Not a Dired buffer \(%s\)" major-mode))))

  (defun funk/dired-drag-and-drop ()
    "Open dragon with the marked files on dired.
Depends on: https://github.com/mwh/dragon"
    (interactive)
    (if (eq major-mode 'dired-mode)
        ;; xdragon rename is a nix thing, pretty sure.
        (make-process
         :name "dragon"
         :command (append '("dragon") (dired-get-marked-files))
         :noquery t)
      (user-error "Not in dired")))

  ;; Delete my files by moving them to the trash
  ;; https://github.com/nivekuil/rip#emacs
  (setq delete-by-moving-to-trash t)
  (defun system-move-file-to-trash (filename)
    (shell-command (concat (executable-find "rip") " " filename)))
  (evil-collection-define-key 'normal 'dired-mode-map
    "h"  'dired-single-up-directory
    "l"  'dired-single-buffer
    "yf" 'funk/dired-copy-filename-at-point
    "yh" 'funk/dired-copy-full-path-at-point
    "yy" 'funk/dired-copy-tilda-path-at-point
    "yp" 'funk/dired-copy-from-project-path-at-point
    "Y"  'funk/dired-copy-tilda-path-at-point
    (kbd "C-<return>") 'funk/open-with
    (kbd "C-SPC") 'funk/hydra-top-menu/body
    ")" 'dired-git-info-mode
    "e" 'dired-find-file
    )
  (which-key-add-key-based-replacements
    "yf" "copy basename path"
    "yh" "copy hard full path"
    "yy" "copy tilda full path"
    "yp" "copy project path"
    "Y" "copy basename"
    )
  (general-define-key
   :keymaps '(dired-mode-map)
   :states '(normal)
   ;; With big powers come big responsibilities.
   ;; These delete commands sometimes drive me crazy.
   ;; Thanks to `rip' (github.com/nivekuil/rip) I haven't lost entire projects.
   ;; So, just open a terminal and run the command `rip' (aliased to `rm').
   ;; In terminal, if you prefer, you can use the select and delete commands
   ;; inside `lf', which are also aliased to `rip'.
   "x" nil
   "d" nil
   "D" nil
   "SPC" nil
   )
  ;; (setq dired-dwim-target t). Then, go to dired, split your window,
  ;; split-window-vertically & go to another dired directory. When you will
  ;; press C to copy, the other dir in the split pane will be default
  ;; destination.
  (setq dired-dwim-target t)
  ;; stop opening so many dired buffers
  (setq dired-kill-when-opening-new-dired-buffer t)
  :init
  (add-hook 'dired-mode-hook 'dired-hide-details-mode)
  (add-hook 'dired-mode-hook 'auto-revert-mode)
  (add-hook 'dired-mode-hook 'hl-line-mode)
  (add-hook 'dired-load-hook
            (function (lambda () (load "dired-x")))))

;;; all-the-icons-dired
(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

;;; dired-single
(use-package dired-single

  :after dired)

;;; dired-git-info
(use-package dired-git-info
  :disabled

  :after dired
  :config
  (setq dgi-auto-hide-details-p nil)
  (add-hook 'dired-after-readin-hook 'dired-git-info-auto-enable))

;;; diredfl
(use-package diredfl
  :hook (dired-mode . diredfl-mode))

;;; dired-subtree
(use-package dired-subtree
  :after dired

  :config
  (custom-set-faces
   '(dired-subtree-depth-1-face ((t (:background unspecified))))
   '(dired-subtree-depth-2-face ((t (:background unspecified))))
   '(dired-subtree-depth-3-face ((t (:background unspecified))))
   '(dired-subtree-depth-4-face ((t (:background unspecified))))
   '(dired-subtree-depth-5-face ((t (:background unspecified))))
   '(dired-subtree-depth-6-face ((t (:background unspecified))))
   )
  (advice-add 'dired-subtree-toggle :after (lambda ()
                                             (interactive)
                                             (when all-the-icons-dired-mode
                                               (revert-buffer))))
  (advice-add 'dired-subtree-toggle :before (lambda () (dired-collapse-mode -1))))

;;; mimetypes
(use-package mimetypes)

;;; dired-filter
(use-package dired-filter

  )


;;; dired-hide-dotfiles
(use-package dired-hide-dotfiles
  :disabled

  ;; :hook (dired-mode . dired-hide-dotfiles-mode)
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "H" 'dired-hide-dotfiles-mode))

;;; dired-narrow
(use-package dired-narrow

  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "?" 'dired-narrow))

;;; dired-open
;; (use-package dired-open
;;   :commands (dired dired-jump)
;;   :config
;;   (setq dired-open-extensions '(("png" . "feh")
;;                                 ("mkv" . "mpv"))))

;;; dired-collapse
(use-package dired-collapse
  :bind
  (:map dired-mode-map
        ("M-c" . dired-collapse-mode))
  :config
  :hook
  (dired-collapse-mode . all-the-icons-dired-mode)
  (dired-mode . dired-collapse-mode))

;;; dired-sidebar

(use-package dired-sidebar
  :commands (dired-sidebar-toggle-sidebar)
  :config
  (add-hook 'dired-sidebar-mode-hook
            (lambda ()
              (unless (file-remote-p default-directory)
                (auto-revert-mode))))
  (add-hook 'dired-sidebar-mode-hook
            (lambda ()
              (funk/unwrap-lines)
              (dired-collapse-mode)))
  (setq dired-sidebar-width 70)
  (setq dired-sidebar-display-alist '((side . right) (slot . -1)))
  :bind (("C-s" . dired-sidebar-toggle-sidebar))
  )

;;; dired-auto-readme
(use-package dired-auto-readme
  :disabled
  :straight (:host github :repo "amno1/dired-auto-readme")
  ;; :config
  ;; (add-hook 'dired-mode-hook (lambda () (dired-auto-readme-mode +1)))
  )

;;; recentf
(use-package recentf
  :config
  (recentf-mode)
  (setq  recentf-max-saved-items 1000
         recentf-exclude '("^/var/"
                           "COMMIT_EDITMSG\\'"
                           ".*-autoloads\\.el\\'"
                           "^/elpa/")))

;;; vlf: open very large files
(use-package vlf

  :config
  (require 'vlf-setup))

;;; openwith
(use-package openwith
  :init
  (openwith-mode t)
  :config
  (setq openwith-associations '(
                                ("\\.pdf\\'" "zathura" (file))
                                ("\\.docx\\'" "libreoffice" (file))
                                ("\\.xls\\'" "vd" (file))
                                ("\\.xlsx\\'" "vd" (file))
                                ("\\.html\\'" "$BROWSER" (file))
                                ;; ("\\.csv\\'" "libreoffice" (file))
                                ("\\.gdsheet\\'" "$BROWSER" (file))
                                ("\\.mp4\\'" "mpv" (file))
                                ))
  (add-hook 'dired-mode-hook #'(lambda () (openwith-mode +1))))

;;; rg
(use-package rg

  ;; :if (executable-find "rga")
  ;; :ensure-system-package
  ;; (rga . "yay -S --noconfirm ripgrep ripgrep-all")
  ;; Cheat the package using another executable. In this way, we can
  ;; search through non-text file with the `ripgrep-all'.
  ;; See: https://github.com/phiresky/ripgrep-all
  ;; Just do: `M-x rg'
  :config
  (defun calibre-search (query)
    (interactive)
    (rg query "*" "~/google-drive/kguidonimartins/calibre-library"))
  (setq rg-executable (executable-find "rga"))
  ;; ;; FROM: https://www.reddit.com/r/emacs/comments/uxiufd/comment/i9yh71x/?utm_source=share&utm_medium=web2x&context=3
  ;; ;; With rg.el you can do something like this:
  ;; (rg-define-search my-grep-project :query ask :format regexp :files "everything" :case-fold-search smart :dir
  ;;   (if (project-current) (project-root (project-current))
  ;;     default-directory)
  ;;   :confirm prefix :flags
  ;;   ("--hidden -g !.git"))
  ;; ;; You can use it like M-x my-grep-project <RET> or bind it to some keys.
  )

;;; fd-dired
(use-package fd-dired)

;;; find-dupes-dired
(use-package find-dupes-dired)

;;; dirvish

;; (use-package dirvish
;;   :config
;;   (setq dirvish-reuse-session nil)
;;   (general-define-key
;;    :keymaps '(dirvish-mode-map)
;;    :states '(normal)
;;    "SPC SPC"  #'dirvish-dispatch
;;    "q" #'dirvish-quit
;;    )
;;   (defun funk/dirvish-disable-some-modes ()
;;     (all-the-icons-dired-mode -1)
;;     (visual-line-mode -1)
;;     (openwith-mode -1)
;;     (toggle-truncate-lines +1)
;;     (setq-local tab-width 2))
;;   (add-hook 'dirvish-mode-hook #'funk/dirvish-disable-some-modes)
;;   (setq dirvish-attributes '(file-size)))

;;; saveplace
;; open files in the last edited position
(use-package saveplace
  :straight (:type built-in)
  :config
  (setq save-place-ignore-files-regexp "\\(?:COMMIT_EDITMSG\\|hg-editor-[[:alnum:]]+\\.txt\\|svn-commit\\.tmp\\|bzr_log\\.[[:alnum:]]+\\|[[:alnum:]]\\.csv\\)$")
  (save-place-mode)
  (add-hook 'save-place-find-file-hook 'recenter)
  (advice-add 'save-place-find-file-hook :after #'(lambda() (when (not (eq (buffer-name) "COMMIT_EDITMSG")) (outline-cycle)))))

;;; dwim-shell-commands

(use-package dwim-shell-command
  :disabled
  ;; https://github.com/xenodium/dwim-shell-command
  :straight (
             :host github
             :repo "xenodium/dwim-shell-command"
             )
  )

;; CHECK: https://github.com/hidaqa/dired-copy-paste
;; CHECK: https://github.com/amno1/.emacs.d/blob/main/lisp/dired-extras.el

;;; open file with sudo
(use-package emacs
  :straight (:type built-in)
  :config
  (defun funk/sudo-find-file (file-name)
    "Like find file, but opens the file as root."
    (interactive "FSudo Find File: ")
    (let ((root-file-name (concat "/sudo::" (expand-file-name file-name))))
      (counsel-find-file root-file-name))))

;;; custom functions

(use-package emacs

  :config

  (defun funk/dos2unix ()
    "Convert the current buffer to UNIX file format.
FROM: centaur-emacs."
    (interactive)
    (set-buffer-file-coding-system 'undecided-unix nil))

  (defun funk/unix2dos ()
    "Convert the current buffer to DOS file format.
FROM: centaur-emacs."
    (interactive)
    (set-buffer-file-coding-system 'undecided-dos nil))

  (defun funk/delete-carriage-returns ()
    "Delete `^M' characters in the buffer.
Same as `replace-string C-q C-m RET RET'.
FROM: centaur-emacs."
    (interactive)
    (save-excursion
      (goto-char 0)
      (while (search-forward "\r" nil :noerror)
        (replace-match ""))))

  (defun funk/rename-file-and-buffer (name)
    "Apply NAME to current file and rename its buffer.
Do not try to make a new directory or anything fancy."
    (interactive
     (list (read-file-name "Rename current file: " (buffer-file-name))))
    (let ((file (buffer-file-name)))
      (if (vc-registered file)
          (vc-rename-file file name)
        (rename-file file name))
      (set-visited-file-name name t t)))

  (defun funk/create-empty-file-if-no-exists (file-path &optional arg)
    "Create a file with FILEPATH parameter."
    (interactive)
    (if (file-exists-p file-path)
        (message (concat  "File " (concat file-path " already exists")))
      (with-temp-buffer (write-file file-path))))

  (defvar readme-front-matter
    "---
Título:             %s
Responsável:        Karlo Guidoni <kguidonimartins@gmail.com>
Data:               %s
Última modificação: %s
---\n\n")

  (defun funk/create-readme-here (&optional arg)
    (interactive)
    (setq readmefile "./README.txt")
    (if (file-exists-p readmefile)
        (progn
          (message (concat "File " readmefile " already exists in this path! Opening..."))
          (find-file readmefile))
      (progn
        (let* ((readmefile-rel-path (file-name-directory (file-truename readmefile)))
               (cur-date (format-time-string "%FT%H%M%S"))
               (rel-path (when (string-match (projectile-project-root) readmefile-rel-path)
                           (replace-match "" t t readmefile-rel-path))))
          (with-current-buffer
              (get-buffer-create readmefile)
            (funcall 'markdown-mode)
            (save-excursion
              (insert (format readme-front-matter rel-path cur-date cur-date))))
          (pop-to-buffer readmefile)
          (write-file readmefile)
          (end-of-buffer)
          (evil-insert 0)
          (newline)))))

  (defun funk/insert-filename-here ()
    (interactive)
    (insert (buffer-name))))

(provide 'init-file-management)
;;; init-file-management.el ends here

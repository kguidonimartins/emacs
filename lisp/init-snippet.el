;;; init-snippet.el --- yasnippet configuration -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; yasnippet

;; (add-to-list 'warning-suppress-types '(yasnippet backquote-change))
(use-package yasnippet
  
  :config
  (yas-global-mode 1)
  ;; setup yasnippet for git-commit-mode and magit
  ;; https://emacs.stackexchange.com/questions/32983/insert-and-expand-snippet-when-committing-with-magit
  ;; https://emacs.stackexchange.com/questions/27946/yasnippets-wont-expand-in-git-commit-mode
  (add-hook 'git-commit-setup-hook
            (lambda ()
              (when (derived-mode-p 'text-mode)
                (yas-activate-extra-mode 'text-mode+git-commit-mode))))
  )

;;; yasnippet-snippets
(use-package yasnippet-snippets
  :after yasnippet
  
  :config
  (defun funk/yasnippets-snippets-initialize ()
    (let ((snip-dir  (expand-file-name "etc/yasnippet/snippets" user-emacs-directory)))
      (when (boundp 'yas-snippet-dirs)
        (add-to-list 'yas-snippet-dirs snip-dir t))
      (yas-load-directory snip-dir)))
  (funk/yasnippets-snippets-initialize))

(use-package warnings
  
  :after (yasnippet yasnippet-snippets)
  :straight (:type built-in)
  :config
  (add-to-list 'warning-suppress-types '(yasnippet backquote-change))
  (require 'warnings)
  (add-to-list 'warning-suppress-types '(yasnippet backquote-change)))


(provide 'init-snippet)
;;; init-snippet.el ends here

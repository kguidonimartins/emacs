;;; -*- lexical-binding: t; -*-
(custom-set-variables
 '(safe-local-variable-values
   '((eval conda-env-activate "py39")
     (magit-todos-exclude-globs ".android/*" ".authinfo/*" ".cache/*" ".conda/*" ".config/*" ".dir-locals.el/*" ".doom.d/*" ".eclipse/*" ".emacs.d/*" ".emacs.d.centaur/*" ".emacs.d.doom/*" ".emacs.d.vanilla/*" ".emacs.dx/*" ".enrich-classpath-cache/*" ".gitmodules/*" ".gnupg/*" ".java/*" ".local/*" ".m2/*" ".mbsyncrc/*" ".mozilla/*" ".pki/*" ".profile/*" ".R/*" ".screenlayout/*" ".slime/*" ".ssh/*" ".ssr/*" ".stremio-server/*" ".swt/*" ".TinyTeX/*" ".tor-browser/*" ".visidata/*" ".visidatarc/*" ".Xauthority/*" ".xinitrc/*" ".xprofile/*" ".zotero/*" ".zprofile/*" "bin/*" "dotfiles-private/*" "dotfiles-public/*" "dropbox/*" "google-drive/*" "LICENSE/*" "onedrive/*" "README/*" "spark/*" "tmp/*" "todo.txt/*" "Zotero/*"
                                )
     (magit-todos-exclude-globs ".config/*" ".local/*" ".TinyTeX/*" ".cache/*" "Zotero/*")
     (magit-todos-exclude-globs ".config/*" ".local/*" ".TinyTeX/*" ".cache/*")
     (magit-todos-exclude-globs "elpa/*" "lisp/github/*" "var/*" "elpy/*" "quelpa/*" "straight/*")
     (magit-todos-exclude-globs "elpa/*" "lisp/github/*" "quelpa/*")
     (magit-todos-exclude-globs "*.csv")
     (magit-todos-exclude-globs "venv/*" "*.js")
     (magit-todos-exclude-globs "*")
     (magit-todos-exclude-globs "elpa/*")
     (magit-todos-exclude-globs "/data/raw/*" "/data/temp/*")
     (magit-todos-exclude-globs "./elpa/*")
     (magit-todos-exclude-globs "./.config/*" "./.local/*" "./.TinyTeX/*" "./.cache/*")
     (magit-todos-exclude-globs "./.config/*" "./.local/*" "./.TinyTeX/*")
     (projectile-project-compilation-cmd . "make all")
     (dired-omit-mode . 1)
     (dired-omit-files . "^\\~$*")
     (projectile-project-compilation-cmd . "make all")
     (projectile-project-compilation-cmd . "make")
     (funk/ess-pipe-character . "%>%")))
 '(warning-suppress-log-types
   '(((yasnippet backquote-change))
     ((yasnippet backquote-change))))
 '(warning-suppress-types '(((yasnippet backquote-change))))
 '(warning-suppress-log-types
   '((use-package)
     (use-package)
     ((yasnippet backquote-change))))
 '(warning-suppress-types '(((yasnippet backquote-change))))
 )

(provide 'init-custom-variables)

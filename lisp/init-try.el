;;; init-try.el --- trying packages -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; subed
;; just open a subtitle .srt and see the magic happen
(use-package subed)

(use-package ytdl
  :config
  (setq ytdl-download-folder "~/")
  (setq ytdl-always-query-default-filename 'yes-confirm)
  )



(provide 'init-try)
;;; init-try.el ends here

;;; init-corfu.el --- corfu config -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; corfu

;; REVIEW: https://kristofferbalintona.me/posts/202202270056/

(use-package corfu
  :straight (:files (:defaults "extensions/*"))
  :custom
  ;; Optional customizations
  ;; :custom
  ;; (corfu-separator ?\s)          ;; Orderless field separator
  ;; (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
  ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
  ;; (corfu-preview-current nil)    ;; Disable current candidate preview
  ;; (corfu-on-exact-match nil)     ;; Configure handling of exact matches
  (corfu-echo-documentation t) ;; Disable documentation in the echo area
  ;; (corfu-scroll-margin 5)        ;; Use scroll margin

  ;; Enable Corfu only for certain modes.
  ;; :hook ((prog-mode . corfu-mode)
  ;;        (shell-mode . corfu-mode)
  ;;        (eshell-mode . corfu-mode))

  ;; Recommended: Enable Corfu globally.
  ;; This is recommended since Dabbrev can be used globally (M-/).
  ;; See also `corfu-excluded-modes'.

  ;; TAB-and-Go customizations
  (corfu-cycle t)             ;; Enable cycling for `corfu-next/previous'
  (corfu-preselect-first t) ;; Disable candidate preselection

  ;; Use TAB for cycling, default is `corfu-complete'.
  :bind
  (:map corfu-map
        ("TAB" . corfu-next)
        ([tab] . corfu-next)
        ("S-TAB" . corfu-previous)
        ([backtab] . corfu-previous))

  :init
  (global-corfu-mode +1)
  (setq evil-collection-corfu-key-themes '(default magic-return))
  :config
  (corfu-history-mode +1)
  (savehist-mode +1)
  (add-to-list 'savehist-additional-variables 'corfu-history)
  (define-key corfu-map (kbd "<f1>") 'corfu-info-documentation)
  (define-key corfu-map "\M-l" 'corfu-info-location)
  (setq corfu-auto t
        corfu-auto-delay 0
        corfu-auto-prefix 2
        completion-styles '(basic))
  (evil-collection-corfu-setup)
  (general-add-advice '(corfu--setup corfu--teardown) :after 'evil-normalize-keymaps)
  (evil-make-overriding-map corfu-map)
  (defun corfu-enable-always-in-minibuffer ()
    "Enable Corfu in the minibuffer if Vertico/Mct are not active."
    (unless (or (bound-and-true-p mct--active) ; Useful if I ever use MCT
                (bound-and-true-p vertico--input))
      (setq-local corfu-auto nil)       ; Ensure auto completion is disabled
      (corfu-mode 1)))
  (add-hook 'minibuffer-setup-hook #'corfu-enable-always-in-minibuffer 1)
  (corfu-popupinfo-mode +1)
  (setq corfu-doc-delay 0.5)
  (setq corfu-doc-max-width 70)
  (setq corfu-doc-max-height 20)

  ;; NOTE 2022-02-05: I've also set this in the `corfu' use-package to be
  ;; extra-safe that this is set when corfu-doc is loaded. I do not want
  ;; documentation shown in both the echo area and in the `corfu-doc' popup.
  (setq corfu-echo-documentation nil)
  )


;; (use-package corfu-history
;;   :straight (corfu-history :local-repo "../straight/repos/corfu/extensions/" :type built-in)
;;   :after corfu
;;   :config
;;   (corfu-history-mode +1)
;;   (savehist-mode +1)
;;   (add-to-list 'savehist-additional-variables 'corfu-history))

;; (use-package corfu-info
;;   :straight (corfu-info :local-repo "../straight/repos/corfu/extensions/" :type built-in)
;;   :after corfu
;;   :commands(corfu-info-documentation corfu-info-location)
;;   :init
;;   (define-key corfu-map (kbd "<f1>") 'corfu-info-documentation)
;;   (define-key corfu-map "\M-l" 'corfu-info-location))

;; A few more useful configurations...
(use-package emacs
  :init
  ;; TAB cycle if there are only few candidates
  (setq completion-cycle-threshold 3)

  ;; Emacs 28: Hide commands in M-x which do not apply to the current mode.
  ;; Corfu commands are hidden, since they are not supposed to be used via M-x.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable indentation+completion using the TAB key.
  ;; `completion-at-point' is often bound to M-TAB.
  (setq tab-always-indent 'complete))

;; Use Dabbrev with Corfu!
(use-package dabbrev
  ;; Swap M-/ and C-M-/
  :bind (("M-/" . dabbrev-completion)
         ("C-M-/" . dabbrev-expand))
  ;; Other useful Dabbrev configurations.
  :custom
  (dabbrev-ignored-buffer-regexps '("\\.\\(?:pdf\\|jpe?g\\|png\\)\\'")))

;;; Complementary packages

;; Corfu works well together with all packages providing code
;; completion via the completion-at-point-functions. Many modes and
;; packages already provide a Capf out of the box. Nevertheless you
;; may want to look into complementary packages to enhance your setup.

;;;; corfu-terminal: The corfu-terminal package provides an
;; overlay-based display for Corfu, such that you can use Corfu in
;; terminal Emacs.

;;;; corfu-doc: The corfu-doc package displays the candidate
;; documentation in a popup next to the Corfu popup, similar to
;; company-quickhelp.

;; (use-package corfu-doc
;;   :config
;;   (add-hook 'corfu-mode-hook #'corfu-doc-mode)
;;   (define-key corfu-map (kbd "M-p") #'corfu-doc-scroll-down) ;; corfu-next
;;   (define-key corfu-map (kbd "M-n") #'corfu-doc-scroll-up)  ;; corfu-previous
;;   )

;;;; Orderless: Corfu supports completion styles, including the advanced
;; Orderless completion style, where the filtering expressions are
;; separated by spaces or another character (see corfu-separator).
;; Optionally use the `orderless' completion style.
(use-package orderless
  :init
  ;; Configure a custom style dispatcher (see the Consult wiki)
  ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
  ;;       orderless-component-separator #'orderless-escapable-split-on-space)
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles . (partial-completion))))))

(use-package corfu-prescient
  :after corfu)

;;;; TODO Cape: Additional Capf backends and completion-in-region commands
;; are provided by the Cape package. Among others, the package
;; supplies a file path and a Dabbrev completion backend. Cape
;; provides the cape-company-to-capf adapter to reuse Company backends
;; in Corfu. Furthermore the function cape-super-capf can merge
;; multiple Capfs, such that the candidates of multiple Capfs are
;; displayed together at the same time.

;; REVIEW: https://kristofferbalintona.me/posts/202203130102/

(use-package cape
  :config
  (use-package company)
  ;; Bind dedicated completion commands
  ;; Alternative prefix keys: C-c p, M-p, M-+, ...
  :bind (("C-c p p" . completion-at-point) ;; capf
         ("C-c p t" . complete-tag)        ;; etags
         ("C-c p d" . cape-dabbrev)        ;; or dabbrev-completion
         ("C-c p h" . cape-history)
         ("C-c p f" . cape-file)
         ("C-c p k" . cape-keyword)
         ("C-c p s" . cape-symbol)
         ("C-c p a" . cape-abbrev)
         ("C-c p i" . cape-ispell)
         ("C-c p l" . cape-line)
         ("C-c p w" . cape-dict)
         ("C-c p \\" . cape-tex)
         ("C-c p _" . cape-tex)
         ("C-c p ^" . cape-tex)
         ("C-c p &" . cape-sgml)
         ("C-c p r" . cape-rfc1345))
  :init
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-history)
  (add-to-list 'completion-at-point-functions #'cape-keyword)
  ;; (add-to-list 'completion-at-point-functions #'cape-tex)
  ;;(add-to-list 'completion-at-point-functions #'cape-sgml)
  ;;(add-to-list 'completion-at-point-functions #'cape-rfc1345)
  ;; (add-to-list 'completion-at-point-functions #'cape-abbrev)
  ;;(add-to-list 'completion-at-point-functions #'cape-ispell)
  ;;(add-to-list 'completion-at-point-functions #'cape-dict)
  ;; (add-to-list 'completion-at-point-functions #'cape-symbol)
  ;;(add-to-list 'completion-at-point-functions #'cape-line)
  (add-to-list 'completion-at-point-functions (cape-company-to-capf #'company-yasnippet))
  )

;;;; kind-icon: Icons are supported by Corfu via an external
;; package. For example the kind-icon package provides beautifully
;; styled SVG icons based on monochromatic icon sets like material
;; design.

;; (use-package kind-icon
;;   :after corfu
;;   :custom
;;   (kind-icon-default-face 'corfu-default) ; to compute blended backgrounds correctly
;;   :config
;;   (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

(use-package kind-icon
  :after corfu
  :custom
  (kind-icon-use-icons t)
  (kind-icon-default-face 'corfu-default) ; Have background color be the same as `corfu' face background
  (kind-icon-blend-background nil)  ; Use midpoint color between foreground and background colors ("blended")?
  (kind-icon-blend-frac 0.08)

  ;; NOTE 2022-02-05: `kind-icon' depends `svg-lib' which creates a cache
  ;; directory that defaults to the `user-emacs-directory'. Here, I change that
  ;; directory to a location appropriate to `no-littering' conventions, a
  ;; package which moves directories of other packages to sane locations.
  (svg-lib-icons-dir (no-littering-expand-var-file-name "svg-lib/cache/")) ; Change cache dir
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter) ; Enable `kind-icon'

  ;; Add hook to reset cache so the icon colors match my theme
  ;; NOTE 2022-02-05: This is a hook which resets the cache whenever I switch
  ;; the theme using my custom defined command for switching themes. If I don't
  ;; do this, then the backgound color will remain the same, meaning it will not
  ;; match the background color corresponding to the current theme. Important
  ;; since I have a light theme and dark theme I switch between. This has no
  ;; function unless you use something similar
  ;; (add-hook 'kb/themes-hooks #'(lambda () (interactive) (kind-icon-reset-cache)))
  )

;;;; pcmpl-args: Extend the Eshell/Shell Pcomplete mechanism with
;; support for many more commands. Similar to the Fish shell,
;; Pcomplete uses man page parsing to dynamically retrieve the
;; completions and helpful annotations. This package brings Eshell
;; completions to another level!

;;;; Tempel: Tiny template/snippet package with templates in Lisp
;; syntax, which can be used in conjunction with Corfu.

;;;; Vertico: You may also want to look into my Vertico package. Vertico
;; is the minibuffer completion counterpart of Corfu.

(provide 'init-corfu)
;;; init-corfu.el ends here

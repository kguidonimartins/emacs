;;; init-language-server.el --- Config for language servers -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;;; lsp-mode

;; (use-package lsp-mode
;;   :commands (lsp lsp-deferred)
;;   :config
;;   (setq lsp-keymap-prefix "C-c C-l")  ;; Or 'C-l', 's-l'
;;   (setq lsp-diagnostic-package :none)
;;   (setq lsp-enable-symbol-highlighting nil)
;;   (setq lsp-ui-doc-show-with-cursor nil)
;;   (setq lsp-headerline-breadcrumb-enable nil)
;;   (lsp-enable-which-key-integration t))

;; ;; lsp-ui
;; (use-package lsp-ui

;;   :hook (lsp-mode . lsp-ui-mode)
;;   :custom
;;   (lsp-ui-doc-position 'top))

;; (require 'keytar)
;; (use-package lsp-grammarly
;;   :hook ((text-mode markdown-mode) . (lambda ()
;;                                        (require 'lsp-grammarly)
;;                                        (lsp))))  ; or lsp-deferred

;; ;; lsp-ivy
;; (use-package lsp-ivy)

;;; eglot
(use-package eglot
  :straight (:type built-in)
  :config
  (setq eglot-ignored-server-capabilites '(:documentHighlightProvider
                                           :hoverProvider))
  (define-key evil-normal-state-map (kbd "K") 'eldoc-doc-buffer)
  (define-key eglot-mode-map (kbd "C-c o") 'eglot-code-action-organize-imports)
  (define-key eglot-mode-map (kbd "C-c h") 'eldoc)
  ;; FIXME 2022-12-05: Add `gd' as this `xref-find-definitions' in `evil-map'.
  (define-key eglot-mode-map (kbd "<f6>") 'xref-find-definitions)
  ;; (add-to-list 'eglot-server-programs
  ;;              `(python-mode . ("pyls" "-v" "--tcp" "--host"
  ;;                               "localhost" "--port" :autoport)))
  )

(provide 'init-language-server)
;;; init-language-server.el ends here
